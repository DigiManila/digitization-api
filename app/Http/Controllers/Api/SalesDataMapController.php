<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\AssessmentRoll;
use App\MapPolygon;
use App\BgyPolygon;
use App\DistrictPolygon;
use App\PoliticalBoundary;
use App\MappingBgyPolygon;
use App\MappingPoliticalBoundary;
use App\MappingActiveMap;
use App\MappingNotActivePinMap;
use App\Road;

use App\SalesData;
use App\SalesDataLatest;

use Illuminate\Support\Facades\Redis;

use File;
use Storage;

class SalesDataMapController extends Controller
{

    public function getBarangay(){
        
        $data = SalesDataLatest::select('brgy')
                        ->distinct()
                        ->orderBy('brgy')
                        ->pluck('brgy')
                        ->toArray();

        return $data;

    }

    public function getPin(Request $request){

        $output = [];

        // ------------------------------------ //

        $data = SalesDataLatest::select('pin')
                        ->whereRaw('SUBSTRING(pin,8,3) = ?',  $request['bgy'] )
                        ->distinct()
                        ->orderBy('pin')
                        ->pluck('pin')
                        ->toArray();

        $output['pins'] = $data;

        // ------------------------------------ //

        $parcels = MapPolygon::select('polygon', 'barangay')
                        ->whereRaw('SUBSTRING(pin,8,3) = ?',  $request['bgy'] )
                        ->get();

        
        $output['all_barangay'] = self::parseBarangay($parcels);

        // ------------------------------------ //
        
        $district_no = BgyPolygon::select('district')
                        ->where('bgy', (int)$request['bgy'])
                        ->pluck('district')
                        ->first();

        $unused_district = DistrictPolygon::select('district', 'polygon')
                        ->where('district', '!=', $district_no)
                        ->get();

        $output['dist_polygon'] = self::parseUnusedDistrict($unused_district);

        // ------------------------------------ //

        $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                        ->where('bgy', '!=', (int)$request['bgy'])
                        ->where('district', $district_no)
                        ->get();

        $output['bgy_polygon'] = self::parseUnusedBarangay($unused_bgy);

        // ------------------------------------ //

        $idx = floor(count($output['all_barangay']) / 2);
        $output['center_coords'] = self::computeCenter($output['all_barangay'][$idx]['path']);

        // ------------------------------------ //

        $output['district'] = $district_no;

        // ------------------------------------ //

        return  $output;

    }

    public function getSalesDataPolygon(Request $request){

        $pin = $request['pin'];

        $data = SalesDataLatest::with('subclass')->with('polygon')->where('pin', $pin)->first();

        if($data){

            if(isset($data['polygon'][0])){
                $data['lat_lng'] = str_replace("|", ",", $data['polygon'][0]['polygon']);
                $data['center_coords'] = $data['polygon'][0]['center_coords'];
                // $data['lat_lng'] = str_replace('\\', "", $data['lat_lng']);
                // $data['lat_lng'] = str_replace("[", "", $data['lat_lng']);
                // $data['lat_lng'] = str_replace("]", "", $data['lat_lng']);
                // $data['lat_lng'] = "";
            } else{
                $data['lat_lng'] = "";
                $data['center_coords'] = "";
            }

        }

        $data['year_record'] = date('Y',strtotime($data['created_at']));

        $pictures = self::getPicturesUrl(substr($pin, 0, 18));

        $data['pictures'] = $pictures;
        $data['date_inst'] = date("F j, Y",strtotime($data['date_inst']));
        $data['selling_price'] = number_format($data['selling_price'], 2, '.', ',');

        // Get District
        return $data;
    }

    public function selectBgyBoundaries(Request $request){

        $unused_bgy = null;

        if($request->sel == "1"){

            $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                                    ->where('bgy', '!=', (int)$request->bgy)
                                    ->where('district', $request->district)
                                    ->get();

        } elseif($request->sel == "2"){

            $unused_bgy = PoliticalBoundary::select('bgy', 'polygon','district')
                                    ->where('bgy', '!=', (int)$request->bgy)
                                    ->where('district', $request->district)
                                    ->get();
            

        }

        return self::parseUnusedBarangay($unused_bgy);
       
    }

    public function getDistrictClick(Request $request){

        $district       = $request['district'];
        $sd_brgy_list   = self::getBarangay();

        $bgy = BgyPolygon::select('bgy')
                ->where('district', $district)
                ->orderBy('id', 'asc')
                ->get();

        foreach ($bgy as $key => $value) {

            $brgyNo = str_pad($value['bgy'],3,'0',STR_PAD_LEFT);

            if(in_array($brgyNo, $sd_brgy_list)){
                break;
            }
        }

        $pin = SalesDataLatest::select('pin')
                ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo)
                ->first();

        $data = array(
            'bgy' => $brgyNo,
            'pin' => $pin['pin'],
        );

        return $data;
        
    }

    private function getPicturesUrl($pin){

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data["BLDG IMAGE"];

        $list = [];

        foreach($path as $paths){
            if(gettype($paths) == 'string'){
                if(strpos($paths, $pin) !== false){
                    $list[] = $paths;
                }
            }
        }

        if(count($list)){

            sort($list, 4);
            
            return array(
                "listCount" => count($list),
                "list"      => $list,
            );
        }

        $list[] = "imagenf.jpg";

        return array(
            "listCount" => count($list),
            "list"      => $list,
        );

        // return $path;

        // $matchingFiles = preg_grep("/" . $pin . "*.png/", $path);

        // rsort($matchingFiles);

        // return $matchingFiles;
        

        // if (!File::isDirectory("Pictures/$pin")) {
        //     return [];
        // }

        // // $pin = "117-01-001-005-015";

        // $url_pic = [];

        // $files = File::allFiles(public_path("Pictures/$pin"));

        // foreach ($files as $filename) {
        //     $url_pic[]['src'] = url("Pictures/$pin/" . basename($filename->getPathname()));
        // }
        // return $url_pic;


    }

    // POLYGONS
    public function getPolygon(Request $request){
        $data = AssessmentRoll::with('polygon')->where('id', $request['id'])->first();

        if($data['polygon']){

            $data['polygon']['gmap_polygon'] = str_replace("|", ",", $data['polygon']['gmap_polygon']);

            if($data['polygon']['center_coords']){
                $data['polygon']['center_coords'] = str_replace("|", ",\n", $data['polygon']['center_coords']);
            } else{
                $data['polygon']['center_coords'] = "";
            }
            
            $all_barangay = MapPolygon::select('gmap_polygon')
                            ->where('barangay', $data['polygon']['barangay'])
                            ->where('pin', '!=', $data['pin'])
                            ->orderBy('pin', 'ASC')
                            ->get();

            $data['all_barangay'] = self::parseBarangay($all_barangay);

            return $data;
        } else{
            return 0;
        }
    }

    private function parseBarangay($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                // 'pin'   => $value['pin'],
                'path'  => str_replace("|", ",", $value['polygon'])
            );

            // if($key == 0){
            //     $str = str_replace("|", ",", $value['gmap_polygon']);
            // } else{
            //     $str = $str . "," . str_replace("|", ",", $value['gmap_polygon']);
            // }
        }
        return $data_arr;
        return $str;

    }

    private function parseUnusedBarangay($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'bgy'       => $value['bgy'],
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function parseUnusedDistrict($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    public function getRoads(){
        
        $data = Road::select('road', 'polygon')->get();
        return self::parseRoad($data);

    }

    private function parseRoad($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'name'   => $value['road'],
                'path'   => json_decode(str_replace("|", ",", $value['polygon']))
            );

        }

        return $data_arr;
    }

    private function computeCenter($data){

        $data = json_decode($data, 1);

        $arrX = [];
        $arrY = [];

        foreach ($data as $key => $value) {

            // $latLng = explode(",", trim($value));

            // if($latLng[0]){
                $arrX[] = $value['lat'] / 0.99999796;
                $arrY[] = $value['lng'] / 1.000000466230960;
            // }
        }

        sort($arrX);
        sort($arrY);

        $minX = reset($arrX);
        $maxX = end($arrX);

        $minY = reset($arrY);
        $maxY = end($arrY);

        $getCenterX = ($maxX - $minX) / 2;
        $getCenterY = ($maxY - $minY) / 2;

        return "{ \"lat\": " . (string)($minX + $getCenterX) . ", \"lng\": " . (string)($minY + $getCenterY) . " }";

    }

    public function createKml(Request $request){

        $sales = SalesDataLatest::with('subclass')
                        ->with('polygon')
                        ->orderBy('pin')
                        ->get();

        $sales_arr = self::formatOutput($sales);

        $xmlstr = <<<XML
            <?xml version="1.0" encoding="UTF-8"?>
            <kml xmlns="http://www.opengis.net/kml/2.2">
            <Document>
                <name>Manila Sales Data KML</name>
                <description/>
                <Style id="poly-000000-1200-77-normal">
                <LineStyle>
                    <color>ff0051e6</color>
                    <width>2.301</width>
                </LineStyle>
                <PolyStyle>
                    <color>4d0051e6</color>
                    <fill>1</fill>
                    <outline>1</outline>
                </PolyStyle>
                </Style>
                <Style id="poly-000000-1200-77-highlight">
                <LineStyle>
                    <color>ff0051e6</color>
                    <width>3.4515</width>
                </LineStyle>
                <PolyStyle>
                    <color>4d0051e6</color>
                    <fill>1</fill>
                    <outline>1</outline>
                </PolyStyle>
                </Style>
                <StyleMap id="poly-000000-1200-77">
                <Pair>
                    <key>normal</key>
                    <styleUrl>#poly-000000-1200-77-normal</styleUrl>
                </Pair>
                <Pair>
                    <key>highlight</key>
                    <styleUrl>#poly-000000-1200-77-highlight</styleUrl>
                </Pair>
                </StyleMap>
            XML;

            foreach ($sales_arr as $key => $value) {

                $xmlstr = $xmlstr . <<<XML
                    <Folder>
                        <name>ZONE $key</name>
                XML;

                    foreach ($value as $k => $v) {

                        if($v['coords']){

                            $xmlstr = $xmlstr . "<Placemark>";

                            $xmlstr = $xmlstr . "<name>" . str_replace('&', '&amp;', $v['pin']) . "</name>";
                            $xmlstr = $xmlstr . "<styleUrl>#poly-000000-1200-77</styleUrl>";
                            $xmlstr = $xmlstr . "<ExtendedData>";
                            $xmlstr = $xmlstr . '<Data name="PIN"><value>' . str_replace('&', '&amp;', $v['pin']) . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Base PIN"><value>' . $v['pin_display'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Brgy"><value>' . $v['brgy'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Zone"><value>' . $v['zone'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Date Inst"><value>' . $v['date_inst'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Seller"><value>' . str_replace('&', '&amp;', $v['seller']) . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Buyer"><value>' . str_replace('&', '&amp;', $v['buyer']) . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Location"><value>' . str_replace('&', '&amp;', $v['location']) . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Lot"><value>' . str_replace('&', '&amp;', $v['lot']) . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Block"><value>' . str_replace('&', '&amp;', $v['block']) . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Area"><value>' . $v['area'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Selling Price"><value>' . $v['selling_price'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Additional Improvements"><value>' . $v['addt_improvements'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Unit Value"><value>' . $v['unit_value'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Sale sq/m"><value>' . $v['sale_sq_m'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="MV Improvement"><value>' . $v['mv_imp'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Class"><value>' . $v['class'] . "</value></Data>";
                            $xmlstr = $xmlstr . '<Data name="Status"><value>' . $v['status'] . "</value></Data>";                               
                            $xmlstr = $xmlstr . "</ExtendedData>"; 

                            $xmlstr = $xmlstr . "<Polygon><outerBoundaryIs><LinearRing><tessellate>1</tessellate><coordinates>";
                            $xmlstr = $xmlstr . $v['coords'];
                            $xmlstr = $xmlstr . "</coordinates></LinearRing></outerBoundaryIs></Polygon>";

                            $xmlstr = $xmlstr . "</Placemark>";

                        }
                        
                    }

                $xmlstr = $xmlstr . <<<XML
                    </Folder>
                XML;

            }

        $xmlstr = $xmlstr . <<<XML
                </Document>
            </kml>
        XML;

        
        return $xmlstr;
    }

    private function formatOutput($data){

        $output = [];

        foreach ($data as $key => $value) {

            $output[$value['zone']][$value['pin']] = $value;

            if(count($value['subclass'])){
                $output[$value['zone']][$value['pin']]['class'] = $value['subclass'][0]['sublass'];
            } else{
                $output[$value['zone']][$value['pin']]['class'] = "";
            }

            if(count($value['polygon'])){
                $output[$value['zone']][$value['pin']]['coords'] = self::formatCoordinates($value['polygon'][0]['polygon']);
            } else{
                $output[$value['zone']][$value['pin']]['coords'] = "";
            }

        }

        return $output;

    }

    private function formatCoordinates($data){

        $output = "";
        
        $tmp = str_replace("[", "", $data);
        $tmp = str_replace("]", "", $tmp);

        $lat_lng = explode("|", $tmp);

        foreach ($lat_lng as $key => $value) {

            $decode = json_decode($value, 0);
            $output = $output . $decode->lng . "," . $decode->lat . ",0\n";

        }

        return trim($output);

    }

}
