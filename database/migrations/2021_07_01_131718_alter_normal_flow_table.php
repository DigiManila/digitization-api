<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNormalFlowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracking_normal_flow', function (Blueprint $table) {
            $table->string("req_others")->after('documents')->nullable();
            $table->string("transaction_remarks")->after('contact_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracking_normal_flow', function (Blueprint $table) {
            //
        });
    }
}
