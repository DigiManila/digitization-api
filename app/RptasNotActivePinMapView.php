<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasNotActivePinMapView extends Model
{
    use HasFactory;
    protected $table = 'rptas_not_active_pin_map';
}
