<?php

namespace App\Http\Controllers\Api;

use App\ActiveAssessmentRoll;
use App\CancelledAssessmentRoll;
use App\RptasTaxdecMastExtn;
use App\RptasTaxdecMastMla;
use App\ArpTraceBook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ControlBookController extends Controller
{
    
    public function postControlBookPerBrgy(Request $request){

        $controlBook = [];

        if ($request->tab == 0){

            $query = ActiveAssessmentRoll::select('active_assessment_roll.ARP', 'Owner', 'Location','active_assessment_roll.PIN', 'TotalAv', 'Prev_Arp',
                                                'Prev_Pin','Prev_Owner','Prev_Av')
                        ->join('rptas_taxdec_mastextn', 'active_assessment_roll.ARP', '=','rptas_taxdec_mastextn.Arp')
                        ->where('barangay', $request->barangay)
                        ->get();

                foreach($query as $key => $value){

                    $controlBook[] = array(
                        'ARP'           =>  $value['ARP'],
                        'TDNo'          => substr($value['ARP'],9,5),
                        'Owner'         =>  $value['Owner'],
                        'Location'      =>  $value['Location'],
                        'PIN'           =>  $value['PIN'],
                        'TotalAv'       =>  $value['TotalAv'],
                        'Prev_Arp'      =>  $value['Prev_Arp'],
                        'Prev_Pin'      =>  $value['Prev_Pin'],
                        'Prev_Owner'    =>  $value['Prev_Owner'],
                        'Prev_Av'       =>  $value['Prev_Av'],

                    );
                    
                }

            return $controlBook;
        }
        else {

            $query = CancelledAssessmentRoll::select('cancelled_assessment_roll.ARP', 'Owner', 'Location','cancelled_assessment_roll.PIN', 'TotalAv', 'Prev_Arp',
                                                'Prev_Pin','Prev_Owner','Prev_Av')
                        ->join('rptas_taxdec_mastextn', 'cancelled_assessment_roll.ARP', '=','rptas_taxdec_mastextn.Arp')
                        ->where('barangay', $request->barangay)
                        ->get();

                foreach($query as $key => $value){

                    $controlBook[] = array(
                        'ARP'           =>  $value['ARP'],
                        'TDNo'          => substr($value['ARP'],9,5),
                        'Owner'         =>  $value['Owner'],
                        'Location'      =>  $value['Location'],
                        'PIN'           =>  $value['PIN'],
                        'TotalAv'       =>  $value['TotalAv'],
                        'Prev_Arp'      =>  $value['Prev_Arp'],
                        'Prev_Pin'      =>  $value['Prev_Pin'],
                        'Prev_Owner'    =>  $value['Prev_Owner'],
                        'Prev_Av'       =>  $value['Prev_Av'],

                    );
                    
                }

            return $controlBook;
        }
    }

    public function postPreviousARP(Request $request){

        $arp = $request->ARP;

        $controlBook = [];

        $whileQuery = [];
        

        while ($arp != null){
           
            
            $query = RptasTaxdecMastMla::select('rptas_taxdec_mast_mla.ARP', 'Owner', 'Location',
                                    'rptas_taxdec_mast_mla.PIN', 'TotalAv', 'Prev_Arp',
                                    'Prev_Pin','Prev_Owner','Prev_Av')
                    ->join('rptas_taxdec_mastextn', 'rptas_taxdec_mast_mla.ARP', '=','rptas_taxdec_mastextn.Arp')
                    ->where('rptas_taxdec_mast_mla.ARP', $arp)
                    ->first();
            
            // \Log::info($query['Prev_Arp']);

            if  (!$query){
                
                $arp = null;
                
            }
            else {
                $arp = $query['Prev_Arp'];
                
                $whileQuery[] = $query;
            }

            
        }

        foreach($whileQuery as $key => $value){

            $controlBook[] = array(
                'ARP'           =>  $value['ARP'],
                'TDNo'          => substr($value['ARP'],9,5),
                'Owner'         =>  $value['Owner'],
                'Location'      =>  $value['Location'],
                'PIN'           =>  $value['PIN'],
                'TotalAv'       =>  $value['TotalAv'],
                'Prev_Arp'      =>  $value['Prev_Arp'],
                'Prev_Pin'      =>  $value['Prev_Pin'],
                'Prev_Owner'    =>  $value['Prev_Owner'],
                'Prev_Av'       =>  $value['Prev_Av'],

            );
            
        }
        
        return $controlBook;        
        
    }

    public function postCancelledByARP(Request $request){

        $arp = $request->ARP;

        $controlBook = [];

        $whileQuery = [];
        
        if ($request->tab == 1){
            while ($arp != null){
            
                
                $query = RptasTaxdecMastMla::select('rptas_taxdec_mast_mla.ARP', 'Owner', 'Location',
                                        'rptas_taxdec_mast_mla.PIN', 'TotalAv', 'Prev_Arp',
                                        'Prev_Pin','Prev_Owner','Prev_Av')
                        ->join('rptas_taxdec_mastextn', 'rptas_taxdec_mast_mla.ARP', '=','rptas_taxdec_mastextn.Arp')
                        ->where('rptas_taxdec_mastextn.Prev_Arp', $arp)
                        ->first();
                
                // \Log::info($query['Prev_Arp']);

                if  (!$query){
                    
                    $arp = null;
                    
                }
                else {
                    $arp = $query['Arp'];
                    
                    $whileQuery[] = $query;
                }

                
            }
        }
        else {
            while ($arp != null){
            
                
                $query = RptasTaxdecMastMla::select('rptas_taxdec_mast_mla.ARP', 'Owner', 'Location',
                                        'rptas_taxdec_mast_mla.PIN', 'TotalAv', 'Prev_Arp',
                                        'Prev_Pin','Prev_Owner','Prev_Av')
                        ->join('rptas_taxdec_mastextn', 'rptas_taxdec_mast_mla.ARP', '=','rptas_taxdec_mastextn.Arp')
                        ->where('rptas_taxdec_mastextn.Arp', $arp)
                        ->first();
                
                // \Log::info($query['Prev_Arp']);

                if  (!$query){
                    
                    $arp = null;
                    
                }
                else {
                    $arp = $query['Arp'];
                    
                    $whileQuery[] = $query;
                }

                
            }
        }

        foreach($whileQuery as $key => $value){

            $controlBook[] = array(
                'ARP'           =>  $value['ARP'],
                'TDNo'          => substr($value['ARP'],9,5),
                'Owner'         =>  $value['Owner'],
                'Location'      =>  $value['Location'],
                'PIN'           =>  $value['PIN'],
                'TotalAv'       =>  $value['TotalAv'],
                'Prev_Arp'      =>  $value['Prev_Arp'],
                'Prev_Pin'      =>  $value['Prev_Pin'],
                'Prev_Owner'    =>  $value['Prev_Owner'],
                'Prev_Av'       =>  $value['Prev_Av'],

            );
            
        }
        
        return $controlBook;        
        
    }

    public function postActiveOrCancelBarangayNo(Request $request){

        if ($request->tab == 0){

            $query = ActiveAssessmentRoll::select('barangay')
                        ->where('barangay', '!=', 000)
                        ->distinct('barangay')
                        ->orderBy('barangay')
                        ->get();
        }

        else {
            $query = CancelledAssessmentRoll::select('barangay')
                        ->where('barangay', '!=', 000)
                        ->distinct('barangay')
                        ->orderBy('barangay')
                        ->get();
        }


        return $query;
    }

    public function traceARP(){

        \Log::info(date('Y-m-d H:i:s'));
        
        $activeARPWithDetails   = [];
        $curr_prev_arp          = [];

        $curr_barangay          = "";

        $query = ActiveAssessmentRoll::select('ARP','PIN','Barangay','MuniDistName')
                                    ->where('Barangay','!=', 'null')
                                    ->where('Barangay','!=', 000)
                                    // ->orderBy('Barangay')
                                    // ->where('Barangay', '001')
                                    // ->limit(30)
                                    ->get();

        \Log::info('Done Query');

        \Log::info('Get All Arp and Prev_Arp');

        $arps = RptasTaxdecMastExtn::select('Arp','Prev_Arp')->whereNotNull('Arp')->get();

        foreach ($arps as $key => $value) {
            if(trim($value['Arp']) != trim($value['Prev_Arp'])){
                if(trim($value['Arp'])){
                    $curr_prev_arp[$value['Arp']] = $value['Prev_Arp'];
                }
            }
        }

        \Log::info('Processing All JSON Files');

        foreach($query as $key => $value){

            if($curr_barangay != $value['Barangay']){
                $curr_barangay = $value['Barangay'];
                \Log::info("Processing Barangay $curr_barangay");
            }

            $history_json = [];

            $a = $value['ARP'];

            if(array_key_exists($a, $curr_prev_arp)){

                $history_json[] = array("ARP" => $a);
                
                $flag = true;

                while ($flag){
                    
                    if(array_key_exists($a, $curr_prev_arp)){

                        $b = $curr_prev_arp[$a];

                        if(strpos($b, "NEW") !== false || strpos($b, "NONE") !== false){
                            break; 
                        }

                        $history_json[] = array("ARP" => $b);
                        $a = $b;

                    } else{
                        $flag = false;
                    }
                }

            }
    
            $activeARPWithDetails[] = array(
                'active_arp'            =>  $value['ARP'],
                'pin'                   =>  $value['PIN'],
                'barangay'              =>  $value['Barangay'],
                'district'              =>  $value['MuniDistName'],
                'history_json'          => json_encode($history_json),
                'created_at'            => date('Y-m-d H:i:s'),
            );
            
        }

        \Log::info('Done Storing');

        
        $chunk_count = 1000;
        foreach (array_chunk($activeARPWithDetails,$chunk_count) as $chunk){

            $status = ArpTraceBook::insert($chunk);
            \Log::info($status);
            
        }

        // $status = ArpTraceBook::insert($activeARPWithDetails);
    
    
        // if($status){
        //     return response()->json([
        //         "data" => [],
        //         'message' => "Saved Succcessfully",
        //         'status' => 1
        //     ], 200);            
        // }

        // return response()->json([
        //     "data" => [],
        //     'message' => "No data found",
        //     'status' => 2
        // ], 200);
        \Log::info(date('Y-m-d H:i:s'));
          
        return 0;
        
        
    }

    private function previousARPLoop($arp){

        $controlBook = [];

        $whileQuery = [];

        \Log::info($arp);

        // while ($arp != null){
           
            
            // $query = RptasTaxdecMastExtn::select('Arp','Prev_Arp')
            //                 ->where('Arp','!=','Prev_Arp')
            //                 ->where('Arp', $arp)
            //                 ->first();

            

            // if  (!$query || $query['Arp'] == $query['Prev_Arp']){
                
            //     $arp = null;
                
            // }
            // else  {
            //     $arp = $query['Prev_Arp'];
                
                
            //     $whileQuery[] = $query;
            // }

            $query = RptasTaxdecMastExtn::select('Arp','Prev_Arp')
                    ->where('Arp','!=','Prev_Arp')
                    ->where('Arp', $arp)
                    ->orWhere('Prev_Arp', $arp)
                    ->get();
            
        // }

        \Log::info($query);

        return 0;

        foreach($whileQuery as $key => $value){
            
            $controlBook[] = array(
                'ARP'           =>  $value['Arp'],
                

            );
            
            
        }
        
        return $controlBook;        
        
    }

    
}
