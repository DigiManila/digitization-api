<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasNotActivePinBgyPolygonsView extends Model
{
    use HasFactory;
    protected $table = 'rptas_not_active_pin_bgy_polygons';
}
