<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TdSubclass extends Model
{
    use HasFactory;

    protected $table = "td_subclass";
}
