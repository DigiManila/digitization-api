<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasActiveArpTaxdecAssmnt extends Model
{
    use HasFactory;

    protected $table = 'rptas_active_arp_taxdec_assmnt';
}
