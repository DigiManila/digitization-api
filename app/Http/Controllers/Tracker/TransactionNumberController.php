<?php

namespace App\Http\Controllers\Tracker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TrackingTransaction;
use App\TrackingNormalFlowTransType;
use App\TrackingFreeFlow;
use App\TrackingNormalFlow;
use App\TrackingProcess;
use App\TrackingStatus;
use App\TrackingFreeFlowNextFlow;
use App\User;

use PDF;

use Auth;

class TransactionNumberController extends Controller
{

    public function saveTransaction(Request $request){

        $file_no = self::getTransactionNumber();

        $kind = $request['kind'];
        $req = $request['req'];
        sort($kind);
        sort($req);

        $status_no = 2;

        if($request['process_flow'] == 2){
            $status_no = 5;
        }

        $status = TrackingTransaction::create([
            'file_no'                   => $file_no,
            'transaction_datetime'      => date("Y-m-d H:i:s", strtotime($request['date_time'])),
            'or'                        => $request['or'],
            'flow_type'                 => $request['process_flow'],
            'status'                    => $status_no,
            'receiving_id'              => Auth::user()->id,
        ]);

        $id = $status->id;

        if($request['process_flow'] == 1){
            $status = TrackingNormalFlow::create([
                'tracking_id'               => $id,
                'owner_name'                => $request['ownerName'],
                'zone'                      => $request['zone'],
                'brgy'                      => $request['brgy'],
                'pin'                       => $request['pin'],
                'appraiser'                 => $request['appraiser'],
                'contact_person'            => $request['contactPerson'],
                'contact_number'            => $request['contactNo'],
                'contact_email'             => $request['contactEmail'],
                'documents'                 => json_encode($req),
                'req_others'                => $request['others_field'],
                'transaction_remarks'       => $request['trans_type_remarks'],
            ]);

            $trans_type_arr = [];

            foreach($request['trans_type_kind'] as $key => $val){
                if($val['type'] == 1){
                    sort($val['kind']);
                    $trans_type_arr[] = array(
                        'tracking_id'       => $id,
                        'transaction_type'  => $val['name'],
                        'kind'              => json_encode($val['kind']),
                    );
                }
            }
            // return $trans_type_arr;
            TrackingNormalFlowTransType::insert($trans_type_arr);


        } else{
            TrackingFreeFlow::create([
                'tracking_id'       => $id,
                'requesting_party'  => $request['contactPerson'],
                'letter_type'       => $request['letter_type'],
                'addt_remarks'      => $request['addt_remarks'],
            ]);

        }

        TrackingStatus::create([
            'tracking_id'           => $id,
            'status'                => 1,
            'created_by'            => Auth::user()->id,
        ]);

        $user =  User::with('profile')->where('id', Auth::user()->id)->first();

        $receiver_name = $user['profile']['first_name'] . " " . $user['profile']['middle_name'] . " " . $user['profile']['last_name'];
        $receiver_name = strtoupper(strtolower($receiver_name));

        if($status){

            if($request['process_flow'] == 1){
                $output = array(
                    'process_flow'          => $request['process_flow'],
                    'file_id'               => $file_no,
                    'contact_person'        => $request['contactPerson'],
                    'contact_number'        => $request['contactNo'],
                    'contact_email'         => $request['contactEmail'],
                    'or'                    => $request['or'],
                    'transaction_datetime'  => $request['date_time'],
                    'received'              => $receiver_name,
                    'pin'                   => $request['pin'],
                    'owner'                 => $request['ownerName'],
                    'transaction'           => $trans_type_arr,
                    'trans_other_remarks'   => $request['trans_type_remarks']
                );
            } else{
                $output = array(
                    'process_flow'          => $request['process_flow'],
                    'file_id'               => $file_no,
                    'contact_person'        => $request['contactPerson'],
                    'or'                    => $request['or'],
                    'transaction_datetime'  => $request['date_time'],
                    'received'              => $receiver_name,
                    'addt_remarks'          => $request['addt_remarks'],
                    'letter_type'           => $request['letter_type'],

                );
            }

            // $output = array(
            //     'process_flow'          => $request['process_flow'],
            //     'file_id'               => $file_no,
            //     'contact_person'        => $request['contactPerson'],
            //     'contact_number'        => $request['contactNo'],
            //     'contact_email'         => $request['contactEmail'],
            //     'or'                    => $request['or'],
            //     'transaction_datetime'  => $request['date_time'],
            // );

            self::createSlip($output);

            $path = storage_path("app/trackerslip/" . $file_no . ".pdf");
            $pdf_file = file_get_contents($path);
            $base64 = 'data:application/pdf;base64,' . base64_encode($pdf_file);
 
            return response()->json([
                "data" => $base64,
                // "data" => [],
                'message' => "Saved Successfully",
                'status' => 1
            ], 200);

        } else{
            return response()->json([
                "data" => "",
                'message' => "Problem saving data. Please try again.",
                'status' => 2
            ], 200);
        }

    }

    public function getAllData(){

        // \Log::info("Start:" . date('H:i:s'));

        $data = TrackingTransaction::select('id', 'file_no', 'flow_type')->get();


        foreach($data as $key => $val){
            $data[$key]['flow_type'] = $val['flow_type'] == 1 ? 'Normal Flow' : 'Free Flow';
        }

        // \Log::info("End:" . date('H:i:s'));

        return $data;
    }

    public function getData(Request $request){
        $data = TrackingTransaction::with('normal_flow')
                                    ->with('free_flow')
                                    ->with('trans_type')
                                    ->with('profile')
                                    ->where('id', $request['id'])
                                    ->first();

                                    $data['profile']['receiving_name'] = $data['profile']['first_name'] . ' ' . $data['profile']['middle_name'] . ' ' . $data['profile']['last_name'];

        // return $data;

        if($data['flow_type'] == 1){

            $data['transaction_datetime'] = date('F j, Y h:i A', strtotime($data['transaction_datetime']));
            
            
            $type_kind = [];
            foreach($data['trans_type'] as $key => $val){
                $type = $val['transaction_type'];

                foreach(json_decode($val['kind'],1) as $v){
                    if($v == "1")
                        $type_kind[$type][] = "Land";
                    elseif($v == "2")
                        $type_kind[$type][] = "Building";
                    elseif($v == "3")
                        $type_kind[$type][] = "Machinery";
                }
            }

            unset($data['trans_type']);
            $data['trans_type'] = $type_kind;
            return $data;

        } else{
            return $data;
        }


        return $data;
    }

    public function getStatus(Request $request){

        $data = TrackingTransaction::with('statusflow')->select('id', 'status', 'flow_type')->where('id', $request['id'])->first();

        if(!$data){
            return response()->json([
                "data" => "",
                'message' => "No Data",
                'status' => 2
            ], 200);            
        }

        $processes      = TrackingProcess::select('process_rank', 'name', 'description')->orderBy('process_rank', 'asc')->get();
        $current_status = $data['status'];

        $proccess_arr   = [];
        $flow           = [];

        $normal_flow_exceptions = [10,11,12,13,14];

        foreach($processes as $val){
            if(!in_array($val['process_rank'], $normal_flow_exceptions)){
                $proccess_arr[$val['process_rank']] = array(
                    'name' => $val['name'],
                    'desc' => $val['description'],
                );
            }    
        }
        
        foreach($data['statusflow'] as $val){

            $proccess_no = $val['status'];

                $flow[] = array(
                    'id' => $val['id'],
                    'name' => $proccess_arr[$proccess_no]['name'],
                    'desc' => $proccess_arr[$proccess_no]['desc'],
                    'date' => date('F j, Y', strtotime($val['created_at'])),
                    'returned' => $val['returned_to'],
                    'in_flow' => 'blue lighten-2',
                );

        }

        if($data['flow_type'] == 1){
            foreach($proccess_arr as $key => $val){
                if($key > $current_status){
                    $flow[] = array(
                        'name' => $proccess_arr[$key]['name'],
                        'desc' => $proccess_arr[$key]['desc'],
                        'date' => "",
                        'returned' => "",
                        'in_flow' => 'grey',
                    );
                } elseif($key == $current_status){
                    $flow[] = array(
                        'name' => $proccess_arr[$key]['name'],
                        'desc' => $proccess_arr[$key]['desc'],
                        'date' => "",
                        'returned' => "",
                        'in_flow' => 'green',
                    );
                }
            }
        } else{

            $current = $data['status'];

            if($current != 99){

                    $flow[] = array(
                        'id' => $val['id'],
                        'name' => $proccess_arr[$current]['name'],
                        'desc' => $proccess_arr[$current]['desc'],
                        'date' => "",
                        'returned' => $val['returned_to'],
                        'in_flow' => 'green',
                    );
                    $flow[] = array(
                        'name' => "TBD",
                        'desc' => "To be determined by the O.I.C. Office",
                        'date' => "",
                        'returned' => "",
                        'in_flow' => 'grey',
                    );  
            } else{
                $flow[] = array(
                    'id' => $val['id'],
                    'name' => $proccess_arr[99]['name'],
                    'desc' => $proccess_arr[99]['desc'],
                    'date' => date('F j, Y', strtotime($val['created_at'])),
                    'returned' => $val['returned_to'],
                    'in_flow' => 'blue lighten-2',
                );  
            }

        }

        // return $flow;

        return response()->json([
            "data" => array('flow' => $flow, 'type' => $data['flow_type']),
            'message' => "Data Retrieved",
            'status' => 1
        ], 200);
        
    }

    public function getTrackingDetails(Request $request){
        $data           = TrackingStatus::with('profile')->with('trackname')->where('id', $request['id'])->first();
        $returned_to    = TrackingProcess::select('name')->where('process_rank', $data['returned_to'])->first();

        $status = array(
            'processed_datetime'    => date('F j, Y, g:i:s A', strtotime($data['created_at'])),
            'processed_by'          => ucwords(strtolower($data['profile']['first_name'] . " " . $data['profile']['middle_name'] . " " . $data['profile']['last_name'])),
            'returned'              => $data['returned_to'] ? 'Y' : 'N',
            'returned_to'           => $data['returned_to'] ? $returned_to['name'] : null,
            'remarks'               => $data['returned_to'] ? $data['remarks'] : null,
        );

        return response()->json([
            "data" => $status,
            'message' => "Data Retrieved",
            'status' => 1
        ], 200);   

    }

    public function getSpecificData(Request $request){

        $ids = self::getProcessIds($request['id']);

        $data = TrackingTransaction::select('id', 'file_no', 'flow_type', 'status')->whereIn('status', $ids)->get();
        return $data;
    }
    
    public function getDropdownData(Request $request){

        $processes = TrackingProcess::select('process_rank', 'name')
                                        ->where('process_rank', '<', $request['process_id'])
                                        ->orderBy('process_rank', 'asc')
                                        ->get();


        if($processes){
            return response()->json([
                'data' =>  $processes,
                'message' => "Track successfully retrieved",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                'data' => [],
                'message' => "Error fetching data",
                'status' => 0
            ], 200);
        }

    }

    public function getAllProcess(Request $request){

        $data = [];

        $processes = TrackingProcess::select('process_rank', 'name', 'description')
                                        ->where('process_rank', '>', 1)
                                        ->where('process_rank', '!=', 99)
                                        ->orderBy('process_rank', 'asc')
                                        ->get();

        foreach($processes as $key => $val){
            if($val['description']){
                $processes[$key]['name'] = $val['name'] . " - " . $val['description']; 
            }
            unset($processes[$key]['description']); 
        }


        if($processes){
            return response()->json([
                'data' =>  $processes,
                'message' => "Track successfully retrieved",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                'data' => [],
                'message' => "Error fetching data",
                'status' => 0
            ], 200);
        }

    }

    public function updateTrack(Request $request){

        // TrackingTransaction
        // TrackingStatus

        // return $request;

        $process = '';
        $status_col = null;
        $returned_status = null;
        $remarks = null;

        // return $request['status'];

        if($request['status']){             // Returned to
            $returned_status = $request['back_process']['process_rank'];
            $process = $request['current_process'];
            $remarks = $request['remarks'];
        } else{
            $process = $request['current_process'] + 1;
        }

        $transaction = TrackingTransaction::find($request['process_id']);
        $transaction->status = $request['status'] ? $returned_status : $process;
        $transaction->save();

        $status = TrackingStatus::create([
            'tracking_id'               => $request['process_id'],
            'status'                    => $process,
            'created_by'                => Auth::user()->id,
            'returned_to'               => $returned_status,
            'remarks'                   => $remarks,
        ]);

        return response()->json([
            "data" => [],
            'message' => "Data Updated",
            'status' => 1
        ], 200);

    }

    private function getTransactionNumber(){

        // $tracking_count = TrackingNumber::count();

        $year = date("y");

        $tracking_count = TrackingTransaction::whereRaw("left(file_no, 2) = $year")
                                            ->count();

        
        $status = null;
        // $year = date('y',strtotime(date("Y-m-d", time()) . " + 730 day"));

        if($tracking_count){ 

            $tracking_count_y = TrackingTransaction::whereRaw("left(file_no, 2) = $year")
                                ->count();

            if($tracking_count_y){
                // get last transaction number for the current year

                $tracking_count_y = TrackingTransaction::whereRaw("left(file_no, 2) = $year")
                                        ->orderBy('file_no', 'desc')
                                        ->first();

                $tracking_no = $tracking_count_y['file_no'] + 1;

            } else{
                
                //initial register transaction number for new year
                $tracking_no = $year . sprintf('%05d', 1);
            }


        } else{

            //initial registration of tracking number (blank data in database)
            $tracking_no = $year . sprintf('%05d', 1);
            
        }

        if($tracking_no){
            return (int)$tracking_no;
        } else{
            return false;
        }

    }

    // public function createSlip(){
    private function createSlip($output){

        // $output = array(
        //     'process_flow'          => 2,
        //     'file_id'               => '2100001',
        //     'contact_person'        => 'Contact Person Name',
        //     'contact_number'        => '09541236547',
        //     'contact_email'         => 'email@email.com',
        //     'or'                    => '12324545',
        //     'transaction_datetime'  => 'September 29, 2021 10:20 AM',
        //     'received'              => 'John Christopher Apuli',
        //     'pin'                   => '117-01-001-001-001',
        //     'owner'                 => 'Owner Name',
        //     'transaction'           => array(
        //                                     // [
        //                                     //     'transaction_type'      => 'New',
        //                                     //     'kind'                  => '["1","2","3"]',                                                
        //                                     // ],
        //                                     // [
        //                                     //     'transaction_type'      => 'Transfer',
        //                                     //     'kind'                  => '["1","2","3"]',                                                
        //                                     // ],
        //                                     // [
        //                                     //     'transaction_type'      => 'Segregation',
        //                                     //     'kind'                  => '["1","2","3"]',                                                
        //                                     // ],
        //                                     // [
        //                                     //     'transaction_type'      => 'Cancellation',
        //                                     //     'kind'                  => '["1","2","3"]',                                                
        //                                     // ],
        //                                     [
        //                                         'transaction_type'      => 'Transfer',
        //                                         'kind'                  => '["1","2"]',                                                
        //                                     ],
        //                                     [
        //                                         'transaction_type'      => 'Segregation',
        //                                         'kind'                  => '["1","2"]',                                                
        //                                     ],
        //                                ),
        //     'trans_other_remarks'   => "",
        //     // 'trans_other_remarks'   => "Segregation of 3 lands",
        //     'letter_type'           => "Office of the Mayor"
        // );

        $number = "+0445412365 loc. 12342";

        $path = storage_path("app/trackerslip/" . $output['file_id'] . ".pdf");

        PDF::setPrintHeader(false);
        PDF::AddPage('P', 'A4');
        PDF::SetAutoPageBreak(false, 0);

        // PDF::setJPEGQuality(25);

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79); 
        PDF::MultiCell(218, 10, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, 9, true);
        PDF::SetFont('times', '', 11, '', false);
        PDF::MultiCell(218, 10, 'City of Manila', 0, 'C', 0, 0, 0, 14, true);
        PDF::SetFont('times', 'B', 15, '', false);
        PDF::SetTextColor(255, 102, 102);
        PDF::MultiCell(218, 10, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, 18, true);

        $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
        PDF::Image($logo1, 18, 6, 20, 20, '', '', '', false, 300, '', false, false, 0);

        $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
        PDF::Image($logo2, 173, 6, 21, 21, '', '', '', false, 300, '', false, false, 0);

        PDF::SetFont('times', 'B', 18, '', false);
        PDF::SetTextColor(54,69,79);
        PDF::SetTextColor(120, 120, 120);
        PDF::MultiCell(0, 0, 'TRACKING SLIP', 0, 'C', 0, 0, 15, 30, true);

        PDF::SetFont('times', '', 11, '', false);

        PDF::MultiCell('', '', 'Transaction Date/Time', 0, '', 0, 0, 130, 45, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, 167, 45, true);
        PDF::MultiCell('', '', $output['transaction_datetime'], 0, '', 0, 0, 140, 50, true);

        $col_1_start_pos = 20;
        $colon_add_pos = $col_1_start_pos + 50;
        $col_1_after_col_pos = $colon_add_pos + 10;

        $row_start_1 = 50;

        PDF::SetFont('times', '', 14, '', false);

        PDF::MultiCell('', '', 'Transaction Number', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
        PDF::MultiCell('', '', $output['file_id'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);

        PDF::SetFont('times', '', 11, '', false);

        $row_start_1 += 10;
        PDF::MultiCell('', '', 'Requesting Party', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
        PDF::MultiCell('', '', $output['contact_person'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);

        $row_start_1 += 5;
        PDF::MultiCell('', '', 'Transaction O.R. Number', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
        PDF::MultiCell('', '', $output['or']  ? $output['or'] : 'N/A', 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);



        if($output['process_flow'] == 1){

            $row_start_1 += 5;
            PDF::MultiCell('', '', 'PIN', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
            PDF::MultiCell('', '', $output['pin'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);

            $row_start_1 += 5;
            PDF::MultiCell('', '', 'Registered Owner', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
            PDF::MultiCell('', '', $output['owner'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);
    
            // $row_start_1 += 5;
            // PDF::MultiCell('', '', 'Nature of Transaction', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
            // PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
            // PDF::MultiCell('', '', $output['transaction_type'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);
    
    
            $row_start_2 = $row_start_1;
    
            foreach($output['transaction'] as $key => $val){
    
                $row_start_2 += 5;
                
                if(!$key){
                    PDF::MultiCell('', '', 'Nature of Transaction', 0, '', 0, 0, $col_1_start_pos, $row_start_2, true);
                }
                
                PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_2, true);
                PDF::MultiCell('', '', $val['transaction_type'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_2, true);
    
    
                $kind = json_decode($val['kind'],1);
    
                if($kind){
    
                    $start_col = $col_1_after_col_pos + 45;
    
                    foreach($kind as $v){
                        if($v === '1'){
                            PDF::MultiCell('', '', '• Land', 0, '', 0, 0, $start_col, $row_start_2, true);
                            $start_col = $start_col + 16;
                        } elseif($v === '2'){
                            PDF::MultiCell('', '', '• Building', 0, '', 0, 0, $start_col, $row_start_2, true);
                            $start_col = $start_col + 20;
                        } elseif($v === '3'){
                            PDF::MultiCell('', '', '• Machinery', 0, '', 0, 0, $start_col, $row_start_2, true);
                            $start_col = $start_col + 20;
                        }
                    }
                }
            }

            if($output['trans_other_remarks']){
                $row_start_2 += 5;
                PDF::MultiCell('', '', 'Other / Remarks', 0, '', 0, 0, $col_1_start_pos, $row_start_2, true);
                PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_2, true);
                PDF::MultiCell(80, '', $output['trans_other_remarks'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_2, true);
            }
    
    
        } else{

            $row_start_1 += 5;
            PDF::MultiCell('', '', 'Letter Type', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
            PDF::MultiCell('', '', $output['letter_type'], 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);

            $row_start_1 += 5;
            PDF::MultiCell('', '', 'Additional Remarks', 0, '', 0, 0, $col_1_start_pos, $row_start_1, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, $row_start_1, true);
            PDF::MultiCell('', '', $output['addt_remarks'] ? $output['addt_remarks'] : 'N/A', 0, '', 0, 0, $col_1_after_col_pos, $row_start_1, true);
        }


        // new style
        $style = array(
            'border' => 1,
            'padding' => 3,
            'fgcolor' => array(128,128,128),
            'bgcolor' => false
        );

        // QRCODE,H : QR-CODE Best error correction
        PDF::write2DBarcode('https://digitization-manila.grimgerdeph.com/', 'QRCODE,H', 170, 110, 30, 30, $style, 'N');


        PDF::MultiCell('', '', 'Received By', 0, '', 0, 0, $col_1_start_pos, 125, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_add_pos, 125, true);
        PDF::MultiCell('', '', $output['received'], 0, '', 0, 0, $col_1_after_col_pos, 125, true);

        PDF::MultiCell(200, '', "For inquiry regarding the status of your transaction,", 0, '', 0, 0, 20, 132, true);
        PDF::MultiCell(200, '', "please call $number.", 0, '', 0, 0, 20, 137, true);
        PDF::MultiCell(200, '', "Please provide the transaction number when you inquire on the status of your request.", 0, '', 0, 0, 20, 142, true);

        $style = array('width' => 0.5, 'color' => array(120, 120, 120));

        $xc=85;
        $yc=149;
        PDF::Line($xc-90, $yc, $xc+125, $yc, $style);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        return "OK";

        // return "data:application/pdf;base64," . $output_pdf;

    }

    private function getProcessIds($desc){

        switch ($desc) {
            case 'RECORDS':
                return array(1,9);
                break;
            case 'TAX_MAP':
                return array(2,6);
                break;
            case 'APPRAISER':
                return array(3,4);
                break;
            case 'OIC':
                return array(5);
                break;
            case 'DATA':
                return array(7);
                break;
            case 'IT':
                return array(8);
                break;
            default:
                # code...
                break;
        }


    }

}
