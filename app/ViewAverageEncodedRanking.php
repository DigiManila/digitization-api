<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAverageEncodedRanking extends Model
{
    use HasFactory;
    protected $table = 'average_encoded_ranking';
}
