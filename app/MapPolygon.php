<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapPolygon extends Model
{
    public function assessment()
    {
        return $this->hasOne('App\AssessmentRoll', 'pin', 'pin');
    }
}
