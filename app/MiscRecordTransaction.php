<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MiscRecordTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'arp', 
        'pin', 
        'cancelled_arp',
        'cancellation_number', 
        'cancellation_number_date_added',
        'td_no', 
        'td_no_date_added',
        'faas', 
        'faas_date_added',
        'sup_doc', 
        'sup_doc_date_added',
    ];
}
