<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterReleaseTaxDecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_tax_decs', function (Blueprint $table) {
            $table->string('processed_by')->after('certifier');
            $table->string('printed_by')->after('file_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_tax_decs', function (Blueprint $table) {
            //
        });
    }
}
