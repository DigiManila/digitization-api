<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRptasCancelledArpnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rptas_cancelled_arpn', function (Blueprint $table) {
            $table->string('RecId')->nullable();
            $table->integer('ForYear')->nullable();
            $table->char('Series',4)->nullable();
            $table->char('Arp',20)->nullable();
            $table->char('PIN',30)->nullable();
            $table->string('RecommendBy',50)->nullable();
            $table->date('RecommendDt')->nullable();
            $table->string('RecommendDesig',50)->nullable();
            $table->string('NotedBy',50)->nullable();
            $table->date('NotedDt')->nullable();
            $table->string('RequestedBy',50)->nullable();
            $table->string('RequestedAddress')->nullable();
            $table->string('InsertedBy', 50)->nullable();
            $table->date('InsertedDt')->nullable();
            $table->string('UpdatedBy', 50)->nullable();
            $table->date('UpdatedDt')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancelled_arpn');
    }
}
