<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingBrgyNo extends Model
{
    use HasFactory;
    protected $table = 'mapping_brgyno';
}
