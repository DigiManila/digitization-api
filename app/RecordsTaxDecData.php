<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordsTaxDecData extends Model
{
    use HasFactory;
    protected $guarded = []; 

    public function profile(){
        return $this->hasOne('App\Profile', 'user_id', 'verified_by');
    }

}
