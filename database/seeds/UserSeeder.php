<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'id'        => 1,
            'username'  => 'sadmin',
            'email'     => 'king@pogi.com',
            'password'  => Hash::make('sadmin'),
        ]);

        DB::table('profiles')->insert([
            'user_id'           => 1,
            'first_name'        => 'Super',
            'last_name'         => 'Admin',
            'department'        => 'IT',
            'position'          => 'IT0II',
            'user_type'         => 'SA',
            'profile_pic_path'  => "images/doa.png",
        ]);

    }
}
