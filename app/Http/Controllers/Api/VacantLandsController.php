<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpFoundation\StreamedResponse;

use App\AssessmentRoll;
use App\ViewVacantLandsDetail;
use App\ViewVacantLandsBrgy;
use App\ViewVacantLandsPin;
use App\ViewVacantLandsReport;

use App\RptasTaxdecMastMla;
use Storage;


class VacantLandsController extends Controller
{
    public function getBarangay(){

        $sum        = 0;
        $count      = 0;

        $details    = new ViewVacantLandsBrgy;
        $vacants    = $details->get()->toArray();
        $sum        = number_format($details->sum('area'));
        $count      = number_format($details->sum('sum_pin'));

        return compact('vacants', 'sum', 'count');


    }

    // Possible for deletion
    public function getBarangayKind(Request $request){

        $bgy = $request['bgyNo'];

        $out = [];

        $data = ViewVacantLandsDetail::select('area', 'kind')
                        ->whereRaw("substring_index(substring_index(`view_vacant_lands_details`.`pin`,'-',3),'-',-(1)) = $bgy")
                        // ->where('area', '>', 1000)
                        // ->where('kind', 'like', 'L-%')
                        ->get();

        foreach($data as $key => $val){
            if(isset($out[$val['kind']])){
                $out[$val['kind']]['total'] = $out[$val['kind']]['total'] + $val['area'];
            } else{
                $out[$val['kind']]['total'] = $val['area'];
            }
        }

        foreach($out as $key => $val){
            $out[$key]['total'] = number_format($val['total'], 2);
            $out[$key] = array_merge($out[$key], self::getIcon($key));
        }

    
        return $out;

    }

    // Possible for deletion
    public function getVacantPin(Request $request){

        $data = ViewVacantLandsDetail::select('view_vacant_lands_details.pin', 'assessment_rolls.current_arp', 'assessment_rolls.owner_name', 'assessment_rolls.area', 'assessment_rolls.market_value', 'assessment_rolls.currentAssessValue', 'assessment_rolls.previousAssessValue')
                        ->leftJoin('assessment_rolls', 'assessment_rolls.pin', '=', 'view_vacant_lands_details.pin')
                        ->where('view_vacant_lands_details.kind', $request['kind'])
                        ->where('assessment_rolls.cancelled', 0)
                        ->whereRaw("substring_index(substring_index(`view_vacant_lands_details`.`pin`,'-',3),'-',-(1)) = " . $request['bgyNo'])
                        ->get();

        return $data;

    }

    public function listPerBarangay(Request $request){

        $per_bgy = [];

        $data = ViewVacantLandsDetail::select('pin', 'area', 'kind')
                        ->whereRaw("substring_index(substring_index(`pin`,'-',3),'-',-(1)) = " . $request['bgyNo'])
                        ->orderBy("PIN", "asc")
                        ->get();

        foreach ($data as $key => $value) {
            if(isset($per_bgy[$value['pin']])){
                $per_bgy[$value['pin']]['area'] = $per_bgy[$value['pin']]['area'] + $value['area'];
                $per_bgy[$value['pin']]['kind'] = $per_bgy[$value['pin']]['kind'] . ", " . $value['kind'];
            } else{
                $per_bgy[$value['pin']] = $value;
            }
        }

        return array_values($per_bgy);        
    }

    public function exportAllData(){

        $data = ViewVacantLandsDetail::get();
        
        return self::createExcel($data, "A", null);

    }

    public function exportPerBrgy(Request $request){

        $data = ViewVacantLandsDetail::whereRaw("substring_index(substring_index(`pin`,'-',3),'-',-(1)) = " . $request['bgyNo'])
                                        ->get();

        return self::createExcel($data, "P", $request['bgyNo']);
        
    }

    public function breakdownPerBgy(){

        $newData = [];

        $data = ViewVacantLandsDetail::select('PIN', 'area', 'kind', 'MV', 'AV')
                        ->get();
        

        foreach ($data as $key => $value) {
            
            $bgy = explode('-', $value['pin'])[2];

            $kind = $value['kind'];

            if(isset($newData[$bgy][$kind])){
                $newData[$bgy][$kind]['total_area'] = $newData[$bgy][$kind]['total_area'] + $value['area'];
                $newData[$bgy][$kind]['total_mv']   = $newData[$bgy][$kind]['total_mv'] + $value['MV'];
                $newData[$bgy][$kind]['total_av']   = $newData[$bgy][$kind]['total_av'] + $value['AV'];
            } else{
                $newData[$bgy][$kind]['total_area'] = $value['area'];
                $newData[$bgy][$kind]['total_mv']   = $value['MV'];
                $newData[$bgy][$kind]['total_av']   = $value['AV'];
            }

        }

        return self::createExcelBreakdown($newData);

    }


    private function createExcel($data, $param, $bgy){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->getColumnDimension('A')->setWidth(19);
        $worksheet->getColumnDimension('B')->setWidth(15);
        $worksheet->getColumnDimension('C')->setWidth(75);
        $worksheet->getColumnDimension('D')->setWidth(60);
        $worksheet->getColumnDimension('E')->setWidth(13);
        $worksheet->getColumnDimension('F')->setWidth(17);
        $worksheet->getColumnDimension('G')->setWidth(9);
        $worksheet->getColumnDimension('H')->setWidth(20);
        $worksheet->getColumnDimension('I')->setWidth(18);

        if($param == "A"){
            $worksheet->setCellValue("A2", "Vacant Lands (All Barangays)");
        } else{
            $worksheet->setCellValue("A2", "Vacant Lands (Barangay $bgy)");
        }

                
        $worksheet
                ->setCellValue("A3", "As of " . date('F j, Y'))
                ->setCellValue("A5", "PIN")
                ->setCellValue("B5", "ARP Number")
                ->setCellValue("C5", "Owner's Name")
                ->setCellValue("D5", "Mailing Address")
                ->setCellValue("E5", "Market Value")
                ->setCellValue("F5", "Assessment Level")
                ->setCellValue("G5", "Area")
                ->setCellValue("H5", "Kind")
                ->setCellValue("I5", "Assessment Value");


        $startRow = 7;
        $row = $startRow;
        
        foreach ($data as $key => $val) {
            $worksheet
                ->setCellValue("A{$row}", $val["pin"])
                ->setCellValue("B{$row}", $val["arp"])
                ->setCellValue("C{$row}", $val["Owner"])
                ->setCellValue("D{$row}", $val["OwnerAddress"])
                ->setCellValue("E{$row}", number_format($val["MV"], 2))
                ->setCellValue("F{$row}", $val["AL"])
                ->setCellValue("G{$row}", $val["area"])
                ->setCellValue("H{$row}", $val["kind"])
                ->setCellValue("I{$row}", number_format($val["AV"], 2));
            $row++;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;
    }

    private function createExcelBreakdown($data){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->getColumnDimension('A')->setWidth(20);
        $worksheet->getColumnDimension('B')->setWidth(10);
        $worksheet->getColumnDimension('C')->setWidth(2);
        $worksheet->getColumnDimension('D')->setWidth(20);
        $worksheet->getColumnDimension('E')->setWidth(2);
        $worksheet->getColumnDimension('F')->setWidth(30);
        $worksheet->getColumnDimension('G')->setWidth(2);
        $worksheet->getColumnDimension('H')->setWidth(36);

        $worksheet->setCellValue("A2", "Vacant Lands (Breakdown Per Barangays)");
    
        $worksheet
                ->setCellValue("A3", "As of " . date('F j, Y'))
                ->setCellValue("A5", "Barangay")
                ->setCellValue("B5", "Kind")
                ->setCellValue("D5", "Total Area per Kind")
                ->setCellValue("F5", "Total Market Value per Kind")
                ->setCellValue("H5", "Total Current Assess Value per Kind");


        $startRow = 7;
        $row = $startRow;

        $row_bgy_start = 0;
        
        foreach ($data as $key => $val) {

            $worksheet->setCellValue("A{$row}", "Barangay " . $key);
            $row++;
            $row_bgy_start = $row;
            $total = 0;
            $total_mv = 0;
            $total_av = 0;

            foreach ($val as $k => $v) {
                $worksheet->setCellValue("B{$row}", $k);
                $worksheet->setCellValue("D{$row}", number_format($v['total_area'], 2));
                $worksheet->setCellValue("F{$row}", number_format($v['total_mv'], 2));
                $worksheet->setCellValue("H{$row}", number_format($v['total_av'], 2));
                $worksheet->getStyle("D{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $worksheet->getStyle("F{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $worksheet->getStyle("H{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $total      = $total + $v['total_area'];
                $total_mv   = $total_mv + $v['total_mv'];
                $total_av   = $total_av + $v['total_av'];
                $row++;
            }

            $worksheet->getStyle("D{$row}")
                        ->getBorders()
                        ->getTop()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $worksheet->getStyle("F{$row}")
                        ->getBorders()
                        ->getTop()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $worksheet->getStyle("H{$row}")
                        ->getBorders()
                        ->getTop()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

            $worksheet->setCellValue("D{$row}",  number_format($total, 2));
            $worksheet->setCellValue("F{$row}",  number_format($total_mv, 2));
            $worksheet->setCellValue("H{$row}",  number_format($total_av, 2));
            $worksheet->getStyle("D{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $worksheet->getStyle("F{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $worksheet->getStyle("H{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                
            $row = $row + 2;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

    private function getIcon($kind){

        switch ($kind) {
            case 'L-CHAR':
                return array(
                    "icon" => "mdi-charity",
                    "title" => "Charitable"
                );
                break;

            case 'L-COMM':
                return array(
                    "icon" => "mdi-office-building",
                    "title" => "Commercial"
                );
                break;

            case 'L-EDUC':
                return array(
                    "icon" => "mdi-school",
                    "title" => "Education"
                );
                break;

            case 'L-GOVT':
                return array(
                    "icon" => "mdi-bank",
                    "title" => "Government"
                );
                break;
            
            case 'L-INDU':
                return array(
                    "icon" => "mdi-factory",
                    "title" => "Industrial"
                );
                break;

            case 'L-OTHE':
                return array(
                    "icon" => "mdi-abugida-thai",
                    "title" => "Others"
                );
                break;

            case 'L-RELI':
                return array(
                    "icon" => "mdi-church",
                    "title" => "Religion"
                );
                break;

            case 'L-RESI':
                return array(
                    "icon" => "mdi-home-group",
                    "title" => "Residential"
                );
                break;

            case 'L-SPC1':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 1"
                );
                break;

            case 'L-SPC2':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 2"
                );
                break;

            case 'L-SPC3':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 3"
                );
                break;


            case 'L-SPC4':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 4"
                );
                break;

            case 'L-SPC5':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 5"
                );
                break;

            case 'L-SPC6':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 6"
                );
                break;

            case 'L-SPEC':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "SPEC"
                );
                break;

            default:
                # code...
                break;
        }

    }


}
