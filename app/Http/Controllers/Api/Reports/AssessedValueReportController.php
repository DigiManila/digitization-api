<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\ReportAssessmentRoll;


class AssessedValueReportController extends Controller
{
    public function getAssessedValueReport(Request $request){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        
        $firstMonth = null;
        $endMonth = null;
        $quarter = $request->quarter;
        $year = null;
        $month = null;
        $rangeDate = null;

        if ($quarter != ""){
            if ($request->quarter == "1st"){
                $firstMonth = "01";
                $endMonth = "03";       
            }
            else if ($quarter== "2nd"){
                $firstMonth = "04";
                $endMonth = "06";
            }
            else if ($quarter == "3rd"){
                $firstMonth = "07";
                $endMonth = "09";
            }
            else if ($quarter == "4th"){
                $firstMonth = "10";
                $endMonth = "12";
            }
            $year = $request->date;
        }
        else if( $request->datePicker == 'Yearly'){
            $year = $request->date;
        }
        else if( $request->datePicker == 'Range'){
            $rangeDate = $request->date;
            
        }
        else {
            $explodeDayMonthYear = explode('-',$request->date);

            $year = $explodeDayMonthYear[0];
            $month = $explodeDayMonthYear[1];
        
        }


        $dateCreated = $request->date;
        $datePick = $request->datePicker;


        $getAllTaxable = [];
        $getAllExempted = [];

        $data = [];

        $queryAllClassReport = ReportAssessmentRoll::select('ARP', 'Prev_Arp', 'AUDesc', 'kind',
                    'AV','AppraisedDt','InsertedDt', 'Taxability', 'Current')
                    
                    ->when($request->datePicker == 'asOfToday', function($query) use ($request){
                            return $query->whereDate('AppraisedDt', '<=', $request->date );
                    })
                    ->when($request->datePicker == 'Daily', function($query) use ($request){
                        return $query->whereDate('AppraisedDt', $request->date );
                    })
                    ->when($request->datePicker == 'Weekly', function($query) use ($request){
                        return $query->whereBetween('AppraisedDt',  [Carbon::parse($request->date)->startOfWeek()->format('Y-m-d'), 
                                        Carbon::parse($request->date)->endOfWeek()->format('Y-m-d')])
                                        ->orderBy('AppraisedDt', 'desc');
                    })
                    ->when($request->datePicker == 'Monthly', function($query) use ($month, $year){
                        return $query->whereMonth('AppraisedDt',  $month)
                                    ->whereYear('AppraisedDt', $year);                                        
                    })
                    ->when($request->datePicker == 'Quarterly', function($query) use ($firstMonth, $endMonth, $year){
                        return $query->whereMonth('AppraisedDt', '>=', 
                        Carbon::createFromFormat('m',$firstMonth))
                        ->whereMonth('AppraisedDt', '<=', 
                         Carbon::createFromFormat('m',$endMonth))
                        ->whereYear('AppraisedDt', $year);
                    })
                    ->when($request->datePicker == 'Yearly', function($query) use ( $year){                            
                        return $query->whereYear('AppraisedDt',  
                                Carbon::createFromFormat('Y',$year));
                    })
                    ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                            
                        return $query->whereBetween('AppraisedDt',   $rangeDate)
                                        ->orderBy('AppraisedDt', 'desc');
                    })
                    
        ->get();
            
            
        foreach($queryAllClassReport as $key => $value){

            foreach($queryAllClassReport as $key1 => $value1){
                if (trim($value1['Prev_Arp']) == trim($value['ARP'])){

                    $value['Current'] = 2;
                }           
            }
            
            $data[] = array(
                'InsertedDt'    =>  $value['InsertedDt'],
                'InsertedDt'    =>  $value['InsertedDt'],
                'Taxability'    =>  $value['Taxability'],
                'ARP'           =>  $value['ARP'],
                'Prev_Arp'      =>  $value['Prev_Arp'],
                'kind'          =>  $value['kind'],
                'AUDesc'        =>  $value['AUDesc'],
                'AV'            =>  $value['AV'],
                'Current'       =>  $value['Current'],
            );
            
        }


        //----------------BALANCING VALUE OF AREA, MV, AV and Prev_Av--------------"
         
        $oldKey = 0;   
        foreach($data as $key => $value){
    
            if($key > 0){
                if ($value['ARP'] == $data[$oldKey]['ARP'] && $value['AUDesc'] == $data[$oldKey]['AUDesc'] ){
                    $data[$key]['Current'] = 2;
                }
            }

            if ($key < 1){
                $oldKey = 0;
            
            }
            else {
                $oldKey++;
                
            }
            
        }
        #-----------------------------------------------Taxable---------------------------------------------#
        
        //------------------------for all month active taxable ---------------------------//
    
        //all land data taxable
        $getAllTaxable['landResi'] = 0.00;
        $getAllTaxable['landComm'] = 0.00;
        $getAllTaxable['landIndu'] = 0.00;
        $getAllTaxable['landResiParcel'] = 0;
        $getAllTaxable['landCommParcel'] = 0;
        $getAllTaxable['landInduParcel'] = 0;

        //all imp data taxable
        $getAllTaxable['bldgResi'] = 0.00;
        $getAllTaxable['bldgComm'] = 0.00;
        $getAllTaxable['bldgIndu'] = 0.00;
        $getAllTaxable['bldgResiParcel'] = 0;
        $getAllTaxable['bldgCommParcel'] = 0;
        $getAllTaxable['bldgInduParcel'] = 0;

        //all mach data taxable
        $getAllTaxable['machResi'] = 0.00;
        $getAllTaxable['machComm'] = 0.00;
        $getAllTaxable['machSpec'] = 0.00;
        $getAllTaxable['machResiParcel'] = 0;
        $getAllTaxable['machCommParcel'] = 0;
        $getAllTaxable['machSpecParcel'] = 0;

            //all spec data taxable
        $getAllTaxable['specRecreationalLand'] = 0.00;
        $getAllTaxable['specHospitalLand'] = 0.00;
        $getAllTaxable['specCulturalLand'] = 0.00;
        $getAllTaxable['specSpecialLand'] = 0.00;
        $getAllTaxable['specScientificLand'] = 0.00;
        $getAllTaxable['specLocWaterDistLand'] = 0.00;
        $getAllTaxable['specOthersLand'] = 0.00;

        $getAllTaxable['specRecreationalBldg'] = 0.00;
        $getAllTaxable['specHospitalBldg'] = 0.00;
        $getAllTaxable['specCulturalBldg'] = 0.00;
        $getAllTaxable['specSpecialBldg'] = 0.00;
        $getAllTaxable['specScientificBldg'] = 0.00;
        $getAllTaxable['specLocWaterDistBldg'] = 0.00;
        $getAllTaxable['specOthersBldg'] = 0.00;

        $getAllTaxable['specRecreationalMach'] = 0.00;
        $getAllTaxable['specHospitalMach'] = 0.00;
        $getAllTaxable['specCulturalMach'] = 0.00;
        $getAllTaxable['specSpecialMach'] = 0.00;
        $getAllTaxable['specScientificMach'] = 0.00;
        $getAllTaxable['specLocWaterDistMach'] = 0.00;
        $getAllTaxable['specOthersMach'] = 0.00;
            
        $getAllTaxable['specRecreationalLandParcel'] = 0;
        $getAllTaxable['specHospitalLandParcel'] = 0;
        $getAllTaxable['specCulturalLandParcel'] = 0;
        $getAllTaxable['specSpecialLandParcel'] = 0;
        $getAllTaxable['specScientificLandParcel'] = 0;
        $getAllTaxable['specLocWaterDistLandParcel'] = 0;
        $getAllTaxable['specOthersLandParcel'] = 0.00;
            
        $getAllTaxable['specRecreationalBldgParcel'] = 0;
        $getAllTaxable['specHospitalBldgParcel'] = 0;
        $getAllTaxable['specCulturalBldgParcel'] = 0;
        $getAllTaxable['specSpecialBldgParcel'] = 0;
        $getAllTaxable['specScientificBldgParcel'] = 0;
        $getAllTaxable['specLocWaterDistBldgParcel'] = 0;
        $getAllTaxable['specOthersBldgParcel'] = 0.00;

        $getAllTaxable['specRecreationalMachParcel'] = 0;
        $getAllTaxable['specHospitalMachParcel'] = 0;
        $getAllTaxable['specCulturalMachParcel'] = 0;
        $getAllTaxable['specSpecialMachParcel'] = 0;
        $getAllTaxable['specScientificMachParcel'] = 0;
        $getAllTaxable['specLocWaterDistMachParcel'] = 0;
        $getAllTaxable['specOthersMachParcel'] = 0.00;

        
        foreach($data as $key => $value){
            
            if(
                $data[$key]['Taxability']=='T' && $value['Current'] != 2
            )
            {
                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'L' ){
                                
                    $getAllTaxable['landResi'] += $value['AV'];
                    $getAllTaxable['landResiParcel'] += 1;
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'L'){
                                
                    $getAllTaxable['landComm'] += $value['AV'];
                    $getAllTaxable['landCommParcel'] += 1;
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'L'){
                    
                    $getAllTaxable['landIndu'] += $value['AV'];
                    $getAllTaxable['landInduParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'B'){
                                    
                    $getAllTaxable['bldgResi'] += $value['AV'];
                    $getAllTaxable['bldgResiParcel'] += 1;
                        
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'B'){
                            
                    $getAllTaxable['bldgComm'] += $value['AV'];
                    $getAllTaxable['bldgCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'B'){
                            
                    $getAllTaxable['bldgIndu'] += $value['AV'];
                    $getAllTaxable['bldgInduParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'M'){
                               
                    $getAllTaxable['machResi'] += $value['AV'];
                    $getAllTaxable['machResiParcel'] += 1;
                     
                }
 
                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'M'){
                            
                    $getAllTaxable['machComm'] += $value['AV'];
                    $getAllTaxable['machCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                            
                    $getAllTaxable['machSpec'] += $value['AV'];
                    $getAllTaxable['machSpecParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'L'){
                    
                    $getAllTaxable['specRecreationalLand']  += $value['AV'];
                    $getAllTaxable['specRecreationalLandParcel'] += 1;
                     
                }
 
                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specRecreationalBldg'] += $value['AV'];
                    $getAllTaxable['specRecreationalBldgParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specRecreationalMach'] += $value['AV'];
                    $getAllTaxable['specRecreationalMachParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'L'){
                    
                    $getAllTaxable['specHospitalLand']  += $value['AV'];
                    $getAllTaxable['specHospitalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specHospitalBldg'] += $value['AV'];
                    $getAllTaxable['specHospitalBldgParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specHospitalMach'] += $value['AV'];
                    $getAllTaxable['specHospitalMachParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'L'){
                    
                    $getAllTaxable['specCulturalLand']  += $value['AV'];
                    $getAllTaxable['specCulturalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specCulturalBldg'] += $value['AV'];
                    $getAllTaxable['specCulturalBldgParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specCulturalMach'] += $value['AV'];
                    $getAllTaxable['specCulturalMachParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'L'){
                    
                    $getAllTaxable['specSpecialLand']  += $value['AV'];
                    $getAllTaxable['specSpecialLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specSpecialBldg'] += $value['AV'];
                    $getAllTaxable['specSpecialBldgParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specSpecialMach'] += $value['AV'];
                    $getAllTaxable['specSpecialMachParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'L'){
                    
                    $getAllTaxable['specRecreationalLand']  += $value['AV'];
                    $getAllTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specScientificBldg'] += $value['AV'];
                    $getAllTaxable['specScientificBldgParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specScientificMach'] += $value['AV'];
                    $getAllTaxable['specScientificMachParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'L'){
                        
                    $getAllTaxable['specLocWaterDistLand']  += $value['AV'];
                    $getAllTaxable['specLocWaterDistLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specLocWaterDistBldg'] += $value['AV'];
                    $getAllTaxable['specLocWaterDistBldgParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specLocWaterDistMach'] += $value['AV'];
                    $getAllTaxable['specLocWaterDistMachParcel'] += 1;
                    
                }    

                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'L'){
                        
                    $getAllTaxable['specOthersLand']  += $value['AV'];
                    $getAllTaxable['specOthersLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'B'){
                    
                    $getAllTaxable['specOthersBldg'] += $value['AV'];
                    $getAllTaxable['specOthersBldgParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'M'){
                    
                    $getAllTaxable['specOthersMach'] += $value['AV'];
                    $getAllTaxable['specOthersMachParcel'] += 1;
                    
                }
                
            }
        }

        #-----------------------------------------------EXEMPTED---------------------------------------------#
        
        //------------------------for all month active exempted ---------------------------//
        
        //all land data taxable
        $getAllExempted['landGovt'] = 0.00;
        $getAllExempted['landReli'] = 0.00;
        $getAllExempted['landChar'] = 0.00;
        $getAllExempted['landEduc'] = 0.00;
        $getAllExempted['landOthe'] = 0.00;
       
        $getAllExempted['landGovtParcel'] = 0;
        $getAllExempted['landReliParcel'] = 0;
        $getAllExempted['landCharParcel'] = 0;
        $getAllExempted['landEducParcel'] = 0;
        $getAllExempted['landOtheParcel'] = 0;
        $getAllExempted['landMachParcel'] = 0;

        //all bldg data exempted
        $getAllExempted['bldgGovt'] = 0.00;
        $getAllExempted['bldgReli'] = 0.00;
        $getAllExempted['bldgChar'] = 0.00;
        $getAllExempted['bldgEduc'] = 0.00;
        $getAllExempted['bldgOthe'] = 0.00;
      
        $getAllExempted['bldgGovtParcel'] = 0;
        $getAllExempted['bldgReliParcel'] = 0;
        $getAllExempted['bldgCharParcel'] = 0;
        $getAllExempted['bldgEducParcel'] = 0;
        $getAllExempted['bldgOtheParcel'] = 0;
        $getAllExempted['bldgMachParcel'] = 0;

        //all mach data exempted
        $getAllExempted['machGovt'] = 0.00;
        $getAllExempted['machReli'] = 0.00;
        $getAllExempted['machChar'] = 0.00;
        $getAllExempted['machEduc'] = 0.00;
        $getAllExempted['machOthe'] = 0.00;
      
        $getAllExempted['machGovtParcel'] = 0;
        $getAllExempted['machReliParcel'] = 0;
        $getAllExempted['machCharParcel'] = 0;
        $getAllExempted['machEducParcel'] = 0;
        $getAllExempted['machOtheParcel'] = 0;
       

        foreach($data as $key => $value){
             
           
            if(
                $data[$key]['Taxability']=='E' && $value['Current'] != 2
              )
            {
                
                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'L' ){
                            
                    $getAllExempted['landGovt'] += $value['AV'];
                    $getAllExempted['landGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'L'){
                            
                    $getAllExempted['landReli'] += $value['AV'];
                    $getAllExempted['landReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'L'){
                        
                    $getAllExempted['landChar'] += $value['AV'];
                    $getAllExempted['landCharParcel'] += 1;
                        
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'L'){
                    
                    $getAllExempted['landEduc'] += $value['AV'];
                    $getAllExempted['landEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'L'){
                    
                    $getAllExempted['landOthe'] += $value['AV'];
                    $getAllExempted['landOtheParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'B' ){
                                
                    $getAllExempted['bldgGovt'] += $value['AV'];
                    $getAllExempted['bldgGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'B'){
                            
                    $getAllExempted['bldgReli'] += $value['AV'];
                    $getAllExempted['bldgReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'B'){
                    
                    $getAllExempted['bldgChar'] += $value['AV'];
                    $getAllExempted['bldgCharParcel'] += 1;
                        
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'B'){
                        
                    $getAllExempted['bldgEduc'] += $value['AV'];
                    $getAllExempted['bldgEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'B'){
                        
                    $getAllExempted['bldgOthe'] += $value['AV'];
                    $getAllExempted['bldgOtheParcel'] += 1;
                        
                }

                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'M' ){
                                
                    $getAllExempted['machGovt'] += $value['AV'];
                    $getAllExempted['machGovtParcel'] += 1;
                }
    
                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'M'){
                            
                    $getAllExempted['machReli'] += $value['AV'];
                    $getAllExempted['machReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'M'){
                    
                    $getAllExempted['machChar'] += $value['AV'];
                    $getAllExempted['machCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'M'){
                    
                    $getAllExempted['machEduc'] += $value['AV'];
                    $getAllExempted['machEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'M'){
                    
                    $getAllExempted['machOthe'] += $value['AV'];
                    $getAllExempted['machOtheParcel'] += 1;
                    
                }
                                 
            }

        }

         #-------------------------------------------------------------------------------------------------#
        

        return self::createExcel( $getAllTaxable, $getAllExempted, $month, 
            $year, $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate);

    }

    private function createExcel($allTaxable, $allExempted, $month, 
            $year, $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate){
        
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $nameMonth = date("F", mktime(0, 0, 0, $month, 10));
        $startMonth = date("F", mktime(0, 0, 0, $firstMonth, 10));
        $endMonth = date("F", mktime(0, 0, 0, $endMonth, 10));
        
        if ($datePick == 'asOfToday'){
            $title = "Assessed Value Report";
            
            $asOfDate = "Date as of: " .$dateCreated;
            
        }
        
        if ($datePick == 'Daily'){
            $title = "Daily Assessed Value Report";
            
            $asOfDate = "Date as of: " .$dateCreated;
            
        }
        if ($datePick == 'Weekly'){
            $title = "Weekly Assessed Value Report";
            
            $asOfDate = "Date as of: " .Carbon::parse($dateCreated)->startOfWeek()->format('Y-m-d'). " to " 
                .Carbon::parse($dateCreated)->endOfWeek()->format('Y-m-d');
        }
        if ($datePick == 'Monthly'){
            $title = "Monthly Assessed Value Report";
           
            $asOfDate = "Date as of: " .$nameMonth ." " .$year;
        }
        if ($datePick == 'Quarterly'){
            $title = $quarter." Quarter Assessed Value Report";
           
            $asOfDate = $startMonth. " to " .$endMonth .", " .$year;
        }

        if ($datePick == 'Yearly'){
            $title = $year." Year Assessed Value Report";
            
            $asOfDate = "Year " .$year;
        }

        if ($datePick == 'Range'){
            $title = "Range Assessed Value Report";
           
            $asOfDate = "From " .$rangeDate[0] ." to " .$rangeDate[1];
        }

                
        // Set value binder
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder() );

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        // $worksheet->setTitle($title);
        $spreadsheet->getProperties()
                ->setTitle($title);
        $spreadsheet->getSheetByName($title);
        $worksheet = $spreadsheet->getActiveSheet();

        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates('O1')
                ->setOffsetX(50)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates('P1')
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
        //REPUBLIC OF THE PHILIPPINES
        $worksheet->mergeCells('A1:R1');
        $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
        // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
        $worksheet->getStyle("A1")->getFont()->setSize(12);
        $worksheet->getStyle('A1:R1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A1:R1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //CITY OF MANILA
        $worksheet->mergeCells('A2:R2');
        $worksheet->setCellValue("A2", "CITY OF MANILA");
        $worksheet->getStyle("A2:R2")->getFont()->setBold(true);
        $worksheet->getStyle("A2")->getFont()->setSize(14);
        $worksheet->getStyle('A2:R2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A2:R2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //DEPARTMENT OF ASSESSMENT
        $worksheet->mergeCells('A3:R3');
        $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
        $worksheet->getStyle("A3:R3")->getFont()->setBold(true);
        $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
        $worksheet->getStyle('A3:R3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A3:R3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //*Title of Report
        $worksheet->mergeCells('A5:R6');
        $worksheet->setCellValue("A5", $title);
        $worksheet->getStyle("A5:R6")->getFont()->setBold(true);
        $worksheet->getStyle("A5")->getFont()->setSize(18);
        $worksheet->getStyle('A5:R6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A5:R6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //*Date of Report
        $worksheet->mergeCells('A7:R7');
        $worksheet->getStyle('A7:R7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("A7", $asOfDate);
        $worksheet->getStyle("A7")->getFont()->setSize(12)->setBold(true);
        // $worksheet->setCellValue("B4", $excelPrintText);
        // $worksheet->getStyle("B4")->getFont()->setSize(8)->setItalic(true);
        $worksheet->mergeCells('A8:R8');
        $worksheet->getStyle('A8:R8')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $worksheet->setCellValue("A8", "printed date:  ". date("F j, Y"));
        $worksheet->getStyle("A8")->getFont()->setSize(8);

        //---------------------setup width
        $worksheet->getColumnDimension('A')->setWidth(24);
        $worksheet->getColumnDimension('B')->setWidth(22);
        $worksheet->getColumnDimension('C')->setWidth(18);
        $worksheet->getColumnDimension('D')->setWidth(18);
        $worksheet->getColumnDimension('E')->setWidth(18);
        $worksheet->getColumnDimension('F')->setWidth(22);
        $worksheet->getColumnDimension('G')->setWidth(28);
        $worksheet->getColumnDimension('H')->setWidth(8);
        $worksheet->getColumnDimension('I')->setWidth(10);
        $worksheet->getColumnDimension('J')->setWidth(12);
        $worksheet->getColumnDimension('K')->setWidth(14);
        $worksheet->getColumnDimension('L')->setWidth(21);
        $worksheet->getColumnDimension('M')->setWidth(10);
        $worksheet->getColumnDimension('N')->setWidth(8);
        $worksheet->getColumnDimension('O')->setWidth(20);
        $worksheet->getColumnDimension('P')->setWidth(16);
        $worksheet->getColumnDimension('Q')->setWidth(10);
        $worksheet->getColumnDimension('R')->setWidth(16);


        //-----------------------setup merge cell, style and text for header

        // $worksheet->mergeCells('A9:B9')
        //         ->setCellValue("A9", "ACTIVE ASSESSED VALUE AS OF " .$asOfDate);
        $worksheet->mergeCells('C9:G9')
                ->setCellValue("C9", "ASSESSED VALUE");
        $worksheet->mergeCells('I9:M9')
                ->setCellValue("I9", "COUNT");
        $worksheet->mergeCells('O9:R9')
                ->setCellValue("O9", "TARGET BASIC TAX + SEF COLLECTION");

        $worksheet->setCellValue('C10', "LAND");
        $worksheet->setCellValue('D10', "BUILDING");
        $worksheet->setCellValue('E10', "MACHINERY");
        $worksheet->setCellValue('F10', "TOTAL IMPROVEMENT");
        $worksheet->setCellValue('G10', "TOTAL");
        
        $worksheet->setCellValue('I10', "LAND");
        $worksheet->setCellValue('J10', "BUILDING");
        $worksheet->setCellValue('K10', "MACHINERY");
        $worksheet->setCellValue('L10', "TOTAL IMPROVEMENT");
        $worksheet->setCellValue('M10', "TOTAL");

        $worksheet->setCellValue('C30', "LAND");
        $worksheet->setCellValue('D30', "BUILDING");
        $worksheet->setCellValue('E30', "MACHINERY");
        $worksheet->setCellValue('F30', "TOTAL IMPROVEMENT");
        $worksheet->setCellValue('G30', "TOTAL");
        
        $worksheet->setCellValue('I30', "LAND");
        $worksheet->setCellValue('J30', "BUILDING");
        $worksheet->setCellValue('K30', "MACHINERY");
        $worksheet->setCellValue('L30', "TOTAL IMPROVEMENT");
        $worksheet->setCellValue('M30', "TOTAL");
        
        $worksheet->setCellValue('O10', "TAX RATE");
        $worksheet->setCellValue('P10', "TAX DUE");
        $worksheet->setCellValue('Q10', "SEF RATE");
        $worksheet->setCellValue('R10', "SEF DUE");

        $worksheet->mergeCells('C29:G29')
                ->setCellValue("C29", "ASSESSED VALUE");
        $worksheet->mergeCells('I29:M29')
                ->setCellValue("I29", "COUNT");
        
        $worksheet->mergeCells('O29:R29')
                ->setCellValue("O29", "LESS DISCOUNTS");

        $worksheet->mergeCells('O32:R32')
                ->setCellValue("O32", "TOTAL ESTIMATED TAX");

        $worksheet->mergeCells('O35:R35')
                ->setCellValue("O35", "RECAP.");

        $worksheet->setCellValue("O36", "Gen. Fund (70%)");
        $worksheet->setCellValue("O37", "Trust Fund (30%)");
        $worksheet->setCellValue("O38", "SEF Fund");

        $worksheet->setCellValue("O40", "TOTAL ALL FUNDS");
        
        
        $styleHeader = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
            'font' => [
                'bold' => true,
            ],

        ];
        $styleBoldFont = [
            'font' => [
                'bold' => true,
            ],
        ];
        
        $worksheet->getStyle('A9:G10')->applyFromArray($styleHeader);
        $worksheet->getStyle('I9:M10')->applyFromArray($styleHeader);
        $worksheet->getStyle('O9:R10')->applyFromArray($styleHeader);
        $worksheet->getStyle('C29:G29')->applyFromArray($styleHeader);
        $worksheet->getStyle('I29:M29')->applyFromArray($styleHeader);
        
        $worksheet->getStyle('A11')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('A27:M27')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('A29')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('A43:M43')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('O29')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('O32')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('O35:O40')->applyFromArray($styleBoldFont);
        $worksheet->getStyle('R40')->applyFromArray($styleBoldFont);
        

        //-----------------------constant text, style for column A
        $worksheet->setCellValue('A11', "A. TAXABLE");
        $worksheet->setCellValue('A12', "1. RESIDENTIAL");
        $worksheet->setCellValue('A13', "2. COMMERCIAL");
        $worksheet->setCellValue('A14', "3. INDUSTRIAL");
        $worksheet->setCellValue('A15', "4. MACHINERY");
        $worksheet->setCellValue('B16', " a. Residential");
        $worksheet->setCellValue('B17', " b. Commercial");
        $worksheet->setCellValue('B18', " c. Special");
        $worksheet->setCellValue('A19', "5. SPECIAL");
        $worksheet->setCellValue('B20', " a. Recreational");
        $worksheet->setCellValue('B21', " b. Hospital");
        $worksheet->setCellValue('B22', " c. Cultural");
        $worksheet->setCellValue('B23', " d. Special");
        $worksheet->setCellValue('B24', " e. Scientific");
        $worksheet->setCellValue('B25', " f. Local Water District");
        $worksheet->setCellValue('B26', " g. Others");

        $worksheet->setCellValue("A27", "GRAND TOTAL (TAXABLE)");
        
        $worksheet->setCellValue('A31', "B. EXEMPT");
        $worksheet->setCellValue('A32', "1. GOVERNMENT");
        $worksheet->setCellValue('A33', "2. RELIGIOUS");
        $worksheet->setCellValue('A34', "3. CHARITABLE");
        $worksheet->setCellValue('A35', "4. EDUCATIONAL");
        $worksheet->setCellValue('A36', "5. OTHER LAWS");
        $worksheet->setCellValue('A37', "6. MACHINERY");
        $worksheet->setCellValue('B38', "a. GOVERNMENT");
        $worksheet->setCellValue('B39', "2. RELIGIOUS");
        $worksheet->setCellValue('B40', "3. CHARITABLE");
        $worksheet->setCellValue('B41', "4. EDUCATIONAL");
        $worksheet->setCellValue('B42', "5. OTHER LAWS");
        
        $worksheet->setCellValue("A43", "GRAND TOTAL (EXEMPTED)");


        //STYLE DATA
        $styleBordersData = [
            
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
        ];

        $styleBorderOutline = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ]
        ];

        $styleColumnA12 = [
            'font' => [
                'bold' => true,
            ],
            
        ];
        $styleColumnA27 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $styleColumnA28A31 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];


        $worksheet->getStyle('A11:G27')->applyFromArray($styleBordersData);
        $worksheet->getStyle('I11:M27')->applyFromArray($styleBordersData);
        $worksheet->getStyle('A29:G43')->applyFromArray($styleBordersData);
        $worksheet->getStyle('I29:M43')->applyFromArray($styleBordersData);
        $worksheet->getStyle('O11:R27')->applyFromArray($styleBordersData);
        $worksheet->getStyle('O35:R38')->applyFromArray($styleBordersData);
       
        //-----------------------constant text, style for data cell

#------------------taxable content------------------------------

        //-------AllTaxable TABLES----------------

        $worksheet->setCellValue('C12', $allTaxable['landResi']);
        $worksheet->setCellValue('C13', $allTaxable['landComm']);
        $worksheet->setCellValue('C14', $allTaxable['landIndu']);
       
        $worksheet->setCellValue('C20', $allTaxable['specRecreationalLand']);
        $worksheet->setCellValue('C21', $allTaxable['specHospitalLand']);
        $worksheet->setCellValue('C22', $allTaxable['specCulturalLand']);
        $worksheet->setCellValue('C23', $allTaxable['specSpecialLand']);
        $worksheet->setCellValue('C24', $allTaxable['specScientificLand']);
        $worksheet->setCellValue('C25', $allTaxable['specLocWaterDistLand']);
        $worksheet->setCellValue('C26', $allTaxable['specOthersLand']);
        
        $worksheet->setCellValue('C27', '=SUM(C12:C26)')
                ->getStyle('C27')->getFont()->setBold(true);
        
        $worksheet->setCellValue('D12', $allTaxable['bldgResi']);
        $worksheet->setCellValue('D13', $allTaxable['bldgComm']);
        $worksheet->setCellValue('D14', $allTaxable['bldgIndu']);
        
        $worksheet->setCellValue('D20', $allTaxable['specRecreationalBldg']);
        $worksheet->setCellValue('D21', $allTaxable['specHospitalBldg']);
        $worksheet->setCellValue('D22', $allTaxable['specCulturalBldg']);
        $worksheet->setCellValue('D23', $allTaxable['specSpecialBldg']);
        $worksheet->setCellValue('D24', $allTaxable['specScientificBldg']);
        $worksheet->setCellValue('D25', $allTaxable['specLocWaterDistBldg']);
        $worksheet->setCellValue('D26', $allTaxable['specOthersBldg']);

        $worksheet->setCellValue('D27', '=SUM(D12:D26)')
            ->getStyle('D27')->getFont()->setBold(true);
        
        $worksheet->setCellValue('E16', $allTaxable['machResi']);
        $worksheet->setCellValue('E17', $allTaxable['machComm']);
        $worksheet->setCellValue('E18', $allTaxable['machSpec']);
        
        $worksheet->setCellValue('E20', $allTaxable['specRecreationalMach']);
        $worksheet->setCellValue('E21', $allTaxable['specHospitalMach']);
        $worksheet->setCellValue('E22', $allTaxable['specCulturalMach']);
        $worksheet->setCellValue('E23', $allTaxable['specSpecialMach']);
        $worksheet->setCellValue('E24', $allTaxable['specScientificMach']);
        $worksheet->setCellValue('E25', $allTaxable['specLocWaterDistMach']);
        $worksheet->setCellValue('E26', $allTaxable['specOthersMach']);

        $worksheet->setCellValue('E27', '=SUM(E12:E26)')
            ->getStyle('E27')->getFont()->setBold(true);
        
        $worksheet->setCellValue('F12', '=D12+E12');
        $worksheet->setCellValue('F13', '=D13+E13');
        $worksheet->setCellValue('F14', '=D14+E14');
        $worksheet->setCellValue('F16', '=D16+E16');
        $worksheet->setCellValue('F17', '=D17+E17');
        $worksheet->setCellValue('F18', '=D18+E18');
        
        $worksheet->setCellValue('F20', '=D20+E20');
        $worksheet->setCellValue('F21', '=D21+E21');
        $worksheet->setCellValue('F22', '=D22+E22');
        $worksheet->setCellValue('F23', '=D23+E23');
        $worksheet->setCellValue('F24', '=D24+E24');
        $worksheet->setCellValue('F25', '=D25+E25');
        $worksheet->setCellValue('F26', '=D26+E26');

        $worksheet->setCellValue('F27', '=SUM(F12:F26)')
            ->getStyle('F27')->getFont()->setBold(true);

        $worksheet->setCellValue('G12', '=C12+F12');
        $worksheet->setCellValue('G13', '=C13+F13');
        $worksheet->setCellValue('G14', '=C14+F14');
        $worksheet->setCellValue('G16', '=C16+F16');
        $worksheet->setCellValue('G17', '=C17+F17');
        $worksheet->setCellValue('G18', '=C18+F18');
        
        $worksheet->setCellValue('G20', '=C20+F20');
        $worksheet->setCellValue('G21', '=C21+F21');
        $worksheet->setCellValue('G22', '=C22+F22');
        $worksheet->setCellValue('G23', '=C23+F23');
        $worksheet->setCellValue('G24', '=C24+F24');
        $worksheet->setCellValue('G25', '=C25+F25');
        $worksheet->setCellValue('G26', '=C26+F26');

        $worksheet->setCellValue('G27', '=SUM(G12:G26)')
            ->getStyle('G27')->getFont()->setBold(true);

        $worksheet->setCellValue('I12', $allTaxable['landResiParcel']);
        $worksheet->setCellValue('I13', $allTaxable['landCommParcel']);
        $worksheet->setCellValue('I14', $allTaxable['landInduParcel']);
        
        $worksheet->setCellValue('I20', $allTaxable['specRecreationalLandParcel']);
        $worksheet->setCellValue('I21', $allTaxable['specHospitalLandParcel']);
        $worksheet->setCellValue('I22', $allTaxable['specCulturalLandParcel']);
        $worksheet->setCellValue('I23', $allTaxable['specSpecialLandParcel']);
        $worksheet->setCellValue('I24', $allTaxable['specScientificLandParcel']);
        $worksheet->setCellValue('I25', $allTaxable['specLocWaterDistLandParcel']);
        $worksheet->setCellValue('I26', $allTaxable['specOthersLandParcel']);
        
        $worksheet->setCellValue('I27', '=SUM(I12:I26)')
                ->getStyle('I27')->getFont()->setBold(true);

        $worksheet->setCellValue('J12', $allTaxable['bldgResiParcel']);
        $worksheet->setCellValue('J13', $allTaxable['bldgCommParcel']);
        $worksheet->setCellValue('J14', $allTaxable['bldgInduParcel']);
        
        $worksheet->setCellValue('J20', $allTaxable['specRecreationalBldgParcel']);
        $worksheet->setCellValue('J21', $allTaxable['specHospitalBldgParcel']);
        $worksheet->setCellValue('J22', $allTaxable['specCulturalBldgParcel']);
        $worksheet->setCellValue('J23', $allTaxable['specSpecialBldgParcel']);
        $worksheet->setCellValue('J24', $allTaxable['specScientificBldgParcel']);
        $worksheet->setCellValue('J25', $allTaxable['specLocWaterDistBldgParcel']);
        $worksheet->setCellValue('J26', $allTaxable['specOthersBldgParcel']);
        
        $worksheet->setCellValue('J27', '=SUM(J12:J26)')
                ->getStyle('J27')->getFont()->setBold(true);

        $worksheet->setCellValue('K16', $allTaxable['machResiParcel']);
        $worksheet->setCellValue('K17', $allTaxable['machCommParcel']);
        $worksheet->setCellValue('K18', $allTaxable['machSpecParcel']);
        
        $worksheet->setCellValue('K20', $allTaxable['specRecreationalMachParcel']);
        $worksheet->setCellValue('K21', $allTaxable['specHospitalMachParcel']);
        $worksheet->setCellValue('K22', $allTaxable['specCulturalMachParcel']);
        $worksheet->setCellValue('K23', $allTaxable['specSpecialMachParcel']);
        $worksheet->setCellValue('K24', $allTaxable['specScientificMachParcel']);
        $worksheet->setCellValue('K25', $allTaxable['specLocWaterDistMachParcel']);
        $worksheet->setCellValue('K26', $allTaxable['specOthersMachParcel']);
        
        $worksheet->setCellValue('K27', '=SUM(K12:K26)')
                ->getStyle('K27')->getFont()->setBold(true);

        $worksheet->setCellValue('L12', '=J12+K12');
        $worksheet->setCellValue('L13', '=J13+K13');
        $worksheet->setCellValue('L14', '=J14+K14');
        $worksheet->setCellValue('L16', '=J16+K16');
        $worksheet->setCellValue('L17', '=J17+K17');
        $worksheet->setCellValue('L18', '=J18+K18');
        
        $worksheet->setCellValue('L20', '=J20+K20');
        $worksheet->setCellValue('L21', '=J21+K21');
        $worksheet->setCellValue('L22', '=J22+K22');
        $worksheet->setCellValue('L23', '=J23+K23');
        $worksheet->setCellValue('L24', '=J24+K24');
        $worksheet->setCellValue('L25', '=J25+K25');
        $worksheet->setCellValue('L26', '=J26+K26');

        $worksheet->setCellValue('L27', '=SUM(L12:L26)')
            ->getStyle('L27')->getFont()->setBold(true);

        $worksheet->setCellValue('M12', '=I12+L12');
        $worksheet->setCellValue('M13', '=I13+L13');
        $worksheet->setCellValue('M14', '=I14+L14');
        $worksheet->setCellValue('M16', '=I16+L16');
        $worksheet->setCellValue('M17', '=I17+L17');
        $worksheet->setCellValue('M18', '=I18+L18');
        
        $worksheet->setCellValue('M20', '=I20+L20');
        $worksheet->setCellValue('M21', '=I21+L21');
        $worksheet->setCellValue('M22', '=I22+L22');
        $worksheet->setCellValue('M23', '=I23+L23');
        $worksheet->setCellValue('M24', '=I24+L24');
        $worksheet->setCellValue('M25', '=I25+L25');
        $worksheet->setCellValue('M26', '=I26+L26');

        $worksheet->setCellValue('M27', '=SUM(M12:M26)')
            ->getStyle('M27')->getFont()->setBold(true);

        
#---------------------------------------------------------------------------------------------------------------

#----------------------------------exempted content--------------------------------------------

    //-------EXEMPTED TABLES----------------
    $worksheet->setCellValue('C32', $allExempted['landGovt']);
    $worksheet->setCellValue('C33', $allExempted['landReli']);
    $worksheet->setCellValue('C34', $allExempted['landChar']);
    $worksheet->setCellValue('C35', $allExempted['landEduc']);
    $worksheet->setCellValue('C36', $allExempted['landOthe']);
    
    $worksheet->setCellValue('C43', '=SUM(C32:D36)')
            ->getStyle('C43')->getFont()->setBold(true);

    $worksheet->setCellValue('D32', $allExempted['bldgGovt']);
    $worksheet->setCellValue('D33', $allExempted['bldgReli']);
    $worksheet->setCellValue('D34', $allExempted['bldgChar']);
    $worksheet->setCellValue('D35', $allExempted['bldgEduc']);
    $worksheet->setCellValue('D36', $allExempted['bldgOthe']);
    
    $worksheet->setCellValue('D43', '=SUM(D32:D36)')
            ->getStyle('D43')->getFont()->setBold(true);

    $worksheet->setCellValue('E38', $allExempted['machGovt']);
    $worksheet->setCellValue('E39', $allExempted['machReli']);
    $worksheet->setCellValue('E40', $allExempted['machChar']);
    $worksheet->setCellValue('E41', $allExempted['machEduc']);
    $worksheet->setCellValue('E42', $allExempted['machOthe']);
    
    $worksheet->setCellValue('E43', '=SUM(E39:E42)')
            ->getStyle('E43')->getFont()->setBold(true);

    $worksheet->setCellValue('F32', '=SUM(D32:E32)');
    $worksheet->setCellValue('F33', '=SUM(D33:E33)');
    $worksheet->setCellValue('F34', '=SUM(D34:E34)');
    $worksheet->setCellValue('F35', '=SUM(D35:E35)');
    $worksheet->setCellValue('F36', '=SUM(D36:E36)');
    
    $worksheet->setCellValue('F38', '=SUM(D38:E38)');
    $worksheet->setCellValue('F39', '=SUM(D39:E39)');
    $worksheet->setCellValue('F40', '=SUM(D40:E40)');
    $worksheet->setCellValue('F41', '=SUM(D41:E41)');
    $worksheet->setCellValue('F42', '=SUM(D42:E42)');

    $worksheet->setCellValue('F43', '=SUM(F32:F42)')
            ->getStyle('F43')->getFont()->setBold(true);

    $worksheet->setCellValue('G32', '=SUM(C32:E32)');
    $worksheet->setCellValue('G33', '=SUM(C33:E33)');
    $worksheet->setCellValue('G34', '=SUM(C34:E34)');
    $worksheet->setCellValue('G35', '=SUM(C35:E35)');
    $worksheet->setCellValue('G36', '=SUM(C36:E36)');
    
    $worksheet->setCellValue('G38', '=SUM(C38:E38)');
    $worksheet->setCellValue('G39', '=SUM(C39:E39)');
    $worksheet->setCellValue('G40', '=SUM(C40:E40)');
    $worksheet->setCellValue('G41', '=SUM(C41:E41)');
    $worksheet->setCellValue('G42', '=SUM(C42:E42)');

    $worksheet->setCellValue('G43', '=SUM(G32:G42)')
            ->getStyle('G43')->getFont()->setBold(true);

    $worksheet->setCellValue('I32', $allExempted['landGovtParcel']);
    $worksheet->setCellValue('I33', $allExempted['landReliParcel']);
    $worksheet->setCellValue('I34', $allExempted['landCharParcel']);
    $worksheet->setCellValue('I35', $allExempted['landEducParcel']);
    $worksheet->setCellValue('I36', $allExempted['landOtheParcel']);
    
    $worksheet->setCellValue('I43', '=SUM(I32:I36)')
            ->getStyle('I43')->getFont()->setBold(true);

    $worksheet->setCellValue('J32', $allExempted['bldgGovtParcel']);
    $worksheet->setCellValue('J33', $allExempted['bldgReliParcel']);
    $worksheet->setCellValue('J34', $allExempted['bldgCharParcel']);
    $worksheet->setCellValue('J35', $allExempted['bldgEducParcel']);
    $worksheet->setCellValue('J36', $allExempted['bldgOtheParcel']);
    
    $worksheet->setCellValue('J43', '=SUM(J32:J36)')
            ->getStyle('J43')->getFont()->setBold(true);

    $worksheet->setCellValue('K38', $allExempted['machGovtParcel']);
    $worksheet->setCellValue('K39', $allExempted['machReliParcel']);
    $worksheet->setCellValue('K40', $allExempted['machCharParcel']);
    $worksheet->setCellValue('K41', $allExempted['machEducParcel']);
    $worksheet->setCellValue('K42', $allExempted['machOtheParcel']);
    
    $worksheet->setCellValue('K43', '=SUM(K38:K42)')
            ->getStyle('K43')->getFont()->setBold(true);

    $worksheet->setCellValue('L32', '=SUM(J32:K32)');
    $worksheet->setCellValue('L33', '=SUM(J33:K33)');
    $worksheet->setCellValue('L34', '=SUM(J34:K34)');
    $worksheet->setCellValue('L35', '=SUM(J35:K35)');
    $worksheet->setCellValue('L36', '=SUM(J36:K36)');
    
    $worksheet->setCellValue('L38', '=SUM(J38:K38)');
    $worksheet->setCellValue('L39', '=SUM(J39:K39)');
    $worksheet->setCellValue('L40', '=SUM(J40:K40)');
    $worksheet->setCellValue('L41', '=SUM(J41:K41)');
    $worksheet->setCellValue('L42', '=SUM(J42:K42)');

    $worksheet->setCellValue('L43', '=SUM(L32:L42)')
            ->getStyle('L43')->getFont()->setBold(true);
    
    $worksheet->setCellValue('M32', '=SUM(I32:K32)');
    $worksheet->setCellValue('M33', '=SUM(I33:K33)');
    $worksheet->setCellValue('M34', '=SUM(I34:K34)');
    $worksheet->setCellValue('M35', '=SUM(I35:K35)');
    $worksheet->setCellValue('M36', '=SUM(I36:K36)');
   
    $worksheet->setCellValue('M38', '=SUM(I38:K38)');
    $worksheet->setCellValue('M39', '=SUM(I39:K39)');
    $worksheet->setCellValue('M40', '=SUM(I40:K40)');
    $worksheet->setCellValue('M41', '=SUM(I41:K41)');
    $worksheet->setCellValue('M42', '=SUM(I42:K42)');

    $worksheet->setCellValue('M43', '=SUM(M32:M42)')
            ->getStyle('M43')->getFont()->setBold(true);


    //-------TARGET BASIC TAX COLLECTION----------------
    $worksheet->setCellValue('O12', '1.50%');
    $worksheet->setCellValue('P12', '=G12*O12');
    $worksheet->setCellValue('Q12', '1%');
    $worksheet->setCellValue('R12', '=G12*Q12');

    $worksheet->setCellValue('O13', '2%');
    $worksheet->setCellValue('P13', '=G13*O13');
    $worksheet->setCellValue('Q13', '1%');
    $worksheet->setCellValue('R13', '=G13*Q13');

    $worksheet->setCellValue('O14', '2%');
    $worksheet->setCellValue('P14', '=G14*O14');
    $worksheet->setCellValue('Q14', '1%');
    $worksheet->setCellValue('R14', '=G14*Q14');

    $worksheet->setCellValue('O16', '1.50%');
    $worksheet->setCellValue('P16', '=G16*O16');
    $worksheet->setCellValue('Q16', '1%');
    $worksheet->setCellValue('R16', '=G16*Q16');

    $worksheet->setCellValue('O17', '2%');
    $worksheet->setCellValue('P17', '=G17*O17');
    $worksheet->setCellValue('Q17', '1%');
    $worksheet->setCellValue('R17', '=G17*Q17');

    $worksheet->setCellValue('O18', '2%');
    $worksheet->setCellValue('P18', '=G18*O18');
    $worksheet->setCellValue('Q18', '1%');
    $worksheet->setCellValue('R18', '=G18*Q18');

    $worksheet->setCellValue('O20', '2%');
    $worksheet->setCellValue('P20', '=G20*O20');
    $worksheet->setCellValue('Q20', '1%');
    $worksheet->setCellValue('R20', '=G20*Q20');

    $worksheet->setCellValue('O21', '2%');
    $worksheet->setCellValue('P21', '=G21*O21');
    $worksheet->setCellValue('Q21', '1%');
    $worksheet->setCellValue('R21', '=G21*Q21');

    $worksheet->setCellValue('O22', '2%');
    $worksheet->setCellValue('P22', '=G22*O22');
    $worksheet->setCellValue('Q22', '1%');
    $worksheet->setCellValue('R22', '=G22*Q22');

    $worksheet->setCellValue('O23', '2%');
    $worksheet->setCellValue('P23', '=G23*O23');
    $worksheet->setCellValue('Q23', '1%');
    $worksheet->setCellValue('R23', '=G23*Q23');

    $worksheet->setCellValue('O24', '2%');
    $worksheet->setCellValue('P24', '=G24*O24');
    $worksheet->setCellValue('Q24', '1%');
    $worksheet->setCellValue('R24', '=G24*Q24');

    $worksheet->setCellValue('O25', '2%');
    $worksheet->setCellValue('P25', '=G25*O25');
    $worksheet->setCellValue('Q25', '1%');
    $worksheet->setCellValue('R25', '=G25*Q25');

    $worksheet->setCellValue('O26', '2%');
    $worksheet->setCellValue('P26', '=G26*O26');
    $worksheet->setCellValue('Q26', '1%');
    $worksheet->setCellValue('R26', '=G26*Q26');

    $worksheet->setCellValue('P27', '=SUM(P12:P26)');
    $worksheet->setCellValue('R27', '=SUM(R12:R26)');

    $worksheet->setCellValue('O30', '15%');
    $worksheet->setCellValue('P30', '=O30*P27');
    $worksheet->setCellValue('Q30', '15%');
    $worksheet->setCellValue('R30', '=Q30*R27');

    $worksheet->setCellValue('P33', '=P27-P30');
    $worksheet->setCellValue('R33', '=R27-R30');

    $worksheet->setCellValue('P36', '=P33*.7');
    $worksheet->setCellValue('P37', '=P33-P36');

    $worksheet->setCellValue('R38', '=R33');
    $worksheet->setCellValue('R40', '=P36+P37+R38');




    

#--------------------------------------------------------------------------------------

        //more style
        $worksheet->getStyle('C12:G27')->getNumberFormat()->setFormatCode('#,###,###,###');
        $worksheet->getStyle('I12:M26')->getNumberFormat()->setFormatCode('#,###,###,###');
        
        $worksheet->getStyle('C32:G43')->getNumberFormat()->setFormatCode('#,###,###,###');
        $worksheet->getStyle('I32:M43')->getNumberFormat()->setFormatCode('#,###,###,###');

        $worksheet->getStyle('P12:P37')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        
        $worksheet->getStyle('R12:R40')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
       
        


        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;
    }
}
