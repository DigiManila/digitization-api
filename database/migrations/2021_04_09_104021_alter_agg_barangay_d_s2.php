<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAggBarangayDS2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agg_barangay_d_s', function (Blueprint $table) {
            $table->integer('L-RESI')->after('PreviousAssessValue')->default(0);
            $table->integer('L-COMM')->after('L-RESI')->default(0);
            $table->bigInteger('L-CHAR')->after('L-COMM')->default(0);
            $table->bigInteger('L-EDUC')->after('L-CHAR')->default(0);
            $table->bigInteger('L-GOVT')->after('L-EDUC')->default(0);
            $table->bigInteger('L-INDU')->after('L-GOVT')->default(0);
            $table->bigInteger('L-RELI')->after('L-INDU')->default(0);
            $table->bigInteger('L-SPC')->after('L-RELI')->default(0);
            $table->integer('B-RESI')->after('L-SPC')->default(0);
            $table->integer('B-COMM')->after('B-RESI')->default(0);
            $table->bigInteger('B-CHAR')->after('B-COMM')->default(0);
            $table->bigInteger('B-EDUC')->after('B-CHAR')->default(0);
            $table->bigInteger('B-GOVT')->after('B-EDUC')->default(0);
            $table->bigInteger('B-INDU')->after('B-GOVT')->default(0);
            $table->bigInteger('B-RELI')->after('B-INDU')->default(0);
            $table->bigInteger('B-SPC')->after('B-RELI')->default(0);
            $table->integer('M-RESI')->after('B-SPC')->default(0);
            $table->integer('M-COMM')->after('M-RESI')->default(0);
            $table->bigInteger('M-CHAR')->after('M-COMM')->default(0);
            $table->bigInteger('M-EDUC')->after('M-CHAR')->default(0);
            $table->bigInteger('M-GOVT')->after('M-EDUC')->default(0);
            $table->bigInteger('M-INDU')->after('M-GOVT')->default(0);
            $table->bigInteger('M-RELI')->after('M-INDU')->default(0);
            $table->bigInteger('M-SPC')->after('B-RELI')->default(0);
          
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agg_barangay_d_s', function (Blueprint $table) {
            //
        });
    }
}
