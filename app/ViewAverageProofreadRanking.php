<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAverageProofreadRanking extends Model
{
    use HasFactory;

    protected $table = 'average_proofread_ranking';
}
