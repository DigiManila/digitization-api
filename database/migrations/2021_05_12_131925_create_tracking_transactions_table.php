<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_transactions', function (Blueprint $table) {
            $table->id();
            $table->mediumInteger("file_no");
            $table->datetime("transaction_datetime");
            $table->string("or");
            $table->string("owner_name");
            $table->string("zone");
            $table->string("brgy");
            $table->string("pin");
            $table->string("appraiser");
            $table->integer("receiving_id");
            $table->string("contact_person");
            $table->string("contact_number");
            $table->string("contact_email")->nullable();
            $table->string("transaction_type");
            $table->string("kind");
            $table->string("documents");
            $table->tinyInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_transactions');
    }
}
