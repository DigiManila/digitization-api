<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewDiffCount96 extends Model
{
    use HasFactory;

    protected $table = 'diff_count_96';
}
