<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTrackingNormalFlowTransTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracking_normal_flow_trans_types', function (Blueprint $table) {
            $table->renameColumn("trans_id", "tracking_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracking_normal_flow_trans_types', function (Blueprint $table) {
            //
        });
    }
}
