<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\MapPolygon;
use App\ActiveAssessmentRoll;
use App\ActiveTaxdecAssmnt;

class TaxMapKmlController extends Controller
{

    public function createKml(Request $request){

        $active = self::formatActivePolygon($request['active'], $request['activePolygon']);
        $array_merge = array_merge($request['allBarangay'], array($active));

        $xmlstr = <<<XML
            <?xml version="1.0" encoding="UTF-8"?>
            <kml xmlns="http://www.opengis.net/kml/2.2">
            <Document>
                <name>Manila Sales Data KML</name>
                <description/>
                <Style id="poly-000000-1200-77-normal">
                <LineStyle>
                    <color>ff0051e6</color>
                    <width>2.301</width>
                </LineStyle>
                <PolyStyle>
                    <color>4d0051e6</color>
                    <fill>1</fill>
                    <outline>1</outline>
                </PolyStyle>
                </Style>
                <Style id="poly-000000-1200-77-highlight">
                <LineStyle>
                    <color>ff0051e6</color>
                    <width>3.4515</width>
                </LineStyle>
                <PolyStyle>
                    <color>4d0051e6</color>
                    <fill>1</fill>
                    <outline>1</outline>
                </PolyStyle>
                </Style>
                <StyleMap id="poly-000000-1200-77">
                <Pair>
                    <key>normal</key>
                    <styleUrl>#poly-000000-1200-77-normal</styleUrl>
                </Pair>
                <Pair>
                    <key>highlight</key>
                    <styleUrl>#poly-000000-1200-77-highlight</styleUrl>
                </Pair>
                </StyleMap>
            XML;

            $bgy = $request['bgy'];

            $xmlstr = $xmlstr . <<<XML
                <Folder>
                    <name>Barangay $bgy</name>
            XML;

                foreach ($array_merge as $k => $v) {

                        $xmlstr = $xmlstr . "<Placemark>";

                        $xmlstr = $xmlstr . "<name>" . $v['pin'] . "</name>";
                        $xmlstr = $xmlstr . "<styleUrl>#poly-000000-1200-77</styleUrl>";
                        $xmlstr = $xmlstr . "<ExtendedData>";
                        $xmlstr = $xmlstr . '<Data name="PIN"><value>' . $v['pin'] . "</value></Data>";                            
                        $xmlstr = $xmlstr . "</ExtendedData>"; 

                        $xmlstr = $xmlstr . "<Polygon><outerBoundaryIs><LinearRing><tessellate>1</tessellate><coordinates>";
                        $xmlstr = $xmlstr . self::formatCoords($v['path']);
                        $xmlstr = $xmlstr . "</coordinates></LinearRing></outerBoundaryIs></Polygon>";

                        $xmlstr = $xmlstr . "</Placemark>";
                    
                }

            $xmlstr = $xmlstr . <<<XML
                </Folder>
            XML;


        $xmlstr = $xmlstr . <<<XML
                </Document>
            </kml>
        XML;

        
        return $xmlstr;
    }

    private function formatActivePolygon($pin, $polygon){

        $polygon_string = "[";

        foreach ($polygon as $key => $value) {
            $polygon_string = $polygon_string . '{ "lat": ' . $value['lat'] . ', "lng": ' . $value['lng'] . ' }, ';
        }

        $polygon_string = substr($polygon_string, 0, -2);
        $polygon_string = $polygon_string . "]";

        return array(
            'pin' => $pin,
            'path' => $polygon_string
        );

    }

    private function formatCoords($data){

        $output = "";
        $decode = json_decode($data, 1);

        foreach ($decode as $key => $value) {
            $output = $output . $value['lng'] . "," . $value['lat'] . ",0 ";
        }

        return trim($output);

    }
    
    public function createExcel(Request $request){

        $output = [];

        $map = MapPolygon::select('pin', 'gmap_polygon')
                    ->where('barangay', $request->bgy)
                    ->get();

        $pin_map = self::extractInvertPin($map);

        $pin_active_q  = ActiveAssessmentRoll::select('ARP','PIN','Owner','OwnerAddress',
                            'Location','BldgLocation','Barangay', 'MuniDistName',
                            'LotNo', 'BlkNo', 'TCTNo', 'CCTNO',
                            'kind', 'BldgStorey', 'BldgTypeDesc',
                            'StrucType','SubClass')
                        ->whereRaw('SUBSTRING(pin,8,3) = ?', $request->bgy)
                        ->orderBy('PIN', 'ASC')
                        ->get()
                        ->toArray();

        // *****
        $pin_active = [];

        foreach ($pin_active_q as $key => $value) {

            $base_pin = substr($value['PIN'], 0, 18);

            if(array_key_exists($base_pin, $pin_map)){
                $pin_active[] = $value;
            }
        }

        
        $pin_active_arp = self::extractArp($pin_active);

        $querytaxdec_assmt_q = ActiveTaxdecAssmnt::select('ARP', 'kind','AUDesc', 'area',
                                'MV','AL', 'AV','Taxability')
                        ->whereIn('ARP', $pin_active_arp)
                        ->orderBy('ARP', 'ASC')
                        ->get()
                        ->toArray();

        // *****
        $querytaxdec_assmt_grp = self::arpGroup($querytaxdec_assmt_q);
        $pin_active_grp = self::pinActiveGroup($pin_active);

        $merge_active = [];

        foreach ($pin_active_grp as $key => $value) {
            foreach ($value as $k => $v) {
                $merge_active[$key][$k] = $v;
                $merge_active[$key][$k]['td_a'] = $querytaxdec_assmt_grp[$v['ARP']];
            }
        }

        return self::outputExcel($merge_active, $request->bgy);

    }

    private function extractInvertPin($data){

        $output = [];

        foreach ($data as $key => $value) {
            $output[$value['pin']] = $key;
        }

        return $output;

    }

    private function extractArp($data){

        $output = [];

        foreach ($data as $key => $value) {
            $output[] = $value['ARP'];
        }

        return $output;

    }

    private function arpGroup($data){
        
        $output = [];

        foreach ($data as $key => $value) {
            $output[$value['ARP']][] = $value;
        }

        return $output;

    }

    private function pinActiveGroup($data){

        $output = [];

        foreach ($data as $key => $value) {

            $base_pin = substr($value['PIN'], 0, 18);
            $output[$base_pin][] = $value;
            
        }

        return $output;

    }

    private function outputExcel($data, $bgy){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->getColumnDimension('A')->setWidth(27);
        $worksheet->getColumnDimension('B')->setWidth(17);
        $worksheet->getColumnDimension('C')->setWidth(65);
        $worksheet->getColumnDimension('D')->setWidth(65);
        $worksheet->getColumnDimension('E')->setWidth(32);
        $worksheet->getColumnDimension('F')->setWidth(37);
        $worksheet->getColumnDimension('G')->setWidth(19);
        $worksheet->getColumnDimension('H')->setWidth(14);

        $worksheet->getColumnDimension('I')->setWidth(14);
        $worksheet->getColumnDimension('J')->setWidth(14);
        $worksheet->getColumnDimension('K')->setWidth(14);
        $worksheet->getColumnDimension('L')->setWidth(16);
        $worksheet->getColumnDimension('M')->setWidth(33);
        $worksheet->getColumnDimension('N')->setWidth(20);
        $worksheet->getColumnDimension('O')->setWidth(10);
        $worksheet->getColumnDimension('P')->setWidth(12);

        $worksheet->getColumnDimension('Q')->setWidth(15);
        $worksheet->getColumnDimension('R')->setWidth(11);
        $worksheet->getColumnDimension('S')->setWidth(20);
        $worksheet->getColumnDimension('T')->setWidth(8);
        $worksheet->getColumnDimension('U')->setWidth(20);
        $worksheet->getColumnDimension('V')->setWidth(10);

        $worksheet->setCellValue("A2", "BARANGAY:");
        $worksheet->setCellValue("B2", $bgy);
        $worksheet->setCellValue("C2", "As of " . date('F j, Y'));

        $worksheet
                ->setCellValue("A4", "PIN")
                ->setCellValue("B4", "ARP")
                ->setCellValue("C4", "Owner")
                ->setCellValue("D4", "Owner's Address")
                ->setCellValue("E4", "Location")
                ->setCellValue("F4", "Bldg Location")
                ->setCellValue("G4", "District")
                ->setCellValue("H4", "Lot No")
                ->setCellValue("I4", "Block No")
                ->setCellValue("J4", "TCT No")
                ->setCellValue("K4", "CCT No")
                ->setCellValue("L4", "Bldg Storey")
                ->setCellValue("M4", "Bldg Type Desc")
                ->setCellValue("N4", "Struc Type")
                ->setCellValue("O4", "SubClass")
                ->setCellValue("P4", "Kind")
                ->setCellValue("Q4", "AU Desc")
                ->setCellValue("R4", "Area")
                ->setCellValue("S4", "Market Value")
                ->setCellValue("T4", "Assess Level")
                ->setCellValue("U4", "Assess Value")
                ->setCellValue("V4", "Taxability");

        $row = 5;
        
        foreach ($data as $key => $val) {
            foreach ($val as $k => $v) {
                $worksheet->setCellValue("A{$row}", trim($v['PIN']));
                $worksheet->setCellValue("B{$row}", trim($v['ARP']));
                $worksheet->setCellValue("C{$row}", trim($v['Owner']));
                $worksheet->setCellValue("D{$row}", trim($v['OwnerAddress']));
                $worksheet->setCellValue("E{$row}", trim($v['Location']));
                $worksheet->setCellValue("F{$row}", trim($v['BldgLocation']));
                $worksheet->setCellValue("G{$row}", trim($v['MuniDistName']));
                $worksheet->setCellValue("H{$row}", trim($v['LotNo']));
                $worksheet->setCellValue("I{$row}", trim($v['BlkNo']));
                $worksheet->setCellValue("J{$row}", trim($v['TCTNo']));
                $worksheet->setCellValue("K{$row}", trim($v['CCTNO']));
                $worksheet->setCellValue("L{$row}", trim($v['BldgStorey']));
                $worksheet->setCellValue("M{$row}", trim($v['BldgTypeDesc']));
                $worksheet->setCellValue("N{$row}", trim($v['StrucType']));
                $worksheet->setCellValue("O{$row}", trim($v['SubClass']));

                foreach ($v['td_a'] as $k2 => $v2) {

                    if($k2){
                        $row++;
                    }

                    $worksheet->setCellValue("P{$row}", self::getKind(trim($v2['kind'])));
                    $worksheet->setCellValue("Q{$row}", trim($v2['AUDesc']));
                    $worksheet->setCellValue("R{$row}", trim($v2['area']));
                    $worksheet->setCellValue("S{$row}", trim($v2['MV']));
                    $worksheet->setCellValue("T{$row}", trim($v2['AL']) . '%');
                    $worksheet->setCellValue("U{$row}", trim($v2['AV']));
                    $worksheet->setCellValue("V{$row}", trim($v2['Taxability']));
                }

                $row++;
            }

        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;


    }

    private function getKind($kind){

        switch ($kind) {
            case 'L':
                return "Land";
                break;

            case 'B':
                return "Building";
                break;

            case 'M':
                return "Machinery";
                break;

            default:
                return "";
                break;
        }


    }

    public function createCsv(Request $request){

        $output = [];

        $map = MapPolygon::select('pin', 'gmap_polygon')
                    ->where('barangay', $request->bgy)
                    ->get();

        $pin_map = self::extractInvertPin($map);

        $pin_active_q  = ActiveAssessmentRoll::select('ARP','PIN','Owner','OwnerAddress',
                            'Location','BldgLocation','Barangay', 'MuniDistName',
                            'LotNo', 'BlkNo', 'TCTNo', 'CCTNO',
                            'kind', 'BldgStorey', 'BldgTypeDesc',
                            'StrucType','SubClass')
                        ->whereRaw('SUBSTRING(pin,8,3) = ?', $request->bgy)
                        ->orderBy('PIN', 'ASC')
                        ->get()
                        ->toArray();

        // *****
        $pin_active = [];

        foreach ($pin_active_q as $key => $value) {
            if(array_key_exists($value['PIN'], $pin_map)){
                $pin_active[] = $value;
                
            }
        }

        $pin_active_arp = self::extractArp($pin_active);

        $querytaxdec_assmt_q = ActiveTaxdecAssmnt::select('ARP', 'kind','AUDesc', 'area',
                                'MV','AL', 'AV','Taxability')
                        ->whereIn('ARP', $pin_active_arp)
                        ->orderBy('ARP', 'ASC')
                        ->get()
                        ->toArray();

        // *****
        $querytaxdec_assmt_grp = self::arpGroup($querytaxdec_assmt_q);
        $pin_active_grp = self::pinActiveGroup($pin_active);

        $merge_active = [];

        foreach ($pin_active_grp as $key => $value) {
            foreach ($value as $k => $v) {
                $merge_active[$key][$k] = $v;
                $merge_active[$key][$k]['td_a'] = $querytaxdec_assmt_grp[$v['ARP']];
            }
        }

        return self::outputCsv($merge_active, $request->bgy);

    }

    private function outputCsv($data, $bgy){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet
                ->setCellValue("A1", "PIN")
                ->setCellValue("B1", "ARP")
                ->setCellValue("C1", "Owner")
                ->setCellValue("D1", "Owner's_Address")
                ->setCellValue("E1", "Location")
                ->setCellValue("F1", "Bldg_Location")
                ->setCellValue("G1", "District")
                ->setCellValue("H1", "Lot_No")
                ->setCellValue("I1", "Block_No")
                ->setCellValue("J1", "TCT_No")
                ->setCellValue("K1", "CCT_No")
                ->setCellValue("L1", "Bldg_Storey")
                ->setCellValue("M1", "Bldg_Type_Desc")
                ->setCellValue("N1", "Struc_Type")
                ->setCellValue("O1", "SubClass")
                ->setCellValue("P1", "Kind")
                ->setCellValue("Q1", "AU_Desc")
                ->setCellValue("R1", "Area")
                ->setCellValue("S1", "Market_Value")
                ->setCellValue("T1", "Assess_Level")
                ->setCellValue("U1", "Assess_Value")
                ->setCellValue("V1", "Taxability");

        $row = 2;
        
        foreach ($data as $key => $val) {
            foreach ($val as $k => $v) {
                $worksheet->setCellValue("A{$row}", trim($v['PIN']));
                $worksheet->setCellValue("B{$row}", trim($v['ARP']));
                $worksheet->setCellValue("C{$row}", trim($v['Owner']));
                $worksheet->setCellValue("D{$row}", trim($v['OwnerAddress']));
                $worksheet->setCellValue("E{$row}", trim($v['Location']));
                $worksheet->setCellValue("F{$row}", trim($v['BldgLocation']));
                $worksheet->setCellValue("G{$row}", trim($v['MuniDistName']));
                $worksheet->setCellValue("H{$row}", trim($v['LotNo']));
                $worksheet->setCellValue("I{$row}", trim($v['BlkNo']));
                $worksheet->setCellValue("J{$row}", trim($v['TCTNo']));
                $worksheet->setCellValue("K{$row}", trim($v['CCTNO']));
                $worksheet->setCellValue("L{$row}", trim($v['BldgStorey']));
                $worksheet->setCellValue("M{$row}", trim($v['BldgTypeDesc']));
                $worksheet->setCellValue("N{$row}", trim($v['StrucType']));
                $worksheet->setCellValue("O{$row}", trim($v['SubClass']));

                $kind       = "";
                $au_desc    = "";
                $area       = "";
                $mv         = "";
                $al         = "";
                $av         = "";
                $taxability = "";

                foreach ($v['td_a'] as $k2 => $v2) {

                    if($k2){
                        $kind       = $kind . " / " . self::getKind(trim($v2['kind']));
                        $au_desc    = $au_desc . " / " . trim($v2['AUDesc']);
                        $area       = $area . " / " . trim($v2['area']);
                        $mv         = $mv . " / " . trim($v2['MV']);
                        $al         = $al . " / " . trim($v2['AL']) . '%';
                        $av         = $av . " / " . trim($v2['AV']);
                        $taxability = $taxability . " / " . trim($v2['Taxability']);
                    } else{
                        $kind       = self::getKind(trim($v2['kind']));
                        $au_desc    = trim($v2['AUDesc']);
                        $area       = trim($v2['area']);
                        $mv         = trim($v2['MV']);
                        $al         = trim($v2['AL']) . '%';
                        $av         = trim($v2['AV']);
                        $taxability = trim($v2['Taxability']);
                    }

                }

                $worksheet->setCellValue("P{$row}", $kind);
                $worksheet->setCellValue("Q{$row}", $au_desc);
                $worksheet->setCellValue("R{$row}", $area);
                $worksheet->setCellValue("S{$row}", $mv);
                $worksheet->setCellValue("T{$row}", $al);
                $worksheet->setCellValue("U{$row}", $av);
                $worksheet->setCellValue("V{$row}", $taxability);

                $row++;
            }

        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
        ob_start();
        $writer->setSheetIndex(0);
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;


    }

}
