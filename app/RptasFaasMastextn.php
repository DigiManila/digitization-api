<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasFaasMastextn extends Model
{
    use HasFactory;
    protected $table = 'rptas_faas_mastextn';
}
