<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Storage;
use Hash;

class AuthController extends Controller
{
    public function loadUsername(){

        $query = User::select('username')
                    ->get();
                    

        return $query;
    }

    public function register(Request $request){

        

        $data = $request;
        $file = $request->file("file");

        // \Log::info($file->getClientOriginalName());

        $created_user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ])->profile()->create([
            'company_id' => $data['company_id'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'job' => $data['job'],
            'position' => $data['position'], //substr($myStr, 0, 5)
            'user_type' => $data['user_type'],
            // 'profile_pic_path' => $data['image'],
            'profile_pic_path' => Storage::disk('local')->putFileAs('profiles', $file, $file->getClientOriginalName()),
        ]);

        if($created_user){
            return response()->json([
                'message' => "User Registered successfully",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                'message' => "Error registering user. Try again later or call IT",
                'status' => 0
            ], 200);
        }

        // return $request;


        // $data = $request->validate([
        //     'username' => 'required|max:55',
        //     'email' => 'email|required|unique:users',
        //     'password' => 'required|confirmed',
        // ]);

        // $data['password'] = bcrypt($request->password);

        // $user = User::create($data);

        // $token = $user->createToken('token')->accessToken;

        // return response(['user' => $user, 'access_token' => $token]);

    }

    public function login(Request $request){

        $data = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        if(!auth()->attempt($data)){
            return response()->json([
                "data" => [],
                'message' => 'Invalid Credentials',
                'status' => 0
            ], 200);
        }

        $profile = Profile::select('user_id','first_name', 'middle_name', 'last_name', 'job', 'position', 'user_type', 'profile_pic_path')
                    ->where('user_id', auth()->user()->id)
                    ->first();

        $token = auth()->user()->createToken('token')->accessToken;

        $path = storage_path("app" . DIRECTORY_SEPARATOR . $profile["profile_pic_path"]);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);


        //call logs with optional parameters
        // Controller::addLog('login', $new_data = "asdf", $prev_data = 'qwer' , $remarks = "asdffqwerzxcv");

        //call Logs
        Controller::addLog('login');

        return response()->json([
            "data" => [
                'profile' => $profile,
                'access_token' => $token,
                'image' => "$base64"
            ],
            'message' => "Login Successful",
            'status' => 1
        ], 200);

    }
}
