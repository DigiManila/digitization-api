<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FloatingBuilding extends Model
{
    use HasFactory;
    protected $table = 'floating_building_active';
}
