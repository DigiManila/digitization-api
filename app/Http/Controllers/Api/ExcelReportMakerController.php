<?php

namespace App\Http\Controllers\Api;

use App\Exports\UsersExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Illuminate\Support\Facades\Storage;

use Auth;


class ExcelReportMakerController extends Controller
{
    public function export() 
    {

        $file = storage_path() . "\app\\temp\\" . Auth::user()->id . '.xlsx';
        $file = str_replace("\\", "/", $file);


        $spreadsheet = new Spreadsheet();       
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World A!');

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);

        // return "OK";

        
        // return str_replace('/', '/', $file);

        return response()->download($file, '2.xlsx');
    }


}
