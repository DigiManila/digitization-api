<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasOwnerextnConcat extends Model
{
    use HasFactory;

    protected $table = 'rptas_ownerextn_concat';
}
