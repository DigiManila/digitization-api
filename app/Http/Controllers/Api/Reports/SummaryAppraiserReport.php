<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ActiveAssessmentRoll;

class SummaryAppraiserReport extends Controller
{
    public function postSummaryAppraiser(Request $request){
        
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        
        $year = null;
        $month = null;
        $rangeDate = null;
       


        if( $request->datePicker == 'Yearly'){
            $year = $request->date;
        }
        else if( $request->datePicker == 'Range'){
            $rangeDate = $request->date;
            
        }
        else {
            $explodeDayMonthYear = explode('-',$request->date);

            $year = $explodeDayMonthYear[0];
            $month = $explodeDayMonthYear[1];
        
        }

        $querySummary = ActiveAssessmentRoll::selectRaw('PIN','active_taxdec_assmnt.ARP', 'active_taxdec_assmnt.kind', 
                    'AppraisedBy', 'active_taxdec_assmnt.MV','active_taxdec_assmnt.AV','AppraisedDt',
                    'active_taxdec_assmnt.Taxability')
        ->join('active_taxdec_assmnt','active_assessment_roll.ARP','=','active_taxdec_assmnt.ARP')
        ->when($request->datePicker == 'Daily', function($query) use ($request){
            return $query->whereDate('active_taxdec_assmnt.InsertedDt', $request->date );
        })

        ->when($request->datePicker == 'weekly', function($query) use ($request){
            return $query->whereBetween('active_taxdec_assmnt.InsertedDt', 
            [Carbon::parse($request->date)->startOfWeek(),
            Carbon::parse($request->date)->endOfWeek()]);
        })
        
        ->when($request->datePicker == 'Monthly', function($query) use ($month, $year){
            return $query->whereMonth('active_taxdec_assmnt.InsertedDt',  $month)
                        ->whereYear('active_taxdec_assmnt.InsertedDt', $year);   
        })
        
        ->when($request->datePicker == 'Yearly', function($query) use ( $year){                            
            return $query->whereYear('active_taxdec_assmnt.InsertedDt',  
                    Carbon::createFromFormat('Y',$year));

        })
        ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                
            return $query->whereBetween('active_taxdec_assmnt.InsertedDt',   $rangeDate)
                            ->orderBy('active_taxdec_assmnt.InsertedDt', 'desc');

        })
        ->
        ->get()
        ->toArray();

        $getAllTaxable['landMV'] = 0.00;
        $getAllTaxable['landAV'] = 0.00;
        $getAllTaxable['bldgMV'] = 0.00;
        $getAllTaxable['bldgAV'] = 0.00;
        $getAllTaxable['machMV'] = 0.00;
        $getAllTaxable['machAV'] = 0.00;
        
        $getAllTaxable['landParcel'] = 0;
        $getAllTaxable['bldgParcel'] = 0;
        $getAllTaxable['machParcel'] = 0;

        foreach($querySummary as $key => $value){

            if ($querySummary[$key]['Taxability'] == 'T'){
                if($value[''])
            }
        }
    }
}
