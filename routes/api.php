<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['auth:api']], function () {

    // -------------------------------------------------------------------------------------------------------- //
    // ----------------------------------------- WEB API ------------------------------------------------------ //
    // -------------------------------------------------------------------------------------------------------- //

    Route::get('/dashboard-data', 'Api\DashboardController@getData');
    Route::get('/dashboard-records-data', 'Api\DashboardController@getRecordsData');
    Route::post('/get-barangay-data', 'Api\DashboardController@getBarangayData');
    Route::post('/get-barangay-pin', 'Api\DashboardController@getBarangayPin');

    Route::post('/register', 'Api\AuthController@register');
    Route::get('/load-username', 'Api\AuthController@loadUsername');
    
    Route::post('/total-encoded', 'Api\UserProfileController@totalEncoded');
    Route::post('/get-ranged-date', 'Api\UserProfileController@rangedDate');
    Route::get('/dashboard-user', 'Api\UserProfileController@getDataUser');
    Route::post('/get-date-pick-user', 'Api\UserProfileController@getDatePick');

    Route::get('/dashboard-admin-encoder', 'Api\EncoderDashboardController@getData');
    Route::post('/timeline', 'Api\EncoderDashboardController@timeline');
    Route::post('/get-date-pick', 'Api\EncoderDashboardController@getDatePick');
    Route::post('/get-monthly-ranking', 'Api\EncoderDashboardController@getMonthlyRanking');
    Route::post('/get-linechart-encoder', 'Api\EncoderDashboardController@getLineChartEncoder');
    Route::post('/update-ed-cew', 'Api\EncoderDashboardController@updateEncoderDashboardCEW');
    Route::post('/exist-pdf-filename-96', 'Api\EncoderDashboardController@existPdfFilename96');

    Route::get('/dashboard-admin-proofreader', 'Api\ProofReaderDashboardController@getData');
    Route::post('/timeline-proofreader', 'Api\ProofReaderDashboardController@timeline');
    Route::post('/get-date-pick-proofreader', 'Api\ProofReaderDashboardController@getDatePick');
    Route::post('/get-monthly-ranking-proofreader', 'Api\ProofReaderDashboardController@getMonthlyRanking');
    Route::post('/get-linechart-proofreader', 'Api\ProofReaderDashboardController@getLineChartEncoder');
    Route::post('/update-ed-cew-proofreader', 'Api\ProofReaderDashboardController@updateEncoderDashboardCEW');
    Route::post('/exist-pdf-filename-96-proofreader', 'Api\ProofReaderDashboardController@existPdfFilename96');

    Route::get('/dashboard-admin-verifier', 'Api\VerifierDashboardController@getData');
    Route::post('/timeline-verifier', 'Api\VerifierDashboardController@timeline');
    Route::post('/get-date-pick-verifier', 'Api\VerifierDashboardController@getDatePick');
    Route::post('/get-monthly-ranking-verifier', 'Api\VerifierDashboardController@getMonthlyRanking');
    Route::post('/get-linechart-verifier', 'Api\VerifierDashboardController@getLineChartEncoder');
    Route::post('/update-ed-cew-verifier', 'Api\VerifierDashboardController@updateEncoderDashboardCEW');
    Route::post('/exist-pdf-filename-96-verifier', 'Api\VerifierDashboardController@existPdfFilename96');

    
    Route::get('/get-data', 'Api\AssessmentRollController@getData');
    Route::post('/post-dialog-barangay', 'Api\AssessmentRollController@postBarangay');
    Route::post('/post-dialog-barangay-residential', 'Api\AssessmentRollController@postBarangayResidential');
    Route::post('/post-dialog-barangay-commercial', 'Api\AssessmentRollController@postBarangayCommercial');
    Route::post('/post-appraiser', 'Api\AssessmentRollController@postAppraiser');
    Route::post('/post-multiple-appraiser', 'Api\AssessmentRollController@postMultipleAppraiser');
    Route::post('/post-multiple-au-barangay', 'Api\AssessmentRollController@postMultipleAUBarangay');
    Route::post('/post-export-to-excel-ARC', 'Api\AssessmentRollController@exportToExcel');
    
    Route::post('/post-barangay-pin', 'Api\AssessmentRollController@postBarangayPin');
    Route::post('/post-multiple-barangay-pin', 'Api\AssessmentRollController@postMultipleBarangayPin');
    Route::post('/post-appraiser-pin', 'Api\AssessmentRollController@postAppraiserPin');
    Route::post('/post-multiple-appraiser-pin', 'Api\AssessmentRollController@postMultipleAppraiserPin');
    
    Route::post('/post-cancelled-dialog-barangay', 'Api\AssessmentRollController@postCancelledBarangay');
    Route::post('/post-cancelled-dialog-appraiser', 'Api\AssessmentRollController@postCancelledAppraiser');
    Route::post('/post-cancelled-multiple-au-barangay', 'Api\AssessmentRollController@postCancelledMultipleAUBarangay');
    
    Route::post('/post-cancelled-barangay-pin', 'Api\AssessmentRollController@postCancelledBarangayPin');
    Route::post('/post-cancelled-appraiser-pin', 'Api\AssessmentRollController@postCancelledAppraiserPin');
    Route::post('/post-cancelled-multiple-barangay-pin', 'Api\AssessmentRollController@postCancelledMultipleBarangayPin');
    Route::post('/select-dialog-barangay', 'Api\AssessmentRollController@selectDialogBarangay');


    //Unused routes. News Routes will be replaced.

    // Route::get('/assessment-roll-data', 'Api\AssessmentRollController@getData');
    // Route::post('/search-assessment-roll-data', 'Api\AssessmentRollController@getSearchData');
    // Route::post('/search-archived-document', 'Api\ArchiveAssessmentRollController@getSearchData');

    // Route::post('/save-scanned-document', 'Api\ScanDocumentController@save');
    // Route::get('/get-scanned-document', 'Api\AssessmentRollController@getAssessmentScanned');
    // Route::post('/get-document-list', 'Api\AssessmentRollController@getLinkedData');
    // Route::post('/get-document', 'Api\AssessmentRollController@getPdfDataUrl');

    Route::get('/get-archived-document', 'Api\ArchiveAssessmentRollController@getArchivedScanned');
    Route::post('/get-archive-document-list', 'Api\ArchiveAssessmentRollController@getLinkedData');
    Route::post('/get-archive-document', 'Api\ArchiveAssessmentRollController@getArchivePdfDataUrl');
    Route::post('/save-archived-document', 'Api\ArchiveAssessmentRollController@save');

    Route::post('/get-polygon', 'Api\PolygonController@getPolygon');
    Route::post('/get-single-polygon', 'Api\PolygonController@getSinglePolygon');
    Route::post('/get-single-polygon-no-sel', 'Api\PolygonController@getSinglePolygonNoSelection');
    Route::post('/get-building-machinery-pin', 'Api\PolygonController@getBuildingMachineryPin');
    Route::post('/get-trace-assessment20052014', 'Api\PolygonController@getTraceAssessment20052014');
    Route::post('/get-trace-assessment-pin20052014', 'Api\PolygonController@getTraceAssessmentPin20052014');
    Route::post('/update-Cancelled-By', 'Api\PolygonController@updateCancelledBy');
    Route::post('/get-bgy-boundaries', 'Api\PolygonController@selectBgyBoundaries');
    Route::post('/get-district-click', 'Api\PolygonController@getDistrictClick');
    Route::get('/get-roads', 'Api\PolygonController@getRoads');

    Route::post('/get-active-map', 'Api\PolygonController@getActiveMap');
    // Route::post('/get-not-active-map', 'Api\PolygonController@getNotActiveMap');
    
    Route::get('/get-mapping-brgyno', 'Api\MappingBarangayNoController@getMappingBrgyno');
    Route::get('/get-mapping-no-pin-brgyno', 'Api\MappingBarangayNoController@getMappingNoPinBrgyno');
    Route::get('/get-mapping-cancelled-pin-have-map-brgyno', 'Api\MappingBarangayNoController@getMappingCancelledPinHaveMapBrgyno');
    Route::get('/get-mapping-no-map-pin-brgyno', 'Api\MappingBarangayNoController@getMappingNoMapPinBrgyno');
    Route::post('/post-not-active-single-polygon', 'Api\MappingBarangayNoController@postNotActiveSinglePolygon');
    Route::post('/post-cancelled-single-polygon', 'Api\MappingBarangayNoController@postCancelledSinglePolygon');
    Route::post('/post-not-active-single-polygon-no-sel', 'Api\MappingBarangayNoController@postNotActiveSinglePolygonNoSelection');
    Route::post('/get-not-active-map', 'Api\MappingBarangayNoController@getNotActiveMap');
    Route::post('/post-no-pin-map', 'Api\MappingBarangayNoController@postNoPinMap');
    Route::get('/get-cancelled-pin-have-map', 'Api\MappingBarangayNoController@getCancelledPinHaveMap');
    Route::post('/post-mapping-municipal-district', 'Api\MappingBarangayNoController@postMappingMunicipalDistrict');
    Route::post('/post-mapping-muni-to-barangay', 'Api\MappingBarangayNoController@postMappingMuniToBarangay');
    Route::post('/post-mapping-muni-to-block', 'Api\MappingBarangayNoController@postMappingMuniToBlock');
    Route::post('/post-mapping-location-with-details', 'Api\MappingBarangayNoController@postMappingLocationWithDetails');
    
    Route::post('/get-file-list','Api\FileController@listFiles');
    Route::post('/get-file-display','Api\FileController@getFile');

    Route::post('/save-or-taxdec', 'Api\PaymentController@savePayment');
    Route::get('/get-certifiers', 'Api\RecordsTaxDecController@getCertifiers');

    Route::get('/get-payments', 'Api\PaymentController@getPayments');
    
    Route::post('/createPDF', 'Api\PdfController@createPdf');
    Route::post('/getPDFPreview', 'Api\PdfController@createPreviewPdf');

    Route::get('/assessment-from-cache/{pin}', 'Api\CacheController@getDataCache');
    Route::get('/pin-from-cache', 'Api\CacheController@getPinCache');
    Route::get('/create-cache', 'Api\CacheController@createCache');

    Route::post('/save-taxdec', 'Api\TaxDecController@saveTaxDec');
    Route::post('/verify-taxdec', 'Api\TaxDecController@verifyTaxDec');
    Route::post('/delete-property-assessment', 'Api\TaxDecController@deletePropertyAssessment');
    Route::post('/save-temp-taxdec', 'Api\TempTaxDecController@saveTempTaxDec');
    
    Route::get('/get-taxdec', 'Api\TaxDecController@getTaxDec');
    Route::post('/get-single-temp-taxdec', 'Api\TempTaxDecController@getSingleTempTaxDec');
    Route::post('/get-single-taxdec', 'Api\TaxDecController@getSingleTaxDec');
    Route::post('/get-trace-assessment', 'Api\TempTaxDecController@getTraceAssessment');
    

    Route::post('/save-records-taxdec', 'Api\RecordsTaxDecController@saveTaxDec');
    Route::post('/validate-taxdec', 'Api\RecordsTaxDecController@checkTdIfExist');

    Route::post('/save-agg-kind-barangay', 'Api\AggKindBarangaySummaryController@saveAggKindBarangay');

    // Route::post('/save-aggbarangay', 'Api\AggBarangayDController@saveAggBarangay');
    // Route::post('/save-aggSingleRecord', 'Api\AggSingleRecordController@saveAggSingleRecord');

    // Route::post('/save-logs', 'Api\LogsController@saveLogs');

    Route::post('/display-totals', 'Api\DashboardController@displayTotals');
    Route::get('/get-databuild-up-user', 'Api\DatabuildUpUserController@getDatabuildUpUser');
    Route::post('/reports-encode-get-line-data', 'Api\DatabuildUpUserController@getLineChartData');
    

    Route::post('/get-folder-structure', 'Api\DigitalLibraryController@getStructure');
    Route::post('/get-book-links', 'Api\DigitalLibraryController@getLinks');
    
    Route::get('/get-idle-per-bgy', 'Api\IdleLandsController@getBarangay');
    Route::post('/get-idle-per-kind', 'Api\IdleLandsController@getBarangayKind');
    Route::post('/get-idle-per-pin', 'Api\IdleLandsController@getIdlePin');
    Route::get('/get-all-idle-report', 'Api\IdleLandsController@getIdleReportsAll');
    Route::post('/get-idle-report-per-bgy', 'Api\IdleLandsController@exportPerBrgy');
    Route::post('/get-idle-all-kind', 'Api\IdleLandsController@listPerBarangay');
    Route::post('/get-idle-all-kind-tagged', 'Api\IdleLandsController@listPerBarangayTagged');
    Route::post('/get-idle-all-kind-insp', 'Api\IdleLandsController@listPerBarangayInsp');
    Route::get('/get-breakdown-report-per-bgy', 'Api\IdleLandsController@breakdownPerBgy');
    Route::post('/idle-lands-check-ocular', 'Api\IdleLandsController@checkOcular');
    Route::post('/idle-land-show-ocular', 'Api\IdleLandsController@showOcular');
    Route::post('/idle-lands-check-tag', 'Api\IdleLandsController@checkTag');
    Route::post('/idle-lands-get-tag', 'Api\IdleLandsController@getTag');
    Route::post('/idle-lands-save-tag', 'Api\IdleLandsController@saveTag');


    Route::get('/get-all-floating', 'Api\FloatingParcelController@getAllFloating');
    Route::post('/get-floating-building', 'Api\FloatingParcelController@getFloatingBuilding');
    Route::post('/get-floating-machinery', 'Api\FloatingParcelController@getFloatingMachinery');

    
    Route::post('/missing-TD','Api\ArchiveTDReportController@missingTD');
    Route::post('/get-Brgyno','Api\ArchiveTDReportController@getBrgyno');
    Route::get('/get-unproper-TD','Api\ArchiveTDReportController@getUnproperTD');
    Route::post('/get-temp-taxdec', 'Api\ArchiveTDReportController@getTempTaxDec');
    Route::post('/get-archive-taxdec', 'Api\ArchiveTDReportController@getArchiveTaxDec');
    
    Route::get('/export','Api\ExcelReportMakerController@export');
    
    

    Route::get('/get-bgy-sales-map','Api\SalesDataController@getBarangaySalesData');
    Route::post('/get-active-sales-map', 'Api\SalesDataController@getActiveSalesMap');
    Route::post('/get-data-sales-map', 'Api\SalesDataController@getDataSalesMap');
    
    Route::post('/get-zone', 'Api\CheckEncoderWorkController@getZone');
    Route::post('/get-barangay', 'Api\CheckEncoderWorkController@getBarangay');
    Route::post('/get-remark', 'Api\CheckEncoderWorkController@getRemark');
    Route::post('/get-cew-dashboard', 'Api\CheckEncoderWorkController@getCEWDashboard');
    Route::post('/update-cew', 'Api\CheckEncoderWorkController@updateCEW');
    
    Route::post('/proofread-get-zone', 'Api\CheckProofreaderWorkController@getZone');
    Route::post('/proofread-get-barangay', 'Api\CheckProofreaderWorkController@getBarangay');
    Route::post('/proofread-get-remark', 'Api\CheckProofreaderWorkController@getRemark');
    Route::post('/proofread-get-cew-dashboard', 'Api\CheckProofreaderWorkController@getCEWDashboard');
    Route::post('/proofread-update-cew', 'Api\CheckProofreaderWorkController@updateCEW');

    Route::post('/verifier-get-zone', 'Api\CheckVerifierWorkController@getZone');
    Route::post('/verifier-get-barangay', 'Api\CheckVerifierWorkController@getBarangay');
    Route::post('/verifier-get-remark', 'Api\CheckVerifierWorkController@getRemark');
    Route::post('/verifier-get-cew-dashboard', 'Api\CheckVerifierWorkController@getCEWDashboard');
    Route::post('/verifier-update-cew', 'Api\CheckVerifierWorkController@updateCEW');

    Route::post('/get-folder-struct', 'Api\FileController@getFolderStructure');
    Route::post('/get-jpg-file-name', 'Api\FileController@getFilenames');
    Route::post('/get-folder-indi', 'Api\FileController@getIndividualFolder');

    Route::post('/get-pictures', 'Api\FileController@getPictures');
    Route::post('/get-pdf', 'Api\FileController@getPdf');

    Route::post('/get-td-encode', 'Api\EncoderController@getDataEncoded');
    Route::post('/get-td-proofread', 'Api\EncoderController@getDataProofread');
    Route::post('/save-td-encode', 'Api\EncoderController@saveDataEncoded');
    Route::post('/update-td-encode', 'Api\EncoderController@updateEncoded');

    Route::post('/verify-td-exist', 'Api\RecordsTaxDecController@verifyTdExist');
    Route::post('/td-print-logs', 'Api\RecordsTaxDecController@printTdLogs');

    Route::get('/get-released-td', 'Api\RecordsTaxDecController@getReleasedTd');
    Route::get('/td-get-zone-bgy', 'Api\RecordsTaxDecController@getZoneBgy');
    Route::post('/get-receipt-preview', 'Api\FileController@getReceiptPreview');

    Route::post('/get-display-images', 'Api\FileController@getImageData');

    Route::get('/sd-get-barangay', 'Api\SalesDataMapController@getBarangay');
    Route::post('/sd-get-pin', 'Api\SalesDataMapController@getPin');
    Route::post('/sd-get-sales-data-record', 'Api\SalesDataMapController@getSalesDataPolygon');
    Route::post('/sd-get-bgy-boundaries', 'Api\SalesDataMapController@selectBgyBoundaries');
    Route::post('/sd-district-click', 'Api\SalesDataMapController@getDistrictClick');
    
    Route::get('/sd-dashboard', 'Api\SalesDataController@getSalesDataDashboard');
    Route::post('/sd-get-barangay-data', 'Api\SalesDataController@getBarangaySalesDataDashboard');
    Route::post('/sd-get-barangay-pin', 'Api\SalesDataController@getBarangayPinSalesDataDashboard');

    Route::get('/sd-tagging', 'Api\SalesDataController@getSalesDataTagging');
    Route::post('/sd-update', 'Api\SalesDataController@updateSalesData');
    Route::post('/sd-export-report', 'Api\SalesDataController@exportReport');

    Route::get('/sales-get-all-barangay', 'Api\SalesDataController@getBarangay');
    Route::post('/sales-get-assessment-roll','Api\SalesDataController@getAssessmentRoll');
    Route::post('/sales-data-get-selected', 'Api\SalesDataController@getSelected');
    Route::post('/sales-data-save', 'Api\SalesDataController@saveData');
    Route::post('/sales-data-import-excel', 'Api\SalesDataController@importExcel');

    Route::get('/get-vacant-per-bgy', 'Api\VacantLandsController@getBarangay');
    Route::post('/get-vacant-per-kind', 'Api\VacantLandsController@getBarangayKind');
    Route::post('/get-vacant-per-pin', 'Api\VacantLandsController@getVacantPin');
    Route::post('/get-vacant-all-kind', 'Api\VacantLandsController@listPerBarangay');
    Route::get('/get-vacant-breakdown-report-per-bgy', 'Api\VacantLandsController@breakdownPerBgy');

    Route::get('/get-all-vacant-report', 'Api\VacantLandsController@exportAllData');
    Route::post('/get-per-bgy-vacant-report', 'Api\VacantLandsController@exportPerBrgy');

    Route::post('/get-file-archive-rec', 'Api\FileController@getPictureForArchiveRecords');

    Route::get('/get-rptas-test', 'Api\RPTASConnectionController@getData');

    Route::get('/delinquency-get-data', 'Api\TaxDeliquencyController@getMounted');
    Route::post('/delinquency-post-district', 'Api\TaxDeliquencyController@postDistrict');
    Route::post('/delinquency-post-barangay', 'Api\TaxDeliquencyController@postBarangay');
    Route::post('/delinquency-post-parcel', 'Api\TaxDeliquencyController@postParcel');
    Route::post('/delinquency-get-sections-polygon', 'Api\TaxDeliquencyController@getSectionMapPolygons');
    Route::post('/delinquency-no-maps-report', 'Api\TaxDeliquencyController@getNoMapReport');
    Route::post('/delinquency-search-from-pin', 'Api\TaxDeliquencyController@postParcelSearch');

    Route::post('/barangay-map-get-sections-polygon', 'Api\BarangayMapController@getSectionMapPolygons');
    Route::post('/barangay-map-get-markers', 'Api\BarangayMapController@getMarkers');
    Route::post('/barangay-map-download-excel', 'Api\BarangayMapController@downloadExcel');

    Route::get('/actual-use-get-land-use', 'Api\ActualUseController@getLandUse');
    Route::post('/actual-use-business-list', 'Api\ActualUseController@getBusinessList');
    Route::post('/actual-use-save-permit', 'Api\ActualUseController@savePermit');

    Route::post('/post-control-book-per-brgy', 'Api\ControlBookController@postControlBookPerBrgy');
    Route::post('/post-previous-arp', 'Api\ControlBookController@postPreviousARP');
    Route::post('/post-cancelled-by-arp', 'Api\ControlBookController@postCancelledByARP');
    Route::post('/post-active-or-cancel-barangay-no', 'Api\ControlBookController@postActiveOrCancelBarangayNo');

    Route::post('/released-get-year-month', 'Api\ReleaseTaxDecController@getYearMonth');

    Route::get('/td-report-dashboard', 'Api\ReleaseTaxDecController@getDashboardData');
    Route::post('/td-report-update-dashboard', 'Api\ReleaseTaxDecController@updateDashboardData');
    Route::post('/td-report-export-excel', 'Api\ReleaseTaxDecController@exportToExcel');
    Route::post('/td-report-get-file-path', 'Api\ReleaseTaxDecController@getFilePath');
    Route::post('/td-report-get-file', 'Api\ReleaseTaxDecController@getFile');
    
    Route::get('/cancellation-report-dashboard', 'Api\ReleaseCancellationController@getDashboardData');
    Route::post('/cancellation-report-update-dashboard', 'Api\ReleaseCancellationController@updateDashboardData');
    Route::post('/cancellation-report-export-excel', 'Api\ReleaseCancellationController@exportToExcel');
    Route::post('/cancellation-report-get-file-path', 'Api\ReleaseCancellationController@getFilePath');
    Route::post('/cancellation-report-get-file', 'Api\ReleaseCancellationController@getFile');

    Route::get('/td-report-viewing-dashboard', 'Api\ReleaseTaxDecViewingController@getDashboardData');
    Route::post('/td-report-viewing-get-files', 'Api\ReleaseTaxDecViewingController@getFiles');

    Route::post('/data-recon-get-data', 'Api\DataReconciliationController@getData');
    Route::post('/data-recon-get-td-assessment', 'Api\DataReconciliationController@getTaxdecAssmnt');
    Route::post('/data-recon-save-data', 'Api\DataReconciliationController@saveData');

    Route::post('/data-recon-get-comparison', 'Api\DataReconciliationController@getComparison');
    Route::post('/data-recon-generate-report', 'Api\DataReconciliationController@generateReport');

    Route::get('/data-recon-table-get-data', 'Api\DataReconciliationController@tableGetData');
    Route::post('/data-recon-table-excel', 'Api\DataReconciliationController@exportReport');

    Route::post('/td-release-log-create-pdf', 'Api\TdReleaseLogController@getPdf');

    Route::post('/td-logging-create-pdf', 'Api\TdLoggingController@getPdf');

    Route::get('/records-certification-get-verifier', 'Api\RecordsCertificationController@getVerifier');
    Route::post('/records-certification-save', 'Api\RecordsCertificationController@saveData');
    Route::post('/records-certification-preview', 'Api\RecordsCertificationController@getPreview');
    Route::post('/records-certification-print-log', 'Api\RecordsCertificationController@updateLog');

    Route::get('/cert-rec-report-dashboard', 'Api\RecordsCertificationReportController@getDashboardData');
    Route::post('/cert-rec-report-update-dashboard', 'Api\RecordsCertificationReportController@updateDashboardData');
    Route::post('/cert-rec-report-export-excel', 'Api\RecordsCertificationReportController@exportToExcel');
    Route::post('/cert-rec-report-get-file', 'Api\RecordsCertificationReportController@getFile');
    
    Route::post('/file-viewer-get-lists', 'Api\FileViewerController@getList');
    Route::post('/file-viewer-get-file', 'Api\FileViewerController@getFile');

    Route::get('/edit-data-recons-get-bgy', 'Api\DataReconciliationController@editGetBgy');
    Route::post('/edit-data-recons-get-lists', 'Api\DataReconciliationController@editGetList');
    Route::post('/edit-data-recons-get-data', 'Api\DataReconciliationController@editGetData');
    Route::post('/edit-data-recons-update-data', 'Api\DataReconciliationController@editUpdateData');

    Route::post('/sales-data-map-create-kml', 'Api\SalesDataMapController@createKml');

    Route::post('/tax-map-create-kml', 'Api\TaxMapKmlController@createKml');
    Route::post('/tax-map-create-excel', 'Api\TaxMapKmlController@createExcel');
    Route::post('/tax-map-create-csv', 'Api\TaxMapKmlController@createCsv');

    Route::post('/cancellation-log-create-pdf', 'Api\CancellationLogController@getPdf');
    // -------------------------------------------------------------------------------------------------------- //
    // ---------------------------------END----- WEB API ------------------------------------------------------ //
    // -------------------------------------------------------------------------------------------------------- //


    // -------------------------------------------------------------------------------------------------------- //
    // ---------------------------------END----- RPTAS API ------------------------------------------------------ //
    // -------------------------------------------------------------------------------------------------------- //

    Route::post('/post-rptas-single-polygon', 'Api\RptasPolygonController@RptasSinglePolygon');
    Route::post('/post-single-polygon-no-selection', 'Api\RptasPolygonController@postSinglePolygonNoSelection');
    Route::post('/get-rptas-taxdec', 'Api\RptasPolygonController@getRptasTaxdec');
    Route::post('/get-pictures-url', 'Api\RptasPolygonController@getPicturesUrl');
    Route::post('/post-building-machinery-pin', 'Api\RptasPolygonController@postBuildingMachineryPin');
    Route::post('/post-district', 'Api\RptasPolygonController@postDistrict');
    Route::post('/post-info-text-unused-parcel', 'Api\RptasPolygonController@postInfoTextUnusedParcel');

    Route::post('/post-rptas-active-or-cancelled', 'Api\RPTASAssessmentController@postRptasActiveOrCancelled');
    // Route::post('/get-rptas-assessment-roll', 'Api\RPTASAssessmentController@rptasAssessmentRoll');
    Route::post('/get-rptas-dialog-backward-arp', 'Api\RPTASAssessmentController@rptasDialogBackward');
    Route::post('/get-rptas-dialog-forward-arp', 'Api\RPTASAssessmentController@rptasDialogForwardArp');
    Route::post('/get-rptas-dialog-search-pin', 'Api\RPTASAssessmentController@rptasDialogSearchPin');
    Route::post('/get-rptas-dialog-search-owner-no', 'Api\RPTASAssessmentController@rptasDialogSearchOwnerNo');
    Route::post('/get-rptas-dialog-search-owner', 'Api\RPTASAssessmentController@rptasDialogSearchOwner');
    Route::post('/get-rptas-dialog-search-title', 'Api\RPTASAssessmentController@rptasDialogSearchTitle');
    
    Route::get('/get-rptas-muni-dist-name', 'Api\RPTASAssessmentController@rptasMuniDistName');
    Route::post('/get-rptas-muni-to-barangay', 'Api\RPTASAssessmentController@rptasMuniToBarangay');
    Route::post('/get-rptas-muni-to-blk', 'Api\RPTASAssessmentController@rptasMuniToBlock');
    Route::post('/get-rptas-muni-to-lot', 'Api\RPTASAssessmentController@rptasMuniToLot');
    Route::post('/get-rptas-location-with-details', 'Api\RPTASAssessmentController@rptasLocationWithDetails');


    //report assessment
    Route::post('/post-data-assessment-report', 'Api\Reports\AssessmentReportController@getDataAssessmentReport');
    Route::post('/post-assessed-value-report', 'Api\Reports\AssessedValueReportController@getAssessedValueReport');
    Route::post('/post-tax-dec-var-report', 'Api\Reports\TaxDecVarianceReportController@getTaxDecVariance');
    Route::post('/post-appraiser-report', 'Api\Reports\AppraiserReportController@getAppraiserReport');
    Route::post('/post-appraiser', 'Api\Reports\AppraiserReportController@postAppraiser');
    Route::post('/post-dialog-barangay-report', 'Api\Reports\ReportDialogBarangayController@exportToExcel');


    Route::post('/post-daily-record-transaction','Api\RecordsDivisionController@postData');
    Route::post('/post-tax-declaration-details','Api\RecordsDivisionController@postTaxDeclarationDetails');
    Route::post('/post-save-data-records-transaction','Api\RecordsDivisionController@saveData');
    Route::post('/post-update-data-records-transaction','Api\RecordsDivisionController@updateData');


     //report chart
     Route::get('/get-chart-report-data', 'Api\Reports\ReportChartController@getData');
    // -------------------------------------------------------------------------------------------------------- //
    // ----------------------------------------- MOBILE API --------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------- //










    // -------------------------------------------------------------------------------------------------------- //
    // ---------------------------------END----- MOBILE API --------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------- //


    // -------------------------------------------------------------------------------------------------------- //
    // ----------------------------------------- TRACKER API -------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------- //



    Route::get('/get-transaction-number', 'Tracker\TransactionNumberController@getTransaction');
    Route::post('/tracking-save', 'Tracker\TransactionNumberController@saveTransaction');
    Route::get('/get-all-tracking', 'Tracker\TransactionNumberController@getAllData');
    Route::post('/get-specific-tracking', 'Tracker\TransactionNumberController@getSpecificData');
    Route::post('/get-all-details-tracking', 'Tracker\TransactionNumberController@getData');
    
    Route::post('/get-status', 'Tracker\TransactionNumberController@getStatus');
    Route::post('/get-tracking-details', 'Tracker\TransactionNumberController@getTrackingDetails');
    Route::post('/get-dropdown-process', 'Tracker\TransactionNumberController@getDropdownData');

    Route::post('/update-tracking-status', 'Tracker\TransactionNumberController@updateTrack');
    Route::post('/update-tracker', 'Tracker\TransactionNumberController@updateTracker');

    Route::post('/tracker-get-list', 'Tracker\TrackerController@getTransactions');
    Route::post('/tracker-get-details', 'Tracker\TrackerController@getDetails');
    Route::post('/tracker-get-track', 'Tracker\TrackerController@getTrack');
    Route::post('/tracker-get-track-details', 'Tracker\TrackerController@getTrackDetails');
    Route::post('/tracker-update-track', 'Tracker\TrackerController@updateTrack');
    Route::post('/tracker-get-dropdown-process', 'Tracker\TrackerController@getDropdownData');
    Route::post('/tracker-tracking-save', 'Tracker\TrackerController@saveTransaction');
    

    // -------------------------------------------------------------------------------------------------------- //
    // ---------------------------------END----- TRACKER API -------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------- //

    // -------------------------------------------------------------------------------------------------------- //
    // ----------------------------------------- VERIFIER API ------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------- //

    Route::post('/authenticate_it', 'Api\AuthenticatorController@getData');
    Route::post('/verification_log', 'Api\AuthenticatorController@logData');
    // -------------------------------------------------------------------------------------------------------- //
    // ---------------------------------END----- VERIFIER API ------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------- //

});

Route::get('/kml-bgy', 'Api\KmlController@extractKmlBrgy');
Route::get('/kml-political', 'Api\KmlController@extractPoliticalBoundaries');
Route::get('/testSlip', 'Tracker\TransactionNumberController@createSlip');
Route::post('/login', 'Api\AuthController@login');
Route::post('/mobile_login', 'Mobile\LoginController@login');
// Route::post('/createPDF', 'Api\PdfController@createPdf');

Route::get('/update-district', 'Api\KmlController@updateDistrict');