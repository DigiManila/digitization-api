<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasActiveArpMappingLandDetailsView extends Model
{
    use HasFactory;

    protected $table = 'rptas_active_arp_mapping_land_details';
}
