<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTempTaxDeclarationTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_tax_declarations', function (Blueprint $table) {
            $table->string('created_by')->change();
            $table->string('proofread_by')->change();
            $table->dateTime('proofread_date')->change();
            $table->dateTime('verified_date')->change();
            $table->Integer('status')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_tax_declarations', function (Blueprint $table) {
            //
        });
    }
}
