<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasFloatingMachineryView extends Model
{
    use HasFactory;

    protected $table = 'rptas_floating_machinery';
}
