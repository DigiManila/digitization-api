<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDataReconsTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_recons', function (Blueprint $table) {
            $table->integer('created_by')->nullable()->after('changed_tax');
            $table->integer('updated_by')->nullable()->after('changed_tax');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_recons', function (Blueprint $table) {
            //
        });
    }
}
