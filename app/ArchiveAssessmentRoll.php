<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveAssessmentRoll extends Model
{
    protected $guarded = [];

    public function scanned()
    {
        return $this->hasMany('App\ScannedDocument', 'assessment_roll_id', 'id')
                    ->where('is_assessment_roll', false);
    }

}
