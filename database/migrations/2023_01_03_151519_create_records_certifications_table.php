<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_certifications', function (Blueprint $table) {
            $table->id();
            $table->string('ctr_no');
            $table->string('type');

            $table->string('owner');
            $table->string('owner_addr')->nullable();

            $table->string('requestor');
            $table->string('requestor_addr')->nullable();

            $table->string('purpose')->nullable();
            $table->text('file_path')->nullable();
            $table->boolean('printed')->default(false);

            $table->string('verifier');
            $table->integer('user_id');
            $table->string('prepared_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_certifications');
    }
}
