<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ScannedDocument;

use Storage;


class ScanDocumentController extends Controller
{
    public function save(Request $request){

        $file_name_explode = explode('-', $request->get('filename'));
        
        $type = $file_name_explode[0];
        $year = $file_name_explode[1];
        $data = $request->get('data')[0];

        $path = Storage::disk('scans')->getDriver()->getAdapter()->getPathPrefix();

        $save_arr = array(
            'assessment_roll_id'    => $data['id'],
            'is_assessment_roll'    => true,
            'type'                  => $type,
            'path'                  => $path . $year. DIRECTORY_SEPARATOR . $request->get('filename'),
            'created_by'            => auth()->user()->username,
        );

        $status = ScannedDocument::create($save_arr);

        return $status;

        // \Log::info(\Storage::disk('scans')->getDriver()->getAdapter()->getPathPrefix());

    }
}
