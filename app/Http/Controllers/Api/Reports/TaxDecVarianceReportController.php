<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ReportAssessmentRoll;
use Maatwebsite\Excel\Concerns\ToArray;

use PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\Wizard;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;


class TaxDecVarianceReportController extends Controller
{
    public function getTaxDecVariance (Request $request){
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
       
        $firstMonth = null;
        $endMonth = null;
        $quarter = $request->quarter;
        $year = null;
        $month = null;
        $rangeDate = null;
        $taxability = "T";

        if($request->taxability == "Exempted"){
            $taxability = "E";
        }
        

        if ($quarter != ""){
            if ($request->quarter == "1st"){
                $firstMonth = "01";
                $endMonth = "03";       
            }
            else if ($quarter== "2nd"){
                $firstMonth = "04";
                $endMonth = "06";
            }
            else if ($quarter == "3rd"){
                $firstMonth = "07";
                $endMonth = "09";
            }
            else if ($quarter == "4th"){
                $firstMonth = "10";
                $endMonth = "12";
            }
            $year = $request->date;
        }
        else if( $request->datePicker == 'Yearly'){
            $year = $request->date;
        }
        else if( $request->datePicker == 'Range'){
            $rangeDate = $request->date;
            
        }
        else {
            $explodeDayMonthYear = explode('-',$request->date);

            $year = $explodeDayMonthYear[0];
            $month = $explodeDayMonthYear[1];
        
        }


        $dateCreated = $request->date;
        $datePick = $request->datePicker;


        $data = [];
      

        $queryAllClassReport = ReportAssessmentRoll::select('Owner', 'Location', 'LotNo', 'BlkNo', "TctNo", 
                    'PIN', 'Effectivity', 'UpdateDesc',
                    'ARP', 'Prev_Arp', 'Taxability',
                    'AUDesc', 'kind', 'area', 
                    'MV','AL', 'AV', 'Prev_Av', 
                    'InsertedDt', 'AppraisedDt','Memoranda','Current')
                    ->where('Taxability', $taxability)
            
                    ->when($request->datePicker == 'Daily', function($query) use ($request){
                        return $query->whereDate('AppraisedDt', $request->date );
                       
                    })
                    
                    ->when($request->datePicker == 'Monthly', function($query) use ($month, $year){
                        return $query->whereMonth('AppraisedDt',  $month)
                                    ->whereYear('AppraisedDt', $year);   
                        
                    })
                    
                    ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){

                        return $query->whereBetween('AppraisedDt',   $rangeDate)
                                        ->orderBy('AppraisedDt', 'desc');
                    })
                    
                    ->orderBy('PIN')
                    ->get();
               
        $pluckPrevArp = [];
    
        foreach($queryAllClassReport as $key => $value){

            if ($value['Prev_Arp'] != null){
                array_push($pluckPrevArp, $value['Prev_Arp']);
            }
            

            if ($value['Prev_Av'] == null) {
                $value['Prev_Av'] = 0;
            }
    
            foreach($queryAllClassReport as $key1 => $value1){
                if (trim($value1['Prev_Arp']) == trim($value['ARP'])){

                    $value['Current'] = 2;
                }
               
            }

            $data[] = array(
                'Owner'         =>  trim($value['Owner']),
                'Location'      =>  trim($value['Location']),
                'LotNo'         =>  trim(str_replace("/", ",",$value['LotNo'])),
                'BlkNo'         =>  trim(str_replace("/", ",",$value['BlkNo'])),
                'TctNo'         =>  trim($value['TctNo']),
                'PIN'           =>  trim($value['PIN']),
                'Effectivity'   =>  substr($value['Effectivity'],0,4),
                'InsertedDt'    =>  $value['InsertedDt'],
                'Taxability'    =>  $value['Taxability'],
                'UpdateDesc'    =>  $value['UpdateDesc'],
                'ARP'           =>  $value['ARP'],
                'Prev_Arp'      =>  $value['Prev_Arp'],
                'kind'          =>  $value['kind'],
                'AUDesc'        =>  $value['AUDesc'],
                'area'          =>  $value['area'],
                'MV'            =>  $value['MV'],
                'AL'            =>  $value['AL'],
                'AV'            =>  $value['AV'],
                'Prev_Av'       =>  $value['Prev_Av'],
                'Memoranda'     =>  $value['Memoranda'],
                'Current'       =>  $value['Current'],
                'Variance'      => 0,

            );
            
        }
            
        $queryAllCancelledReport = ReportAssessmentRoll::select('ARP','AV','AUDesc')
        ->whereIn('ARP', $pluckPrevArp)       
        ->get();
        
        //----------------BALANCING VALUE OF AREA, MV, AV and Prev_Av--------------"
            $oldKey = 0;
            $prevKey = 0;
           
        foreach($data as $key => $value){
           
            // getting the assessed value of previous arp
           foreach($queryAllCancelledReport as $key1 => $value1){
                if ($value['Prev_Arp'] == $value1['ARP'] && $value['AUDesc'] == $value1['AUDesc'] ){
                    $data[$key]['Prev_Av'] = $value1['AV'];
                }
            }
            
            // balance consolidation
            if($key > 0){
                if ($value['ARP'] == $data[$oldKey]['ARP'] && $value['AUDesc'] == $data[$oldKey]['AUDesc'] ){
                    $data[$key]['AV'] = 0;
                    $data[$key]['MV'] = 0;
                    $data[$key]['area'] = 0;
                }

                if($key < 11){
                    
                    $prevKey = 0;
                }
                else
                {
                    $prevKey = $key - 10;
                }
                
                // balance subdivision
                while ($prevKey < $key){

                    if ($value['Prev_Arp'] == $data[$prevKey]['Prev_Arp'] && $value['AUDesc'] == $data[$prevKey]['AUDesc'] ){
                        // \Log::info($value['Prev_Arp']. " - ". $data[$prevKey]['Prev_Arp']);
                        $data[$key]['Prev_Av'] = 0;
                        break;
                    }
                    
                    $prevKey++;
                }
                
            }

            if ($key < 1){
                 $oldKey = 0;
              
            }
            else {
                $oldKey++;
                
            }
            
        }
        

        foreach($data as $key => $value){
            $data[$key]['Variance'] = $value['AV'] - $value['Prev_Av'];
        }
    
        return self::createExcel( $data, $month, 
            $year, $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate);
        
    }

    public function createExcel($data, $month, 
        $year, $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate){

        $nameMonth = date("F", mktime(0, 0, 0, $month, 10));
        $startMonth = date("F", mktime(0, 0, 0, $firstMonth, 10));
        $endMonth = date("F", mktime(0, 0, 0, $endMonth, 10));

        
        if ($datePick == 'Daily'){
            $title = "Daily Tax Declaration Transaction Report";
            
            $asOfDate = "Date as of: " .$dateCreated;
            
        }
        if ($datePick == 'weekly'){
            $title = "Weekly Tax Declaration Transaction Report";
            
            $asOfDate = "Date as of: " .Carbon::parse($dateCreated)->startOfWeek()->format('Y-m-d'). " to " 
                .Carbon::parse($dateCreated)->endOfWeek()->format('Y-m-d');
        }
        if ($datePick == 'Monthly'){
            $title = "Monthly Tax Declaration Transaction Report";
           
            $asOfDate = "For  " .$nameMonth ." " .$year;
        }
       

        if ($datePick == 'Yearly'){
            $title = $year." Yearly Tax Declaration Transaction Report";
            
            $asOfDate = "Year " .$year;
        }

        if ($datePick == 'Range'){
            $title = "Range Tax Declaration Transaction Report";
           
            $asOfDate = "From " .$rangeDate[0] ." to " .$rangeDate[1];
        }

        // Set value binder
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder());
        

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        // $worksheet->setTitle($title);
        $spreadsheet->getProperties()
                ->setTitle($title);
        $spreadsheet->getSheetByName($title);
        $worksheet = $spreadsheet->getActiveSheet();

        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates('O1')
                ->setOffsetX(50)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates('P1')
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
         //REPUBLIC OF THE PHILIPPINES
         $worksheet->mergeCells('A1:W1');
         $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
         // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
         $worksheet->getStyle("A1")->getFont()->setSize(12);
         $worksheet->getStyle('A1:W1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A1:W1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
 
         //CITY OF MANILA
         $worksheet->mergeCells('A2:W2');
         $worksheet->setCellValue("A2", "CITY OF MANILA");
         $worksheet->getStyle("A2:W2")->getFont()->setBold(true);
         $worksheet->getStyle("A2")->getFont()->setSize(14);
         $worksheet->getStyle('A2:W2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A2:W2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
         
         //DEPARTMENT OF ASSESSMENT
         $worksheet->mergeCells('A3:W3');
         $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
         $worksheet->getStyle("A3:W3")->getFont()->setBold(true);
         $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
         $worksheet->getStyle('A3:W3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A3:W3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
 
         //*Title of Report
         $worksheet->mergeCells('A5:W6');
         $worksheet->setCellValue("A5", $title);
         $worksheet->getStyle("A5:W6")->getFont()->setBold(true);
         $worksheet->getStyle("A5")->getFont()->setSize(18);
         $worksheet->getStyle('A5:W6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A5:W6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
         
         //*Date of Report
         $worksheet->mergeCells('A7:W7');
         $worksheet->getStyle('A7:W7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
         $worksheet->setCellValue("A7", $asOfDate);
         $worksheet->getStyle("A7")->getFont()->setSize(12)->setBold(true);
         // $worksheet->setCellValue("B4", $excelPrintText);
         // $worksheet->getStyle("B4")->getFont()->setSize(8)->setItalic(true);
         
         $worksheet->mergeCells('A8:W8');
         $worksheet->getStyle('A8:W8')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
         $worksheet->setCellValue("A8", "printed date:  ". date("F j, Y"));
         $worksheet->getStyle("A8")->getFont()->setSize(8);
 
         //---------------------setup width
         $worksheet->getColumnDimension('A')->setWidth(30);
         $worksheet->getColumnDimension('B')->setWidth(30);
         $worksheet->getColumnDimension('C')->setWidth(15);
         $worksheet->getColumnDimension('D')->setWidth(15);
         $worksheet->getColumnDimension('E')->setWidth(15);
         $worksheet->getColumnDimension('F')->setWidth(24);
         $worksheet->getColumnDimension('G')->setWidth(10);
         $worksheet->getColumnDimension('H')->setWidth(10);
         $worksheet->getColumnDimension('I')->setWidth(11);
         $worksheet->getColumnDimension('J')->setWidth(12);
         $worksheet->getColumnDimension('K')->setWidth(16);
         $worksheet->getColumnDimension('L')->setWidth(16);
         $worksheet->getColumnDimension('M')->setWidth(5);
         $worksheet->getColumnDimension('N')->setWidth(14);
         $worksheet->getColumnDimension('O')->setWidth(9);
         $worksheet->getColumnDimension('P')->setWidth(13);
         $worksheet->getColumnDimension('Q')->setWidth(5);
         $worksheet->getColumnDimension('R')->setWidth(12);
         $worksheet->getColumnDimension('S')->setWidth(12);
         $worksheet->getColumnDimension('T')->setWidth(0);
         $worksheet->getColumnDimension('U')->setWidth(0);
         $worksheet->getColumnDimension('V')->setWidth(12);
         $worksheet->getColumnDimension('W')->setWidth(50);
         $worksheet->getColumnDimension('X')->setWidth(0);
        
         //-----------------------setup merge cell, style and text for header
 
         $worksheet->setCellValue('A10', "Owner");
         $worksheet->setCellValue('B10', "Location");
         $worksheet->setCellValue('C10', "Lot #");
         $worksheet->setCellValue('D10', "Blk #");
         $worksheet->setCellValue('E10', "TCT #");
         $worksheet->setCellValue('F10', "PIN");
         $worksheet->setCellValue('G10', "Effectivity");
         $worksheet->setCellValue('H10', "Inserted Date");
         $worksheet->setCellValue('I10', "Taxability");
         $worksheet->setCellValue('J10', "Update Description");
         $worksheet->setCellValue('K10', "Current ARP");
         $worksheet->setCellValue('L10', "Previous ARP");
         $worksheet->setCellValue('M10', "Kind");
         $worksheet->setCellValue('N10', "Actual Use");
         $worksheet->setCellValue('O10', "Area");
         $worksheet->setCellValue('P10', "Market Value");
         $worksheet->setCellValue('Q10', "AL");
         $worksheet->setCellValue('R10', "Current AV");
         $worksheet->setCellValue('S10', "Previous AV");
         $worksheet->setCellValue('V10', "Variance");
         $worksheet->setCellValue('W10', "Memoranda");
         $worksheet->setCellValue('X10', "Current");

        $styleHeader = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
            'font' => [
                'bold' => true,
            ],

        ];
        $styleBoldFont = [
            'font' => [
                'bold' => true,
            ],
        ];
        

        $worksheet->getStyle('A10:W10')->applyFromArray($styleHeader);
        

        #------------------data content------------------------------
        $styleBordersData = [
            
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
        ];
        $row = 11;
        $rowMinus = 10;
        $rowAdd = 12;
        $rowAdd1 = 13;

        $conditional = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
        $conditional->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
        $conditional->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_LESSTHAN);
        $conditional->addCondition(0);
        $conditional->getStyle()->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $conditional->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $conditional->getStyle()->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

       
        foreach ($data as $key => $val){

       
            $worksheet->setCellValue("A{$row}", $val['Owner']);
            $worksheet->setCellValue("B{$row}", $val['Location']);
            $worksheet->setCellValue("C{$row}", $val['LotNo']);
            $worksheet->setCellValue("D{$row}", $val['BlkNo']);
            $worksheet->setCellValue("E{$row}", $val['TctNo']);
            $worksheet->setCellValue("F{$row}", $val['PIN']);
            $worksheet->setCellValue("G{$row}", $val['Effectivity']);
            $worksheet->setCellValue("H{$row}", $val['InsertedDt']);
            $worksheet->setCellValue("I{$row}", $val['Taxability']);
            $worksheet->setCellValue("J{$row}", $val['UpdateDesc']);
            $worksheet->setCellValue("K{$row}", $val['ARP']);
            $worksheet->setCellValue("L{$row}", $val['Prev_Arp']);
            $worksheet->setCellValue("M{$row}", $val['kind']);
            $worksheet->setCellValue("N{$row}", $val['AUDesc']);
            $worksheet->setCellValue("O{$row}", $val['area']);
            $worksheet->setCellValue("P{$row}", $val['MV']);
            $worksheet->setCellValue("Q{$row}", $val['AL']);
            $worksheet->setCellValue("R{$row}", $val['AV']);
            $worksheet->setCellValue("S{$row}", $val['Prev_Av']);
            $worksheet->setCellValue("V{$row}", $val['Variance']);
            $worksheet->setCellValue("W{$row}", $val['Memoranda']);
            $worksheet->setCellValue("X{$row}", $val['Current']);
            

            $worksheet->getStyle("A{$row}:W{$row}")->applyFromArray($styleBordersData);
               
            $worksheet->getStyle("N{$row}")->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            $worksheet->getStyle("O{$row}:V{$row}")->getNumberFormat()->setFormatCode('#,###,###,###');
            
            

           
            // $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("V{$row}")->getConditionalStyles();
            // $conditionalStyles[] = $conditional;

            // $spreadsheet->getActiveSheet()->getStyle("V{$row}")->setConditionalStyles($conditionalStyles);
            
            
            $row++;
            $rowMinus++;
            $rowAdd++;
            $rowAdd1++;
        }

   

        //wizard style formatting"
        $redStyle = new Style(false, true);
        $redStyle->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()->setARGB(Color::COLOR_RED);
        $redStyle->getFill()
            ->getEndColor()->setARGB(Color::COLOR_RED);
        $redStyle->getFont()->setColor(new Color(Color::COLOR_WHITE));

        $fontRedStyle = new Style(false, true);

        $fontRedStyle->getFont()->setColor(new Color(Color::COLOR_RED));

        $cellRange = "A11:W{$row}";
        $conditionalStyles = [];
        $wizardFactory = new Wizard($cellRange);
        /** @var Wizard\CellValue $cellWizard */
        $cellWizard = $wizardFactory->newRule(Wizard::FORMULA);

        $cellWizard->expression('SEARCH(0,$X1)>0')
        ->setStyle($fontRedStyle);
        $conditionalStyles[] = $cellWizard->getConditional();

        $cellWizard->expression('SEARCH(2,$X1)>0')
        ->setStyle($redStyle);
        $conditionalStyles[] = $cellWizard->getConditional();

        
        $spreadsheet->getActiveSheet()
            ->getStyle($cellWizard->getCellRange())
            ->setConditionalStyles($conditionalStyles);

        $lastRow = $row + 1;
        $diff = $lastRow +1;

        $totalRow = $diff + 1;
        $countRow = $totalRow + 1;
        $countRow1 = $countRow + 1;
        $countRow2 = $countRow1 + 1;
        $worksheet->setCellValue("K{$lastRow}", "TOTAL: ")->getStyle("K{$lastRow}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("O{$lastRow}", "=SUM(O11:O{$row})")->getStyle("O{$lastRow}")->getFont()->setBold(true);
        $worksheet->setCellValue("P{$lastRow}", "=SUM(P11:P{$row})")->getStyle("P{$lastRow}")->getFont()->setBold(true);
        $worksheet->setCellValue("R{$lastRow}", "=SUM(R11:R{$row})")->getStyle("R{$lastRow}")->getFont()->setBold(true);
        $worksheet->setCellValue("S{$lastRow}", "=SUM(S11:S{$row})")->getStyle("S{$lastRow}")->getFont()->setBold(true);
        $worksheet->setCellValue("V{$lastRow}", "=SUM(V11:V{$row})")->getStyle("V{$lastRow}")->getFont()->setBold(true);
        $worksheet->setCellValue("K{$countRow}", "COUNT RPUS :")->getStyle("K{$countRow}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("L{$countRow}", "=COUNTA(K11:K{$row})")->getStyle("K{$countRow}")->getFont();
        
        $worksheet->setCellValue("K{$diff}", "LESS :")->getStyle("K{$diff}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("O{$diff}", "=SUMIF(X11:X{$row},2,O11:O{$row})")->getStyle("O{$diff}")->getFont();
        $worksheet->setCellValue("P{$diff}", "=SUMIF(X11:X{$row},2,P11:P{$row})")->getStyle("P{$diff}")->getFont();
        $worksheet->setCellValue("R{$diff}", "=SUMIF(X11:X{$row},2,R11:R{$row})")->getStyle("R{$diff}")->getFont();
        $worksheet->setCellValue("S{$diff}", "=SUMIF(X11:X{$row},2,S11:S{$row})")->getStyle("S{$diff}")->getFont();
        $worksheet->setCellValue("V{$diff}", "=SUMIF(X11:X{$row},2,V11:V{$row})")->getStyle("V{$diff}")->getFont();
        $worksheet->setCellValue("K{$countRow1}", "LESS")->getStyle("K{$countRow1}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("L{$countRow1}", "=COUNTIF(X11:X{$row}, 2)")->getStyle("U{$countRow1}")->getFont();
        $worksheet->setCellValue("L{$countRow2}", "=L{$countRow}-L{$countRow1}")->getStyle("L{$countRow2}")->getFont()->setBold(true)->setUnderline(true);
        
        $worksheet->setCellValue("O{$totalRow}", "=O{$lastRow}-O{$diff}")->getStyle("O{$totalRow}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("P{$totalRow}", "=P{$lastRow}-P{$diff}")->getStyle("P{$totalRow}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("R{$totalRow}", "=R{$lastRow}-R{$diff}")->getStyle("R{$totalRow}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("S{$totalRow}", "=S{$lastRow}-S{$diff}")->getStyle("S{$totalRow}")->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue("V{$totalRow}", "=V{$lastRow}-V{$diff}")->getStyle("V{$totalRow}")->getFont()->setBold(true)->setUnderline(true);

        $worksheet->getStyle("O{$lastRow}:V{$totalRow}")->getNumberFormat()->setFormatCode('#,###,###,###');
        // $worksheet->getStyle("R{$diff}")->getNumberFormat()->setFormatCode('#,###,###,###');
        
        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("V{$totalRow}")->getConditionalStyles();
        $conditionalStyles[] = $conditional;
        $spreadsheet->getActiveSheet()->getStyle("V{$totalRow}")->setConditionalStyles($conditionalStyles);


        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

}
