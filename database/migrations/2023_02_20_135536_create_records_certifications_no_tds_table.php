<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsCertificationsNoTdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_certifications_no_tds', function (Blueprint $table) {
            $table->id();
            $table->string("ctr_no");
            $table->string("type");
            $table->string("bgy");
            $table->string("owner");
            $table->string("owner_addr")->nullable();
            $table->string("pin");
            $table->string("lot")->nullable();
            $table->string("blk")->nullable();
            $table->string("year");
            $table->string("imp_yr")->nullable();
            $table->text("imp_text")->nullable();
            $table->string("requestor")->nullable();
            $table->text('file_path')->nullable();
            $table->integer("copies");
            $table->string("or_num");
            $table->date('or_date');
            $table->decimal("amount");
            $table->string('certifier_1');
            $table->string('certifier_2');
            $table->string('verifier');
            $table->string('prepared_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_certifications_no_tds');
    }
}
