<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ReleaseTaxDec;

use App\Http\Controllers\Api\EncryptDecryptController;

use Illuminate\Support\Facades\Redis;

use PDF;
use Storage;

use QrCode;
use File;
use Imagick;
use Auth;

class TdLoggingController extends Controller
{

    public function getPdf(Request $request){

        $date           = date("Y-m-d");

        $epoch          = $request['epoch'];
        $td             = $request['td'];
        $or_text        = $request['or_num'];
        $requestor      = $request['requestor'];
        $verifier       = $request['verifier'];
        $bgy            = $request['bgy'];
        $district       = $request['district'];
        $year           = $request['year'];

        $or_file        = $request->file('file');

        $user           = json_decode($request['processed_by'], 1);
        $processed_by   = $user['first_name'] . " " . $user['last_name'];

        $certified_by = "MR. JOSE BONAPARTE HARON R. MAMUTUK";

        $return = ReleaseTaxDec::create(array(
            'td_no'     => $td,
            'requestor' => $requestor,
            // 'printed_by' => $printed_by,
            'printed_by' => $processed_by,
            'certifier' => $certified_by,
            'processed_by' => $processed_by,
        ));

        $insert_id = $return->id;

        $encryptIt = new EncryptDecryptController;
        $barcode_data = $encryptIt->createEncryptedBarcode($insert_id);


        $released_td_dir = 'released_taxdecs_logs';

        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //
        $released_td_dir = 'released_taxdecs_logs_test';
        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //

        if(strpos($request['or_num'], "SR") !== false){
            
            $srcount = ReleaseTaxDec::where('or_no', 'like', $request['or_num'] . date('Y').'%')->count();
            $end_no = 0;

            if($srcount){
                $end_no = $srcount + 1;
            } else{
                $end_no = 1;
            }

            $or_text = $request['or_num'] . date('Y') . sprintf('%04d', $end_no);

        }

        $data = compact(
            'date',
            'epoch',
            'td',
            'or_text',
            'or_file',
            'processed_by',
            'insert_id',
            'barcode_data',
            'released_td_dir',
            'requestor',
            'verifier',
            'bgy',
            'district',
            'year',
        );

        return self::createPdf($data);
    }

    private function createPdf($data){

        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => false,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 6,
            'stretchtext' => 0
        );

        PDF::setPrintHeader(false);
        PDF::AddPage('P', 'A4');
        PDF::SetAutoPageBreak(false, 0);

        // 1st

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79); 
        PDF::MultiCell(218, 10, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, 9, true);
        PDF::SetFont('times', '', 11, '', false);
        PDF::MultiCell(218, 10, 'City of Manila', 0, 'C', 0, 0, 0, 14, true);
        PDF::SetFont('times', 'B', 15, '', false);
        PDF::SetTextColor(255, 102, 102);
        PDF::MultiCell(218, 10, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, 18, true);

        $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
        PDF::Image($logo1, 18, 6, 20, 20, '', '', '', false, 300, '', false, false, 0);

        $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
        PDF::Image($logo2, 173, 6, 21, 21, '', '', '', false, 300, '', false, false, 0);

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79);

        $col_1_start_pos = 20;
        $colon_1_add_pos = $col_1_start_pos + 50;
        $col_1_after_col_pos = $colon_1_add_pos + 6;

        $row_init_pos = 40;
        $row_add_pos = $row_init_pos;

        PDF::MultiCell('', '', 'OR No.', 0, '', 0, 0, $col_1_start_pos, $row_init_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_init_pos, true);
        PDF::MultiCell('', '', $data['or_text'], 0, '', 0, 0, $col_1_after_col_pos, $row_init_pos, true);

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Requestor', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['requestor'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);        

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Verified By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['verifier'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['processed_by'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['barcode_data'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::write1DBarcode($data['barcode_data'], 'C128', 20, $row_add_pos, 150, 17, 0.5, $style, 'N');

        // 2nd

        $row_add_pos = $row_add_pos + 50;

        $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
        PDF::Image($logo1, 18, $row_add_pos, 20, 20, '', '', '', false, 300, '', false, false, 0);

        $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
        PDF::Image($logo2, 173, $row_add_pos, 21, 21, '', '', '', false, 300, '', false, false, 0);

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79); 
        PDF::MultiCell(218, 10, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, $row_add_pos, true);
        PDF::SetFont('times', '', 11, '', false);
        $row_add_pos = $row_add_pos + 5;
        PDF::MultiCell(218, 10, 'City of Manila', 0, 'C', 0, 0, 0, $row_add_pos, true);
        PDF::SetFont('times', 'B', 15, '', false);
        PDF::SetTextColor(255, 102, 102);
        $row_add_pos = $row_add_pos + 4;
        PDF::MultiCell(218, 10, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, $row_add_pos, true);

        $row_add_pos = $row_add_pos + 20;

        PDF::SetFont('times', '', 14, '', false);
        PDF::SetTextColor(54,69,79);

        PDF::MultiCell('', '', 'OR No.', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['or_text'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Requestor', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['requestor'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);        

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Verified By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['verifier'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['processed_by'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['barcode_data'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::write1DBarcode($data['barcode_data'], 'C128', 20, $row_add_pos, 150, 17, 0.5, $style, 'N');

        $fn = $data['td'] . "_" . $data['epoch'] . ".pdf";
        $path = storage_path("app/temp/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        $file = Storage::get("temp/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        $created_pdf = Storage::get("temp/" . $fn);

        Storage::disk('svnas')->putFileAs("/" . $data['released_td_dir'] . "/" . $data['date'] , $temp_file_path, $fn);
        Storage::disk('svnas')->putFileAs("/" . $data['released_td_dir'] . "/" . $data['date'] , $data['or_file'], "or_" . $data['td'] . "_" . $data['epoch'] .".png");

        Storage::delete("temp/$fn");

        $release_td = ReleaseTaxDec::find($data['insert_id']);
            
        $release_td->or_no      = $data['or_text'];
        $release_td->or_path    = "/" . $data['released_td_dir'] . "/" . $data['date'] . "/" . "or_" . $data['td'] . "_" . $data['epoch'] .".png";
        $release_td->file_path  = "/" . $data['released_td_dir'] . "/" . $data['date'] . "/$fn";
        $release_td->barcode    = $data['barcode_data'];
        $release_td->created_at = date('Y-m-d H:i:s');

        $release_td->save();

        $td_output_file = self::getTdFile($data['td'], $data['bgy'], $data['district'], $data['year']);

        foreach($td_output_file as $key => $value){

            PDF::setPrintHeader(false);
            // PDF::AddPage('P', 'LEGAL');
            PDF::AddPage('P', 'A4');
            PDF::SetAutoPageBreak(false, 0);

            // PDF::setJPEGQuality(25);

            // Tax Declaration Image
            $img_file = $value;

            if($value == "Not Exist"){
                $img_file = storage_path("app/not_exist.jpg");
            } else{
                $img_file = storage_path("app/$value");
            }

            $h = 300;

            PDF::Image($img_file, 1, 1, 0, $h, '', '', '', false, 300, '', false, false, 0);

            // if($page_id === "Page 0" && $request['flip1'] == "true"){
            //     // Load the image
            //     $source = imagecreatefromjpeg($img_file);
            //     // Rotate
            //     $rotate = imagerotate($source, 180, 0);
            //     //and save it on your server...
            //     imagejpeg($rotate, storage_path("app/$value"));

            //     PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            // } elseif($page_id === "Page 1" && $request['flip2'] == "true"){
            //     // Load the image
            //     $source = imagecreatefromjpeg($img_file);
            //     // Rotate
            //     $rotate = imagerotate($source, 180, 0);
            //     //and save it on your server...
            //     imagejpeg($rotate, storage_path("app/$value"));

            //     PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            // } else{
            //     PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            // }

            // PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            
        }

        // Copy Files

        $fn = $data['td'] . "_" . $data['epoch'] . ".pdf";
        $path = storage_path("app/temp/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        Storage::delete("temp/forpdf/1" . $data['td'] . ".jpg");
        Storage::delete("temp/forpdf/2" . $data['td'] . ".jpg");

        $file = Storage::get("temp/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        $created_pdf = Storage::get("temp/" . $fn);

        Storage::delete("temp/$fn");

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);




        // $headers = array(
        //     'Content-Description: File Transfer',
        //     'Content-Type: application/octet-stream',
        //     'Content-Disposition: attachment; filename="' . $fn,
        // );

        // return response()->download($temp_file_path, $fn, $headers);

    }

    private function getTdFile($td, $bgy, $district, $yr){

        if($yr == '1996'){

            $bgy = explode("-", $td);
            $search_name = "B-" . (int)$bgy[0] . " " . $yr;

            $redis_data = json_decode(Redis::get('folderStruct'), 1);
            $data = $redis_data[$yr];

            foreach ($data as $key => $val) {
                if(strpos($key, $search_name) !== false){
                    $folder_name = $key;
                    break;
                }

            }

            $exp = explode(' ', $folder_name);

            $z = explode('-', $exp[0])[1];
            $b = explode('-', $exp[1])[1];

            $filenames = self::getFilenames96($td, $yr, $z, $b);

        } else if($yr == '2005'){

            $td_exp = explode("-", $td);

            $search_name = "B-" . sprintf('%03d', $bgy);

            $redis_data = json_decode(Redis::get('folderStruct'), 1);
            $data = $redis_data[$yr];

            foreach ($data as $key => $val) {
                if(strpos($key, $search_name) !== false){
                    $folder_name = $key;
                    break;
                }

            }

            $exp = explode(' ', $folder_name);



            $z = explode('-', $exp[0])[1];
            $b = explode('-', $exp[1])[1];

            $td = "C-" . sprintf('%02d', $z) . "-" . sprintf('%03d', $bgy) . "-" . $td_exp[2];

            $filenames = self::getFilenames05($td, $yr, $z, $b);

        } else{
            $filenames = self::getFilenames($td, $yr, $district);
        }



        return $filenames;


    }


    private function getFilenames($td, $yr, $dst){

        $explode_td = explode('-', $td);
        $getZone = $explode_td[1];

        $zone = "ZONE " . intval($getZone);

        if($dst){
            $zone = $zone . " " . $dst;
        }

        $filenames = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $dir = $redis_data[$yr];

        if(in_array($td . ".pdf", $dir[$zone])){

            if(Storage::disk('svnas')->exists("/taxdecs_img/$yr/$zone/1$td.jpg") &&  Storage::disk('svnas')->exists("/taxdecs_img/$yr/$zone/2$td.jpg")){

                $file1 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$zone/1$td.jpg");
                $file2 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$zone/2$td.jpg");

                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);

            } else{
                $file = Storage::disk('sftp')->get("/$yr/$zone/$td.pdf");
                // $file = Storage::disk('svnas')->get("/taxdecs/$yr/$zone/$td.pdf");

                Storage::put("/temp/$td.pdf", $file);
    
                $path = storage_path("app/temp/$td.pdf");
    
                $imagick = new Imagick();
                $imagick->readimage("$path");
                $imagick->writeImages(storage_path("app/temp/forpdf/" . $td . ".jpg"), false);
    
                $image_path_1 = storage_path("app/temp/forpdf/") . "1" . $td . ".jpg";
                $image_path_2 = storage_path("app/temp/forpdf/") . "2" . $td . ".jpg";
    
                rename( storage_path("app/temp/forpdf/") .  $td . "-0.jpg",  $image_path_1);
                rename( storage_path("app/temp/forpdf/") .  $td . "-1.jpg",  $image_path_2);
    
                Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg"));
                Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/2" . $td . ".jpg", Storage::get("temp/forpdf/2" . $td . ".jpg"));

                Storage::delete("/temp/$td.pdf");
                // Storage::delete("temp/1" . $td . ".jpg");
                // Storage::delete("temp/2" . $td . ".jpg");
    
                // $filenames[] = "/taxdecs_img/$yr/$zone/1$td.jpg";
                // $filenames[] = "/taxdecs_img/$yr/$zone/2$td.jpg";
            }

        } else{
            if(in_array("1" . $td . ".jpg", $dir[$zone])){
                $file1 = Storage::disk('sftp')->get("/$yr/$zone/1$td.jpg");
                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
            } else{
                $filenames[] = "Not Exist";
            }
            
            if(in_array("2" . $td . ".jpg", $dir[$zone])){
                $file2 = Storage::disk('sftp')->get("/$yr/$zone/2$td.jpg");
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);
            } else{
                $filenames[] = "Not Exist";
            }
        }

        $filenames[] = "temp/forpdf/1" . $td . ".jpg";
        $filenames[] = "temp/forpdf/2" . $td . ".jpg";

        return $filenames;

    }

    private function getFilenames96($td, $yr, $zone, $bgy){

        $folder_name = "Z-$zone B-$bgy $yr";

        $filenames = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $dir = $redis_data[$yr];

        if(in_array($td . ".pdf", $dir[$folder_name])){

            if(Storage::disk('svnas')->exists("/taxdecs_img/$yr/$folder_name/1$td.jpg") &&  Storage::disk('svnas')->exists("/taxdecs_img/$yr/$folder_name/2$td.jpg")){

                $file1 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$folder_name/1$td.jpg");
                $file2 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$folder_name/2$td.jpg");

                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);


            } else{
                $file = Storage::disk('sftp')->get("/$yr/$folder_name/$td.pdf");
                // $file = Storage::disk('svnas')->get("/taxdecs/$yr/$zone/$td.pdf");

                Storage::put("/temp/$td.pdf", $file);
    
                $path = storage_path("app/temp/$td.pdf");
    
                $imagick = new Imagick();

                $imagick->readimage("$path");

                $no_pages = $imagick->getNumberImages();

                $imagick->writeImages(storage_path("app/temp/forpdf/" . $td . ".jpg"), false);


    
                if($no_pages > 1){

                    $image_path_1 = storage_path("app/temp/forpdf/") . "1" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . "-0.jpg",  $image_path_1);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg"));

                    $image_path_2 = storage_path("app/temp/forpdf/") . "2" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . "-1.jpg",  $image_path_2);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/2" . $td . ".jpg", Storage::get("temp/forpdf/2" . $td . ".jpg"));
                } else{
                    $image_path_1 = storage_path("app/temp/forpdf/") .  "1" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . ".jpg",  $image_path_1);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg")); 
                    
                    $image_path_2 = "";
                }

                Storage::delete("/temp/$td.pdf");
                // Storage::delete("temp/1" . $td . ".jpg");
                // Storage::delete("temp/2" . $td . ".jpg");
    
                // $filenames[] = "/taxdecs_img/$yr/$zone/1$td.jpg";
                // $filenames[] = "/taxdecs_img/$yr/$zone/2$td.jpg";
            }

        } else{
            $filenames[] = "Not Exist";
        }

        $filenames[] = "temp/forpdf/1" . $td . ".jpg";
        $filenames[] = "temp/forpdf/2" . $td . ".jpg";

        return $filenames;

    }

    private function getFilenames05($td, $yr, $zone, $bgy){

        $folder_name = "Z-$zone B-$bgy";

        $filenames = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $dir = $redis_data[$yr];

        if(in_array($td . ".pdf", $dir[$folder_name])){

            if(Storage::disk('svnas')->exists("/taxdecs_img/$yr/$folder_name/1$td.jpg") &&  Storage::disk('svnas')->exists("/taxdecs_img/$yr/$folder_name/2$td.jpg")){

                $file1 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$folder_name/1$td.jpg");
                $file2 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$folder_name/2$td.jpg");

                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);


            } else{
                $file = Storage::disk('sftp')->get("/$yr/$folder_name/$td.pdf");
                // $file = Storage::disk('svnas')->get("/taxdecs/$yr/$zone/$td.pdf");

                Storage::put("/temp/$td.pdf", $file);
    
                $path = storage_path("app/temp/$td.pdf");
    
                $imagick = new Imagick();

                $imagick->readimage("$path");

                $no_pages = $imagick->getNumberImages();

                $imagick->writeImages(storage_path("app/temp/forpdf/" . $td . ".jpg"), false);


    
                if($no_pages > 1){

                    $image_path_1 = storage_path("app/temp/forpdf/") . "1" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . "-0.jpg",  $image_path_1);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg"));

                    $image_path_2 = storage_path("app/temp/forpdf/") . "2" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . "-1.jpg",  $image_path_2);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/2" . $td . ".jpg", Storage::get("temp/forpdf/2" . $td . ".jpg"));
                } else{
                    $image_path_1 = storage_path("app/temp/forpdf/") .  "1" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . ".jpg",  $image_path_1);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg")); 
                    
                    $image_path_2 = "";
                }

                Storage::delete("/temp/$td.pdf");
                // Storage::delete("temp/1" . $td . ".jpg");
                // Storage::delete("temp/2" . $td . ".jpg");
    
                // $filenames[] = "/taxdecs_img/$yr/$zone/1$td.jpg";
                // $filenames[] = "/taxdecs_img/$yr/$zone/2$td.jpg";
            }

        } else{
            $filenames[] = "Not Exist";
        }

        $filenames[] = "temp/forpdf/1" . $td . ".jpg";
        $filenames[] = "temp/forpdf/2" . $td . ".jpg";

        return $filenames;

    }


}
