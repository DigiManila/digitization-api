<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Storage;
use Response;
use Illuminate\Support\Facades\Redis;

use App\TempTaxDeclarations;
use App\TaxDeclaration;
use App\ReleaseTaxDec;

use \Mpdf\Mpdf as MPDF;

use PDF;
use Imagick;
use ImagickDraw;
// use Barryvdh\DomPDF\Facade\Pdf;

class FileViewerController extends Controller
{

    private $merge_fn_td        = ['1979', '1985', "1996", "2005", "2014", "EXEMPT AND OTHERS"];
    private $sv_pdf             = "td_pdf";
    private $td_has_serial      = false;
    private $faas_has_serial    = true;


    public function getList(Request $request){

        $path = $request->path;
        $lastPath = end($path);

        if($lastPath == "CANCELLATIONS FOLDER" || $lastPath == "CONTROL BOOKS FOLDER"){
            return self::getRootFolder($lastPath);
        } elseif($path[1] == "CONTROL BOOKS FOLDER" && count($path) > 3){
            return self::getControlBookFilename($path);
        } elseif($path[1] == "MANILA RASTER FOLDER"){
            return self::getManilaRasterFolder($path);
        } else{
            return self::getFolderPath($request->path);
        }

    }

    public function getFile(Request $request){

        $path_arr = self::cleanPathArray($request->path);

        if(in_array($path_arr[0], $this->merge_fn_td)){
            return self::getTdFile($path_arr, $request->fn);
        } elseif(strpos($path_arr[0], "MANILA RASTER") !== false || strpos($path_arr[0], "TAX MAP CONTROL ROLL") !== false 
                            || strpos($path_arr[0], "TAX MAP CONTROL MAP") !== false 
                            || strpos($path_arr[0], "LOCATION MAP") !== false) {
            return self::getManilaRaster($path_arr, $request->fn);
        } elseif(strpos($path_arr[0], "CONTROL") !== false) {
            return self::getControlBook($path_arr, $request->fn);
        } else{
            return self::getPdfFile($path_arr, $request->fn);
        }

    }

    private function getFolderPath($arr){

        $path_arr = self::cleanPathArray($arr);
        $path_str = "";

        $tmp_list = [];
        $list = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        foreach ($path_arr as $key => $value) {

            if($tmp_list){
                $tmp_list = $tmp_list[$value];
            } else{
                $tmp_list = $redis_data[$value];
            }

        }

        foreach ($tmp_list as $key => $value) {
            if(is_array($value)){
                $list[] = $key;
            } else{
                $list[] = $value;
            }
        }

        sort($list, SORT_NATURAL);

        $output = [];

        foreach ($list as $key => $value) {
            $output[]['val'] = $value;
        }

        // merge filenames with jpg in TD
        if(in_array($path_arr[0], $this->merge_fn_td) && isset($path_arr[1])){
            return self::mergeFnTd($output);
        }

        return $output;

    }

    private function getRootFolder($folder){

        $list = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        if($folder == "CANCELLATIONS FOLDER"){
            foreach ($redis_data as $key => $value) {
                if(strpos($key, "CANCELLATION") !== false){
                    $list[]['val'] = $key;
                }
            }
        } else if($folder == "CONTROL BOOKS FOLDER"){
            foreach ($redis_data as $key => $value) {
                if(strpos($key, "CONTROL BOOK") !== false || strpos($key, "CONTROL COLUMNAR") !== false){
                    $list[]['val'] = $key;
                }
            }
        }
        
        return $list;

    }

    private function getManilaRasterFolder($arr){

        $path_arr = self::cleanPathArray($arr);
        $path_str = "";

        $tmp_list = [];
        $list = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        foreach ($path_arr as $key => $value) {

            if($tmp_list){
                $tmp_list = $tmp_list[$value];
            } else{
                $tmp_list = $redis_data[$value];
            }

        }

        foreach ($tmp_list as $key => $value) {
            if(is_array($value)){
                $list[] = $key;
            } else{
                $list[] = $value;
            }
        }

        sort($list, SORT_NATURAL);

        $output = [];

        foreach ($list as $key => $value) {

            if (strpos($value, ".") === false) {
                $output[]['val'] = $value;
            } else{
                $ext = pathinfo($value, PATHINFO_EXTENSION);

                if($ext == "jpg" || $ext == "jpeg"){
                    $output[]['val'] = $value;
                }
            }
            
        }

        return $output;

    }

    private function mergeFnTd($arr){

        $fns = [];
        $output = [];

        foreach ($arr as $key => $value) {

            $exp = explode(".", $value['val']);

            if($exp[1] == "pdf"){

                $fns[$exp[0]] = $exp[1];

            } else{

                $tmp = substr($exp[0], 1);

                if(!array_key_exists($exp[0], $fns)){
                    $fns[$tmp] = $exp[1];
                }

            }

        }

        ksort($fns, SORT_NATURAL);

        foreach ($fns as $key => $value) {
            $output[]['val'] = $key . "." . $value;
        }

        return $output;

    }

    private function cleanPathArray($arr){

        $path_arr = $arr;

        if (($key = array_search("home", $path_arr)) !== false) {
            unset($path_arr[$key]);
        }

        if (($key = array_search("CANCELLATIONS FOLDER", $path_arr)) !== false) {
            unset($path_arr[$key]);
        }

        if (($key = array_search("CONTROL BOOKS FOLDER", $path_arr)) !== false) {
            unset($path_arr[$key]);
        }

        return array_values($path_arr);

    }

    private function getPathText($path, $fn){

        $path_text = "/";

        foreach ($path as $key => $value) {
            $path_text = $path_text . $value . "/";
        }

        return $path_text . $fn;

    }

    private function checkIfJpgExist($p, $fn){

        $path = dirname($p);

        $sv_nas_exist = Storage::disk('svnas')->exists("/raw_files$path/1$fn.jpg");

        return $sv_nas_exist;

    }

    // ------------------------------- SHOW FILES -------------------------------- //

    // -------------------------  Tax Declarations ------------------------------- //

    private function getTdFile($path, $fn){

        $new_fn = "";

        $path_text = self::getPathText($path, $fn);

        $exp = explode(".", $fn);

        if($exp[1] == "pdf"){ // Convert to jpg

            // Check if jpg exist
            
            $jpg_exist = self::checkIfJpgExist($path_text, $exp[0]);

            if($jpg_exist){

                $filenames = array("1" . $exp[0] . ".jpg" , "2" . $exp[0] . ".jpg");

            } else{
                
                $new_path = dirname($path_text);
    
                // check if exist in server NAS
                $sv_nas_exist = Storage::disk('svnas')->exists("/raw_files$path_text");
    
                if($sv_nas_exist){
                    $pdf = Storage::disk('svnas')->get("/raw_files$path_text");
                } else{
                    $pdf = Storage::disk('sftp')->get($path_text);
                }
    
                Storage::disk('local')->put("/taxdecs/$new_path/$fn", $pdf);

                $filenames = self::pdfConvertToJpg($fn, $path_text);

            }
            
        } else{

            $filenames = array("1" . $exp[0] . ".jpg", "2" . $exp[0] . ".jpg");
            
        }


        $page_3_exist = Storage::disk('svnas')->exists("/raw_files" . dirname($path_text) ."/3" . $exp[0] . ".jpg");

        if($page_3_exist){
            $filenames[] = "3" . $exp[0] . ".jpg";
        }

        return self::processJpg($filenames, dirname($path_text));

    }

    // Not Used //
    private function tdJpgToPdf($full_path, $fns, $base){

        $path = dirname($full_path);
        $base_name = explode(".", $base)[0];

        foreach ($fns as $key => $value) {

            if(Storage::disk(config('fsconf.sv'))->exists("raw_files/$path/" . $value)){
                $jpg = Storage::disk('svnas')->get("/raw_files$path/$value");
            } else{
                $jpg = Storage::disk('sftp')->get("$path/$value");
            }

            Storage::disk('local')->put("taxdecs$path/" . $value, $jpg);

        }

        $pdf->loadHTML($html);
        $path_pdf = storage_path("app/taxdecs$path")  . "/$base_name.pdf";
        $pdf->save($path_pdf);
        // return $pdf->stream();
        // $out = PDF::Output($path_pdf, 'F');
        // PDF::reset();

        return "taxdecs$path/$base_name.pdf";

    }
    // Not Used //

    private function pdfConvertToJpg($fn, $full_path){

        $filename_arr = [];

        $path = "app" . DIRECTORY_SEPARATOR . "taxdecs" . dirname($full_path) . DIRECTORY_SEPARATOR;

        $imagick = new Imagick();
        $imagick->readimage(storage_path("$path$fn"));

        $num_page = $imagick->getnumberimages();

        $imagick->writeImages(storage_path("$path$fn ". ".jpg"), false);

        $fn_exp = explode(".", $fn);

        // if($num_page > 1){
        for ($i = 0; $i < $num_page; $i++) { 
            $img_fn         = ($i + 1) . $fn_exp[0] . ".jpg";
            $filename_arr[] = $img_fn;
            $image_path     = storage_path($path) . $img_fn;

            if($num_page > 1){
                rename( storage_path($path) .  $fn . " -$i.jpg",  $image_path);
            } else{
                rename( storage_path($path) .  $fn . " .jpg",  $image_path);
            }

            if(!Storage::disk('svnas')->exists("/raw_files" . dirname($full_path) . "/" . $img_fn)){
                Storage::disk('svnas')->putFileAs("/raw_files" . dirname($full_path), storage_path($path) . $img_fn, $img_fn);
            }
        }
        // }

        foreach ($filename_arr as $key => $value) {
            Storage::disk('local')->delete( "taxdecs" . dirname($full_path) . $value);
        }

        Storage::disk('local')->delete("taxdecs$full_path");

        return $filename_arr;

    }

    private function processJpg($fns, $path){

        $output = [];

        foreach ($fns as $key => $value) {

            $pic = null;
            
            $sv_nas_exist = Storage::disk('svnas')->exists("/raw_files$path/$value");
            
            if($sv_nas_exist){

                $pic = Storage::disk('svnas')->get("/raw_files$path/$value");

            } else{
                
                $syn_nas_exist = Storage::disk('sftp')->exists("$path/$value");

                if($syn_nas_exist){
                    $pic = Storage::disk('sftp')->get("$path/$value");
                    Storage::disk('local')->put("/taxdecs/$path/$value", $pic);
                    Storage::disk('svnas')->putFileAs("/raw_files$path" , storage_path("app/taxdecs$path/$value"), $value);
                    Storage::disk('local')->delete("taxdecs$path/$value");
                } else{
                    continue;
                }

            }

            if($this->td_has_serial){
                $output[] = self::tdPutSerial($path, $value);
            } else{
                $output[] = "data:image/jpeg;base64," . base64_encode($pic);
            }

        }

        return $output;

    }

    private function tdPutSerial($path, $fn){

        $pic64 = "";

        $base_fn = explode(".", $fn);

        $pic = Storage::disk('svnas')->get("/raw_files$path/$fn");
        Storage::disk('local')->put("/taxdecs/$path/$fn", $pic);

        $image  = new Imagick(storage_path("app/taxdecs/$path/$fn"));
        $draw   = new ImagickDraw();

        $draw->setFont('Arial');
        $draw->setFontSize(10);
        $draw->setFillColor('black');
        $draw->setGravity(Imagick::GRAVITY_SOUTHEAST);

        $image->annotateImage($draw, 10, 12, 0, $base_fn[0]);

        $pic64 = "data:image/jpeg;base64," . base64_encode($image);

        Storage::disk('local')->delete("taxdecs$path/$fn");

        return $pic64;

    }

    // -------------------------  Tax Declarations ------------------------------- //

    // --------------------------------------------------------------------------- //

    // --------------------------------- PDFs ------------------------------------ //

    private function getPdfFile($path, $fn){

        $path_text = self::getPathText($path, $fn);

        // check if exist in server NAS
        $sv_nas_exist = Storage::disk('svnas')->exists("/raw_files$path_text");

        if(!$sv_nas_exist){
            $sftp_pdf = Storage::disk('sftp')->get($path_text);
            Storage::disk('local')->put("$path_text", $sftp_pdf);
            Storage::disk('svnas')->putFileAs("/raw_files" . dirname($path_text) , storage_path("app$path_text"), $fn);
            Storage::disk('local')->delete("$path_text");
        }

        $pdf = Storage::disk('svnas')->get("/raw_files$path_text");

        if(strpos($path[0], "FAAS") !== false && $this->faas_has_serial){

            Storage::disk('local')->put("$path_text", $pdf);
            return self::faasSerial(dirname($path_text), $fn);

        }

        $fs = Storage::disk(config('fsconf.sv'))->getDriver();
        $stream = $fs->readStream("/raw_files$path_text");
        
        return Response::stream(function() use($stream) {
            fpassthru($stream);
        }, 200, [
            "Content-Type" => "application/pdf",
            "Content-disposition" => "attachment; filename=\"" . $fn . "\"",
        ]);

    }

    private function faasSerial($path, $fn){

        $img_fn = self::createFaasSerialToImage($path, $fn);

        $page_count = 0;

        $pdftext = file_get_contents(storage_path("app$path/$fn"));
        $page_count = preg_match_all("/\/Page\W/", $pdftext, $dummy);

        $pdftext = null;

        $config = [
            'tempDir' => storage_path("app/temp"),
        ];

        $mpdf = new MPDF($config);

        $mpdf->setSourceFile(storage_path("app$path/$fn"));
        $mpdf->autoLangToFont = true;

        for ($i=1; $i <= $page_count; $i++) { 

            $mpdf->AddPage();

            $tplIdx = $mpdf->importPage($i);
    
            $mpdf->useTemplate($tplIdx, 0, 0, 200);

            // $mpdf->Image(storage_path("app$path/$img_fn"), 70, 250, 20, 3, 'png', '', true, false);

            $mpdf->SetXY(190, 275);

            // $serial = 'MDAR' . date('Ymd');
            // $mpdf->showWatermarkText = true;
            // $html = '<span style="position:absolute;rotate:90;">' . $serial . '</span>';
            $html = '<img src="' . storage_path("app$path/$img_fn") . '" id="pmObj-1" class="shape obj" style="transform: rotate(90deg); position: fixed; right: 0mm; bottom: 0mm;">';
            $mpdf->WriteHTML($html);
            // $mpdf->WriteFixedPosHTML($html, 0, 0, 17, 1);
            
            // $mpdf->Write(0, 'MDAR' . date('Ymd'));
        }

        $mpdf->Output(storage_path("app$path/test_$fn"));

        $file = Storage::get("$path/test_$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        Storage::delete("$path/$fn");
        Storage::delete("$path/test_$fn");
        Storage::delete("$path/$img_fn");

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);

        // \Log::info($path);
        // \Log::info($fn);

    }

    private function createFaasSerialToImage($path, $fn){

        $string = ' MDAR' . date('Ymd') . ' ';                                            
        $font   = 4;
        $width  = ImageFontWidth($font) * strlen($string);
        $height = ImageFontHeight($font);
        $im = @imagecreate ($width,$height);
        $background_color = imagecolorallocate ($im, 105, 105, 105); 
        $text_color = imagecolorallocate ($im, 255, 255, 255);
        imagestring ($im, $font, 0, 0, $string, $text_color);
        ob_start();
        imagepng($im);
        $imstr = ob_get_clean();

        Storage::disk('local')->put("$path/serial_" . pathinfo($fn, PATHINFO_FILENAME) . ".png", $imstr);

        imagedestroy($im);
        return "serial_" . pathinfo($fn, PATHINFO_FILENAME) . ".png";
    }

    // --------------------------------- PDFs ------------------------------------ //

    // --------------------------------------------------------------------------- //

    // ----------------------------- Control Books ------------------------------- //

    private function getControlBookFilename($path){

        $list = [];
        $path_arr = self::cleanPathArray($path);

        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        $control_book_list = $redis_data[$path_arr[0]][$path_arr[1]];

        if(strpos($path_arr[0], "1970") !== false){
            foreach ($control_book_list as $key => $value) {
                $list[]['val'] = $value;
            }
        } else{

            $new_fn = [];

            foreach ($control_book_list as $key => $value) {
                
                $pos = strpos($value, ".") -1;

                $letter = substr($value, $pos, 1);
                $no     = substr($value, 0, $pos);
                
                $fn = self::getFilenameControlBook($no, $letter);

                if($fn){
                    $new_fn[] = $fn;
                }
            }

            sort($new_fn, 6);

            foreach ($new_fn as $key => $value) {
                $list[]['val'] = $value;
            }

        }

        return $list;

    }

    private function getFilenameControlBook($no, $letter){

        switch ($letter) {
            case 'A':
                return $no . ".jpg";
                break;

            case 'C':
                return $no . "_2.jpg";
                break;

            case 'E':
                return $no . "_3.jpg";
                break;

            case 'G':
                return $no . "_4.jpg";
                break;

            case 'I':
                return $no . "_5.jpg";
                break;

            case 'K':
                return $no . "_6.jpg";
                break;

            case 'M':
                return $no . "_7.jpg";
                break;

            case 'O':
                return $no . "_8.jpg";
                break;

            case 'Q':
                return $no . "_9.jpg";
                break;

            case 'S':
                return $no . "_10.jpg";
                break;

            case 'U':
                return $no . "_11.jpg";
                break;

            case 'W':
                return $no . "_12.jpg";
                break;

            case 'Y':
                return $no . "_13.jpg";
                break;

            default:
                return false;
                break;
        }

    }

    private function getDisplayFnCb($fn){

        $base_fn = pathinfo($fn)['filename'];

        if(strpos($base_fn, "_") !== false){

            $exp = explode("_", $base_fn);

            switch ($exp[1]) {
                case '2':
                    return array(
                        $exp[0] . "C.jpg",
                        $exp[0] . "D.jpg"
                    );
                    break;

                case '3':
                    return array(
                        $exp[0] . "E.jpg",
                        $exp[0] . "F.jpg"
                    );
                    break;

                case '4':
                    return array(
                        $exp[0] . "G.jpg",
                        $exp[0] . "H.jpg"
                    );
                    break;

                case '5':
                    return array(
                        $exp[0] . "I.jpg",
                        $exp[0] . "J.jpg"
                    );
                    break;

                case '6':
                    return array(
                        $exp[0] . "K.jpg",
                        $exp[0] . "L.jpg"
                    );
                    break;

                case '7':
                    return array(
                        $exp[0] . "M.jpg",
                        $exp[0] . "N.jpg"
                    );
                    break;

                case '8':
                    return array(
                        $exp[0] . "O.jpg",
                        $exp[0] . "P.jpg"
                    );
                    break;

                case '9':
                    return array(
                        $exp[0] . "Q.jpg",
                        $exp[0] . "R.jpg"
                    );
                    break;

                case '10':
                    return array(
                        $exp[0] . "S.jpg",
                        $exp[0] . "T.jpg"
                    );
                    break;

                case '11':
                    return array(
                        $exp[0] . "U.jpg",
                        $exp[0] . "V.jpg"
                    );
                    break;

                case '12':
                    return array(
                        $exp[0] . "W.jpg",
                        $exp[0] . "X.jpg"
                    );
                    break;

                case '13':
                    return array(
                        $exp[0] . "Y.jpg",
                        $exp[0] . "Z.jpg"
                    );
                    break;

                default:
                    return 0;
                    break;
            }

        } else{

            return array(
                $base_fn . "A.jpg",
                $base_fn . "B.jpg"
            );

        }


    }

    private function getControlBook($path, $fn){

        $path_text = self::getPathText($path, "");

        if(strpos($path_text, "1970") !== false){
            $fns = array($fn);
        } else{     
            $fns = self::getDisplayFnCb($fn);
        }

        $pic = null;
            
        foreach ($fns as $key => $value) {

            $pic = null;
            
            $sv_nas_exist = Storage::disk('svnas')->exists("/raw_files$path_text/$value");
            
            if($sv_nas_exist){

                $pic = Storage::disk('svnas')->get("/raw_files$path_text/$value");

            } else{
                
                $syn_nas_exist = Storage::disk('sftp')->exists("$path_text/$value");

                if($syn_nas_exist){
                    $pic = Storage::disk('sftp')->get("$path_text/$value");
                    Storage::disk('local')->put("$path_text/$value", $pic);
                    Storage::disk('svnas')->putFileAs("/raw_files$path_text" , storage_path("app$path_text/$value"), $value);
                    Storage::disk('local')->delete("$path_text/$value");
                } else{
                    continue;
                }

            }

            $output[] = "data:image/jpeg;base64," . base64_encode($pic);
            
        }

        return $output;
    }

    // ----------------------------- Control Books ------------------------------- //

    // --------------------------------------------------------------------------- //

    // ----------------------------- Manila Raster ------------------------------- //

    private function getManilaRaster($path, $fn){

        $path_text = self::getPathText($path, "");

        $pic = null;
            
        $sv_nas_exist = Storage::disk('svnas')->exists("/raw_files$path_text$fn");
        
        if($sv_nas_exist){

            $pic = Storage::disk('svnas')->get("/raw_files$path_text$fn");

        } else{
            
            $syn_nas_exist = Storage::disk('sftp')->exists("$path_text$fn");

            if($syn_nas_exist){
                $pic = Storage::disk('sftp')->get("$path_text$fn");
                Storage::disk('local')->put("$path_text$fn", $pic);
                Storage::disk('svnas')->putFileAs("/raw_files$path_text" , storage_path("app$path_text$fn"), $fn);
                Storage::disk('local')->delete("$path_text$fn");
            }

        }

        // return "OK";

        // return $pic;

        return "data:image/jpeg;base64," . base64_encode($pic);
        
    }

    // ----------------------------- Manila Raster ------------------------------- //


}
