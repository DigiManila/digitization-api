<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingNormalFlowTransTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_normal_flow_trans_types', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('trans_id');
            $table->string("transaction_type");
            $table->string("kind");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_normal_flow_trans_types');
    }
}
