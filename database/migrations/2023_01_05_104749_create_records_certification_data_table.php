<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsCertificationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_certification_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cert_id');
            $table->string('kind')->nullable();
            $table->string('pin')->nullable();
            $table->string('lot')->nullable();
            $table->string('blk')->nullable();
            $table->string('area')->nullable();
            $table->string('yr_fr')->nullable();
            $table->string('yr_to')->nullable();
            $table->string('mv')->nullable();
            $table->string('av')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_certification_data');
    }
}
