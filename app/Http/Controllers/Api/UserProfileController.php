<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\TempTaxDeclarations;
use App\TaxDeclaration;
use App\ViewDatabuildUpUserWorks;
use App\ViewAverageEncodedRanking;
use App\ViewAverageProofreadRanking;
use App\ViewDiffCount96;
use App\User;


use Carbon\Carbon;
use Auth;

class UserProfileController extends Controller
{
      // ----- dependencies for getUserData() ----- //


    public function totalEncoded(Request $request){

        $user_id = $request['user_id'];
        $status = $request['user_type'];
        $data = [];

        if ($status == 'Encoder'){
            $status = 1;

            $get1979 = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(current_arp)) AS encoded79, MAX(created_at) as latestDate")
            ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'A-' )
            ->where( 'created_by', $user_id)
            ->where('status',$status)
            ->groupBy('created_by')
            ->first();

            if ($get1979 != null){

            
                
                $get1979['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1979['latestDate']));      
            }
            else
            {
            
                $get1979['encoded79']  = 0;
                $get1979['latestDate']  = "n/a";
            }


            $get1985 = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(current_arp)) AS encoded85, MAX(created_at) as latestDate")
                        ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'B-' )
                        ->where( 'created_by', $user_id)
                        ->where('status',$status)
                        ->groupBy('created_by')
                        ->first();

            if ($get1985 != null){

            
                
                $get1985['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1985['latestDate']));      
            }
            else
            {
            
                $get1985['encoded85']  = 0;
                $get1985['latestDate']  = "n/a";
            }


            $get1996 = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(pdf_filename_96)) AS encoded96, MAX(created_at) as latestDate")

                        ->where( 'created_by', $user_id)
                        ->where('status',$status)
                        ->groupBy('created_by')
                        ->first();

            if ($get1996 != null){
            
                
                $get1996['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1996['latestDate']));
            }
            else
            {
            
                $get1996['encoded96']  = 0;
                $get1996['latestDate']  = "n/a";
            }

            $getOthers = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(current_arp)) AS others, MAX(created_at) as latestDate")
            ->where(function($query) {
                $query->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'A-')
                            ->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'B-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '96-' )
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '97-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '98-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '99-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','00-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','01-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','02-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','03-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','04-')
                            ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','05-');
                            
                        })
                            ->where('pdf_filename_96','!=', null )
                            ->where( 'created_by', $user_id)
                            ->where('status',$status)
                            ->groupBy('created_by')
                            ->first();

            if ($get1996 != null){
            
                
                $getOthers['latestDate']  = date('m-d-Y g:i:s A', strtotime($getOthers['latestDate']));
            }
            else    
            {
                
                $getOthers['others']  = 0;
                $getOthers['latestDate']  = "n/a";
            }
                            
        }

        else if ($status == 'ProofReader')
        {
            $status = 2;

            $get1979 = TaxDeclaration::selectRaw("proofread_by,  COUNT(pdf_filename_96) AS encoded79, MAX(created_at) as latestDate")
            ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'A-' )
            ->where( 'proofread_by', $user_id)
            ->where('status',$status)
            ->groupBy('proofread_by')
            ->first();

            if ($get1979 != null){

            
                
                $get1979['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1979['latestDate']));      
            }
            else
            {
            
                $get1979['encoded79']  = 0;
                $get1979['latestDate']  = "n/a";
            }


            $get1985 = TaxDeclaration::selectRaw("proofread_by,  COUNT(pdf_filename_96) AS encoded85, MAX(created_at) as latestDate")
                        ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'B-' )
                        ->where( 'proofread_by', $user_id)
                        ->where('status',$status)
                        ->groupBy('proofread_by')
                        ->first();

            if ($get1985 != null){

            
                
                $get1985['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1985['latestDate']));      
            }
            else
            {
            
                $get1985['encoded85']  = 0;
                $get1985['latestDate']  = "n/a";
            }

            $get1996 = TaxDeclaration::selectRaw("proofread_by, COUNT(pdf_filename_96) AS encoded96, MAX(created_at) as latestDate")
                        ->where( 'proofread_by', $user_id)
                        ->where('status',$status)
                        ->groupBy('proofread_by')
                        ->first();

            if ($get1996 != null){
            
                
                $get1996['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1996['latestDate']));
            }
            else
            {
            
                $get1996['encoded96']  = 0;
                $get1996['latestDate']  = "n/a";
            }

            $getOthers['others']  = 0;
            $getOthers['latestDate']  = "n/a";

        }
        else if ($status == "Verifier")
        {
            $status = 3;
        }
        else
        {
            $status = 0;
        }

       
                          
        $data[] = array(
            $get1979,
            $get1985,
            $get1996,
            $getOthers
        );        

        return $data;
    }

    public function rangedDate(Request $request){

       

        $user_id = $request['user_id'];
        $status = $request['user_type'];

        // $rangeDate= [];

        $rangeDate['startDate'] = $request['rangeDate'][0] . ' 00:00:00';
        $rangeDate['endDate'] = $request['rangeDate'][1] . ' 23:59:59';

        // \Log::info($rangeDate);

        if ($status == 'Encoder'){
            $status = 1;
            $query =TempTaxDeclarations:: select('current_arp')
                ->where( 'created_by', $user_id)
                ->where('status',$status) 
                ->whereBetween('created_at', [$rangeDate['startDate'], $rangeDate['endDate']])
                ->distinct()    
                ->count('current_arp');
        }
        else if ($status == 'ProofReader')
        {
            $status = 2;

            $query =TaxDeclaration:: select('pdf_filename_96')
                ->where( 'proofread_by', $user_id)
                ->where('status',$status) 
                ->whereBetween('created_at', [$rangeDate['startDate'], $rangeDate['endDate']])
                ->distinct()    
                ->count('pdf_filename_96');
        }
        else if ($status == "Verifier")
        {
            $status = 3;
        }
        else
        {
            $status = 0;
        }
       

        

        // \Log::info($query);

        return $query;

    }

    //dashboard function

    

    public function getDataUser(){

       
        $average_rank = self::getAverageRank();
        $self_rank = self::getSelfRank();
        $encoded_per_day = self::getEncodePerDay();
        $updated_per_day = self::getUpdatedPerDay();
        $encoded_per_week = self::getWeeklyEncoded();
        $encoded_per_month = self::getMonthlyEncoded();
        
        
        return response()->json([
            
            "data" => compact(
                
                
                'average_rank',
                'self_rank',
                'encoded_per_day',
                'updated_per_day',
                'encoded_per_week',
                'encoded_per_month',
                

                
                
            ),
            'message' => 'Dashboard Retrieved Successfully',
            'status' => 1
        ], 200);
    }

 

    private function getSelfRank() {
        
        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];
        
        if ($user_type == "Encoder"){

            $query = ViewAverageEncodedRanking::select('ranking','average')
                                            ->where('created_by', Auth::user()->id )
                                            ->first();

        }
        else if ($user_type == "ProofReader"){
            $query = ViewAverageProofreadRanking::select('ranking','average')
                                            ->where('proofread_by', Auth::user()->id )
                                            ->first();
        }
        else if ($user_type == "Verifier"){
            $query = ViewAverageProofreadRanking::select('ranking','average')
                                            ->where('verified_by', Auth::user()->id )
                                            ->first();
        }
                                    
        $query['ranking'] = $query['ranking'];
        $query['average'] = number_format($query['average'],2,".",",");
        
        return $query;
    }

    
    private function getAverageRank(){

        $averageRank = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){

            $query = ViewAverageEncodedRanking::select('fName','number_works','login_date','average','ranking')
                                        ->get();

        }
        else if ($user_type == "ProofReader"){
            $query = ViewAverageProofreadRanking::select('fName','number_works','login_date','average','ranking')
                                        ->get();
        }
        
        foreach($query as $key => $value){
            $averageRank[] = array(
                'fname' => ucwords(strtolower($value['fName'])),
                'number_works' => $value['number_works'],
                'login_date' => $value['login_date'],
                'average' => number_format($value['average'],2,".",","),
                'ranking' => $value['ranking'],

            );
        }

        return $averageRank;

    }

    private function getEncodePerDay(){

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){


            $query = TempTaxDeclarations::select("pdf_filename_96")
                        ->where( 'created_by',  Auth::user()->id)
                        ->where( 'status',  1)
                        ->whereDate('created_at', Carbon::today())
                        ->distinct()
                        ->count();
            
                        
            
                    $data['dailyTaxDec']= $query;
                
        }
        else if($user_type == "ProofReader"){

            $query = TaxDeclaration::select('pdf_filename_96')
                        ->where( 'proofread_by',  Auth::user()->id)
                        ->whereDate('created_at', Carbon::today())
                        ->distinct()
                        ->count('pdf_filename_96');
            
            $data['dailyTaxDec']= $query;
        }

        else if($user_type == "Verifier"){

            $query = TaxDeclaration::select('pdf_filename_96')
                        ->where( 'verified_by',  Auth::user()->id)
                        ->whereDate('created_at', Carbon::today())
                        ->distinct()
                        ->count('pdf_filename_96');
            
            $data['dailyTaxDec']= $query;
        }
                    
        
        return $data;

    }

    private function getUpdatedPerDay(){

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){

            $today = substr(Carbon::today(),0,10);
                
            $query = TempTaxDeclarations::select("pdf_filename_96")
                        ->where( 'created_by',  Auth::user()->id)
                        ->where( 'status',  1)
                        ->whereRaw('created_at <> updated_at' )
                        ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $today )
                        // ->whereRaw('updated_at',Carbon::today() )
                        
                        ->distinct()
                        ->count();
        
                        
                    $data['updatedTaxDec']= $query;
                  
        }
        else if($user_type == "ProofReader"){

            $query = TaxDeclaration::select('pdf_filename_96')
                        ->where( 'proofread_by',  Auth::user()->id)
                        ->whereDate('updated_at', Carbon::today())
                        ->distinct()
                        ->count('pdf_filename_96');
            
            $data['updatedTaxDec']= $query;
        }

        else if($user_type == "Verifier"){

            $query = TaxDeclaration::select('pdf_filename_96')
                        ->where( 'verified_by',  Auth::user()->id)
                        ->whereDate('updated_at', Carbon::today())
                        ->distinct()
                        ->count('pdf_filename_96');
            
            $data['updatedTaxDec']= $query;
        }
                    
        return $data;

    }

    private function getWeeklyEncoded(){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){

            $query = TempTaxDeclarations::select("pdf_filename_96")
                ->where( 'created_by',  Auth::user()->id)
                ->where('status', 1)
                ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])       
                ->distinct()
                ->count();

            $data['weeklyTaxDec']=$query;
        
        }
        else if($user_type == "ProofReader")
        {
            $query = TaxDeclaration::select('pdf_filename_96')
            ->where( 'proofread_by',  Auth::user()->id)
            ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
            ->distinct()       
            ->count('pdf_filename_96');

            $data['weeklyTaxDec']=$query;
        }
        else if($user_type == "Verifier")
        {
            $query = TaxDeclaration::select('pdf_filename_96')
            ->where( 'verified_by',  Auth::user()->id)
            ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
            ->distinct()       
            ->count('pdf_filename_96');

            $data['weeklyTaxDec']=$query;
        }


        return $data;
    }

    private function getMonthlyEncoded(){

        $month = Carbon::today()->month;

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){

            $query = TempTaxDeclarations::select("pdf_filename_96")
                ->where( 'created_by',  Auth::user()->id)
                ->whereMonth('created_at', $month)
                ->distinct()
                ->count();

                
            $data['monthlyTaxDec']=$query;
        }
        else if ($user_type == "ProofReader"){
            $query = TaxDeclaration::select('pdf_filename_96')
                ->where( 'proofread_by',  Auth::user()->id)
                ->whereMonth('created_at', $month)
                ->distinct()
                ->count('pdf_filename_96');

                $data['monthlyTaxDec']=$query;
        }

        else if ($user_type == "Verifier"){
            $query = TaxDeclaration::select('pdf_filename_96')
                ->where( 'verified_by',  Auth::user()->id)
                ->whereMonth('created_at', $month)
                ->distinct()
                ->count('pdf_filename_96');

                $data['monthlyTaxDec']=$query;
        }
        
        return $data;
    }

    public function getDatePick(Request $request){

        $data = [];
        $expDateMonth = explode('-',$request->sel);

        $year = $expDateMonth[0];
        $month = $expDateMonth[1];
         
         $data['pick_encoded_per_day']   = self::getPickEncodedPerDay($request->sel);
         $data['pick_updated_per_day']   = self::getPickUpdatedPerDay($request->sel);
         $data['pick_encoded_per_week']  = self::getPickWeeklyEncoded($request->sel);
         $data['pick_encoded_per_month'] = self::getPickMonthlyEncoded($year, $month);

         
 
         
         return $data;
        
     }
 
    private function getPickEncodedPerDay($e){

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){
        
            
            $query = TempTaxDeclarations::select("pdf_filename_96")
                                    ->where( 'created_by',  Auth::user()->id)
                                    ->where( 'status',  1)
                                    ->whereDate('created_at', $e)
                                    ->distinct()
                                    ->count();

            
            $data['dailyTaxDec']=$query;
            $data['today'] = date('M. d, Y', strtotime($e));; 
        }
        else if ($user_type == "ProofReader"){
            $query = TaxDeclaration::select('pdf_filename_96')
                                    ->where( 'proofread_by',  Auth::user()->id)
                                    ->whereDate('created_at', $e)
                                    ->distinct()
                                    ->count();
                      
            $data['dailyTaxDec']=$query;
            $data['today'] = date('M. d, Y', strtotime($e)); 
        }

        else if ($user_type == "Verifier"){
            $query = TaxDeclaration::select('pdf_filename_96')
                                    ->where( 'verified_by',  Auth::user()->id)
                                    ->whereDate('created_at', $e)
                                    ->distinct()
                                    ->count();
                      
            $data['dailyTaxDec']=$query;
            $data['today'] = date('M. d, Y', strtotime($e)); 
        }

        
        
        return $data;

    }

    private function getPickUpdatedPerDay($e){

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){
            

            $query = TempTaxDeclarations::select("pdf_filename_96")
                                    ->where( 'created_by',  Auth::user()->id)
                                    ->whereRaw('created_at <> updated_at' )
                                    ->where( 'status',  1)
                                    ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                                    ->distinct()
                                    ->count();

            
            $data['updatedTaxDec']=$query;
            $data['today'] = date('M. d, Y', strtotime($e));; 
        }
        else if ($user_type == "ProofReader"){
            $query = TaxDeclaration::select('pdf_filename_96')
                                    ->whereRaw('created_at <> updated_at' )
                                    ->where( 'proofread_by',  Auth::user()->id)
                                    ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                                    ->distinct()
                                    ->count();
                      
            $data['updatedTaxDec']=$query;
            $data['today'] = date('M. d, Y', strtotime($e)); 
        }

        else if ($user_type == "Verifier"){
            $query = TaxDeclaration::select('pdf_filename_96')
                                    ->where( 'verified_by',  Auth::user()->id)
                                    ->whereRaw('created_at <> updated_at' )
                                     ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                                    ->distinct()
                                    ->count();
                      
            $data['updatedTaxDec']=$query;
            $data['today'] = date('M. d, Y', strtotime($e)); 
        }

        
        
        return $data;

    }
 
    private function getPickWeeklyEncoded($e){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){

            $query = TempTaxDeclarations::select('pdf_filename_96')
                ->where( 'created_by',  Auth::user()->id)
                ->whereBetween('created_at', [Carbon::parse($e)->startOfWeek()->format('Y-m-d'), 
                    Carbon::parse($e)->endOfWeek()->format('Y-m-d')])
                ->distinct()
                ->count();
             
    
            $data['weeklyTaxDec']=$query;
            $data['week'] = Carbon::parse($e)->weekOfMonth;
        }
        else if ($user_type == "ProofReader"){
            $query = TaxDeclaration::select("pdf_filename_96") 
                ->where( 'proofread_by',  Auth::user()->id)
                ->whereBetween('created_at', [Carbon::parse($e)->startOfWeek()->format('Y-m-d'), 
                    Carbon::parse($e)->endOfWeek()->format('Y-m-d')])
                ->distinct()
                ->count();
            
            $data['weeklyTaxDec']=$query;
            $data['week'] = Carbon::parse($e)->weekOfMonth;
        }

        else if ($user_type == "Verifier"){
            $query = TaxDeclaration::select("pdf_filename_96") 
                ->where( 'verified_by',  Auth::user()->id)
                ->whereBetween('created_at', [Carbon::parse($e)->startOfWeek()->format('Y-m-d'), 
                    Carbon::parse($e)->endOfWeek()->format('Y-m-d')])
                ->distinct()
                ->count();
            
            $data['weeklyTaxDec']=$query;
            $data['week'] = Carbon::parse($e)->weekOfMonth;
        }
            
        return $data;
    }
 
    private function getPickMonthlyEncoded($year, $month){

        $data = [];

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();
        $user_type = $user_type["profile"]["user_type"];

        if ($user_type == "Encoder"){

            $query = TempTaxDeclarations::select("pdf_filename_96")
                                    ->where( 'created_by',  Auth::user()->id) 
                                    ->whereMonth('created_at', '=', $month)
                                    ->whereYear('created_at', '=', $year)
                                    ->distinct()
                                    ->count();
            
            $data['monthlyTaxDec']=$query;
            $data['month'] = Carbon::createFromDate($month)->format('F');
        }
        
        else if ($user_type == "ProofReader"){
            $query = TaxDeclaration::select('pdf_filename_96')
                                    ->where( 'proofread_by',  Auth::user()->id) 
                                    ->whereMonth('created_at', '=', $month)
                                    ->whereYear('created_at', '=', $year)
                                    ->distinct()
                                    ->count();
            $data['monthlyTaxDec']=$query;
            $data['month'] = Carbon::createFromDate($month)->format('F');
        }

        else if ($user_type == "Verifier"){
            $query = TaxDeclaration::select('pdf_filename_96')
                                    ->where( 'proofread_by',  Auth::user()->id) 
                                    ->whereMonth('created_at', '=', $month)
                                    ->whereYear('created_at', '=', $year)
                                    ->distinct()
                                    ->count();
            $data['monthlyTaxDec']=$query;
            $data['month'] = Carbon::createFromDate($month)->format('F');
        }
        
        
        return $data;
    }
     
    


   
}
