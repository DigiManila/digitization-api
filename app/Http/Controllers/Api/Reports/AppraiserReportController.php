<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ActiveAssessmentRoll;
use App\CancelledAssessmentRoll;
use App\RptasFaasMastextn;
use App\RptasTaxdecMastExtn;
use App\CancelledTaxdecAssmnt;
use App\RptasTaxdecMastMla;
use App\ReportAssessmentRoll;

use PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\Wizard;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;


class AppraiserReportController extends Controller
{   

    public function postAppraiser(Request $request){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        
        $year = null;
        $month = null;
        $rangeDate = null;
       


        if( $request->datePicker == 'Yearly'){
            $year = $request->date;
        }
        else if( $request->datePicker == 'Range'){
            $rangeDate = $request->date;
            
        }
        else {
            $explodeDayMonthYear = explode('-',$request->date);

            $year = $explodeDayMonthYear[0];
            $month = $explodeDayMonthYear[1];
        
        }
  
        
        $queryAppraiser = ReportAssessmentRoll::select('AppraisedBy','AppraisedDt')
                    ->when($request->datePicker == 'Daily', function($query) use ($request){
                        
                        return $query->whereDate('AppraisedDt', $request->date );
                    })

                    ->when($request->datePicker == 'weekly', function($query) use ($request){
                            
                        return $query->whereBetween('AppraisedDt', 
                        [Carbon::parse($request->date)->startOfWeek(),
                        Carbon::parse($request->date)->endOfWeek()]);
                    })
                    
                    ->when($request->datePicker == 'Monthly', function($query) use ($month, $year){
                         
                        return $query->whereMonth('AppraisedDt',  $month)
                                    ->whereYear('AppraisedDt', $year);                                        
                    })
                    
                    ->when($request->datePicker == 'Yearly', function($query) use ( $year){                            
                       
                        return $query->whereYear('AppraisedDt',  
                                Carbon::createFromFormat('Y',$year));
                    })
                    ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                

                        return $query->whereBetween('AppraisedDt',   $rangeDate);
                                        
                    })
                    ->orderBy('AppraisedBy')
                    ->distinct('AppraisedBy')
                    ->get();

        
        return $queryAppraiser;

    }

    public function getAppraiserReport(Request $request){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
        $firstMonth = null;
        $endMonth = null;
        $quarter = $request->quarter;
        $year = null;
        $month = null;
        $rangeDate = null;
        $taxability = "T";

        if($request->taxability == "Exempted"){
            $taxability = "E";
        }
        

        if ($quarter != ""){
            if ($request->quarter == "1st"){
                $firstMonth = "01";
                $endMonth = "03";       
            }
            else if ($quarter== "2nd"){
                $firstMonth = "04";
                $endMonth = "06";
            }
            else if ($quarter == "3rd"){
                $firstMonth = "07";
                $endMonth = "09";
            }
            else if ($quarter == "4th"){
                $firstMonth = "10";
                $endMonth = "12";
            }
            $year = $request->date;
        }
        else if( $request->datePicker == 'Yearly'){
            $year = $request->date;
        }
        else if( $request->datePicker == 'Range'){
            $rangeDate = $request->date;
            
        }
        else {
            $explodeDayMonthYear = explode('-',$request->date);

            $year = $explodeDayMonthYear[0];
            $month = $explodeDayMonthYear[1];
        
        }


        $dateCreated = $request->date;
        $datePick = $request->datePicker;


       $data = [];

       $queryAllClassReport = ReportAssessmentRoll::select('Owner', 'Location', 'LotNo', 'BlkNo', "TctNo", 
                    'PIN', 'Effectivity', 'UpdateDesc',
                    'ARP', 'Prev_Arp', 'Taxability',
                    'AUDesc', 'kind', 'area', 
                    'MV','AL', 'AV', 'Prev_Av', 
                    'InsertedDt', 'AppraisedDt','Memoranda','Current')
                    
                    ->where('AppraisedBy', $request->appraiser)
            
                    ->when($request->datePicker == 'Daily', function($query) use ($request){
                        return $query->whereDate('AppraisedDt', $request->date );
                    })

                    ->when($request->datePicker == 'weekly', function($query) use ($request){
                        return $query->whereBetween('AppraisedDt', 
                        [Carbon::parse($request->date)->startOfWeek(),
                        Carbon::parse($request->date)->endOfWeek()]);
                    })
                    
                    ->when($request->datePicker == 'Monthly', function($query) use ($month, $year){
                        return $query->whereMonth('AppraisedDt',  $month)
                                    ->whereYear('AppraisedDt', $year);   
                    })
                    
                    ->when($request->datePicker == 'Yearly', function($query) use ( $year){                            
                        return $query->whereYear('AppraisedDt',  
                                Carbon::createFromFormat('Y',$year));

                    })
                    ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                            
                        return $query->whereBetween('AppraisedDt',   $rangeDate)
                                        ->orderBy('AppraisedDt', 'desc');

                    })
                    
                    ->orderBy('PIN')
                    ->get();
            
                
        foreach($queryAllClassReport as $key => $value){

            if ($value['Prev_Av'] == null) {
                $value['Prev_Av'] = 0;
            }

            foreach($queryAllClassReport as $key1 => $value1){
                if (trim($value1['Prev_Arp']) == trim($value['ARP'])){

                    $value['Current'] = 2;
                }
               
            }

            $data[] = array(
                'Owner'         =>  $value['Owner'],
                'Location'      =>  $value['Location'],
                'LotNo'         =>  str_replace("/", ",",$value['LotNo']),
                'BlkNo'         =>  str_replace("/", ",",$value['BlkNo']),
                'TctNo'         =>  $value['TctNo'],
                'PIN'           =>  $value['PIN'],
                'Effectivity'   =>  substr($value['Effectivity'],0,4),
                'InsertedDt'    =>  $value['InsertedDt'],
                'Taxability'    =>  $value['Taxability'],
                'UpdateDesc'    =>  $value['UpdateDesc'],
                'ARP'           =>  $value['ARP'],
                'Prev_Arp'      =>  $value['Prev_Arp'],
                'kind'          =>  $value['kind'],
                'AUDesc'        =>  $value['AUDesc'],
                'area'          =>  $value['area'],
                'MV'            =>  $value['MV'],
                'AL'            =>  $value['AL'],
                'AV'            =>  $value['AV'],
                'Prev_Av'       =>  $value['Prev_Av'],
                'Memoranda'     =>  $value['Memoranda'],
                'Current'       =>  $value['Current'],

            );
            
        }

        return self::createExcel( $data, $month, $request->appraiser,
        $year, $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate);

    }

    public function createExcel($data, $month, $appraiser,
        $year, $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate){

        $nameMonth = date("F", mktime(0, 0, 0, $month, 10));
        $startMonth = date("F", mktime(0, 0, 0, $firstMonth, 10));
        $endMonth = date("F", mktime(0, 0, 0, $endMonth, 10));

        
        if ($datePick == 'Daily'){
            $title = "Daily Appraiser Report";
            
            $asOfDate = "Date as of: " .$dateCreated;
            
        }
        if ($datePick == 'weekly'){
            $title = "Weekly Appraiser Report";
            
            $asOfDate = "Date as of: " .Carbon::parse($dateCreated)->startOfWeek()->format('Y-m-d'). " to " 
                .Carbon::parse($dateCreated)->endOfWeek()->format('Y-m-d');
        }
        if ($datePick == 'Monthly'){
            $title = "Monthly Appraiser Report";
           
            $asOfDate = "For  " .$nameMonth ." " .$year;
        }
       

        if ($datePick == 'Yearly'){
            $title = $year." Yearly Tax Appraiser Report";
            
            $asOfDate = "Year " .$year;
        }

        if ($datePick == 'Range'){
            $title = "Range Tax Appraiser Report";
           
            $asOfDate = "From " .$rangeDate[0] ." to " .$rangeDate[1];
        }

        // Set value binder
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder());
        

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        // $worksheet->setTitle($title);
        $spreadsheet->getProperties()
                ->setTitle($title);
        $spreadsheet->getSheetByName($title);
        $worksheet = $spreadsheet->getActiveSheet();

        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates('O1')
                ->setOffsetX(50)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates('P1')
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
        //REPUBLIC OF THE PHILIPPINES
        $worksheet->mergeCells('A1:W1');
        $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
        // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
        $worksheet->getStyle("A1")->getFont()->setSize(12);
        $worksheet->getStyle('A1:W1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A1:W1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //CITY OF MANILA
        $worksheet->mergeCells('A2:W2');
        $worksheet->setCellValue("A2", "CITY OF MANILA");
        $worksheet->getStyle("A2:W2")->getFont()->setBold(true);
        $worksheet->getStyle("A2")->getFont()->setSize(14);
        $worksheet->getStyle('A2:W2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A2:W2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //DEPARTMENT OF ASSESSMENT
        $worksheet->mergeCells('A3:W3');
        $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
        $worksheet->getStyle("A3:W3")->getFont()->setBold(true);
        $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
        $worksheet->getStyle('A3:W3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A3:W3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //*Title of Report
        $worksheet->mergeCells('A5:W6');
        $worksheet->setCellValue("A5", $title);
        $worksheet->getStyle("A5:W6")->getFont()->setBold(true);
        $worksheet->getStyle("A5")->getFont()->setSize(18);
        $worksheet->getStyle('A5:W6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A5:W6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //*Date of Report
        $worksheet->mergeCells('A7:W7');
        $worksheet->getStyle('A7:W7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("A7", $asOfDate);
        $worksheet->getStyle("A7")->getFont()->setSize(12)->setBold(true);
        // $worksheet->setCellValue("B4", $excelPrintText);
        // $worksheet->getStyle("B4")->getFont()->setSize(8)->setItalic(true);
        $worksheet->mergeCells('A8:W8');
        $worksheet->getStyle('A8:W8')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("A8", "Appraised by:  ". $appraiser);
        $worksheet->getStyle("A8")->getFont()->setSize(8);

        $worksheet->mergeCells('A9:W9');
        $worksheet->getStyle('A9:W9')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $worksheet->setCellValue("A9", "printed date:  ". date("F j, Y"));
        $worksheet->getStyle("A9")->getFont()->setSize(8);

        //---------------------setup width
        $worksheet->getColumnDimension('A')->setWidth(30);
        $worksheet->getColumnDimension('B')->setWidth(30);
        $worksheet->getColumnDimension('C')->setWidth(15);
        $worksheet->getColumnDimension('D')->setWidth(15);
        $worksheet->getColumnDimension('E')->setWidth(15);
        $worksheet->getColumnDimension('F')->setWidth(24);
        $worksheet->getColumnDimension('G')->setWidth(10);
        $worksheet->getColumnDimension('H')->setWidth(10);
        $worksheet->getColumnDimension('I')->setWidth(11);
        $worksheet->getColumnDimension('J')->setWidth(12);
        $worksheet->getColumnDimension('K')->setWidth(16);
        $worksheet->getColumnDimension('L')->setWidth(16);
        $worksheet->getColumnDimension('M')->setWidth(5);
        $worksheet->getColumnDimension('N')->setWidth(14);
        $worksheet->getColumnDimension('O')->setWidth(9);
        $worksheet->getColumnDimension('P')->setWidth(13);
        $worksheet->getColumnDimension('Q')->setWidth(5);
        $worksheet->getColumnDimension('R')->setWidth(12);
        $worksheet->getColumnDimension('S')->setWidth(12);
        $worksheet->getColumnDimension('T')->setWidth(17);
        $worksheet->getColumnDimension('U')->setWidth(17);
        $worksheet->getColumnDimension('V')->setWidth(12);
        $worksheet->getColumnDimension('W')->setWidth(50);
        $worksheet->getColumnDimension('X')->setWidth(0);
       
        //-----------------------setup merge cell, style and text for header

        $worksheet->setCellValue('A10', "Owner");
         $worksheet->setCellValue('B10', "Location");
         $worksheet->setCellValue('C10', "Lot #");
         $worksheet->setCellValue('D10', "Blk #");
         $worksheet->setCellValue('E10', "TCT #");
         $worksheet->setCellValue('F10', "PIN");
         $worksheet->setCellValue('G10', "Effectivity");
         $worksheet->setCellValue('H10', "Inserted Date");
         $worksheet->setCellValue('I10', "Taxability");
         $worksheet->setCellValue('J10', "Update Description");
         $worksheet->setCellValue('K10', "Current ARP");
         $worksheet->setCellValue('L10', "Previous ARP");
         $worksheet->setCellValue('M10', "Kind");
         $worksheet->setCellValue('N10', "Actual Use");
         $worksheet->setCellValue('O10', "Area");
         $worksheet->setCellValue('P10', "Market Value");
         $worksheet->setCellValue('Q10', "AL");
         $worksheet->setCellValue('R10', "Current AV");
         $worksheet->setCellValue('S10', "Previous AV");
         $worksheet->setCellValue('T10', "Total Current AV");
         $worksheet->setCellValue('U10', "Total Previous AV");
         $worksheet->setCellValue('V10', "Variance");
         $worksheet->setCellValue('W10', "Memoranda");
         $worksheet->setCellValue('X10', "Current");

        $styleHeader = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
            'font' => [
                'bold' => true,
            ],

        ];
        $styleBoldFont = [
            'font' => [
                'bold' => true,
            ],
        ];
        

        $worksheet->getStyle('A10:W10')->applyFromArray($styleHeader);
        

        #------------------data content------------------------------
        $styleBordersData = [
            
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
        ];
        $row = 11;
        $rowMinus = 10;
        $rowAdd = 12;
        $rowAdd1 = 13;

        $conditional = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
        $conditional->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
        $conditional->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_LESSTHAN);
        $conditional->addCondition(0);
        $conditional->getStyle()->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $conditional->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $conditional->getStyle()->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

        
        foreach ($data as $key => $val){
            $worksheet->setCellValue("A{$row}", $val['Owner']);
            $worksheet->setCellValue("B{$row}", $val['Location']);
            $worksheet->setCellValue("C{$row}", $val['LotNo']);
            $worksheet->setCellValue("D{$row}", $val['BlkNo']);
            $worksheet->setCellValue("E{$row}", $val['TctNo']);
            $worksheet->setCellValue("F{$row}", $val['PIN']);
            $worksheet->setCellValue("G{$row}", $val['Effectivity']);
            $worksheet->setCellValue("H{$row}", $val['InsertedDt']);
            $worksheet->setCellValue("I{$row}", $val['Taxability']);
            $worksheet->setCellValue("J{$row}", $val['UpdateDesc']);
            $worksheet->setCellValue("K{$row}", $val['ARP']);
            $worksheet->setCellValue("L{$row}", $val['Prev_Arp']);
            $worksheet->setCellValue("M{$row}", $val['kind']);
            $worksheet->setCellValue("N{$row}", $val['AUDesc']);
            $worksheet->setCellValue("O{$row}", $val['area']);
            $worksheet->setCellValue("P{$row}", $val['MV']);
            $worksheet->setCellValue("Q{$row}", $val['AL']);
            $worksheet->setCellValue("R{$row}", $val['AV']);
            $worksheet->setCellValue("S{$row}", $val['Prev_Av']);
            $worksheet->setCellValue("T{$row}", "=IF(F{$row}=F{$rowMinus},(IF(F{$row}=F{$rowAdd},0,(IF(N{$rowMinus}=N{$row},R{$row},R{$row}+R{$rowMinus})))),(IF(F{$row}<>F{$rowAdd},R{$row},0)) )");
            $worksheet->setCellValue("U{$row}", "=IF(L{$row}=L{$rowMinus},(IF(L{$row}=L{$rowAdd},0,S{$row})),(IF(L{$row}<>L{$rowAdd},S{$row},0)) )");
            $worksheet->setCellValue("V{$row}", "=T{$row} - U{$row}");
            $worksheet->setCellValue("W{$row}", $val['Memoranda']);
            $worksheet->setCellValue("X{$row}", $val['Current']);
            

            $worksheet->getStyle("A{$row}:W{$row}")->applyFromArray($styleBordersData);
               
            $worksheet->getStyle("N{$row}")->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            $worksheet->getStyle("O{$row}:V{$row}")->getNumberFormat()->setFormatCode('#,###,###,###');;

           
            $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("V{$row}")->getConditionalStyles();
            $conditionalStyles[] = $conditional;

            $spreadsheet->getActiveSheet()->getStyle("V{$row}")->setConditionalStyles($conditionalStyles);
            
            $row++;
            $rowMinus++;
            $rowAdd++;
            $rowAdd1++;
        }

        // for ($c=11; $c < $row; $c++){
        //     $worksheet->setCellValue("X{$c}", "=IFNA(IF((INDEX(L11:L{$row},MATCH(K{$c},L11:L{$row},0),1))=K{$c},1,),)");
        // }

       //wizard style formatting"
       $redStyle = new Style(false, true);
       $redStyle->getFill()
           ->setFillType(Fill::FILL_SOLID)
           ->getStartColor()->setARGB(Color::COLOR_RED);
       $redStyle->getFill()
           ->getEndColor()->setARGB(Color::COLOR_RED);
       $redStyle->getFont()->setColor(new Color(Color::COLOR_WHITE));

       $fontRedStyle = new Style(false, true);
       // $darkYellowStyle->getFill()
       //     ->setFillType(Fill::FILL_SOLID)
       //     ->getStartColor()->setARGB(Color::COLOR_DARKYELLOW);
       // $darkYellowStyle->getFill()
       //     ->getEndColor()->setARGB(Color::COLOR_DARKYELLOW);
       $fontRedStyle->getFont()->setColor(new Color(Color::COLOR_RED));

       $cellRange = "A11:W{$row}";
       $conditionalStyles = [];
       $wizardFactory = new Wizard($cellRange);
       /** @var Wizard\CellValue $cellWizard */
       $cellWizard = $wizardFactory->newRule(Wizard::FORMULA);

       $cellWizard->expression('SEARCH(0,$X1)>0')
       ->setStyle($fontRedStyle);
       $conditionalStyles[] = $cellWizard->getConditional();

       $cellWizard->expression('SEARCH(2,$X1)>0')
       ->setStyle($redStyle);
       $conditionalStyles[] = $cellWizard->getConditional();

        
        $spreadsheet->getActiveSheet()
            ->getStyle($cellWizard->getCellRange())
            ->setConditionalStyles($conditionalStyles);
        
            $lastRow = $row + 1;
            $diff = $lastRow +1;
            $totalRow = $diff + 1;
            $worksheet->setCellValue("P{$lastRow}", "TOTAL :")->getStyle("P{$lastRow}")->getFont()->setBold(true)->setUnderline(true);
            $worksheet->setCellValue("R{$lastRow}", "=SUM(R11:R{$row})")->getStyle("R{$lastRow}")->getFont();
            $worksheet->setCellValue("T{$lastRow}", "COUNT RPUS :")->getStyle("P{$lastRow}")->getFont()->setBold(true)->setUnderline(true);
            $worksheet->setCellValue("U{$lastRow}", "=COUNTA(K11:K{$row})")->getStyle("K{$lastRow}")->getFont();
            
            $worksheet->setCellValue("P{$diff}", "LESS :")->getStyle("P{$diff}")->getFont()->setBold(true)->setUnderline(true);
            $worksheet->setCellValue("R{$diff}", "=SUMIF(X11:X{$row},2,R11:R{$row})")->getStyle("R{$diff}")->getFont();
            $worksheet->setCellValue("T{$diff}", "Count of RPUs that was cancelled on this date - $asOfDate:")->getStyle("P{$diff}")->getFont()->setBold(true)->setUnderline(true);
            $worksheet->setCellValue("U{$diff}", "=COUNTIF(X11:X{$row},2)")->getStyle("U{$lastRow}")->getFont();
            
            $worksheet->setCellValue("R{$totalRow}", "=R{$lastRow}-R{$diff}")->getStyle("R{$totalRow}")->getFont()->setBold(true)->setUnderline(true);
        // $worksheet->setCellValue("T{$lastRow}", "=SUM(T11:T{$row})")->getStyle("T{$lastRow}")->getFont()->setBold(true)->setUnderline(true);
        // $worksheet->setCellValue("U{$lastRow}", "=SUM(U11:U{$row})")->getStyle("U{$lastRow}")->getFont()->setBold(true)->setUnderline(true);
        
        $worksheet->getStyle("R{$lastRow}:U{$totalRow}")->getNumberFormat()->setFormatCode('#,###,###,###');
        
        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("V{$lastRow}")->getConditionalStyles();
        $conditionalStyles[] = $conditional;
        $spreadsheet->getActiveSheet()->getStyle("V{$lastRow}")->setConditionalStyles($conditionalStyles);

        
        

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }
}
