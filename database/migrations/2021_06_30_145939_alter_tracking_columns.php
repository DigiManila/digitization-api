<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTrackingColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracking_transactions', function (Blueprint $table) {
            $table->tinyInteger("flow_type")->after('or');
        });

        Schema::table('tracking_normal_flow', function (Blueprint $table) {
            $table->dropColumn('transaction_type');
            $table->dropColumn('kind');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
