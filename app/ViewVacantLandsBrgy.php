<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewVacantLandsBrgy extends Model
{
    use HasFactory;
    protected $table = "view_vacant_lands_brgy";
}
