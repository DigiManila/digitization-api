<?php

namespace App\Http\Controllers\Api\Reports;

use App\RptasTaxdecMastMla;
use App\ReportAssessmentRoll;
use App\Http\Controllers\Controller;
use App\RptasTaxdecMastExtn;
use Illuminate\Http\Request;

class TestQueryController extends Controller
{
    public function testQuery(){
        \Log::info(date('Y-m-d H:i:s'));
        $queryAllClassReport = RptasTaxdecMastMla::select("Owner", 'Location', 'LotNo', 'BlkNo', "TctNo", 
                'rptas_taxdec_mast_mla.PIN', 'Effectivity', 'UpdateDesc',
                'rptas_taxdec_mast_mla.ARP', 'Prev_Arp', 'rptas_taxdec_assmnt.Taxability',
                'rptas_taxdec_assmnt.AUDesc', 'rptas_taxdec_assmnt.kind', 'rptas_taxdec_assmnt.area', 
                'rptas_taxdec_assmnt.MV','rptas_taxdec_assmnt.AL', 'rptas_taxdec_assmnt.AV', 'Prev_Av', 
                'rptas_taxdec_mast_mla.InsertedDt', 'rptas_taxdec_mast_mla.AppraisedBy', 
                'rptas_taxdec_mast_mla.AppraisedDt','Memoranda')
        
        ->join('rptas_taxdec_assmnt','rptas_taxdec_mast_mla.ARP','=','rptas_taxdec_assmnt.ARP')
        ->leftJoin('rptas_taxdec_mastextn','rptas_taxdec_mast_mla.ARP','=','rptas_taxdec_mastextn.ARP')
        
        ->orderBy('PIN')
        ->get()->toArray();

        \Log::info('Done query all class report '. date('Y-m-d H:i:s'));

        // $AA = 'AA-';
        // $AD = 'AD-';
        $queryPrev_Arp = RptasTaxdecMastExtn:: select('Prev_Arp')
                                ->whereNotNull('Prev_Arp')
                                // ->where('Prev_Arp', "like", "%" . $AA . "%")
                                // ->orWhere('Prev_Arp', "like", "%" . $AD . "%")
                                // ->pluck('Prev_Arp')
                                ->get();
                                // ->toArray();

        \Log::info('Done query all prev_arp '. date('Y-m-d H:i:s')); 
        $temp_arr = self::formatArray($queryPrev_Arp);
        $temp_arr = array_flip($temp_arr);

        foreach($queryAllClassReport as $key => $value){
         
                if ( array_key_exists($value['ARP'], $temp_arr) ){
                 
                    $queryAllClassReport[$key]['Current'] = 0;
                    
                }
                else {
                    $queryAllClassReport[$key]['Current'] = 1;
                 
                }
                
        }

        \Log::info('Done inserting current column '. date('Y-m-d H:i:s'));


        $chunk_count = 1000;
        foreach (array_chunk($queryAllClassReport,$chunk_count) as $chunk){

            $status = ReportAssessmentRoll::insert($chunk);
            \Log::info($status);
            
        }

        \Log::info(date('Y-m-d H:i:s'));

        return 0;
    }

    private function formatArray($val){

        $output = [];

        foreach ($val as $key => $value) {
            $tmp = json_decode($value, 1);
            // \Log::info($tmp['Prev_Arp']);
            $output[] = $tmp['Prev_Arp'];
        }

        return $output;

    }
}
