<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CancelledAssessmentRoll extends Model
{
    use HasFactory;
    protected $table = 'cancelled_assessment_roll';
}
