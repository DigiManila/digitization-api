<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;

use Illuminate\Support\Str;
use App\ReportAssessmentRoll;
use App\RptasTaxdecMastExtn;
class AssessmentReportController extends Controller
{

    public function getDataAssessmentReport(Request $request){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $firstMonth = null;
        $endMonth = null;
        $quarter = $request->quarter;
        $year = null;
        $month = null;
        $rangeDate = null;
       
        
        if ($quarter != ""){
            if ($request->quarter == "1st"){
                $firstMonth = "01";
                $endMonth = "03";       
            }
            else if ($quarter== "2nd"){
                $firstMonth = "04";
                $endMonth = "06";
            }
            else if ($quarter == "3rd"){
                $firstMonth = "07";
                $endMonth = "09";
            }
            else if ($quarter == "4th"){
                $firstMonth = "10";
                $endMonth = "12";
            }
            $year = $request->date;
        }
        else if( $request->datePicker == 'Yearly'){
            $year = $request->date;
        }
        else if( $request->datePicker == 'Range'){
            $rangeDate = $request->date;
            
        }
        else {
            $explodeDayMonthYear = explode('-',$request->date);

            $year = $explodeDayMonthYear[0];
            $month = $explodeDayMonthYear[1];
        
        }


        $dateCreated = $request->date;
        $datePick = $request->datePicker;
                
    
        $getAllActiveTaxable = [];
        $getGreaterThanCancelledTaxable = [];
        $getCurrentActiveTaxable = [];
        $getCurrentCancelledTaxable = [];
        $xGetAllExempted = [];
        $xGetGreaterThanCancelledExempted = [];
        $xGetCurrentActiveExempted = [];
        
        $xGetCurrentCancelledExempted = [];


        $queryAllAssessmentReport = ReportAssessmentRoll::select('PIN','ARP',
                'AUDesc', 'kind','AV','AppraisedDt', 'Taxability','Current')
                ->where('Current', 1)
                ->whereRaw("LENGTH(ARP) > ?", 3)
                ->when($request->datePicker == 'Daily', function($query) use ($request){
                    return $query->whereDate('AppraisedDt', '<=', $request->date );
                })
              
                ->when($request->datePicker == 'Monthly', function($query) use ($request){
                    return $query->whereRaw('SUBSTRING(AppraisedDt,1,7) <= ?', $request->date);                                        
                })
              
                ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                        
                    return $query->whereDate('AppraisedDt', '<=',  $rangeDate[1]);                        
                })
                ->orderBy('PIN')
                ->get()
                ->toArray();
        
        $queryAllAssessmentReport =self::findingConsolidation($queryAllAssessmentReport);
       
       
        $queryAllPresentReport = ReportAssessmentRoll::select('ARP', 'Prev_Arp',
            'AUDesc', 'kind','AV','AppraisedDt', 'Taxability', 'Current')
            ->when($request->datePicker == 'Daily', function($query) use ($request){
                return $query->whereDate('AppraisedDt', $request->date );
            })
          
            ->when($request->datePicker == 'Monthly', function($query) use ($month, $year){
                return $query->whereMonth('AppraisedDt',  $month)
                            ->whereYear('AppraisedDt', $year);                                        
            })
          
            ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                    
                return $query->whereBetween('AppraisedDt',   $rangeDate)
                                ->orderBy('AppraisedDt', 'desc');
            })
            ->orderBy('PIN') 
            ->get()
            ->toArray();

            $removePluckPrevArp = [];

            foreach($queryAllPresentReport as $key => $value){
                if ($value['Prev_Arp'] != null)
                {
                    foreach($queryAllPresentReport as $key1 => $value1){
                        if (trim($value1['Prev_Arp']) == trim($value['ARP']) 
                        ){
                            $value['Current'] = 2;
                          
                        }
                    }
                    
                    if ($value['Current'] != 2){
                        array_push($removePluckPrevArp, $value['Prev_Arp']);
                    }
                }
            }
            
            $queryAllCancelledReport = ReportAssessmentRoll::select('ARP', 'AUDesc', 'AV',  'kind', 'Taxability', 'Current')
            ->whereIn('ARP', $removePluckPrevArp)       
            ->get()
            ->toArray();
            
            $queryAllPresentReport =self::findingConsolidation($queryAllPresentReport);

            $queryPresidingAssessmentReport = ReportAssessmentRoll::select('PIN','ARP',
                'AUDesc', 'kind','AV','AppraisedDt', 'Taxability','Current')
                ->where('Current', 1)
                ->whereRaw("LENGTH(ARP) > ?", 3)
                ->when($request->datePicker == 'Daily', function($query) use ($request){
                    return $query->whereDate('AppraisedDt', '<', $request->date );
                })
              
                ->when($request->datePicker == 'Monthly', function($query) use ($request){
                    return $query->whereRaw('SUBSTRING(AppraisedDt,1,7) < ?', $request->date);                                        
                })
              
                ->when($request->datePicker == 'Range', function($query) use ( $rangeDate){
                        
                    return $query->whereDate('AppraisedDt', '<',  $rangeDate[0]);                        
                })
                ->orderBy('PIN')
                ->get()
                ->toArray();
        
        $queryPresidingAssessmentReport =self::findingConsolidation($queryPresidingAssessmentReport);
                   
        

        #-----------------------------------------------Taxable---------------------------------------------#
        
        //------------------------for all active taxable ---------------------------//
    
         //all land data taxable
         $getAllActiveTaxable['landResi'] = 0.00;
         $getAllActiveTaxable['landComm'] = 0.00;
         $getAllActiveTaxable['landIndu'] = 0.00;
         $getAllActiveTaxable['landResiParcel'] = 0;
         $getAllActiveTaxable['landCommParcel'] = 0;
         $getAllActiveTaxable['landInduParcel'] = 0;
 
         //all imp data taxable
         $getAllActiveTaxable['bldgResi'] = 0.00;
         $getAllActiveTaxable['bldgComm'] = 0.00;
         $getAllActiveTaxable['bldgIndu'] = 0.00;
         $getAllActiveTaxable['bldgResiParcel'] = 0;
         $getAllActiveTaxable['bldgCommParcel'] = 0;
         $getAllActiveTaxable['bldgInduParcel'] = 0;
 
         //all mach data taxable
         $getAllActiveTaxable['machResi'] = 0.00;
         $getAllActiveTaxable['machComm'] = 0.00;
         $getAllActiveTaxable['machSpec'] = 0.00;
         $getAllActiveTaxable['machResiParcel'] = 0;
         $getAllActiveTaxable['machCommParcel'] = 0;
         $getAllActiveTaxable['machSpecParcel'] = 0;
 
             //all spec data taxable
         $getAllActiveTaxable['specRecreationalLand'] = 0.00;
         $getAllActiveTaxable['specHospitalLand'] = 0.00;
         $getAllActiveTaxable['specCulturalLand'] = 0.00;
         $getAllActiveTaxable['specSpecialLand'] = 0.00;
         $getAllActiveTaxable['specScientificLand'] = 0.00;
         $getAllActiveTaxable['specLocWaterDistLand'] = 0.00;
         $getAllActiveTaxable['specOthersLand'] = 0.00;
 
         $getAllActiveTaxable['specRecreationalImp'] = 0.00;
         $getAllActiveTaxable['specHospitalImp'] = 0.00;
         $getAllActiveTaxable['specCulturalImp'] = 0.00;
         $getAllActiveTaxable['specSpecialImp'] = 0.00;
         $getAllActiveTaxable['specScientificImp'] = 0.00;
         $getAllActiveTaxable['specLocWaterDistImp'] = 0.00;
         $getAllActiveTaxable['specOthersImp'] = 0.00;
             
         $getAllActiveTaxable['specRecreationalLandParcel'] = 0;
         $getAllActiveTaxable['specHospitalLandParcel'] = 0;
         $getAllActiveTaxable['specCulturalLandParcel'] = 0;
         $getAllActiveTaxable['specSpecialLandParcel'] = 0;
         $getAllActiveTaxable['specScientificLandParcel'] = 0;
         $getAllActiveTaxable['specLocWaterDistLandParcel'] = 0;
         $getAllActiveTaxable['specOthersLandParcel'] = 0;
             
         $getAllActiveTaxable['specRecreationalImpParcel'] = 0;
         $getAllActiveTaxable['specHospitalImpParcel'] = 0;
         $getAllActiveTaxable['specCulturalImpParcel'] = 0;
         $getAllActiveTaxable['specSpecialImpParcel'] = 0;
         $getAllActiveTaxable['specScientificImpParcel'] = 0;
         $getAllActiveTaxable['specLocWaterDistImpParcel'] = 0;
         $getAllActiveTaxable['specOthersImpParcel'] = 0;
 
    
        foreach($queryAllAssessmentReport as $key => $value){

            
            if(
                
                $value['Taxability']=='T'
            )
            
            {
            
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'L' ){
                    \Log::info($value['ARP']. " - ". $value['AV']);
                    $getAllActiveTaxable['landResi'] += $value['AV'];
                    $getAllActiveTaxable['landResiParcel'] += 1;

                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['landComm'] += $value['AV'];
                    $getAllActiveTaxable['landCommParcel'] += 1;
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['landIndu'] += $value['AV'];
                    $getAllActiveTaxable['landInduParcel'] += 1;
                    
                }

                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'B'){
                            
                    $getAllActiveTaxable['bldgResi'] += $value['AV'];
                    $getAllActiveTaxable['bldgResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'B'){
                                
                    $getAllActiveTaxable['bldgComm'] += $value['AV'];
                    $getAllActiveTaxable['bldgCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'B'){
                            
                $getAllActiveTaxable['bldgIndu'] += $value['AV'];
                $getAllActiveTaxable['bldgInduParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'M'){
                            
                    $getAllActiveTaxable['machResi'] += $value['AV'];
                    $getAllActiveTaxable['machResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'M'){
                                
                    $getAllActiveTaxable['machComm'] += $value['AV'];
                    $getAllActiveTaxable['machCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                                
                    $getAllActiveTaxable['machSpec'] += $value['AV'];
                    $getAllActiveTaxable['machSpecParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specRecreationalLand']  += $value['AV'];
                    $getAllActiveTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specRecreationalImp'] += $value['AV'];
                    $getAllActiveTaxable['specRecreationalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specHospitalLand']  += $value['AV'];
                    $getAllActiveTaxable['specHospitalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specHospitalImp'] += $value['AV'];
                    $getAllActiveTaxable['specHospitalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specCulturalLand']  += $value['AV'];
                    $getAllActiveTaxable['specCulturalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specCulturalImp'] += $value['AV'];
                    $getAllActiveTaxable['specCulturalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specSpecialLand']  += $value['AV'];
                    $getAllActiveTaxable['specSpecialLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specSpecialImp'] += $value['AV'];
                    $getAllActiveTaxable['specSpecialImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specRecreationalLand']  += $value['AV'];
                    $getAllActiveTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specScientificImp'] += $value['AV'];
                    $getAllActiveTaxable['specScientificImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specLocWaterDistLand']  += $value['AV'];
                    $getAllActiveTaxable['specLocWaterDistLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specLocWaterDistImp'] += $value['AV'];
                    $getAllActiveTaxable['specLocWaterDistImpParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'L'){
                    
                    $getAllActiveTaxable['specOthersLand']  += $value['AV'];
                    $getAllActiveTaxable['specOthersLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHERS' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getAllActiveTaxable['specOthersImp'] += $value['AV'];
                    $getAllActiveTaxable['specOthersImpParcel'] += 1;
                    
                }                
                
            }
        }

        // // all land data taxable
        // $getGreaterThanCancelledTaxable['landResi'] = 0.00;
        // $getGreaterThanCancelledTaxable['landComm'] = 0.00;
        // $getGreaterThanCancelledTaxable['landIndu'] = 0.00;
        // $getGreaterThanCancelledTaxable['landResiParcel'] = 0;
        // $getGreaterThanCancelledTaxable['landCommParcel'] = 0;
        // $getGreaterThanCancelledTaxable['landInduParcel'] = 0;

        // //all imp data taxable
        // $getGreaterThanCancelledTaxable['bldgResi'] = 0.00;
        // $getGreaterThanCancelledTaxable['bldgComm'] = 0.00;
        // $getGreaterThanCancelledTaxable['bldgIndu'] = 0.00;
        // $getGreaterThanCancelledTaxable['bldgResiParcel'] = 0;
        // $getGreaterThanCancelledTaxable['bldgCommParcel'] = 0;
        // $getGreaterThanCancelledTaxable['bldgInduParcel'] = 0;

        // //all mach data taxable
        // $getGreaterThanCancelledTaxable['machResi'] = 0.00;
        // $getGreaterThanCancelledTaxable['machComm'] = 0.00;
        // $getGreaterThanCancelledTaxable['machSpec'] = 0.00;
        // $getGreaterThanCancelledTaxable['machResiParcel'] = 0;
        // $getGreaterThanCancelledTaxable['machCommParcel'] = 0;
        // $getGreaterThanCancelledTaxable['machSpecParcel'] = 0;

        //     //all spec data taxable
        // $getGreaterThanCancelledTaxable['specRecreationalLand'] = 0.00;
        // $getGreaterThanCancelledTaxable['specHospitalLand'] = 0.00;
        // $getGreaterThanCancelledTaxable['specCulturalLand'] = 0.00;
        // $getGreaterThanCancelledTaxable['specSpecialLand'] = 0.00;
        // $getGreaterThanCancelledTaxable['specScientificLand'] = 0.00;
        // $getGreaterThanCancelledTaxable['specLocWaterDistLand'] = 0.00;
        // $getGreaterThanCancelledTaxable['specOthersLand'] = 0.00;

        // $getGreaterThanCancelledTaxable['specRecreationalImp'] = 0.00;
        // $getGreaterThanCancelledTaxable['specHospitalImp'] = 0.00;
        // $getGreaterThanCancelledTaxable['specCulturalImp'] = 0.00;
        // $getGreaterThanCancelledTaxable['specSpecialImp'] = 0.00;
        // $getGreaterThanCancelledTaxable['specScientificImp'] = 0.00;
        // $getGreaterThanCancelledTaxable['specLocWaterDistImp'] = 0.00;
        // $getGreaterThanCancelledTaxable['specOthersImp'] = 0.00;
            
        // $getGreaterThanCancelledTaxable['specRecreationalLandParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specHospitalLandParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specCulturalLandParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specSpecialLandParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specScientificLandParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specLocWaterDistLandParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specOthersLandParcel'] = 0;
            
        // $getGreaterThanCancelledTaxable['specRecreationalImpParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specHospitalImpParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specCulturalImpParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specSpecialImpParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specScientificImpParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specLocWaterDistImpParcel'] = 0;
        // $getGreaterThanCancelledTaxable['specOthersImpParcel'] = 0;

    
        // foreach($queryGreaterCancelledReport as $key => $value){

        //     if(
                
        //         $value['Taxability']=='T' 
        //     )
            
        //     {
            
        //         if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'L' ){
                
        //             $getGreaterThanCancelledTaxable['landResi'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['landResiParcel'] += 1;

                    
        //         }

        //         if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['landComm'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['landCommParcel'] += 1;
        //         }

        //         if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['landIndu'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['landInduParcel'] += 1;
                    
        //         }

                
        //         if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'B'){
                            
        //             $getGreaterThanCancelledTaxable['bldgResi'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['bldgResiParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'B'){
                                
        //             $getGreaterThanCancelledTaxable['bldgComm'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['bldgCommParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'B'){
                            
        //         $getGreaterThanCancelledTaxable['bldgIndu'] += $value['AV'];
        //         $getGreaterThanCancelledTaxable['bldgInduParcel'] += 1;
                    
        //         }
                
        //         if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'M'){
                            
        //             $getGreaterThanCancelledTaxable['machResi'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['machResiParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'M'){
                                
        //             $getGreaterThanCancelledTaxable['machComm'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['machCommParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                                
        //             $getGreaterThanCancelledTaxable['machSpec'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['machSpecParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specRecreationalLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specRecreationalLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'RECREATIONAL' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specRecreationalImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specRecreationalImpParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specHospitalLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specHospitalLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'HOSPITAL' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specHospitalImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specHospitalImpParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specCulturalLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specCulturalLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'CULTURAL' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specCulturalImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specCulturalImpParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specSpecialLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specSpecialLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'SPECIAL' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specSpecialImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specSpecialImpParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specRecreationalLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specRecreationalLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'SCIENTIFIC' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specScientificImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specScientificImpParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specLocWaterDistLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specLocWaterDistLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specLocWaterDistImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specLocWaterDistImpParcel'] += 1;
                    
        //         }
                
        //         if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'L'){
                    
        //             $getGreaterThanCancelledTaxable['specOthersLand']  += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specOthersLandParcel'] += 1;
                    
        //         }

        //         if ($value['AUDesc'] == 'OTHERS' && 
        //             ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
        //             $getGreaterThanCancelledTaxable['specOthersImp'] += $value['AV'];
        //             $getGreaterThanCancelledTaxable['specOthersImpParcel'] += 1;
                    
        //         }                
                
        //     }
        // }

        //------------------------for current active taxable ---------------------------//

        $getCurrentActiveTaxable['landResi'] = 0.00;
        $getCurrentActiveTaxable['landComm'] = 0.00;
        $getCurrentActiveTaxable['landIndu'] = 0.00;
       
        $getCurrentActiveTaxable['landResiParcel'] = 0;
        $getCurrentActiveTaxable['landCommParcel'] = 0;
        $getCurrentActiveTaxable['landInduParcel'] = 0;

        $getCurrentActiveTaxable['bldgResi'] = 0.00;
        $getCurrentActiveTaxable['bldgComm'] = 0.00;
        $getCurrentActiveTaxable['bldgIndu'] = 0.00;
        
        $getCurrentActiveTaxable['bldgResiParcel'] = 0;
        $getCurrentActiveTaxable['bldgCommParcel'] = 0;
        $getCurrentActiveTaxable['bldgInduParcel'] = 0;

        $getCurrentActiveTaxable['machResi'] = 0.00;
        $getCurrentActiveTaxable['machComm'] = 0.00;
        $getCurrentActiveTaxable['machSpec'] = 0.00;
        
        $getCurrentActiveTaxable['machResiParcel'] = 0;
        $getCurrentActiveTaxable['machCommParcel'] = 0;
        $getCurrentActiveTaxable['machSpecParcel'] = 0;

        $getCurrentActiveTaxable['specRecreationalLand'] = 0.00;
        $getCurrentActiveTaxable['specHospitalLand'] = 0.00;
        $getCurrentActiveTaxable['specCulturalLand'] = 0.00;
        $getCurrentActiveTaxable['specSpecialLand'] = 0.00;
        $getCurrentActiveTaxable['specScientificLand'] = 0.00;
        $getCurrentActiveTaxable['specLocWaterDistLand'] = 0.00;
        $getCurrentActiveTaxable['specOthersLand'] = 0.00;

        $getCurrentActiveTaxable['specRecreationalLandParcel'] = 0;
        $getCurrentActiveTaxable['specHospitalLandParcel'] = 0;
        $getCurrentActiveTaxable['specCulturalLandParcel'] = 0;
        $getCurrentActiveTaxable['specSpecialLandParcel'] = 0;
        $getCurrentActiveTaxable['specScientificLandParcel'] = 0;
        $getCurrentActiveTaxable['specLocWaterDistLandParcel'] = 0;
        $getCurrentActiveTaxable['specOthersLandParcel'] = 0;

        $getCurrentActiveTaxable['specRecreationalImp'] = 0.00;
        $getCurrentActiveTaxable['specHospitalImp'] = 0.00;
        $getCurrentActiveTaxable['specCulturalImp'] = 0.00;
        $getCurrentActiveTaxable['specSpecialImp'] = 0.00;
        $getCurrentActiveTaxable['specScientificImp'] = 0.00;
        $getCurrentActiveTaxable['specLocWaterDistImp'] = 0.00;
        $getCurrentActiveTaxable['specOthersImp'] = 0.00;
       
        $getCurrentActiveTaxable['specRecreationalImpParcel'] = 0;
        $getCurrentActiveTaxable['specHospitalImpParcel'] = 0;
        $getCurrentActiveTaxable['specCulturalImpParcel'] = 0;
        $getCurrentActiveTaxable['specSpecialImpParcel'] = 0;
        $getCurrentActiveTaxable['specScientificImpParcel'] = 0;
        $getCurrentActiveTaxable['specLocWaterDistImpParcel'] = 0;
        $getCurrentActiveTaxable['specOthersImpParcel'] = 0;

        foreach($queryAllPresentReport as $key => $value){
            
            foreach($queryAllPresentReport as $key1 => $value1){
                if (trim($value1['Prev_Arp']) == trim($value['ARP'])){

                    $value['Current'] = 2;
                }           
            }

            if(
                $value['Taxability']=='T' 
                && $value['Current'] != 2
            )
            
            {
                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'L' ){
                    
                    $getCurrentActiveTaxable['landResi'] += $value['AV'];
                    $getCurrentActiveTaxable['landResiParcel'] += 1;

                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['landComm'] += $value['AV'];
                    $getCurrentActiveTaxable['landCommParcel'] += 1;
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['landIndu'] += $value['AV'];
                    $getCurrentActiveTaxable['landInduParcel'] += 1;
                    
                }

                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'B'){
                            
                    $getCurrentActiveTaxable['bldgResi'] += $value['AV'];
                    $getCurrentActiveTaxable['bldgResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'B'){
                                
                    $getCurrentActiveTaxable['bldgComm'] += $value['AV'];
                    $getCurrentActiveTaxable['bldgCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'B'){
                            
                    $getCurrentActiveTaxable['bldgIndu'] += $value['AV'];
                    $getCurrentActiveTaxable['bldgInduParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'M'){
                            
                    $getCurrentActiveTaxable['machResi'] += $value['AV'];
                    $getCurrentActiveTaxable['machResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'M'){
                                
                    $getCurrentActiveTaxable['machComm'] += $value['AV'];
                    $getCurrentActiveTaxable['machCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                                
                    $getCurrentActiveTaxable['machSpec'] += $value['AV'];
                    $getCurrentActiveTaxable['machSpecParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specRecreationalLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specRecreationalImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specRecreationalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specHospitalLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specHospitalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specHospitalImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specHospitalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specCulturalLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specCulturalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specCulturalImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specCulturalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specSpecialLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specSpecialLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specSpecialImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specSpecialImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specRecreationalLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specScientificImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specScientificImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specLocWaterDistLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specLocWaterDistLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specLocWaterDistImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specLocWaterDistImpParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'L'){
                    
                    $getCurrentActiveTaxable['specOthersLand']  += $value['AV'];
                    $getCurrentActiveTaxable['specOthersLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHERS' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentActiveTaxable['specOthersImp'] += $value['AV'];
                    $getCurrentActiveTaxable['specOthersImpParcel'] += 1;
                    
                }
                
            }
        }

        #-------------------------------------------------------------------------------------------------#

        //------------------------for current cancelled taxable ---------------------------//
    
        //cancelled land data taxable
       $getCurrentCancelledTaxable['landResi'] = 0.00;
       $getCurrentCancelledTaxable['landComm'] = 0.00;
       $getCurrentCancelledTaxable['landIndu'] = 0.00;
       $getCurrentCancelledTaxable['landResiParcel'] = 0;
       $getCurrentCancelledTaxable['landCommParcel'] = 0;
       $getCurrentCancelledTaxable['landInduParcel'] = 0;
        
        //cancelled  imp data taxable
       $getCurrentCancelledTaxable['bldgResi'] = 0.00;
       $getCurrentCancelledTaxable['bldgComm'] = 0.00;
       $getCurrentCancelledTaxable['bldgIndu'] = 0.00;
       $getCurrentCancelledTaxable['bldgResiParcel'] = 0;
       $getCurrentCancelledTaxable['bldgCommParcel'] = 0;
       $getCurrentCancelledTaxable['bldgInduParcel'] = 0;

        //cancelled  mach data taxable
       $getCurrentCancelledTaxable['machResi'] = 0.00;
       $getCurrentCancelledTaxable['machComm'] = 0.00;
       $getCurrentCancelledTaxable['machSpec'] = 0.00;
       $getCurrentCancelledTaxable['machResiParcel'] = 0;
       $getCurrentCancelledTaxable['machCommParcel'] = 0;
       $getCurrentCancelledTaxable['machSpecParcel'] = 0;

        //cancelled  spec data taxable
       $getCurrentCancelledTaxable['specRecreationalLand'] = 0.00;
       $getCurrentCancelledTaxable['specHospitalLand'] = 0.00;
       $getCurrentCancelledTaxable['specCulturalLand'] = 0.00;
       $getCurrentCancelledTaxable['specSpecialLand'] = 0.00;
       $getCurrentCancelledTaxable['specScientificLand'] = 0.00;
       $getCurrentCancelledTaxable['specLocWaterDistLand'] = 0.00;
       $getCurrentCancelledTaxable['specOthersLand'] = 0.00;

       $getCurrentCancelledTaxable['specRecreationalImp'] = 0.00;
       $getCurrentCancelledTaxable['specHospitalImp'] = 0.00;
       $getCurrentCancelledTaxable['specCulturalImp'] = 0.00;
       $getCurrentCancelledTaxable['specSpecialImp'] = 0.00;
       $getCurrentCancelledTaxable['specScientificImp'] = 0.00;
       $getCurrentCancelledTaxable['specLocWaterDistImp'] = 0.00;
       $getCurrentCancelledTaxable['specOthersImp'] = 0.00;
        
       $getCurrentCancelledTaxable['specRecreationalLandParcel'] = 0;
       $getCurrentCancelledTaxable['specHospitalLandParcel'] = 0;
       $getCurrentCancelledTaxable['specCulturalLandParcel'] = 0;
       $getCurrentCancelledTaxable['specSpecialLandParcel'] = 0;
       $getCurrentCancelledTaxable['specScientificLandParcel'] = 0;
       $getCurrentCancelledTaxable['specLocWaterDistLandParcel'] = 0;
       $getCurrentCancelledTaxable['specOthersLandParcel'] = 0;
        
       $getCurrentCancelledTaxable['specRecreationalImpParcel'] = 0;
       $getCurrentCancelledTaxable['specHospitalImpParcel'] = 0;
       $getCurrentCancelledTaxable['specCulturalImpParcel'] = 0;
       $getCurrentCancelledTaxable['specSpecialImpParcel'] = 0;
       $getCurrentCancelledTaxable['specScientificImpParcel'] = 0;
       $getCurrentCancelledTaxable['specLocWaterDistImpParcel'] = 0;
       $getCurrentCancelledTaxable['specOthersImpParcel'] = 0;
         
        foreach($queryAllCancelledReport as $key => $value){  

           
            if(
               $value['Taxability']=='T' 
            
            )
            {   
                   
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'L' ){
                            
                    $getCurrentCancelledTaxable['landResi'] += $value['AV'];
                    $getCurrentCancelledTaxable['landResiParcel'] += 1;
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'L'){
                            
                    $getCurrentCancelledTaxable['landComm'] += $value['AV'];
                    $getCurrentCancelledTaxable['landCommParcel'] += 1;
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['landIndu'] += $value['AV'];
                    $getCurrentCancelledTaxable['landInduParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'B'){
                            
                    $getCurrentCancelledTaxable['bldgResi'] += $value['AV'];
                    $getCurrentCancelledTaxable['bldgResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'B'){
                            
                    $getCurrentCancelledTaxable['bldgComm'] += $value['AV'];
                    $getCurrentCancelledTaxable['bldgCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'B'){
                            
                    $getCurrentCancelledTaxable['bldgIndu'] += $value['AV'];
                    $getCurrentCancelledTaxable['bldgInduParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'M'){
                                
                    $getCurrentCancelledTaxable['machResi'] += $value['AV'];
                    $getCurrentCancelledTaxable['machResiParcel'] += 1;
                        
                }
    
                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'M'){
                            
                $getCurrentCancelledTaxable['machComm'] += $value['AV'];
                $getCurrentCancelledTaxable['machCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                            
                $getCurrentCancelledTaxable['machSpec'] += $value['AV'];
                $getCurrentCancelledTaxable['machSpecParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specRecreationalLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentCancelledTaxable['specRecreationalImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specRecreationalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specHospitalLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specHospitalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentCancelledTaxable['specHospitalImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specHospitalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specCulturalLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specCulturalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentCancelledTaxable['specCulturalImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specCulturalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specSpecialLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specSpecialLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentCancelledTaxable['specSpecialImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specSpecialImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specRecreationalLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentCancelledTaxable['specScientificImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specScientificImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specLocWaterDistLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specLocWaterDistLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getCurrentCancelledTaxable['specLocWaterDistImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specLocWaterDistImpParcel'] += 1;
                    
                } 
                
                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'L'){
                    
                    $getCurrentCancelledTaxable['specOthersLand']  += $value['AV'];
                    $getCurrentCancelledTaxable['specOthersLandParcel'] += 1;
                        
                    }
    
                if ($value['AUDesc'] == 'OTHERS' && ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                        
                    $getCurrentCancelledTaxable['specOthersImp'] += $value['AV'];
                    $getCurrentCancelledTaxable['specOthersImpParcel'] += 1;
                        
                }   
                
            }
        }

        //------------------------for all presiding taxable ---------------------------//
    
        //all land data taxable
        $getPresidingActiveTaxable['landResi'] = 0.00;
        $getPresidingActiveTaxable['landComm'] = 0.00;
        $getPresidingActiveTaxable['landIndu'] = 0.00;
        $getPresidingActiveTaxable['landResiParcel'] = 0;
        $getPresidingActiveTaxable['landCommParcel'] = 0;
        $getPresidingActiveTaxable['landInduParcel'] = 0;

        //all imp data taxable
        $getPresidingActiveTaxable['bldgResi'] = 0.00;
        $getPresidingActiveTaxable['bldgComm'] = 0.00;
        $getPresidingActiveTaxable['bldgIndu'] = 0.00;
        $getPresidingActiveTaxable['bldgResiParcel'] = 0;
        $getPresidingActiveTaxable['bldgCommParcel'] = 0;
        $getPresidingActiveTaxable['bldgInduParcel'] = 0;

        //all mach data taxable
        $getPresidingActiveTaxable['machResi'] = 0.00;
        $getPresidingActiveTaxable['machComm'] = 0.00;
        $getPresidingActiveTaxable['machSpec'] = 0.00;
        $getPresidingActiveTaxable['machResiParcel'] = 0;
        $getPresidingActiveTaxable['machCommParcel'] = 0;
        $getPresidingActiveTaxable['machSpecParcel'] = 0;

            //all spec data taxable
        $getPresidingActiveTaxable['specRecreationalLand'] = 0.00;
        $getPresidingActiveTaxable['specHospitalLand'] = 0.00;
        $getPresidingActiveTaxable['specCulturalLand'] = 0.00;
        $getPresidingActiveTaxable['specSpecialLand'] = 0.00;
        $getPresidingActiveTaxable['specScientificLand'] = 0.00;
        $getPresidingActiveTaxable['specLocWaterDistLand'] = 0.00;
        $getPresidingActiveTaxable['specOthersLand'] = 0.00;

        $getPresidingActiveTaxable['specRecreationalImp'] = 0.00;
        $getPresidingActiveTaxable['specHospitalImp'] = 0.00;
        $getPresidingActiveTaxable['specCulturalImp'] = 0.00;
        $getPresidingActiveTaxable['specSpecialImp'] = 0.00;
        $getPresidingActiveTaxable['specScientificImp'] = 0.00;
        $getPresidingActiveTaxable['specLocWaterDistImp'] = 0.00;
        $getPresidingActiveTaxable['specOthersImp'] = 0.00;
            
        $getPresidingActiveTaxable['specRecreationalLandParcel'] = 0;
        $getPresidingActiveTaxable['specHospitalLandParcel'] = 0;
        $getPresidingActiveTaxable['specCulturalLandParcel'] = 0;
        $getPresidingActiveTaxable['specSpecialLandParcel'] = 0;
        $getPresidingActiveTaxable['specScientificLandParcel'] = 0;
        $getPresidingActiveTaxable['specLocWaterDistLandParcel'] = 0;
        $getPresidingActiveTaxable['specOthersLandParcel'] = 0;
            
        $getPresidingActiveTaxable['specRecreationalImpParcel'] = 0;
        $getPresidingActiveTaxable['specHospitalImpParcel'] = 0;
        $getPresidingActiveTaxable['specCulturalImpParcel'] = 0;
        $getPresidingActiveTaxable['specSpecialImpParcel'] = 0;
        $getPresidingActiveTaxable['specScientificImpParcel'] = 0;
        $getPresidingActiveTaxable['specLocWaterDistImpParcel'] = 0;
        $getPresidingActiveTaxable['specOthersImpParcel'] = 0;
 
    
        foreach($queryPresidingAssessmentReport as $key => $value){

            
            if(
                
                $value['Taxability']=='T'
            )
            
            {
            
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'L' ){
                    $getPresidingActiveTaxable['landResi'] += $value['AV'];
                    $getPresidingActiveTaxable['landResiParcel'] += 1;
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['landComm'] += $value['AV'];
                    $getPresidingActiveTaxable['landCommParcel'] += 1;
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['landIndu'] += $value['AV'];
                    $getPresidingActiveTaxable['landInduParcel'] += 1;
                    
                }

                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'B'){
                            
                    $getPresidingActiveTaxable['bldgResi'] += $value['AV'];
                    $getPresidingActiveTaxable['bldgResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'B'){
                                
                    $getPresidingActiveTaxable['bldgComm'] += $value['AV'];
                    $getPresidingActiveTaxable['bldgCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'INDUSTRIAL' && $value['kind'] == 'B'){
                            
                $getPresidingActiveTaxable['bldgIndu'] += $value['AV'];
                $getPresidingActiveTaxable['bldgInduParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'RESIDENTIAL' && $value['kind'] == 'M'){
                            
                    $getPresidingActiveTaxable['machResi'] += $value['AV'];
                    $getPresidingActiveTaxable['machResiParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'COMMERCIAL' && $value['kind'] == 'M'){
                                
                    $getPresidingActiveTaxable['machComm'] += $value['AV'];
                    $getPresidingActiveTaxable['machCommParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'M'){
                                
                    $getPresidingActiveTaxable['machSpec'] += $value['AV'];
                    $getPresidingActiveTaxable['machSpecParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specRecreationalLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'RECREATIONAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specRecreationalImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specRecreationalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specHospitalLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specHospitalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'HOSPITAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specHospitalImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specHospitalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specCulturalLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specCulturalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'CULTURAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specCulturalImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specCulturalImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specSpecialLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specSpecialLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SPECIAL' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specSpecialImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specSpecialImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specRecreationalLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specRecreationalLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'SCIENTIFIC' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specScientificImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specScientificImpParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specLocWaterDistLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specLocWaterDistLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'LOCAL WATER DISTRICT' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specLocWaterDistImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specLocWaterDistImpParcel'] += 1;
                    
                }
                
                if ($value['AUDesc'] == 'OTHERS' && $value['kind'] == 'L'){
                    
                    $getPresidingActiveTaxable['specOthersLand']  += $value['AV'];
                    $getPresidingActiveTaxable['specOthersLandParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHERS' && 
                    ($value['kind'] == 'B' || $value['kind'] == 'M') ){
                    
                    $getPresidingActiveTaxable['specOthersImp'] += $value['AV'];
                    $getPresidingActiveTaxable['specOthersImpParcel'] += 1;
                    
                }                
                
            }
        }
    
    
        #-----------------------------------------------EXEMPTED---------------------------------------------#
        
        //------------------------for all month active exempted ---------------------------//
        
        //all land data taxable
        $xGetAllExempted['landGovt'] = 0.00;
        $xGetAllExempted['landReli'] = 0.00;
        $xGetAllExempted['landChar'] = 0.00;
        $xGetAllExempted['landEduc'] = 0.00;
        $xGetAllExempted['landOthe'] = 0.00;
    
        $xGetAllExempted['landGovtParcel'] = 0;
        $xGetAllExempted['landReliParcel'] = 0;
        $xGetAllExempted['landCharParcel'] = 0;
        $xGetAllExempted['landEducParcel'] = 0;
        $xGetAllExempted['landOtheParcel'] = 0;
      

        //all imp data exempted
        $xGetAllExempted['impGovt'] = 0.00;
        $xGetAllExempted['impReli'] = 0.00;
        $xGetAllExempted['impChar'] = 0.00;
        $xGetAllExempted['impEduc'] = 0.00;
        $xGetAllExempted['impOthe'] = 0.00;
        $xGetAllExempted['impMach'] = 0.00;
        $xGetAllExempted['impGovtParcel'] = 0;
        $xGetAllExempted['impReliParcel'] = 0;
        $xGetAllExempted['impCharParcel'] = 0;
        $xGetAllExempted['impEducParcel'] = 0;
        $xGetAllExempted['impOtheParcel'] = 0;
        $xGetAllExempted['impMachParcel'] = 0;

        foreach($queryAllAssessmentReport as $key => $value){
        
            if(
                $value['Taxability']=='E'
            )
            {
                
                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'L' ){
                                
                    $xGetAllExempted['landGovt'] += $value['AV'];
                    $xGetAllExempted['landGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'L'){
                                
                    $xGetAllExempted['landReli'] += $value['AV'];
                    $xGetAllExempted['landReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'L'){
                    
                    $xGetAllExempted['landChar'] += $value['AV'];
                    $xGetAllExempted['landCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'L'){
                    
                    $xGetAllExempted['landEduc'] += $value['AV'];
                    $xGetAllExempted['landEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'L'){
                    
                    $xGetAllExempted['landOthe'] += $value['AV'];
                    $xGetAllExempted['landOtheParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'B' ){
                                
                    $xGetAllExempted['impGovt'] += $value['AV'];
                    $xGetAllExempted['impGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'B'){
                                
                    $xGetAllExempted['impReli'] += $value['AV'];
                    $xGetAllExempted['impReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'B'){
                    
                    $xGetAllExempted['impChar'] += $value['AV'];
                    $xGetAllExempted['impCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'B'){
                    
                    $xGetAllExempted['impEduc'] += $value['AV'];
                    $xGetAllExempted['impEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'B'){
                    
                    $xGetAllExempted['impOthe'] += $value['AV'];
                    $xGetAllExempted['impOtheParcel'] += 1;
                    
                }

                if ($value['kind'] == 'M'){
                    
                    $xGetAllExempted['impMach'] += $value['AV'];
                    $xGetAllExempted['impMachParcel'] += 1;
                    
                }
                
            }
        }
              
    

         //------------------------for present exemptende ---------------------------//
 
        $xGetCurrentActiveExempted['landGovt'] = 0.00;
        $xGetCurrentActiveExempted['landReli'] = 0.00;
        $xGetCurrentActiveExempted['landChar'] = 0.00;
        $xGetCurrentActiveExempted['landEduc'] = 0.00;
        $xGetCurrentActiveExempted['landOthe'] = 0.00;
    
        $xGetCurrentActiveExempted['impGovt'] = 0.00;
        $xGetCurrentActiveExempted['impReli'] = 0.00;
        $xGetCurrentActiveExempted['impChar'] = 0.00;
        $xGetCurrentActiveExempted['impEduc'] = 0.00;
        $xGetCurrentActiveExempted['impOthe'] = 0.00;
        $xGetCurrentActiveExempted['impMach'] = 0.00;

        $xGetCurrentActiveExempted['landGovtParcel'] = 0;
        $xGetCurrentActiveExempted['landReliParcel'] = 0;
        $xGetCurrentActiveExempted['landCharParcel'] = 0;
        $xGetCurrentActiveExempted['landEducParcel'] = 0;
        $xGetCurrentActiveExempted['landOtheParcel'] = 0;
    
        $xGetCurrentActiveExempted['impGovtParcel'] = 0;
        $xGetCurrentActiveExempted['impReliParcel'] = 0;
        $xGetCurrentActiveExempted['impCharParcel'] = 0;
        $xGetCurrentActiveExempted['impEducParcel'] = 0;
        $xGetCurrentActiveExempted['impOtheParcel'] = 0;
        $xGetCurrentActiveExempted['impMachParcel'] = 0;
  
        foreach($queryAllPresentReport as $key => $value){

            foreach($queryAllPresentReport as $key1 => $value1){
                if (trim($value1['Prev_Arp']) == trim($value['ARP'])){

                    $value['Current'] = 2;
                }
            }
        
            if(
                $value['Taxability']=='E'
                && $value['Current'] != 2
            )
            {
                
                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'L' ){
                                
                    $xGetCurrentActiveExempted['landGovt'] += $value['AV'];
                    $xGetCurrentActiveExempted['landGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'L'){
                                
                    $xGetCurrentActiveExempted['landReli'] += $value['AV'];
                    $xGetCurrentActiveExempted['landReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'L'){
                    
                    $xGetCurrentActiveExempted['landChar'] += $value['AV'];
                    $xGetCurrentActiveExempted['landCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'L'){
                    
                    $xGetCurrentActiveExempted['landEduc'] += $value['AV'];
                    $xGetCurrentActiveExempted['landEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'L'){
                    
                    $xGetCurrentActiveExempted['landOthe'] += $value['AV'];
                    $xGetCurrentActiveExempted['landOtheParcel'] += 1;
                    
                }

            

                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'B' ){
                                
                    $xGetCurrentActiveExempted['impGovt'] += $value['AV'];
                    $xGetCurrentActiveExempted['impGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'B'){
                                
                    $xGetCurrentActiveExempted['impReli'] += $value['AV'];
                    $xGetCurrentActiveExempted['impReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'B'){
                    
                    $xGetCurrentActiveExempted['impChar'] += $value['AV'];
                    $xGetCurrentActiveExempted['impCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'B'){
                    
                    $xGetCurrentActiveExempted['impEduc'] += $value['AV'];
                    $xGetCurrentActiveExempted['impEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'B'){
                    
                    $xGetCurrentActiveExempted['impOthe'] += $value['AV'];
                    $xGetCurrentActiveExempted['impOtheParcel'] += 1;
                    
                }

                if ($value['kind'] == 'M'){
                    
                    $xGetCurrentActiveExempted['impMach'] += $value['AV'];
                    $xGetCurrentActiveExempted['impMachParcel'] += 1;
                    
                }
                
            }
        }
               
           //------------------------for cancelled exempted ---------------------------//
         
        //cancelled land data exempted
        $xGetCurrentCancelledExempted['landGovt'] = 0.00;
        $xGetCurrentCancelledExempted['landReli'] = 0.00;
        $xGetCurrentCancelledExempted['landChar'] = 0.00;
        $xGetCurrentCancelledExempted['landEduc'] = 0.00;
        $xGetCurrentCancelledExempted['landOthe'] = 0.00;
        
        $xGetCurrentCancelledExempted['landGovtParcel'] = 0;
        $xGetCurrentCancelledExempted['landReliParcel'] = 0;
        $xGetCurrentCancelledExempted['landCharParcel'] = 0;
        $xGetCurrentCancelledExempted['landEducParcel'] = 0;
        $xGetCurrentCancelledExempted['landOtheParcel'] = 0;
        

        //cancelled  imp data exempted
        $xGetCurrentCancelledExempted['impGovt'] = 0.00;
        $xGetCurrentCancelledExempted['impReli'] = 0.00;
        $xGetCurrentCancelledExempted['impChar'] = 0.00;
        $xGetCurrentCancelledExempted['impEduc'] = 0.00;
        $xGetCurrentCancelledExempted['impOthe'] = 0.00;
        $xGetCurrentCancelledExempted['impMach'] = 0.00;
        $xGetCurrentCancelledExempted['impGovtParcel'] = 0;
        $xGetCurrentCancelledExempted['impReliParcel'] = 0;
        $xGetCurrentCancelledExempted['impCharParcel'] = 0;
        $xGetCurrentCancelledExempted['impEducParcel'] = 0;
        $xGetCurrentCancelledExempted['impOtheParcel'] = 0;
        $xGetCurrentCancelledExempted['impMachParcel'] = 0;

        
        foreach($queryAllCancelledReport as $key => $value){
        
            if(
                $value['Taxability']=='E'
            )
            { 
            
                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'L' ){
                                
                    $xGetCurrentCancelledExempted['landGovt'] += $value['AV'];
                    $xGetCurrentCancelledExempted['landGovtParcel'] += 1;
                }
    
                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'L'){
                                
                    $xGetCurrentCancelledExempted['landReli'] += $value['AV'];
                    $xGetCurrentCancelledExempted['landReliParcel'] += 1;
                }
    
                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'L'){
                    
                    $xGetCurrentCancelledExempted['landChar'] += $value['AV'];
                    $xGetCurrentCancelledExempted['landCharParcel'] += 1;
                    
                }
    
                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'L'){
                    
                    $xGetCurrentCancelledExempted['landEduc'] += $value['AV'];
                    $xGetCurrentCancelledExempted['landEducParcel'] += 1;
                    
                }
    
                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'L'){
                    
                    $xGetCurrentCancelledExempted['landOthe'] += $value['AV'];
                    $xGetCurrentCancelledExempted['landOtheParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'B' ){
                                    
                    $xGetCurrentCancelledExempted['impGovt'] += $value['AV'];
                    $xGetCurrentCancelledExempted['impGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'B'){
                                
                    $xGetCurrentCancelledExempted['impReli'] += $value['AV'];
                    $xGetCurrentCancelledExempted['impReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'B'){
                    
                    $xGetCurrentCancelledExempted['impChar'] += $value['AV'];
                    $xGetCurrentCancelledExempted['impCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'B'){
                    
                    $xGetCurrentCancelledExempted['impEduc'] += $value['AV'];
                    $xGetCurrentCancelledExempted['impEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'B'){
                    
                    $xGetCurrentCancelledExempted['impOthe'] += $value['AV'];
                    $xGetCurrentCancelledExempted['impOtheParcel'] += 1;
                    
                }

                if ($value['kind'] == 'M' ){
                    
                    $xGetCurrentCancelledExempted['impMach'] += $value['AV'];
                    $xGetCurrentCancelledExempted['impMachParcel'] += 1;
                    
                }

            }
        }

         //------------------------for all month active exempted ---------------------------//
        
        //all land data taxable
        $xGetPresidingExempted['landGovt'] = 0.00;
        $xGetPresidingExempted['landReli'] = 0.00;
        $xGetPresidingExempted['landChar'] = 0.00;
        $xGetPresidingExempted['landEduc'] = 0.00;
        $xGetPresidingExempted['landOthe'] = 0.00;
    
        $xGetPresidingExempted['landGovtParcel'] = 0;
        $xGetPresidingExempted['landReliParcel'] = 0;
        $xGetPresidingExempted['landCharParcel'] = 0;
        $xGetPresidingExempted['landEducParcel'] = 0;
        $xGetPresidingExempted['landOtheParcel'] = 0;
      

        //all imp data exempted
        $xGetPresidingExempted['impGovt'] = 0.00;
        $xGetPresidingExempted['impReli'] = 0.00;
        $xGetPresidingExempted['impChar'] = 0.00;
        $xGetPresidingExempted['impEduc'] = 0.00;
        $xGetPresidingExempted['impOthe'] = 0.00;
        $xGetPresidingExempted['impMach'] = 0.00;
        $xGetPresidingExempted['impGovtParcel'] = 0;
        $xGetPresidingExempted['impReliParcel'] = 0;
        $xGetPresidingExempted['impCharParcel'] = 0;
        $xGetPresidingExempted['impEducParcel'] = 0;
        $xGetPresidingExempted['impOtheParcel'] = 0;
        $xGetPresidingExempted['impMachParcel'] = 0;

        foreach($queryPresidingAssessmentReport as $key => $value){
        
            if(
                $value['Taxability']=='E'
            )
            {
                
                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'L' ){
                                
                    $xGetPresidingExempted['landGovt'] += $value['AV'];
                    $xGetPresidingExempted['landGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'L'){
                                
                    $xGetPresidingExempted['landReli'] += $value['AV'];
                    $xGetPresidingExempted['landReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'L'){
                    
                    $xGetPresidingExempted['landChar'] += $value['AV'];
                    $xGetPresidingExempted['landCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'L'){
                    
                    $xGetPresidingExempted['landEduc'] += $value['AV'];
                    $xGetPresidingExempted['landEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'L'){
                    
                    $xGetPresidingExempted['landOthe'] += $value['AV'];
                    $xGetPresidingExempted['landOtheParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'GOVERNMENT' && $value['kind'] == 'B' ){
                                
                    $xGetPresidingExempted['impGovt'] += $value['AV'];
                    $xGetPresidingExempted['impGovtParcel'] += 1;
                }

                if ($value['AUDesc'] == 'RELIGIOUS' && $value['kind'] == 'B'){
                                
                    $xGetPresidingExempted['impReli'] += $value['AV'];
                    $xGetPresidingExempted['impReliParcel'] += 1;
                }

                if ($value['AUDesc'] == 'CHARITABLE' && $value['kind'] == 'B'){
                    
                    $xGetPresidingExempted['impChar'] += $value['AV'];
                    $xGetPresidingExempted['impCharParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'EDUCATIONAL' && $value['kind'] == 'B'){
                    
                    $xGetPresidingExempted['impEduc'] += $value['AV'];
                    $xGetPresidingExempted['impEducParcel'] += 1;
                    
                }

                if ($value['AUDesc'] == 'OTHER LAWS' && $value['kind'] == 'B'){
                    
                    $xGetPresidingExempted['impOthe'] += $value['AV'];
                    $xGetPresidingExempted['impOtheParcel'] += 1;
                    
                }

                if ($value['kind'] == 'M'){
                    
                    $xGetPresidingExempted['impMach'] += $value['AV'];
                    $xGetPresidingExempted['impMachParcel'] += 1;
                    
                }
                
            }
        }
            
        return self::createExcel( $getCurrentActiveTaxable, $getCurrentCancelledTaxable,  
            $getAllActiveTaxable, $getPresidingActiveTaxable,
            $xGetCurrentActiveExempted, $xGetCurrentCancelledExempted, $xGetAllExempted, $xGetPresidingExempted,
            $month, $year,  $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate);
    }

    //for monthly assessment report
    private function createExcel($current, $cancellation, $getAllAssmntTaxable, $getPresidingTaxable, $xCurrent,  
        $xCancellation, $xGetAllAssmntExempted, $xGetPresidingExempted,
         $month, $year,  $dateCreated, $datePick, $quarter, $firstMonth, $endMonth, $rangeDate){
        
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $nameMonth = date("F", mktime(0, 0, 0, $month, 10));
        $startMonth = date("F", mktime(0, 0, 0, $firstMonth, 10));
        $endMonth = date("F", mktime(0, 0, 0, $endMonth, 10));

        
        if ($datePick == 'Daily'){
            $title = "Data Assessment Daily Report";
            $titleHeading = "Day";
            $asOfDate = "Date as of: " .$dateCreated;
            
        }
        
        if ($datePick == 'Monthly'){
            $title = "Data Assessment Monthly Report";
            $titleHeading = "Month";
            $asOfDate = "For the month of " .$nameMonth ." " .$year;
        }
        

        if ($datePick == 'Yearly'){
            $title = $year."Data Assessment Yearly Report";
            $titleHeading = $year;
            $asOfDate = "Year " .$year;
        }

        if ($datePick == 'Range'){
            $title = "Data Assessment Range Report";
            $titleHeading = 'Range';
            $asOfDate = "From " .$rangeDate[0] ." to " .$rangeDate[1];
        }


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setTitle($title);
        
        $spreadsheet->getSheetByName($title);
        $worksheet = $spreadsheet->getActiveSheet();

        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates('O1')
                ->setOffsetX(50)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates('P1')
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
        //REPUBLIC OF THE PHILIPPINES
        $worksheet->mergeCells('A1:Q1');
        $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
        $worksheet->getStyle("A1")->getFont()->setSize(12);
        $worksheet->getStyle('A1:Q1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A1:Q1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //CITY OF MANILA
        $worksheet->mergeCells('A2:Q2');
        $worksheet->setCellValue("A2", "CITY OF MANILA");
        $worksheet->getStyle("A2:Q2")->getFont()->setBold(true);
        $worksheet->getStyle("A2")->getFont()->setSize(14);
        $worksheet->getStyle('A2:Q2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A2:Q2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //DEPARTMENT OF ASSESSMENT
        $worksheet->mergeCells('A3:Q3');
        $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
        $worksheet->getStyle("A3:Q3")->getFont()->setBold(true);
        $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
        $worksheet->getStyle('A3:Q3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A3:Q3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //*Title of Report
        $worksheet->mergeCells('A5:Q6');
        $worksheet->setCellValue("A5", $title);
        $worksheet->getStyle("A5:Q6")->getFont()->setBold(true);
        $worksheet->getStyle("A5")->getFont()->setSize(18);
        $worksheet->getStyle('A5:Q6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A5:Q6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //*Date of Report
        $worksheet->mergeCells('A7:Q7');
        $worksheet->getStyle('A7:Q7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("A7", $asOfDate);
        $worksheet->getStyle("A7")->getFont()->setSize(12)->setBold(true);

        $worksheet->setCellValue("K4", "printed date:  ". date("F j, Y"));
        $worksheet->getStyle("K4")->getFont()->setSize(8);

        //---------------------setup width
        $worksheet->getColumnDimension('A')->setWidth(30);
        $worksheet->getColumnDimension('B')->setWidth(20);
        $worksheet->getColumnDimension('C')->setWidth(20);
        $worksheet->getColumnDimension('D')->setWidth(10);
        $worksheet->getColumnDimension('E')->setWidth(10);
        $worksheet->getColumnDimension('F')->setWidth(20);
        $worksheet->getColumnDimension('G')->setWidth(20);
        $worksheet->getColumnDimension('H')->setWidth(10);
        $worksheet->getColumnDimension('I')->setWidth(10);
        $worksheet->getColumnDimension('J')->setWidth(20);
        $worksheet->getColumnDimension('K')->setWidth(20);
        $worksheet->getColumnDimension('L')->setWidth(10);
        $worksheet->getColumnDimension('M')->setWidth(10);
        $worksheet->getColumnDimension('N')->setWidth(20);
        $worksheet->getColumnDimension('O')->setWidth(20);
        $worksheet->getColumnDimension('P')->setWidth(10);
        $worksheet->getColumnDimension('Q')->setWidth(10);


        //-----------------------setup merge cell, style and text for header

        $worksheet->mergeCells('A9:A11')
                ->setCellValue("A9", "PROPERTY CLASSIFICATION ");
        $worksheet->mergeCells('B9:E9')
                ->setCellValue("B9", "Assessment existing at the end of the preceeding ". $titleHeading);
        $worksheet->mergeCells('F9:I9')
                ->setCellValue("F9", "Assessment made during the present " .$titleHeading);
        $worksheet->mergeCells('J9:M9')
                ->setCellValue("J9", "Cancellation made during the present ".$titleHeading);
        $worksheet->mergeCells('N9:Q9')
                ->setCellValue("N9", "Assessment during the end of the present ".$titleHeading);

        $worksheet->mergeCells('B10:C10')
                ->setCellValue("B10", "ASSESSED VALUE");
        $worksheet->mergeCells('D10:E10')
                ->setCellValue("D10", "PARCEL COUNT");
        $worksheet->mergeCells('F10:G10')
                ->setCellValue("F10", "ASSESSED VALUE");
        $worksheet->mergeCells('H10:I10')
                ->setCellValue("H10", "PARCEL COUNT");
        $worksheet->mergeCells('J10:K10')
                ->setCellValue("J10", "ASSESSED VALUE");
        $worksheet->mergeCells('L10:M10')
                ->setCellValue("L10", "PARCEL COUNT");
        $worksheet->mergeCells('N10:O10')
                ->setCellValue("N10", "ASSESSED VALUE");
        $worksheet->mergeCells('P10:Q10')
                ->setCellValue("P10", "PARCEL COUNT");
        
        $worksheet->setCellValue('B11', "Land");
        $worksheet->setCellValue('C11', "Imp");
        $worksheet->setCellValue('D11', "Land");
        $worksheet->setCellValue('E11', "Imp");
        $worksheet->setCellValue('F11', "Land");
        $worksheet->setCellValue('G11', "Imp");
        $worksheet->setCellValue('H11', "Land");
        $worksheet->setCellValue('I11', "Imp");
        $worksheet->setCellValue('J11', "Land");
        $worksheet->setCellValue('K11', "Imp");
        $worksheet->setCellValue('L11', "Land");
        $worksheet->setCellValue('M11', "Imp");
        $worksheet->setCellValue('N11', "Land");
        $worksheet->setCellValue('O11', "Imp");
        $worksheet->setCellValue('P11', "Land");
        $worksheet->setCellValue('Q11', "Imp");
        
        $styleHeader = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
        ];
        
        $worksheet->getStyle('A9:Q11')->applyFromArray($styleHeader);

        //-----------------------constant text, style for column A
        $worksheet->setCellValue('A12', "A. TAXABLE");
        $worksheet->setCellValue('A13', "1. Residential");
        $worksheet->setCellValue('A14', "2. Commercial");
        $worksheet->setCellValue('A15', "3. Industrial");
        $worksheet->setCellValue('A16', "4. Machinery");
        $worksheet->setCellValue('A17', " a. Residential");
        $worksheet->setCellValue('A18', " b. Commercial");
        $worksheet->setCellValue('A19', " c. Special");
        $worksheet->setCellValue('A20', "5. Special");
        $worksheet->setCellValue('A21', " a. Recreational");
        $worksheet->setCellValue('A22', " b. Hospital");
        $worksheet->setCellValue('A23', " c. Cultural");
        $worksheet->setCellValue('A24', " d. Special");
        $worksheet->setCellValue('A25', " e. Scientific");
        $worksheet->setCellValue('A26', " f. Local Water District");
        $worksheet->setCellValue('A27', " g. Others");
        $worksheet->setCellValue('A28', "TOTAL");

        $worksheet->mergeCells('A29:A32')
                ->setCellValue("A32", "GRAND TOTAL(TAXABLE)");
        
        $worksheet->setCellValue('A33', "B. EXEMPT");
        $worksheet->setCellValue('A35', "1. Government");
        $worksheet->setCellValue('A36', "2. Religious");
        $worksheet->setCellValue('A37', "3. Charitable");
        $worksheet->setCellValue('A38', "4. Educational");
        $worksheet->setCellValue('A39', "5. Other Laws");
        $worksheet->setCellValue('A40', "6. Machinery");
        $worksheet->setCellValue('A41', "TOTAL");
        
        $worksheet->mergeCells('A42:A44')
                ->setCellValue("A42", "GRAND TOTAL (EXEMPT)");
        
                // $worksheet->mergeCells('A46:Q47')
                // ->setCellValue("A46", "NOTE: In view of th 60% increment limitations under Sec. d(i) of Ord. 8330 in " . 
                //     "2014, repealing the 2nd tranche of 40% under Ord. 8516 in 2017 and the 20% reduction under " .  
                //     "Ord. 8567 in 2020, the AV we used as Tax Base as of " . date("F Y") . "is only " . "instead of " . 
                //     "with a difference of " . "representing the impact of the foregoing restrictions.")
                // ->getStyle('A46')->getAlignment()
                // ->setHorizontal('center')
                // ->setWrapText(true);
        
        //STYLE DATA
        $styleBordersData = [
            
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
        ];

        $styleBorderOutline = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ]
        ];

        $styleColumnA12 = [
            'font' => [
                'bold' => true,
            ],
            
        ];
        $styleColumnA27 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $styleColumnA28A31 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];


        $worksheet->getStyle('A12:Q28')->applyFromArray($styleBordersData);
        $worksheet->getStyle('A33:E41')->applyFromArray($styleBordersData);
        $worksheet->getStyle('A34:Q41')->applyFromArray($styleBordersData);
        $worksheet->getStyle('B29:E32')->applyFromArray($styleBorderOutline);
        $worksheet->getStyle('F29:I33')->applyFromArray($styleBorderOutline);
        $worksheet->getStyle('J29:M33')->applyFromArray($styleBorderOutline);
        $worksheet->getStyle('N29:Q33')->applyFromArray($styleBorderOutline);

        $worksheet->getStyle('B42:E44')->applyFromArray($styleBorderOutline);
        $worksheet->getStyle('F42:I44')->applyFromArray($styleBorderOutline);
        $worksheet->getStyle('J42:M44')->applyFromArray($styleBorderOutline);
        $worksheet->getStyle('N42:Q44')->applyFromArray($styleBorderOutline);
        
        $worksheet->getStyle('A12')->applyFromArray($styleColumnA12);
        $worksheet->getStyle('A28')->applyFromArray($styleColumnA27);
        $worksheet->getStyle('A29:A32')->applyFromArray($styleColumnA28A31);
        //DUPLICATE STYLE
        $worksheet->getStyle('A33')->applyFromArray($styleColumnA12);
        $worksheet->getStyle('A41')->applyFromArray($styleColumnA27);
        $worksheet->getStyle('A42')->applyFromArray($styleColumnA28A31);

        //-----------------------constant text, style for data cell

#------------------taxable content------------------------------

         //-------ASSESSMENT TABLES----------------

         $worksheet->setCellValue('N13', $getAllAssmntTaxable['landResi']);
         $worksheet->setCellValue('N14', $getAllAssmntTaxable['landComm']);
         $worksheet->setCellValue('N15', $getAllAssmntTaxable['landIndu']);
         $worksheet->setCellValue('N21', $getAllAssmntTaxable['specRecreationalLand']);
         $worksheet->setCellValue('N22', $getAllAssmntTaxable['specHospitalLand']);
         $worksheet->setCellValue('N23', $getAllAssmntTaxable['specCulturalLand']);
         $worksheet->setCellValue('N24', $getAllAssmntTaxable['specSpecialLand']);
         $worksheet->setCellValue('N25', $getAllAssmntTaxable['specScientificLand']);
         $worksheet->setCellValue('N26', $getAllAssmntTaxable['specLocWaterDistLand']);
         $worksheet->setCellValue('N27', $getAllAssmntTaxable['specOthersLand']);
         
         //total column B13-B26
         $worksheet->setCellValue('N28', '=SUM(N13:N27)')
                 ->getStyle('N28')->getFont()->setBold(true);
         
         $worksheet->setCellValue('O13', $getAllAssmntTaxable['bldgResi']);
         $worksheet->setCellValue('O14', $getAllAssmntTaxable['bldgComm']);
         $worksheet->setCellValue('O15', $getAllAssmntTaxable['bldgIndu']);
         $worksheet->setCellValue('O17', $getAllAssmntTaxable['machResi']);
         $worksheet->setCellValue('O18', $getAllAssmntTaxable['machComm']);
         $worksheet->setCellValue('O19', $getAllAssmntTaxable['machSpec']);
         $worksheet->setCellValue('O21', $getAllAssmntTaxable['specRecreationalImp']);
         $worksheet->setCellValue('O22', $getAllAssmntTaxable['specHospitalImp']);
         $worksheet->setCellValue('O23', $getAllAssmntTaxable['specCulturalImp']);
         $worksheet->setCellValue('O24', $getAllAssmntTaxable['specSpecialImp']);
         $worksheet->setCellValue('O25', $getAllAssmntTaxable['specScientificImp']);
         $worksheet->setCellValue('O26', $getAllAssmntTaxable['specLocWaterDistImp']);
         $worksheet->setCellValue('O27', $getAllAssmntTaxable['specOthersImp']);
 
         //total column C13-C26
         $worksheet->setCellValue('O28', '=SUM(O13:O27)')
                 ->getStyle('O28')->getFont()->setBold(true);
 
         $worksheet->setCellValue('P13', $getAllAssmntTaxable['landResiParcel']);
         $worksheet->setCellValue('P14', $getAllAssmntTaxable['landCommParcel']);
         $worksheet->setCellValue('P15', $getAllAssmntTaxable['landInduParcel']);
         
         $worksheet->setCellValue('P21', $getAllAssmntTaxable['specRecreationalLandParcel']);
         $worksheet->setCellValue('P22', $getAllAssmntTaxable['specHospitalLandParcel']);
         $worksheet->setCellValue('P23', $getAllAssmntTaxable['specCulturalLandParcel']);
         $worksheet->setCellValue('P24', $getAllAssmntTaxable['specSpecialLandParcel']);
         $worksheet->setCellValue('P25', $getAllAssmntTaxable['specScientificLandParcel']);
         $worksheet->setCellValue('P26', $getAllAssmntTaxable['specLocWaterDistLandParcel']);
         $worksheet->setCellValue('P27', $getAllAssmntTaxable['specOthersLandParcel']);
 
         //total column D13-D26
         $worksheet->setCellValue('P28', '=SUM(P13:P27)')
                 ->getStyle('P28')->getFont()->setBold(true);
         
         $worksheet->setCellValue('Q13', $getAllAssmntTaxable['bldgResiParcel']);
         $worksheet->setCellValue('Q14', $getAllAssmntTaxable['bldgCommParcel']);
         $worksheet->setCellValue('Q15', $getAllAssmntTaxable['bldgInduParcel']);
         $worksheet->setCellValue('Q17', $getAllAssmntTaxable['machResiParcel']);
         $worksheet->setCellValue('Q18', $getAllAssmntTaxable['machCommParcel']);
         $worksheet->setCellValue('Q19', $getAllAssmntTaxable['machSpecParcel']);
         $worksheet->setCellValue('Q21', $getAllAssmntTaxable['specRecreationalImpParcel']);
         $worksheet->setCellValue('Q22', $getAllAssmntTaxable['specHospitalImpParcel']);
         $worksheet->setCellValue('Q23', $getAllAssmntTaxable['specCulturalImpParcel']);
         $worksheet->setCellValue('Q24', $getAllAssmntTaxable['specSpecialImpParcel']);
         $worksheet->setCellValue('Q25', $getAllAssmntTaxable['specScientificImpParcel']);
         $worksheet->setCellValue('Q26', $getAllAssmntTaxable['specLocWaterDistImpParcel']);
         $worksheet->setCellValue('Q27', $getAllAssmntTaxable['specOthersImpParcel']);
 
         //total column E13-E26
         $worksheet->setCellValue('Q28', '=SUM(Q13:Q27)')
                 ->getStyle('Q28')->getFont()->setBold(true);

        //-------CURRENT TABLES----------------

        $worksheet->setCellValue('F13', $current['landResi']);
        $worksheet->setCellValue('F14', $current['landComm']);
        $worksheet->setCellValue('F15', $current['landIndu']);
        $worksheet->setCellValue('F21', $current['specRecreationalLand']);
        $worksheet->setCellValue('F22', $current['specHospitalLand']);
        $worksheet->setCellValue('F23', $current['specCulturalLand']);
        $worksheet->setCellValue('F24', $current['specSpecialLand']);
        $worksheet->setCellValue('F25', $current['specScientificLand']);
        $worksheet->setCellValue('F26', $current['specLocWaterDistLand']);
        $worksheet->setCellValue('F27', $current['specOthersLand']);
        
        //total column F13-F26
        $worksheet->setCellValue('F28', '=SUM(F13:F27)')
                ->getStyle('F28')->getFont()->setBold(true);
        
        $worksheet->setCellValue('G13', $current['bldgResi']);
        $worksheet->setCellValue('G14', $current['bldgComm']);
        $worksheet->setCellValue('G15', $current['bldgIndu']);
        $worksheet->setCellValue('G17', $current['machResi']);
        $worksheet->setCellValue('G18', $current['machComm']);
        $worksheet->setCellValue('G19', $current['machSpec']);
        $worksheet->setCellValue('G21', $current['specRecreationalImp']);
        $worksheet->setCellValue('G22', $current['specHospitalImp']);
        $worksheet->setCellValue('G23', $current['specCulturalImp']);
        $worksheet->setCellValue('G24', $current['specSpecialImp']);
        $worksheet->setCellValue('G25', $current['specScientificImp']);
        $worksheet->setCellValue('G26', $current['specLocWaterDistImp']);
        $worksheet->setCellValue('G27', $current['specOthersImp']);

        //total column G13-G26
        $worksheet->setCellValue('G28', '=SUM(G13:G27)')
                ->getStyle('G28')->getFont()->setBold(true);

        $worksheet->setCellValue('H13', $current['landResiParcel']);
        $worksheet->setCellValue('H14', $current['landCommParcel']);
        $worksheet->setCellValue('H15', $current['landInduParcel']);
        
        $worksheet->setCellValue('H21', $current['specRecreationalLandParcel']);
        $worksheet->setCellValue('H22', $current['specHospitalLandParcel']);
        $worksheet->setCellValue('H23', $current['specCulturalLandParcel']);
        $worksheet->setCellValue('H24', $current['specSpecialLandParcel']);
        $worksheet->setCellValue('H25', $current['specScientificLandParcel']);
        $worksheet->setCellValue('H26', $current['specLocWaterDistLandParcel']);
        $worksheet->setCellValue('H27', $current['specOthersLandParcel']);

        //total column H13-H26
        $worksheet->setCellValue('H28', '=SUM(H13:H27)')
                ->getStyle('H28')->getFont()->setBold(true);
        
        $worksheet->setCellValue('I13', $current['bldgResiParcel']);
        $worksheet->setCellValue('I14', $current['bldgCommParcel']);
        $worksheet->setCellValue('I15', $current['bldgInduParcel']);
        $worksheet->setCellValue('I17', $current['machResiParcel']);
        $worksheet->setCellValue('I18', $current['machCommParcel']);
        $worksheet->setCellValue('I19', $current['machSpecParcel']);
        $worksheet->setCellValue('I21', $current['specRecreationalImpParcel']);
        $worksheet->setCellValue('I22', $current['specHospitalImpParcel']);
        $worksheet->setCellValue('I23', $current['specCulturalImpParcel']);
        $worksheet->setCellValue('I24', $current['specSpecialImpParcel']);
        $worksheet->setCellValue('I25', $current['specScientificImpParcel']);
        $worksheet->setCellValue('I26', $current['specLocWaterDistImpParcel']);
        $worksheet->setCellValue('I27', $current['specOthersImpParcel']);

        //total column I13-I26
        $worksheet->setCellValue('I28', '=SUM(I13:I27)')
                ->getStyle('I28')->getFont()->setBold(true);

         //-------CANCELLED TABLES----------------

        $worksheet->setCellValue('J13', $cancellation['landResi']);
        $worksheet->setCellValue('J14', $cancellation['landComm']);
        $worksheet->setCellValue('J15', $cancellation['landIndu']);
        $worksheet->setCellValue('J21', $cancellation['specRecreationalLand']);
        $worksheet->setCellValue('J22', $cancellation['specHospitalLand']);
        $worksheet->setCellValue('J23', $cancellation['specCulturalLand']);
        $worksheet->setCellValue('J24', $cancellation['specSpecialLand']);
        $worksheet->setCellValue('J25', $cancellation['specScientificLand']);
        $worksheet->setCellValue('J26', $cancellation['specLocWaterDistLand']);
        $worksheet->setCellValue('J27', $cancellation['specOthersLand']);
         
        //total column J13-J26
        $worksheet->setCellValue('J28', '=SUM(J13:J27)')
                ->getStyle('J28')->getFont()->setBold(true);
        
        $worksheet->setCellValue('K13', $cancellation['bldgResi']);
        $worksheet->setCellValue('K14', $cancellation['bldgComm']);
        $worksheet->setCellValue('K15', $cancellation['bldgIndu']);
        $worksheet->setCellValue('K17', $cancellation['machResi']);
        $worksheet->setCellValue('K18', $cancellation['machComm']);
        $worksheet->setCellValue('K19', $cancellation['machSpec']);
        $worksheet->setCellValue('K21', $cancellation['specRecreationalImp']);
        $worksheet->setCellValue('K22', $cancellation['specHospitalImp']);
        $worksheet->setCellValue('K23', $cancellation['specCulturalImp']);
        $worksheet->setCellValue('K24', $cancellation['specSpecialImp']);
        $worksheet->setCellValue('K25', $cancellation['specScientificImp']);
        $worksheet->setCellValue('K26', $cancellation['specLocWaterDistImp']);
        $worksheet->setCellValue('K27', $cancellation['specOthersImp']);
 
        //total column K13-K26
        $worksheet->setCellValue('K28', '=SUM(K13:K27)')
                ->getStyle('K28')->getFont()->setBold(true);

        $worksheet->setCellValue('L13', $cancellation['landResiParcel']);
        $worksheet->setCellValue('L14', $cancellation['landCommParcel']);
        $worksheet->setCellValue('L15', $cancellation['landInduParcel']);
        
        $worksheet->setCellValue('L21', $cancellation['specRecreationalLandParcel']);
        $worksheet->setCellValue('L22', $cancellation['specHospitalLandParcel']);
        $worksheet->setCellValue('L23', $cancellation['specCulturalLandParcel']);
        $worksheet->setCellValue('L24', $cancellation['specSpecialLandParcel']);
        $worksheet->setCellValue('L25', $cancellation['specScientificLandParcel']);
        $worksheet->setCellValue('L26', $cancellation['specLocWaterDistLandParcel']);
        $worksheet->setCellValue('L27', $cancellation['specOthersLandParcel']);
 
        //total column L13-L26
        $worksheet->setCellValue('L28', '=SUM(L13:L27)')
                ->getStyle('L28')->getFont()->setBold(true);
        
        $worksheet->setCellValue('M13', $cancellation['bldgResiParcel']);
        $worksheet->setCellValue('M14', $cancellation['bldgCommParcel']);
        $worksheet->setCellValue('M15', $cancellation['bldgInduParcel']);
        $worksheet->setCellValue('M17', $cancellation['machResiParcel']);
        $worksheet->setCellValue('M18', $cancellation['machCommParcel']);
        $worksheet->setCellValue('M19', $cancellation['machSpecParcel']);
        $worksheet->setCellValue('M21', $cancellation['specRecreationalImpParcel']);
        $worksheet->setCellValue('M22', $cancellation['specHospitalImpParcel']);
        $worksheet->setCellValue('M23', $cancellation['specCulturalImpParcel']);
        $worksheet->setCellValue('M24', $cancellation['specSpecialImpParcel']);
        $worksheet->setCellValue('M25', $cancellation['specScientificImpParcel']);
        $worksheet->setCellValue('M26', $cancellation['specLocWaterDistImpParcel']);
        $worksheet->setCellValue('M27', $cancellation['specOthersImpParcel']);
 
        //total column I13-I26
        $worksheet->setCellValue('M28', '=SUM(M13:M27)')
                ->getStyle('M28')->getFont()->setBold(true);
        
        //------- PRECEEDING TABLES----------------

        $worksheet->setCellValue('B13', $getPresidingTaxable['landResi']);
        $worksheet->setCellValue('B14', $getPresidingTaxable['landComm']);
        $worksheet->setCellValue('B15', $getPresidingTaxable['landIndu']);
        $worksheet->setCellValue('B21', $getPresidingTaxable['specRecreationalLand']);
        $worksheet->setCellValue('B22', $getPresidingTaxable['specHospitalLand']);
        $worksheet->setCellValue('B23', $getPresidingTaxable['specCulturalLand']);
        $worksheet->setCellValue('B24', $getPresidingTaxable['specSpecialLand']);
        $worksheet->setCellValue('B25', $getPresidingTaxable['specScientificLand']);
        $worksheet->setCellValue('B26', $getPresidingTaxable['specLocWaterDistLand']);
        $worksheet->setCellValue('B27', $getPresidingTaxable['specOthersLand']);


         
        //total column N13-N26
        $worksheet->setCellValue('B28', '=SUM(B13:B27)')
                ->getStyle('B28')->getFont()->setBold(true);
        
        $worksheet->setCellValue('C13', $getPresidingTaxable['bldgResi']);
        $worksheet->setCellValue('C14', $getPresidingTaxable['bldgComm']);
        $worksheet->setCellValue('C15', $getPresidingTaxable['bldgIndu']);
        $worksheet->setCellValue('C17', $getPresidingTaxable['machResi']);
        $worksheet->setCellValue('C18', $getPresidingTaxable['machComm']);
        $worksheet->setCellValue('C19', $getPresidingTaxable['machSpec']);
        $worksheet->setCellValue('C21', $getPresidingTaxable['specRecreationalImp']);
        $worksheet->setCellValue('C22', $getPresidingTaxable['specHospitalImp']);
        $worksheet->setCellValue('C23', $getPresidingTaxable['specCulturalImp']);
        $worksheet->setCellValue('C24', $getPresidingTaxable['specSpecialImp']);
        $worksheet->setCellValue('C25', $getPresidingTaxable['specScientificImp']);
        $worksheet->setCellValue('C26', $getPresidingTaxable['specLocWaterDistImp']);
        $worksheet->setCellValue('C27', $getPresidingTaxable['specOthersImp']);

 
        //total column O13-O26
        $worksheet->setCellValue('C28', '=SUM(C13:C27)')
                ->getStyle('C28')->getFont()->setBold(true);

        $worksheet->setCellValue('D13', $getPresidingTaxable['landResiParcel']);
        $worksheet->setCellValue('D14', $getPresidingTaxable['landCommParcel']);
        $worksheet->setCellValue('D15', $getPresidingTaxable['landInduParcel']);
        $worksheet->setCellValue('D21', $getPresidingTaxable['specRecreationalLandParcel']);
        $worksheet->setCellValue('D22', $getPresidingTaxable['specHospitalLandParcel']);
        $worksheet->setCellValue('D23', $getPresidingTaxable['specCulturalLandParcel']);
        $worksheet->setCellValue('D24', $getPresidingTaxable['specSpecialLandParcel']);
        $worksheet->setCellValue('D25', $getPresidingTaxable['specScientificLandParcel']);
        $worksheet->setCellValue('D26', $getPresidingTaxable['specLocWaterDistLandParcel']);
        $worksheet->setCellValue('D27', $getPresidingTaxable['specOthersLandParcel']);


        //total column P13-P26
        $worksheet->setCellValue('D28', '=SUM(D13:D27)')
                ->getStyle('D28')->getFont()->setBold(true);
        
        $worksheet->setCellValue('E13', $getPresidingTaxable['bldgResiParcel']);
        $worksheet->setCellValue('E14', $getPresidingTaxable['bldgCommParcel']);
        $worksheet->setCellValue('E15', $getPresidingTaxable['bldgInduParcel']);
        $worksheet->setCellValue('E17', $getPresidingTaxable['machResiParcel']);
        $worksheet->setCellValue('E18', $getPresidingTaxable['machCommParcel']);
        $worksheet->setCellValue('E19', $getPresidingTaxable['machSpecParcel']);
        $worksheet->setCellValue('E21', $getPresidingTaxable['specRecreationalImpParcel']);
        $worksheet->setCellValue('E22', $getPresidingTaxable['specHospitalImpParcel']);
        $worksheet->setCellValue('E23', $getPresidingTaxable['specCulturalImpParcel']);
        $worksheet->setCellValue('E24', $getPresidingTaxable['specSpecialImpParcel']);
        $worksheet->setCellValue('E25', $getPresidingTaxable['specScientificImpParcel']);
        $worksheet->setCellValue('E26', $getPresidingTaxable['specLocWaterDistImpParcel']);
        $worksheet->setCellValue('E27', $getPresidingTaxable['specOthersImpParcel']);

 
        //total column Q13-Q26
        $worksheet->setCellValue('E28', '=SUM(E13:E27)')
                ->getStyle('E28')->getFont()->setBold(true);
        //Grand Total Taxable
        $worksheet->setCellValue('B30', '=SUM(B28:C28)')
                ->getStyle('B30')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('D30', '=SUM(D28:E28)')
                ->getStyle('D30')->getFont()->setBold(true)->setUnderline(true);
        
        $worksheet->setCellValue('F30', '=SUM(F28:G28)')
                ->getStyle('F30')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('H30', '=SUM(H28:I28)')
                ->getStyle('H30')->getFont()->setBold(true)->setUnderline(true);
        
        $worksheet->setCellValue('J30', '=SUM(J28:K28)')
                ->getStyle('J30')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('L30', '=SUM(L28:M28)')
                ->getStyle('L30')->getFont()->setBold(true)->setUnderline(true);
        
        $worksheet->setCellValue('N30', '=SUM(N28:O28)')
                ->getStyle('N30')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('P30', '=SUM(P28:Q28)')
                ->getStyle('P30')->getFont()->setBold(true)->setUnderline(true);

#---------------------------------------------------------------------------------------------------------------

#----------------------------------exempted content--------------------------------------------

//-------ASSESSMENT TABLES----------------

        $worksheet->setCellValue('N35', $xGetAllAssmntExempted['landGovt']);
        $worksheet->setCellValue('N36', $xGetAllAssmntExempted['landReli']);
        $worksheet->setCellValue('N37', $xGetAllAssmntExempted['landChar']);
        $worksheet->setCellValue('N38', $xGetAllAssmntExempted['landEduc']);
        $worksheet->setCellValue('N39', $xGetAllAssmntExempted['landOthe']);
      
       
        
        $worksheet->setCellValue('N41', '=SUM(N35:N39)')
                ->getStyle('N41')->getFont()->setBold(true);

        $worksheet->setCellValue('O35', $xGetAllAssmntExempted['impGovt']);
        $worksheet->setCellValue('O36', $xGetAllAssmntExempted['impReli']);
        $worksheet->setCellValue('O37', $xGetAllAssmntExempted['impChar']);
        $worksheet->setCellValue('O38', $xGetAllAssmntExempted['impEduc']);
        $worksheet->setCellValue('O39', $xGetAllAssmntExempted['impOthe']);
        $worksheet->setCellValue('O40', $xGetAllAssmntExempted['impMach']);
        
        
        $worksheet->setCellValue('O41', '=SUM(O35:O40)')
                ->getStyle('O41')->getFont()->setBold(true);

        $worksheet->setCellValue('P35', $xGetAllAssmntExempted['landGovtParcel']);
        $worksheet->setCellValue('P36', $xGetAllAssmntExempted['landReliParcel']);
        $worksheet->setCellValue('P37', $xGetAllAssmntExempted['landCharParcel']);
        $worksheet->setCellValue('P38', $xGetAllAssmntExempted['landEducParcel']);
        $worksheet->setCellValue('P39', $xGetAllAssmntExempted['landOtheParcel']);
       
    
         $worksheet->setCellValue('P41', '=SUM(P35:P39)')
         ->getStyle('P41')->getFont()->setBold(true);

         $worksheet->setCellValue('Q35', $xGetAllAssmntExempted['impGovtParcel']);
         $worksheet->setCellValue('Q36', $xGetAllAssmntExempted['impReliParcel']);
         $worksheet->setCellValue('Q37', $xGetAllAssmntExempted['impCharParcel']);
         $worksheet->setCellValue('Q38', $xGetAllAssmntExempted['impEducParcel']);
         $worksheet->setCellValue('Q39', $xGetAllAssmntExempted['impOtheParcel']);
         $worksheet->setCellValue('Q40', $xGetAllAssmntExempted['impMachParcel']);
     

          $worksheet->setCellValue('Q41', '=SUM(Q35:Q40)')
          ->getStyle('Q41')->getFont()->setBold(true);

        //-------CURRENT TABLES----------------

        $worksheet->setCellValue('F35', $xCurrent['landGovt']);
        $worksheet->setCellValue('F36', $xCurrent['landReli']);
        $worksheet->setCellValue('F37', $xCurrent['landChar']);
        $worksheet->setCellValue('F38', $xCurrent['landEduc']);
        $worksheet->setCellValue('F39', $xCurrent['landOthe']);
    

        $worksheet->setCellValue('F41', '=SUM(F35:F39)')
                ->getStyle('F41')->getFont()->setBold(true);

        $worksheet->setCellValue('G35', $xCurrent['impGovt']);
        $worksheet->setCellValue('G36', $xCurrent['impReli']);
        $worksheet->setCellValue('G37', $xCurrent['impChar']);
        $worksheet->setCellValue('G38', $xCurrent['impEduc']);
        $worksheet->setCellValue('G39', $xCurrent['impOthe']);
        $worksheet->setCellValue('G40', $xCurrent['impMach']);


        $worksheet->setCellValue('G41', '=SUM(G35:G40)')
                ->getStyle('G41')->getFont()->setBold(true);

        $worksheet->setCellValue('H35', $xCurrent['landGovtParcel']);
        $worksheet->setCellValue('H36', $xCurrent['landReliParcel']);
        $worksheet->setCellValue('H37', $xCurrent['landCharParcel']);
        $worksheet->setCellValue('H38', $xCurrent['landEducParcel']);
        $worksheet->setCellValue('H39', $xCurrent['landOtheParcel']);
       

         $worksheet->setCellValue('H41', '=SUM(H35:H40)')
         ->getStyle('H41')->getFont()->setBold(true);

         $worksheet->setCellValue('I35', $xCurrent['impGovtParcel']);
         $worksheet->setCellValue('I36', $xCurrent['impReliParcel']);
         $worksheet->setCellValue('I37', $xCurrent['impCharParcel']);
         $worksheet->setCellValue('I38', $xCurrent['impEducParcel']);
         $worksheet->setCellValue('I39', $xCurrent['impOtheParcel']);
         $worksheet->setCellValue('I40', $xCurrent['impMachParcel']);
     

          $worksheet->setCellValue('I41', '=SUM(I35:I40)')
          ->getStyle('I41')->getFont()->setBold(true);

        //-------CANCELLED TABLES----------------

        $worksheet->setCellValue('J35', $xCancellation['landGovt']);
        $worksheet->setCellValue('J36', $xCancellation['landReli']);
        $worksheet->setCellValue('J37', $xCancellation['landChar']);
        $worksheet->setCellValue('J38', $xCancellation['landEduc']);
        $worksheet->setCellValue('J39', $xCancellation['landOthe']);
       

        $worksheet->setCellValue('J41', '=SUM(J35:J39)')
                ->getStyle('J41')->getFont()->setBold(true);

        $worksheet->setCellValue('K35', $xCancellation['impGovt']);
        $worksheet->setCellValue('K36', $xCancellation['impReli']);
        $worksheet->setCellValue('K37', $xCancellation['impChar']);
        $worksheet->setCellValue('K38', $xCancellation['impEduc']);
        $worksheet->setCellValue('K39', $xCancellation['impOthe']);
        $worksheet->setCellValue('K40', $xCancellation['impMach']);
        
       
        $worksheet->setCellValue('K41', '=SUM(K35:K40)')
                ->getStyle('K41')->getFont()->setBold(true);

        $worksheet->setCellValue('L35', $xCancellation['landGovtParcel']);
        $worksheet->setCellValue('L36', $xCancellation['landReliParcel']);
        $worksheet->setCellValue('L37', $xCancellation['landCharParcel']);
        $worksheet->setCellValue('L38', $xCancellation['landEducParcel']);
        $worksheet->setCellValue('L39', $xCancellation['landOtheParcel']);
       
    
        $worksheet->setCellValue('L41', '=SUM(L35:L39)')
        ->getStyle('L41')->getFont()->setBold(true);

        $worksheet->setCellValue('M35', $xCancellation['impGovtParcel']);
        $worksheet->setCellValue('M36', $xCancellation['impReliParcel']);
        $worksheet->setCellValue('M37', $xCancellation['impCharParcel']);
        $worksheet->setCellValue('M38', $xCancellation['impEducParcel']);
        $worksheet->setCellValue('M39', $xCancellation['impOtheParcel']);
        $worksheet->setCellValue('M40', $xCancellation['impMachParcel']);
    
        //total column M34-M39
        $worksheet->setCellValue('M41', '=SUM(M35:M40)')
        ->getStyle('M41')->getFont()->setBold(true);

        //-------PRESIDING EXEMPTED----------------

        $worksheet->setCellValue('B35', $xGetPresidingExempted['landGovt']);
        $worksheet->setCellValue('B36', $xGetPresidingExempted['landReli']);
        $worksheet->setCellValue('B37', $xGetPresidingExempted['landChar']);
        $worksheet->setCellValue('B38', $xGetPresidingExempted['landEduc']);
        $worksheet->setCellValue('B39', $xGetPresidingExempted['landOthe']);
       
        //total column N34-N39
        $worksheet->setCellValue('B41', '=SUM(B35:B40)')
                ->getStyle('B41')->getFont()->setBold(true);

        $worksheet->setCellValue('C35',  $xGetPresidingExempted['impGovt']);
        $worksheet->setCellValue('C36',  $xGetPresidingExempted['impReli']);
        $worksheet->setCellValue('C37',  $xGetPresidingExempted['impChar']);
        $worksheet->setCellValue('C38',  $xGetPresidingExempted['impEduc']);
        $worksheet->setCellValue('C39',  $xGetPresidingExempted['impOthe']);
        $worksheet->setCellValue('C40',  $xGetPresidingExempted['impMach']);
        
        //total column O34-O39
        $worksheet->setCellValue('C41', '=SUM(C35:C40)')
                ->getStyle('C41')->getFont()->setBold(true);
        
        $worksheet->setCellValue('D35', $xGetPresidingExempted['landGovtParcel']);
        $worksheet->setCellValue('D36', $xGetPresidingExempted['landReliParcel']);
        $worksheet->setCellValue('D37', $xGetPresidingExempted['landCharParcel']);
        $worksheet->setCellValue('D38', $xGetPresidingExempted['landEducParcel']);
        $worksheet->setCellValue('D39', $xGetPresidingExempted['landOtheParcel']);
        $worksheet->setCellValue('D40', '=P40+L40-H40');
        
        //total column P34-P39
        $worksheet->setCellValue('D41', '=SUM(D35:D40)')
                ->getStyle('D41')->getFont()->setBold(true);
        
        $worksheet->setCellValue('E35', $xGetPresidingExempted['impGovtParcel']);
        $worksheet->setCellValue('E36', $xGetPresidingExempted['impReliParcel']);
        $worksheet->setCellValue('E37', $xGetPresidingExempted['impCharParcel']);
        $worksheet->setCellValue('E38', $xGetPresidingExempted['impEducParcel']);
        $worksheet->setCellValue('E39', $xGetPresidingExempted['impOtheParcel']);
        $worksheet->setCellValue('E40', $xGetPresidingExempted['impMachParcel']);
        //total column Q34-Q39
        $worksheet->setCellValue('E41', '=SUM(E35:E40)')
                ->getStyle('E41')->getFont()->setBold(true);

     
        //Grand Total Exempted
        $worksheet->setCellValue('B43', '=SUM(B41:C41)')
                ->getStyle('B43')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('D43', '=SUM(D41:E41)')
                ->getStyle('D43')->getFont()->setBold(true)->setUnderline(true);

        $worksheet->setCellValue('F43', '=SUM(F41:G41)')
                ->getStyle('F43')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('H43', '=SUM(H41:I41)')
                ->getStyle('H43')->getFont()->setBold(true)->setUnderline(true);

        $worksheet->setCellValue('J43', '=SUM(J41:K41)')
                ->getStyle('J43')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('L43', '=SUM(L41:M41)')
                ->getStyle('L43')->getFont()->setBold(true)->setUnderline(true);

        $worksheet->setCellValue('N43', '=SUM(N41:O41)')
                ->getStyle('N43')->getFont()->setBold(true)->setUnderline(true);
        $worksheet->setCellValue('P43', '=SUM(P41:Q41)')
                ->getStyle('P43')->getFont()->setBold(true)->setUnderline(true);

#--------------------------------------------------------------------------------------

        //more style
        $worksheet->getStyle('B13:C43')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $worksheet->getStyle('D13:E43')->getNumberFormat()->setFormatCode('#,###,###,###');
        
        $worksheet->getStyle('F13:G43')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $worksheet->getStyle('H13:I43')->getNumberFormat()->setFormatCode('#,###,###,###');
        
        $worksheet->getStyle('J13:K43')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $worksheet->getStyle('L13:M43')->getNumberFormat()->setFormatCode('#,###,###,###');

        $worksheet->getStyle('N13:O43')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $worksheet->getStyle('P13:Q43')->getNumberFormat()->setFormatCode('#,###,###,###');
        
        


        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;
    }

    private function findingConsolidation($query){
        $oldKey = 0;   
        foreach($query as $key => $value){
           

            if($key > 0){
                if ($value['ARP'] == $query[$oldKey]['ARP'] && $value['AUDesc'] == $query[$oldKey]['AUDesc'] ){
                    $query[$key]['Current'] = 2;
                }
            }

            if ($key < 1){
                $oldKey = 0;
            
            }
            else {
                $oldKey++;
                
            }
            
        }

        return $query;
    }

    // private function findingSubdivision($query){
    //     $prevKey = 0;   
    //     foreach($query as $key => $value){
    
    //         if($key < 11){
                    
    //             $prevKey = 0;
    //         }
    //         else
    //         {
    //             $prevKey = $key - 10;
    //         }
            
    //         while ($prevKey < $key){

    //             if ($value['Prev_Arp'] == $query[$prevKey]['Prev_Arp'] && $value['AUDesc'] == $query[$prevKey]['AUDesc'] ){
    //                 // \Log::info($value['Prev_Arp']. " - ". $data[$prevKey]['Prev_Arp']);
    //                 $query[$key]['Prev_Av'] = 0;
    //                 break;
    //             }
                
    //             $prevKey++;
    //         }
            
    //     }

    //     return $query;
    // }
    
}
