<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingBgyPolygon extends Model
{
    use HasFactory;
    protected $table = 'mapping_bgy_polygons';
}
