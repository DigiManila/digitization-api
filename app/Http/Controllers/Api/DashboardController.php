<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

use App\ScannedDocument;
use App\AssessmentRoll;


use App\RecordsTaxDecData;
use App\TempTaxDeclarations;

use App\AggBarangayD;
use App\ViewReportAssessment;
use App\ViewReportBarangayKind;
use App\AggSingleRecord;
use App\AggKindBarangaySummary;



use Carbon\Carbon;

class DashboardController extends Controller
{

    public function getData(){

        // $scanned_document = self::getScannedDocument();
        // $archived_document = self::getArchivedDocument();

        

        
        // $encoder = number_format(5);
        // $no_documents = number_format($total_assessment - (int)ScannedDocument::where('is_assessment_roll', 1)->distinct('assessment_roll_id')->count('assessment_roll_id'));

        // $monthly_report = self::getMonthlyReport();
        // $weekly_progress = self::weeklyProgress();


        // $series_1979 = (int)TempTaxDeclarations::where('current_arp', 'LIKE', 'A-%')->distinct('current_arp')->count();
        // $series_report = self::getSeries();
            $barangay_report = self::getBarangay();
            $single_record_report = self::getSingleRecord();
            
            // $barangay_summary_kind_report = self::getKindBarangay();
       

        

        
        return response()->json([
            "data" => compact(
                // 'scanned_document',
                // 'archived_document',
                // 'total_records',
                // 'total_active',
                // 'total_cancelled',
                // 'total_archive',
                'barangay_report',
                'single_record_report',
                // 'barangay_summary_kind_report',
                // 'encoder',
                // 'no_documents',
                // 'monthly_report',
                // 'weekly_progress',
                // 'series_report'
            ),
            'message' => 'Dashboard Retrieved Successfully',
            'status' => 1
        ], 200);

    }

    public function getRecordsData(){

        $daily_encoded = self::getEncodedRecords();
        $weekly_encoded = self::getWeeklyEncodedRecords();
        $monthly_encoded = self::getMonthlyEncodedRecords();
        $total_encoded = number_format(RecordsTaxDecData::count());
        $table_data = RecordsTaxDecData::select('arp', 'owner_name', 'address', 'lot', 'block')->get();

        return response()->json([
            "data" => compact(
                'daily_encoded',
                'weekly_encoded',
                'monthly_encoded',
                'total_encoded',
                'table_data'
            ),
            'message' => 'Dashboard Retrieved Successfully',
            'status' => 1
        ], 200);

    }

    // ----- dependencies for getData() ----- //

    private function getSeries() {
     
        $series = [];

        $data79 = TempTaxDeclarations::where('current_arp', 'LIKE', 'A-%')->distinct('current_arp')->count();
        $data85 = TempTaxDeclarations::where('current_arp', 'LIKE', 'B-%')->distinct('current_arp')->count();
        $data96 = TempTaxDeclarations::where('current_arp', 'LIKE', '96-%')->distinct('current_arp')->count();
        $data05 = AssessmentRoll::where('current_arp', 'LIKE', 'AA-%')->distinct('current_arp')->count();
        $data14 = AssessmentRoll::where('current_arp', 'LIKE', 'AD-%')->distinct('current_arp')->count();                         
        
        $series[] = array(
            $data14,
            $data05,
            $data96,
            $data85,
            $data79,
        );

        // $series[] = array(
        //     12,
        //     23,
        //     34,
        //     45,
        //     56,
        // );

        return $series;
    }

    private function getSingleRecord(){
        
       

        $singleCount = AggSingleRecord::select('id')
                                        ->count();
                                       
        if ($singleCount == 0){
            app('App\Http\Controllers\AggSingleRecordController')->saveAggSingleRecord();
        }

        $singleRecord = AggSingleRecord::select('total_records','total_active_pin','total_cancelled_records',
                                            'total_archive_records')
                                            ->first();
        
        
            $singleRecord['total_records'] = number_format($singleRecord['total_records']);
            $singleRecord['total_active_pin'] = number_format($singleRecord['total_active_pin']);
            $singleRecord['total_cancelled_records'] = number_format($singleRecord['total_cancelled_records']);
            $singleRecord['total_archive_records'] = number_format($singleRecord['total_archive_records']);
           
        
        return $singleRecord;
    }

  
    private function getBarangay(){

        // $brgyCount = AggBarangayD::select('id')
        //                                 ->count();
                                       
       
            // app('App\Http\Controllers\AggBarangayDController')->saveAggBarangay();
        
        
        // $barangay = [];

        // $bQuery= AggBarangayD::select('brgyNo','area','market_value','CurrentAssessValue','PreviousAssessValue')
        //                         ->orderBy('brgyNo')
        //                         ->get();
        
        // foreach($bQuery as $key => $value){
        //     $barangay[] = array(
        //         'brgyNo' => $value['brgyNo'],
        //         'area' => number_format($value['area'],2,'.',',') . ".sqm",
        //         'market_value' =>   "₱ " . number_format($value['market_value'],2,".",","),
        //         'CurrentAssessValue' =>   "₱ " . number_format($value['CurrentAssessValue'],2,".",","),
        //         'PreviousAssessValue' =>   "₱ " . number_format($value['PreviousAssessValue'],2,".",","),
        //     );
        // }

        $barangay = [];

        $bQuery= ViewReportAssessment::select('brgyNo','total_count','sum_area', 'sum_market_value_val', 
                                        'sum_currassess_val', 'sum_prevassess_val')
                                ->orderBy('brgyNo')
                                ->get();
        
        foreach($bQuery as $key => $value){
            $barangay[] = array(
                'brgyNo' => $value['brgyNo'],
                'sum_area' => number_format($value['sum_area']),
                'sum_market_value_val' =>    number_format($value['sum_market_value_val'],2,".",","),
                'sum_currassess_val' =>    number_format($value['sum_currassess_val'],2,".",","),
                'sum_prevassess_val' =>    number_format($value['sum_prevassess_val'],2,".",","),
            );
        }
       
        return $barangay;
    }

    private function getKindBarangay(){
        // Request $request
        // $brgyNo = $request['brgyNo'];
        // $kind = $request['kind'];

        // $brgyCount = AggBarangayD::select('id')
        //                                 ->count();
                                       
        for ($i = 2; $i<=3; $i++) {
            app('App\Http\Controllers\Api\AggKindBarangaySummaryController')->saveAggKindBarangay($i);
        
        }
        // $barangay = [];
        

        // $bQuery= AggKindBarangaySummary::select('area','market_value','CurrentAssessValue',
        //                                     'PreviousAssessValue')
        //                         ->where('brgyNo',  $brgyNo)
        //                         ->where('kind', $kind)
        //                         ->get();
        
        // foreach($bQuery as $key => $value){
        //     $barangay[] = array(
        //         'area' => number_format($value['area'],2,'.',',') . ".sqm",
        //         'market_value' =>   "₱ " . number_format($value['market_value'],2,".",","),
        //         'CurrentAssessValue' =>   "₱ " . number_format($value['CurrentAssessValue'],2,".",","),
        //         'PreviousAssessValue' =>   "₱ " . number_format($value['PreviousAssessValue'],2,".",","),
        //     );
        // }
       
        // return $barangayKind;
    }


   

    private function getScannedDocument(){

        if(date('D')!='Mon'){
            $mon = date('Y-m-d 00:00:00',strtotime('last Monday'));
            $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
        } else{
            $mon = date('Y-m-d 00:00:00');
            $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
        }

        // $labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $labels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $series = [];

        $data = ScannedDocument::select('id', 'created_at')
                            ->where('is_assessment_roll', 1)
                            ->whereBetween('created_at', [$mon, $sun])
                            ->get()
                            ->groupBy(function($date) {
                                return Carbon::parse($date->created_at)->format('D');
                            });

        $series = self::getCount($data, $labels);

        return compact('series');
    }

    private function getArchivedDocument(){

        if(date('D')!='Mon'){
            $mon = date('Y-m-d 00:00:00',strtotime('last Monday'));
            $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
        } else{
            $mon = date('Y-m-d 00:00:00');
            $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
        }

        // $labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $labels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $series = [];

        $data = ScannedDocument::select('id', 'created_at')
                            ->where('is_assessment_roll', 0)
                            ->whereBetween('created_at', [$mon, $sun])
                            ->get()
                            ->groupBy(function($date) {
                                return Carbon::parse($date->created_at)->format('D');
                            });

        // return $data;


        $series = self::getCount($data, $labels);

        return compact('series');
    }

    private function getMonthlyReport(){

        $labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $series = [];

        $data = ScannedDocument::select('id', 'created_at')
                            // ->where('is_assessment_roll', 0)
                            // ->whereBetween('created_at', [$mon, $sun])
                            ->get()
                            ->groupBy(function($date) {
                                return Carbon::parse($date->created_at)->format('M');
                            });

        $series = self::getCount($data, $labels);

        return compact('series');

    }

    private function weeklyProgress(){

        $this_sunday        = date('Y-m-d', strtotime('Sunday'));
        $past_four_weeks    = date('Y-m-d',strtotime($this_sunday . ' - 27 days'));

        $this_week = date('W');

        $weeks = [];

        for ($i=0; $i < 4; $i++) { 
            $weeks[] = $this_week - $i;
        }

        sort($weeks);

        $data = ScannedDocument::select('id', 'created_at')
                                // ->where('is_assessment_roll', 0)
                                ->whereBetween('created_at', [$past_four_weeks, $this_sunday])
                                ->get()
                                ->groupBy(function($date) {
                                    return Carbon::parse($date->created_at)->format('W');
                                });

        $series = self::getCount($data, $weeks);

        //for label purposes
        $firstOfMonth = strtotime(date("Y-m-01"));
        $w = intval(date("W", strtotime($this_sunday))) - intval(date("W", $firstOfMonth)) + 1;

        $labels = self::getWeeksLabel($w);

        return compact('series', 'labels');
    }

    private function getCount($data, $labels){

        $arr = [];

        foreach($labels as $val){

            if(isset($data[$val])){
                $arr[] = count($data[$val]);
                
            } else{
                $arr[] = 0;
            }
        }

        return array($arr);

    }

    private function getWeeksLabel($week){

        $initial_label = [1,2,3,4];

        while (end($initial_label) !== $week) {
            $end = end($initial_label);
            array_pop($initial_label);
            array_unshift($initial_label, $end);
        }

        foreach ($initial_label as $key => $value) {
            $initial_label[$key] = "Week " . $value;
        }

        return $initial_label;

    }

    // ----- dependencies for getData() ----- //

    // ----- dependencies for getRecordsData() ----- //

    private function getEncodedRecords(){

        if(date('D')!='Mon'){
            $mon = date('Y-m-d 00:00:00',strtotime('last Monday'));
            $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
        } else{
            $mon = date('Y-m-d 00:00:00');
            $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
        }

        // $labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $labels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $series = [];

        $data = RecordsTaxDecData::select('id', 'created_at')
                            ->whereBetween('created_at', [$mon, $sun])
                            ->get()
                            ->groupBy(function($date) {
                                return Carbon::parse($date->created_at)->format('D');
                            });

        $series = self::getCount($data, $labels);

        return compact('series');

    }

    private function getWeeklyEncodedRecords(){

        $this_sunday        = date('Y-m-d', strtotime('Sunday'));
        $past_four_weeks    = date('Y-m-d',strtotime($this_sunday . ' - 27 days'));

        $this_week = date('W');

        $weeks = [];

        for ($i=0; $i < 4; $i++) { 
            $weeks[] = $this_week - $i;
        }

        sort($weeks);

        $data = RecordsTaxDecData::select('id', 'created_at')
                                ->whereBetween('created_at', [$past_four_weeks, $this_sunday])
                                ->get()
                                ->groupBy(function($date) {
                                    return Carbon::parse($date->created_at)->format('W');
                                });

        $series = self::getCount($data, $weeks);

        //for label purposes
        $firstOfMonth = strtotime(date("Y-m-01"));
        $w = intval(date("W", strtotime($this_sunday))) - intval(date("W", $firstOfMonth)) + 1;

        $labels = self::getWeeksLabel($w);

        return compact('series', 'labels');
    }

    private function getMonthlyEncodedRecords(){

        $labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $series = [];

        $data = RecordsTaxDecData::select('id', 'created_at')
                            ->get()
                            ->groupBy(function($date) {
                                return Carbon::parse($date->created_at)->format('M');
                            });

        $series = self::getCount($data, $labels);

        return compact('series');

    }


    // ----- dependencies for getBarangayData() ----- //

    public function getBarangayData(Request $request){
        

        $brgyNo = $request['brgyNo'];
            

            $bQuery = ViewReportBarangayKind::select('L_RESI','L_COMM','L_CHAR','L_EDUC','L_GOVT','L_INDU','L_RELI', 
                                        'B_RESI','B_COMM','B_CHAR','B_EDUC','B_GOVT','B_INDU','B_RELI', 
                                        'M_RESI','M_COMM','M_CHAR','M_EDUC','M_GOVT','M_INDU','M_RELI',
                                        'L_SPC1','L_SPC2','L_SPC3','L_SPC4','L_SPC5','L_SPC6',
                                        'B_SPC1','B_SPC2','B_SPC3','B_SPC4','B_SPC5','B_SPC6',
                                        'M_SPC1','M_SPC2','M_SPC3','M_SPC4','M_SPC5','M_SPC6',
                                        'L_SPEC','B_SPEC','M_SPEC',
                                        'L_OTHE','B_OTHE','M_OTHE')
                                    ->where('brgyNo',  $brgyNo )
                                    ->first();
            
                                   
            $bQuery['L_RESI'] = number_format($bQuery['L_RESI']);
            $bQuery['L_COMM'] = number_format($bQuery['L_COMM']);
            $bQuery['L_CHAR'] = number_format($bQuery['L_CHAR']);
            $bQuery['L_EDUC'] = number_format($bQuery['L_EDUC']);
            $bQuery['L_GOVT'] = number_format($bQuery['L_GOVT']);
            $bQuery['L_INDU'] = number_format($bQuery['L_INDU']);
            $bQuery['L_RELI'] = number_format($bQuery['L_RELI']);
          

            $bQuery['B_RESI'] = number_format($bQuery['B_RESI']);
            $bQuery['B_COMM'] = number_format($bQuery['B_COMM']);
            $bQuery['B_CHAR'] = number_format($bQuery['B_CHAR']);
            $bQuery['B_EDUC'] = number_format($bQuery['B_EDUC']);
            $bQuery['B_GOVT'] = number_format($bQuery['B_GOVT']);
            $bQuery['B_INDU'] = number_format($bQuery['B_INDU']);
            $bQuery['B_RELI'] = number_format($bQuery['B_RELI']);
           

            $bQuery['M_RESI'] = number_format($bQuery['M_RESI']);
            $bQuery['M_COMM'] = number_format($bQuery['M_COMM']);
            $bQuery['M_CHAR'] = number_format($bQuery['M_CHAR']);
            $bQuery['M_EDUC'] = number_format($bQuery['M_EDUC']);
            $bQuery['M_GOVT'] = number_format($bQuery['M_GOVT']);
            $bQuery['M_INDU'] = number_format($bQuery['M_INDU']);
            $bQuery['M_RELI'] = number_format($bQuery['M_RELI']);

            $bQuery['L_SPC1'] = number_format($bQuery['L_SPC1']);
            $bQuery['L_SPC2'] = number_format($bQuery['L_SPC2']);
            $bQuery['L_SPC3'] = number_format($bQuery['L_SPC3']);
            $bQuery['L_SPC4'] = number_format($bQuery['L_SPC4']);
            $bQuery['L_SPC5'] = number_format($bQuery['L_SPC5']);
            $bQuery['L_SPC6'] = number_format($bQuery['L_SPC6']);

            $bQuery['B_SPC1'] = number_format($bQuery['B_SPC1']);
            $bQuery['B_SPC2'] = number_format($bQuery['B_SPC2']);
            $bQuery['B_SPC3'] = number_format($bQuery['B_SPC3']);
            $bQuery['B_SPC4'] = number_format($bQuery['B_SPC4']);
            $bQuery['B_SPC5'] = number_format($bQuery['B_SPC5']);
            $bQuery['B_SPC6'] = number_format($bQuery['B_SPC6']);

            $bQuery['M_SPC1'] = number_format($bQuery['M_SPC1']);
            $bQuery['M_SPC2'] = number_format($bQuery['M_SPC2']);
            $bQuery['M_SPC3'] = number_format($bQuery['M_SPC3']);
            $bQuery['M_SPC4'] = number_format($bQuery['M_SPC4']);
            $bQuery['M_SPC5'] = number_format($bQuery['M_SPC5']);
            $bQuery['M_SPC6'] = number_format($bQuery['M_SPC6']);

            $bQuery['L_SPEC'] = number_format($bQuery['L_SPEC']);
            $bQuery['B_SPEC'] = number_format($bQuery['B_SPEC']);
            $bQuery['M_SPEC'] = number_format($bQuery['M_SPEC']);

            $bQuery['L_OTHE'] = number_format($bQuery['L_OTHE']);
            $bQuery['B_OTHE'] = number_format($bQuery['B_OTHE']);
            $bQuery['M_OTHE'] = number_format($bQuery['M_OTHE']);
            

            $total = self::displayTotals($brgyNo);

            $arr_bQuery = $bQuery->toArray();

            foreach($total as $key => $val){
                // \Log::info($key);
                // \Log::info($bQuery[trim($key)]);
                $total[$key]['total'] = $arr_bQuery[$key];
                $total[$key]['area'] = number_format($total[$key]['area'],2,'.',',') . ".sqm";
                $total[$key]['market_value'] = number_format($total[$key]['market_value'],2,".",",");
                $total[$key]['currentAssessValue'] =  number_format($total[$key]['currentAssessValue'],2,".",",");
                $total[$key]['previousAssessValue'] =  number_format($total[$key]['previousAssessValue'],2,".",",");
        //         
            }


            return $total;


        
        return $bQuery;
    }

    public function getBarangayPin(Request $request){

        $brgyNo = $request['brgyNo'];
        $kind = $request['kind'];
        
        // $bQuery = AssessmentRoll::select('pin','current_arp', 'owner_name', 'area','market_value',
        //     'CurrentAssessValue','PreviousAssessValue',)
        // ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
        // ->where('kind', $kind)
        // ->where('cancelled', 0)
        // ->get();

        $bQuery = AssessmentRoll::select('pin','current_arp', 'owner_name', 'area','market_value',
                                        'CurrentAssessValue','PreviousAssessValue',)
                                    ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                                    ->where('kind', $kind)
                                    ->whereRaw('SUBSTRING(current_arp,1,2) = ?', 'AD')
                                    ->get();

     
        
        foreach($bQuery as $key => $value){
            $barangay[] = array(
                'pin'                   =>          $value['pin'],
                'current_arp'           =>          $value['current_arp'],
                'owner_name'            =>          $value['owner_name'],
                // 'lot'                   =>          $value['lot'],
                // 'block'                 =>          $value['block'],
                'area'                  =>          number_format($value['area'],2,'.',',') . ".sqm",
                'market_value'          =>           number_format($value['market_value'],2,".",","),
                'CurrentAssessValue'    =>           number_format($value['CurrentAssessValue'],2,".",","),
                'PreviousAssessValue'   =>           number_format($value['PreviousAssessValue'],2,".",","),
            );
        }
        
        return $barangay;

    }


    private function displayTotals($brgyNo){

       

        // $bQuery = AssessmentRoll::select('pin','area','market_value','currentAssessValue','previousAssessValue','kind')
        //             ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo)
        //             ->where('cancelled', 0)
        //             ->get();

        
        $bQuery = AssessmentRoll::select('pin','area','market_value','currentAssessValue','previousAssessValue','kind')
        ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo)
        ->whereRaw('SUBSTRING(current_arp,1,2) =?', 'AD')
        ->get();                   

        $data = [];

        foreach($bQuery as $val){

            $kind = str_replace("-", "_", $val['kind']);

            if(array_key_exists($kind, $data)){
                $data[$kind]['area']                 = $data[$kind]['area'] + $val['area'];
                $data[$kind]['market_value']         = $data[$kind]['market_value'] + $val['market_value'];
                $data[$kind]['currentAssessValue']   = $data[$kind]['currentAssessValue'] + $val['currentAssessValue'];
                $data[$kind]['previousAssessValue']  = $data[$kind]['previousAssessValue'] + $val['previousAssessValue'];


            } else{
                $data[$kind]['area']                 = $val['area'];
                $data[$kind]['market_value']         = $val['market_value'];
                $data[$kind]['currentAssessValue']   = $val['currentAssessValue'];
                $data[$kind]['previousAssessValue']  = $val['previousAssessValue'];
            }

        }


        return $data;

    }

}
