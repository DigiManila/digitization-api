<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ArchiveAssessmentRoll;
use App\ScannedDocument;

use Storage;

class ArchiveAssessmentRollController extends Controller
{

    public function getArchivedScanned(){

        $data = ArchiveAssessmentRoll::has('scanned')
                                ->select('id', 'pin_no', 'arp_no', 'owner_name', 'address', 'lot', 'block')
                                ->get();

        return $data;

    }

    public function save(Request $request){

        // return $request;

        //{"pin":"117-14-003-02-029-000","arpno":"A-001-0005","owner":"Basilisa M. Cabral de Reyes and Mamerta M. Cablar (Deceased)","address":"Valenzuela, Metro Manila","block":"10-A-2 23-A-2","lot":"2723","filename":"TD-1979-001-0005.pdf"}

        $archive_data = array(
            'pin_no'                => $request['pin'],
            'arp_no'                => $request['arpno'],
            'owner_name'            => $request['owner'],
            'address'               => $request['address'],
            'block'                 => $request['block'],
            'lot'                   => $request['lot'],
            'created_by'            => auth()->user()->username,
        );

        $status = ArchiveAssessmentRoll::create($archive_data);

        // $scanned_document_id = "A" . $status['id'];

        // $id = ArchiveAssessmentRoll::where('id', $status['id'])->update(['scanned_document_id' => $scanned_document_id]);

        $file_name_explode = explode('-', $request->get('filename'));
        
        $type = $file_name_explode[0];
        $year = $file_name_explode[1];
        // $data = $request->get('data')[0];

        $path = Storage::disk('scans')->getDriver()->getAdapter()->getPathPrefix();

        $save_arr = array(
            'assessment_roll_id'    => $status['id'],
            'is_assessment_roll'    => false,
            'type'                  => null,
            'path'                  => $path . $year. DIRECTORY_SEPARATOR . $request->get('filename'),
            'created_by'            => auth()->user()->username,
        );

        $status = ScannedDocument::create($save_arr);

        return $status;

    }

    public function getLinkedData(Request $request){

        $data = ScannedDocument::select('id', 'path')
                                ->where('assessment_roll_id', $request->get('id'))
                                ->where('is_assessment_roll', false)
                                ->get();

        foreach($data as $key => $val){
            $data[$key]['filename'] = basename($val['path']);
            unset($data[$key]['path']);
        }


        return $data;

    }

    public function getArchivePdfDataUrl(Request $request){

        $data = ScannedDocument::select('path')
                                ->where('id', $request->get('id'))
                                ->where('is_assessment_roll', false)
                                ->first();

        $url = base64_encode(file_get_contents($data['path']));

        return $url;

    }
}
