<?php

namespace App\Http\Traits;

use App\Models\AggBarangayD;
use App\AssessmentRoll;

trait AggBarangayTrait{

    public function index(){
        $barangay = [];

        $b001 = [];

        for($i = 1; $i<=999; $i++){
            $brgyNo = str_pad($i,3,'0',STR_PAD_LEFT);
            
        
        

            $bQuery = AssessmentRoll::select('pin','area','market_value','currentAssessValue','previousAssessValue')
                                    ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                                    -> where('cancelled', 0)
                                    ->get();
            
            $countPin = $bQuery->count();
            $sumArea = number_format($bQuery->sum('area'),2,'.',',');
            $sumMarketValue = number_format($bQuery->sum('market_value'),2,'.',',');
            $sumAssessValue = number_format($bQuery->sum('currentAssessValue'),2,'.',',');
            $sumPreviousValue = number_format($bQuery->sum('previousAssessValue'),2,'.',',');
           
        

            $barangay[] = array(
                $brgyNo,
                $countPin,
                $sumArea,
                $sumMarketValue,
                $sumAssessValue,
                $sumPreviousValue
            );
        }
        
        $status = AggBarangayD::upsert($barangay);
        
        
        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }
}