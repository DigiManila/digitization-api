<?php

namespace App\Http\Controllers;

use App\AggSingleRecord;
use App\AssessmentRoll;
use App\TempTaxDeclarations;

use Illuminate\Http\Request;

class AggSingleRecordController extends Controller
{
    public function saveAggSingleRecord(){
       
        $total_assessment = (int)AssessmentRoll::select('pin')
                            ->count() + (int)TempTaxDeclarations::select('current_arp')
                            ->count();
        

        
        $total_active = (int)AssessmentRoll::select('pin')
                        ->where('cancelled',0)
                        ->count();
        $total_cancelled = (int)AssessmentRoll::select('pin')
                        ->where('cancelled',1)
                        ->count();

        $total_archive = (int)TempTaxDeclarations::select('current_arp')
                        ->count();
    
        $status = AggSingleRecord::create([
            'total_records'     =>  $total_assessment,
            'total_active_pin'  =>  $total_active,
            'total_cancelled_records' => $total_cancelled,
            'total_archive_records' => $total_archive,
        ]);
        
        
        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
        
    }
}
