<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewIdleLandsInsp extends Model
{
    use HasFactory;

    protected $table = 'view_idle_lands_insp';
}
