<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingTransaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function profile()
    {
        return $this->hasOne('App\Profile', 'user_id', 'receiving_id');
    }

    public function normal_flow()
    {
        return $this->hasOne(TrackingNormalFlow::class, 'tracking_id', 'id');
    }

    public function free_flow()
    {
        return $this->hasOne(TrackingFreeFlow::class, 'tracking_id', 'id');
    }

    public function statusflow()
    {
        return $this->hasMany(TrackingStatus::class, 'tracking_id');
    }

    public function trans_type()
    {
        return $this->hasMany(TrackingNormalFlowTransType::class, 'tracking_id');
    }

    public function process()
    {
        return $this->hasOne(TrackingProcess::class, 'process_rank', 'status');
    }
}
