<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOrTaxdecsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('or_taxdecs', function (Blueprint $table) {
            $table->date('or_date')->after('tax_declaration')->nullable();
            $table->integer('amount')->after('or_date');
            $table->string('payor')->after('amount');
            $table->string('nature')->after('payor');
            $table->string('collecting_officer')->after('nature');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('or_taxdecs', function (Blueprint $table) {
            //
        });
    }
}
