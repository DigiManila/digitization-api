<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\EncryptDecryptController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

use App\OrTaxdec;
use App\RecordsTaxDecData;
use App\Profile;
use App\ReleaseTaxDec;

use Illuminate\Support\Facades\Redis;


use Spatie\PdfToImage\Pdf as PdfToImg;

use PDF;
use DNS1D;
use QrCode;
use File;
use Imagick;
use Storage;
use Auth;

class PdfController extends Controller
{
    public function createPdf(Request $request){

        $is_save = $request['saveStat'];

        $date = date("Y-m-d");
        // $epoch = time();
        $epoch  = $request['epoch'];

        $td = $request['td'];

        $user = json_decode($request['user'], 1);

        if($request['year'] == '1996'){

            $bgy = explode("-", $request['td']);
            $search_name = "B-" . (int)$bgy[0] . " " . $request['year'];

            $redis_data = json_decode(Redis::get('folderStruct'), 1);
            $data = $redis_data[$request['year']];

            foreach ($data as $key => $val) {
                if(strpos($key, $search_name) !== false){
                    $folder_name = $key;
                    break;
                }

            }

            $exp = explode(' ', $folder_name);

            $z = explode('-', $exp[0])[1];
            $b = explode('-', $exp[1])[1];

            $filenames = self::getFilenames96($td, $request['year'], $z, $b);
        } else{
            $filenames = self::getFilenames($td, $request['year'], $request['district']);
        }

        // return "OK";

        // $path = storage_path("app/taxdecs/Barcode/" . $td['tax_declaration'] . "_" . $or_data['or_date_epoch'] . ".pdf");
        $user   = json_decode($request['processed_by'], 1);
        $processed_by = $user['first_name'] . " " . $user['last_name'];

        $printed_by = "MR. JOSE BONAPARTE HARON R. MAMUTUK";
        // $certified_by = "MS. ELOISA A. BEHAN";
        $certified_by = "MR. JOSE BONAPARTE HARON R. MAMUTUK";
        // $certifier_position = "INFORMATION TECHNOLOGY OFFICER II";
        $certifier_position = "LOCAL ASSESSMENT OPERATIONS OFFICER V";

        if($is_save){
            $return = ReleaseTaxDec::create(array(
                'td_no'     => $td,
                'requestor' => $request['requestor'],
                // 'printed_by' => $printed_by,
                'printed_by' => $processed_by,
                'certifier' => $certified_by,
                'processed_by' => $processed_by,
            ));
    
            $insert_id = $return->id;        
        } else{
            $insert_id = "";
        }



        $encryptIt = new EncryptDecryptController;
        $barcode_data = $encryptIt->createEncryptedBarcode($insert_id);

        // $certifier = Profile::select('first_name', 'middle_name', 'last_name', 'position')->where('id', $request['certifier'])->first();
        // $printed_by = $user['first_name'] . " " . $user['middle_name'] . " " . $user['last_name'];
        
        
        $released_td_dir = 'released_taxdecs';

        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //
        // $released_td_dir = 'released_taxdecs_test';
        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //


         // Check if special request

        $or_text = $request['or_num'];

        if(strpos($request['or_num'], "SR") !== false){
            
            $srcount = ReleaseTaxDec::where('or_no', 'like', $request['or_num'] . date('Y').'%')->count();
            $end_no = 0;

            if($srcount){
                $end_no = $srcount + 1;
            } else{
                $end_no = 1;
            }

            $or_text = $request['or_num'] . date('Y') . sprintf('%04d', $end_no);

        }
        

        foreach($filenames as $key => $value){

            PDF::setPrintHeader(false);
            PDF::AddPage('P', 'LEGAL');
            // PDF::AddPage('P', 'A4');
            PDF::SetAutoPageBreak(false, 0);

            // PDF::setJPEGQuality(25);

            if($request['header'] === 'true'){
                PDF::SetFont('times', '', 13, '', false);
                PDF::SetTextColor(54,69,79); 
                PDF::MultiCell(218, 10, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, 9, true);
                PDF::SetFont('times', '', 11, '', false);
                PDF::MultiCell(218, 10, 'City of Manila', 0, 'C', 0, 0, 0, 14, true);
                PDF::SetFont('times', 'B', 15, '', false);
                PDF::SetTextColor(255, 102, 102);
                PDF::MultiCell(218, 10, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, 18, true);
        
                $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
                PDF::Image($logo1, 18, 6, 20, 20, '', '', '', false, 300, '', false, false, 0);
        
                $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
                PDF::Image($logo2, 173, 6, 21, 21, '', '', '', false, 300, '', false, false, 0);
            }
    
            // $img_file = storage_path("app/taxdecs/zone 1/1A-001-0001.jpg");

            PDF::SetFont('times', '', 9, '', false);
            PDF::SetTextColor(54,69,79);

            PDF::StartTransform();          
            PDF::Rotate(90,123,110);
            PDF::MultiCell('', '', 'IMPORTANT: Issued for taxation purposes and should not be considered as title to the property.', 0, '', 0, 0, 0, 0, true);
            PDF::StopTransform();


            // Tax Declaration Image
            $img_file = $value;

            if($value == "Not Exist"){
                $img_file = storage_path("app/not_exist.jpg");
            } else{
                $img_file = storage_path("app/$value");
            }

            // \Log::info();

            // $h = 228;
            $h = 250;

            $page_id = "Page $key";

            if($page_id === "Page 0" && $request['flip1'] == "true"){
                // Load the image
                $source = imagecreatefromjpeg($img_file);
                // Rotate
                $rotate = imagerotate($source, 180, 0);
                //and save it on your server...
                imagejpeg($rotate, storage_path("app/$value"));

                PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            } elseif($page_id === "Page 1" && $request['flip2'] == "true"){
                // Load the image
                $source = imagecreatefromjpeg($img_file);
                // Rotate
                $rotate = imagerotate($source, 180, 0);
                //and save it on your server...
                imagejpeg($rotate, storage_path("app/$value"));

                PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            } else{
                PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            }

            // PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
            
            $style = array(
                'position' => '',
                'align' => 'C',
                'stretch' => false,
                'fitwidth' => false,
                'cellfitalign' => '',
                'border' => false,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255),
                'text' => true,
                'font' => 'helvetica',
                'fontsize' => 6,
                'stretchtext' => 0
            );

            $col_2_start_pos = 110;
            $colon_2_add_pos = $col_2_start_pos + 18;
            $col_2_after_col_pos = $colon_2_add_pos + 4;

            $col_1_start_pos = 18;
            $colon_1_add_pos = $col_1_start_pos + 18;
            $col_1_after_col_pos = $colon_1_add_pos + 4;

            // $row_init_pos = 261;
            $row_init_pos = 282;
            $row_add_pos = $row_init_pos;

            PDF::SetFont('times', 'I', 8, '', false);
            PDF::SetTextColor(120, 120, 120);

            PDF::MultiCell('', '', 'OR No.', 0, '', 0, 0, $col_1_start_pos, $row_init_pos, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_init_pos, true);
            PDF::MultiCell('', '', $or_text, 0, '', 0, 0, $col_1_after_col_pos, $row_init_pos, true);



            $row_add_pos = $row_add_pos + 4;

            PDF::MultiCell('', '', 'Requestor', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
            PDF::MultiCell('', '', $request['requestor'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);

            PDF::MultiCell('', '', 'Certified By', 0, '', 0, 0, $col_2_start_pos, $row_add_pos, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_2_add_pos, $row_add_pos, true);
            // PDF::MultiCell('', '', 'By authority of the Officer in Charge' , 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);
            PDF::MultiCell('', '', 'By authority of the City Assessor' , 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);


            ////// FIX 


            $row_add_pos = $row_add_pos + 11;
            
            // PDF::MultiCell('', '', strtoupper($certifier['first_name'] . " " . $certifier['middle_name'] . " " . $certifier['last_name']), 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);
            PDF::MultiCell('', '', $certified_by, 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);

            PDF::MultiCell('', '', 'Printed By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
            PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
            // PDF::MultiCell('', '', strtoupper($printed_by), 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos  , true);
            PDF::MultiCell('', '', strtoupper($processed_by), 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos  , true);

            $row_add_pos = $row_add_pos + 4;

            PDF::SetFont('times', 'I', 6, '', false);
            PDF::SetTextColor(120, 120, 120);
            // PDF::MultiCell('', '', $certifier['position'], 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);
            PDF::MultiCell('', '', $certifier_position, 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);

            // PDF::MultiCell('', '', $certifier_position, 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);

            // $row_add_pos = $row_add_pos + 5;

            // PDF::SetFont('times', 'I', 8, '', false);
            // PDF::SetTextColor(120, 120, 120);
            // PDF::MultiCell(170, '', 'This is a system-generated certified true copy of the Tax Declaration and does not require a manually affixed signature pursuant to R.A. No. 8792.', 0, '', 0, 0, 20, $row_add_pos, true);

            $row_add_pos = $row_add_pos + 3;

            // Barcode
            PDF::write1DBarcode($barcode_data, 'C128', 16, $row_add_pos, 135, 15, 0.4, $style, 'N');
    
            // Go Manila logo

            // $logo3 = public_path('img') . DIRECTORY_SEPARATOR . 'GoManila.png';
            // PDF::Image($logo3, 165, 303, 25, 15, '', '', '', false, 150, '', false, false, 0);

            $row_add_pos = $row_add_pos + 8;

            // Page
            PDF::SetFont('times', 'I', 7, '', false);
            PDF::SetTextColor(120, 120, 120);
            PDF::MultiCell('', '', 'Page ' . (intval($key) + 1) . ' of ' . count($filenames), 0, 'C', 0, 0, 152, $row_add_pos, true);
    
            // set alpha to semi-transparency
            PDF::SetAlpha(0.05);
    
        }

        // Copy Files

        $fn = $td . "_" . $epoch. ".pdf";
        $path = storage_path("app/temp/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        Storage::delete("temp/forpdf/1" . $td . ".jpg");
        Storage::delete("temp/forpdf/2" . $td . ".jpg");

        $file = Storage::get("temp/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        $created_pdf = Storage::get("temp/" . $fn);

        Storage::disk('svnas')->putFileAs("/$released_td_dir/$date" , $temp_file_path, $fn);
        Storage::disk('svnas')->putFileAs("/$released_td_dir/$date" , $request->file('file'), "or_$td"."_"."$epoch.png");

        Storage::delete("temp/$fn");

        // Save to database

       // Prevent double saving from rotation image
       // Check if file exist 

       $ifExist = ReleaseTaxDec::where('file_path', 'like', "%_" . $epoch . ".%")->get();


       if($is_save){
        if(!count($ifExist)){
            $release_td = ReleaseTaxDec::find($insert_id);
            
            $release_td->or_no      = $or_text;
            $release_td->or_path    = "/$released_td_dir/$date/or_$td"."_"."$epoch.png";
            $release_td->file_path  = "/$released_td_dir/$date/$fn";
            $release_td->barcode    = $barcode_data;
            $release_td->created_at = date('Y-m-d H:i:s');
    
            $release_td->save();
    
           }
       }


        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);

        // return "data:application/pdf;base64," . $output_pdf;

    }

    private function getFilenames($td, $yr, $dst){

        $explode_td = explode('-', $td);
        $getZone = $explode_td[1];

        $zone = "ZONE " . intval($getZone);

        if($dst){
            $zone = $zone . " " . $dst;
        }

        $filenames = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $dir = $redis_data[$yr];

        if(in_array($td . ".pdf", $dir[$zone])){

            if(Storage::disk('svnas')->exists("/taxdecs_img/$yr/$zone/1$td.jpg") &&  Storage::disk('svnas')->exists("/taxdecs_img/$yr/$zone/2$td.jpg")){

                $file1 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$zone/1$td.jpg");
                $file2 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$zone/2$td.jpg");

                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);

            } else{
                $file = Storage::disk('sftp')->get("/$yr/$zone/$td.pdf");
                // $file = Storage::disk('svnas')->get("/taxdecs/$yr/$zone/$td.pdf");

                Storage::put("/temp/$td.pdf", $file);
    
                $path = storage_path("app/temp/$td.pdf");
    
                $imagick = new Imagick();
                $imagick->readimage("$path");
                $imagick->writeImages(storage_path("app/temp/forpdf/" . $td . ".jpg"), false);
    
                $image_path_1 = storage_path("app/temp/forpdf/") . "1" . $td . ".jpg";
                $image_path_2 = storage_path("app/temp/forpdf/") . "2" . $td . ".jpg";
    
                rename( storage_path("app/temp/forpdf/") .  $td . "-0.jpg",  $image_path_1);
                rename( storage_path("app/temp/forpdf/") .  $td . "-1.jpg",  $image_path_2);
    
                Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg"));
                Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/2" . $td . ".jpg", Storage::get("temp/forpdf/2" . $td . ".jpg"));

                Storage::delete("/temp/$td.pdf");
                // Storage::delete("temp/1" . $td . ".jpg");
                // Storage::delete("temp/2" . $td . ".jpg");
    
                // $filenames[] = "/taxdecs_img/$yr/$zone/1$td.jpg";
                // $filenames[] = "/taxdecs_img/$yr/$zone/2$td.jpg";
            }

        } else{
            if(in_array("1" . $td . ".jpg", $dir[$zone])){
                $file1 = Storage::disk('sftp')->get("/$yr/$zone/1$td.jpg");
                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
            } else{
                $filenames[] = "Not Exist";
            }
            
            if(in_array("2" . $td . ".jpg", $dir[$zone])){
                $file2 = Storage::disk('sftp')->get("/$yr/$zone/2$td.jpg");
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);
            } else{
                $filenames[] = "Not Exist";
            }
        }

        $filenames[] = "temp/forpdf/1" . $td . ".jpg";
        $filenames[] = "temp/forpdf/2" . $td . ".jpg";

        return $filenames;

    }

    private function getFilenames96($td, $yr, $zone, $bgy){

        $folder_name = "Z-$zone B-$bgy $yr";

        $filenames = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $dir = $redis_data[$yr];

        if(in_array($td . ".pdf", $dir[$folder_name])){

            if(Storage::disk('svnas')->exists("/taxdecs_img/$yr/$folder_name/1$td.jpg") &&  Storage::disk('svnas')->exists("/taxdecs_img/$yr/$folder_name/2$td.jpg")){

                $file1 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$folder_name/1$td.jpg");
                $file2 = Storage::disk('svnas')->get("/taxdecs_img/$yr/$folder_name/2$td.jpg");

                Storage::put("/temp/forpdf/" . "1$td.jpg", $file1);
                Storage::put("/temp/forpdf/" . "2$td.jpg", $file2);


            } else{
                $file = Storage::disk('sftp')->get("/$yr/$folder_name/$td.pdf");
                // $file = Storage::disk('svnas')->get("/taxdecs/$yr/$zone/$td.pdf");

                Storage::put("/temp/$td.pdf", $file);
    
                $path = storage_path("app/temp/$td.pdf");
    
                $imagick = new Imagick();

                $imagick->readimage("$path");

                $no_pages = $imagick->getNumberImages();

                $imagick->writeImages(storage_path("app/temp/forpdf/" . $td . ".jpg"), false);


    
                if($no_pages > 1){

                    $image_path_1 = storage_path("app/temp/forpdf/") . "1" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . "-0.jpg",  $image_path_1);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg"));

                    $image_path_2 = storage_path("app/temp/forpdf/") . "2" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . "-1.jpg",  $image_path_2);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/2" . $td . ".jpg", Storage::get("temp/forpdf/2" . $td . ".jpg"));
                } else{
                    $image_path_1 = storage_path("app/temp/forpdf/") .  "1" . $td . ".jpg";
                    rename( storage_path("app/temp/forpdf/") .  $td . ".jpg",  $image_path_1);
                    Storage::disk('svnas')->put("/taxdecs_img/$yr/$zone/1" . $td . ".jpg", Storage::get("temp/forpdf/1" . $td . ".jpg")); 
                    
                    $image_path_2 = "";
                }

                Storage::delete("/temp/$td.pdf");
                // Storage::delete("temp/1" . $td . ".jpg");
                // Storage::delete("temp/2" . $td . ".jpg");
    
                // $filenames[] = "/taxdecs_img/$yr/$zone/1$td.jpg";
                // $filenames[] = "/taxdecs_img/$yr/$zone/2$td.jpg";
            }

        } else{
            $filenames[] = "Not Exist";
        }

        $filenames[] = "temp/forpdf/1" . $td . ".jpg";
        $filenames[] = "temp/forpdf/2" . $td . ".jpg";

        return $filenames;

    }

    public function createPreviewPdf(Request $request){

        $td = $request->td;
        $filenames = [];
        $folder_year = "";

        if(strpos($td, "A-") !== false){
            $folder_year = "1979";

            $getZone = explode('-', $td)[1];

            if(strpos($td, "-073-") !== false){ // Ermita, Paco
                $folder = "ZONE " . intval($getZone) . "-" . strtoupper($request->param);
            } elseif(strpos($td, "-095-") !== false){ //Pandacan, Sta ana
                if(strpos($request->param, "Sta. Ana") !== false){
                    $folder = "ZONE " . intval($getZone) . "-STAANA";
                } else{
                    $folder = "ZONE " . intval($getZone) . "-" . strtoupper($request->param);
                }
            } else{
                $folder = "ZONE " . intval($getZone);
            }

        } elseif(strpos($td, "B-") !== false){
            $folder_year = "1985";
        } else{
            $folder_year = "1996";
        }

        $path = storage_path("app/taxdecs/$folder_year/$folder/" . $td . ".pdf");

        if (file_exists( $path )) {

            $headers = array(
                'Content-Description: File Transfer',
                'Content-Type: application/octet-stream',
                'Content-Disposition: attachment; filename="taxdec.pdf"',
            );
    
            return response()->download($path, 'taxdec.pdf', $headers);

        }

        $filenames[] = storage_path("app/taxdecs/$folder_year/$folder/1" . $td . ".jpg");
        $filenames[] = storage_path("app/taxdecs/$folder_year/$folder/2" . $td . ".jpg");

        foreach($filenames as $key => $value){

            PDF::setPrintHeader(false);
            PDF::AddPage('P', 'LEGAL');
            PDF::SetAutoPageBreak(false, 0);
    
            $img_file = $value;
            PDF::Image($img_file, 0, 0, 219, 355, '', '', '', false, 300, '', false, false, 0);

        }

        $filepath = storage_path("app/taxdecs/$folder_year/$folder/" . $td . ".pdf");

        $out = PDF::Output($filepath, 'F');

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $td . '.pdf"',
        );


        // $ret = response()->download($filepath, "$td.pdf", $headers);

        Storage::delete("/temp/$td.pdf");

        return response()->download($filepath, "$td.pdf", $headers);

        return $out;

    }

    // private function getOrData($id){

    //     $data = [];

    //     $or = OrTaxdec::with('or')->where('id', $id)->first();
    //     $profile = RecordsTaxDecData::with('profile')->where('arp', $or['or']['arp'])->first();

    //     $data['or_num']         = $or['or_num'];
    //     $data['or_date']        = date('F j, Y', strtotime($or['or_date']));
    //     $data['or_date_epoch']  = strtotime($or['or_date']);
    //     $data['amount_paid']    = number_format($or['amount'], 2);
    //     $data['verifier']       = strtoupper($profile['profile']['first_name'] . " " . $profile['profile']['middle_name'] . " " . $profile['profile']['last_name']);
    //     $data['position']       = strtoupper($profile['profile']['position']);
    //     // $profile = RecordsTaxDecData::with('profile')
    //     //     ->where('arp', $td['tax_declaration'])
    //     //     ->first();

    //     return $data;

    // }

    // private function getYear($data){

    //     if(strpos($data, "A") !== false){
    //         return 1979;
    //     }

    // }

}

// $img = QrCode::format('svg')->size(100)->generate('Make me a QrCode!');
// \Log::info($img);
// $img = base64_encode($img);
// PDF::ImageSVG('@' . $img, $x=10, $y=270, $w=25, $h=25, $link='http://www.tcpdf.org', $align='', $palign='', $border=1, $fitonpage=false);
// PDF::ImageSVG('@' . DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T'), $x=10, $y=270, $w='', $h='', $link='', $align='', $palign='', $border=false, $fitonpage=false);
// DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T');