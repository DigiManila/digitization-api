<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnionAllAd extends Model
{
    use HasFactory;

    protected $table = 'union_all_ad';

}
