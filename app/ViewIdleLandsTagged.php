<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewIdleLandsTagged extends Model
{
    use HasFactory;
    protected $table = 'view_idle_lands_tagged';
}
