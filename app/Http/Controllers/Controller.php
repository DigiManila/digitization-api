<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Logs;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //$mode = required, $new_data,  $prev_data, $remarks = optional
    public function addLog($mode, $new_data = null, $prev_data = null, $remarks = null){

        $route = explode("@",  \Route::getCurrentRoute()->getActionName());

        $exp_route = explode("\\", $route[0]);
        
        $data = array(
            'user_id'       => \Auth::user()->id,
            'controller'    => $exp_route[count($exp_route) -2] . " > " . $exp_route[count($exp_route) -1],
            'method'        => $route[1],
            'mode'          => $mode,
            'new_data'      => $new_data,
            'previous_data' => $prev_data,
            'remarks'       => $remarks,
        );

        // \Log::info($data);

        Logs::create($data);

        return "Logs Saved";
    }
}
