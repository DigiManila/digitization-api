<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordsCertificationsNoTd extends Model
{
    use HasFactory;

    protected $guarded = [];
}
