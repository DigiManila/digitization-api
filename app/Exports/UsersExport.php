<?php

namespace App\Exports;
use App\TempTaxDeclarations;

use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return TempTaxDeclarations::all();
    }
}
