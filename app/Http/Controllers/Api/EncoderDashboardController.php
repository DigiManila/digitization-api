<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ViewAverageEncodedRanking;

use App\TempTaxDeclarations;

use App\Profiles;

use Carbon\Carbon;

use App\CheckEncoderWork;
use Illuminate\Support\Facades\Log;

class EncoderDashboardController extends Controller
{
     //dashboard function

     public function getData(){

       
        $average_rank = self::getAverageRank();
        $top_rank = self::getTopRank();
        $max_encoded = self::getMaxEncoded();
        $fastest_encoder = self::getFastestEncoder();
        $encoded_per_day = self::getEncodePerDay();
        $updated_per_day = self::getUpdatedPerDay();
        $encoded_per_week = self::getWeeklyEncoded();
        $encoded_per_month = self::getMonthlyEncoded();
        $total_encoded = self::getTotalEncoded();
        $login_daily = self::getLoginDaily();
        // $check_encoder_work = self::getCheckEncoderWork();
        $sum_all_series = self::sumOfAllSeries();

        
        
        return response()->json([
            
            "data" => compact(
                
                
                'average_rank',
                'top_rank',
                'max_encoded',
                'fastest_encoder',
                'encoded_per_day',
                'updated_per_day',
                'encoded_per_week',
                'encoded_per_month',
                'total_encoded',
                'login_daily',
                // 'check_encoder_work',
                'sum_all_series'

                

                
                
            ),
            'message' => 'Dashboard Retrieved Successfully',
            'status' => 1
        ], 200);
    }

 

    private function getTopRank() {
        
        
        $query = ViewAverageEncodedRanking::select('fname','average')
                                        ->first();

                                    
        $query['fname'] = ucwords(strtolower($query['fname']));
        $query['average'] = number_format($query['average'],2,".",",");
        
        return $query;
    }

    private function getMaxEncoded() {
        
        
        $query = ViewAverageEncodedRanking::selectRaw("fname, number_works")
                                        ->orderBy('number_works','DESC')
                                        ->first();

                                    
        $query['fname'] = ucwords(strtolower($query['fname']));
        $query['number_works'] = $query['number_works'];
        
        return $query;
    }

    private function getFastestEncoder() {
        
        
        $query = TempTaxDeclarations::selectRaw("SUBSTR(temp_tax_declarations.created_at,1,10) as created_At, 
                                            COUNT(DISTINCT(pdf_filename_96)) AS highest, 
                                            CONCAT(profiles.last_name, ', ' ,profiles.first_name) AS fname")
                                        ->leftJoin('profiles', 'temp_tax_declarations.created_by', 
                                        '=', 'profiles.user_id')
                                        ->groupBy('fname', 'created_At')
                                        ->orderBy('highest','DESC')
                                        ->first();

                                    
        $query['fname'] = ucwords(strtolower($query['fname']));
        $query['highest'] = $query['highest'];
        $query['created_At'] = date('F d, Y', strtotime($query['created_At']));
        
        return $query;
    }

    private function getAverageRank(){

        $averageRank = [];
        $output = [];

        $query = ViewAverageEncodedRanking::select('created_by','fName','number_works','login_date','average','ranking')
                                    ->get();

        $query2 = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(pdf_filename_96)) AS pdf_filename_96")
                                    ->where( 'status',  1)
                                    ->whereRaw('created_at <> updated_at' )
                                    // ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                                    ->groupBy('created_by')
                                    ->get();

       

        
        foreach($query as $key => $value){
            $averageRank[$value['created_by']] = array(
                'fname' => ucwords(strtolower($value['fName'])),
                'total' => $value['number_works'],
                'login' => $value['login_date'],
                'average' => number_format($value['average'],2,".",","),
                'ranking' => $value['ranking'],
                // 'created_by' => $value['created_by'],
            );

            // $averageRank[] = array(
            //     'fname' => ucwords(strtolower($value['fName'])),
            //     'total' => $value['number_works'],
            //     'login' => $value['login_date'],
            //     'average' => number_format($value['average'],2,".",","),
            //     'ranking' => $value['ranking'],
            //     'created_by' => $value['created_by'],
            // );
        }
        
        foreach($query2 as $key => $value){
            
                $averageRank[$value['created_by']]['updated'] =  (int)$value['pdf_filename_96'];
                // $averageRank[$value['created_by']]['average'] =  (int)$value['pdf_filename_96'];
           
        }

        foreach($averageRank as $key => $value){

            $averageRank[$key]['created_by'] = $key;
        }

        foreach ($averageRank as $key => $value) {
            $output[] = (array)$value;
        }

        \Log::info($output);
        return $output;

    }

    // private function getCheckEncoderWork(){

    //     $checkEncoderWork = [];

    //     $query = CheckEncoderWork::select('series','zone','barangay','fname','work_pdf','total_pdf',
    //                                         'remarks','created_at')
    //                                 ->get();

        
    //     foreach($query as $key => $value){
    //         $checkEncoderWork[] = array(
    //             'series' => $value['series'],
    //             'zone' => $value['zone'],
    //             'barangay' => $value['barangay'],
    //             'fname' => ucwords(strtolower($value['fname'])),
    //             'work_pdf' => $value['work_pdf'],
    //             'total_pdf' => $value['total_pdf'],
    //             'remarks' => $value['remarks'],
    //             'created_at' => date('m-d-Y g:i:s A', strtotime($value['created_at'])),
                
            
    //         );
    //     }

    //     return $checkEncoderWork;

    // }



    private function getEncodePerDay(){

        $data = [];

        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                    ->whereDate('created_at', Carbon::today())
                    ->distinct('created_by')
                    ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                    ->whereDate('created_at', Carbon::today())
                    ->distinct('pdf_filename_96')
                    ->count('pdf_filename_96');
        
        
        $data['dailyTaxDec']=$queryPdfFilename96;
        
        $data['dailyEncoder'] = $queryCountCreatedBy;
        
      
        return $data;
    }

    private function getUpdatedPerDay(){

        $data = [];

        $today = substr(Carbon::today(),0,10);

        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                    
                    ->where( 'status',  1)
                    ->whereRaw('created_at <> updated_at' )
                    ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $today )
                    ->distinct('created_by')
                    ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                    
                    ->where( 'status',  1)
                    ->whereRaw('created_at <> updated_at' )
                    ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $today )
                    ->distinct('pdf_filename_96')
                    ->count('pdf_filename_96');
        
        
        $data['updatedTaxDec']= $queryPdfFilename96;
        
        $data['updateEncoder'] = $queryCountCreatedBy;
        
        

        return $data;
    }

    

    private function getWeeklyEncoded(){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $data = [];

        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
                ->where( 'status',  1)
                ->distinct('created_by')
                ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
                ->where( 'status',  1)
                ->distinct('pdf_filename_96')
                ->count('pdf_filename_96');
        
        $data['weeklyTaxDec']=$queryPdfFilename96;

        $data['weeklyEncoder'] =$queryCountCreatedBy;

        return $data;
    }

    private function getMonthlyEncoded(){

        $data = [];

        $month = Carbon::today()->month;   
    
        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                        ->whereMonth('created_at', $month)
                        ->where( 'status',  1)
                        ->distinct('created_by')
                        ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                        ->whereMonth('created_at', $month)
                        ->where( 'status',  1)
                        ->distinct('pdf_filename_96')
                        ->count('pdf_filename_96');
        
        $data['monthlyTaxDec']= $queryPdfFilename96;

        $data['monthlyEncoder'] =  $queryCountCreatedBy;
        return $data;
    }


    //Incase i need weekly report for a month
    private function getWeekRange(){
	
        //format string
        $f = 'Y-m-d';
    
        //if you want to record time as well, then replace today() with now()
        //and remove startOfDay()
        $today = Carbon::today();
        $date = $today->copy()->firstOfMonth()->startOfDay();
        $eom = $today->copy()->endOfMonth()->startOfDay();
      
        $dates1 = [];
    
        for($i = 1; $date->lte($eom); $i++){
            
            //record start date 
            $startDate = $date->copy();
               
            //loop to end of the week while not crossing the last date of month
            while($date->dayOfWeek != Carbon::SUNDAY && $date->lte($eom)){
                    $date->addDay(); 
                }
            
            $dates['w'.$i] = $startDate->format($f) . ' - ' . $date->format($f);
            $date->addDay();
          }
        
        return $dates1;
    }

    public function getTotalEncoded(){

        

        $data = [];

        $get1979 = TempTaxDeclarations::selectRaw("COUNT(DISTINCT(current_arp)) AS encoded79, MAX(created_at) as latestDate")
                    ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'A-' )
                    ->first();

            if ($get1979 != null){

             
                
                $get1979['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1979['latestDate']));      
            }
            else
            {
              
                $get1979['encoded79']  = 0;
                $get1979['latestDate']  = "n/a";
            }
        
        
        $get1985 = TempTaxDeclarations::selectRaw("COUNT(DISTINCT(current_arp)) AS encoded85, MAX(created_at) as latestDate")
                    ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'B-' )
                    ->first();
        
            if ($get1985 != null){

             
                
                $get1985['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1985['latestDate']));      
            }
            else
            {
               
                $get1985['encoded85']  = 0;
                $get1985['latestDate']  = "n/a";
            }

        
        $get1996 = TempTaxDeclarations::selectRaw("COUNT(DISTINCT(pdf_filename_96)) AS encoded96, MAX(created_at) as latestDate")
                    ->whereRaw('SUBSTRING(current_arp,1,3) = ?',  '96-' )
                    ->first();
        
            if ($get1996 != null){
             
                
                $get1996['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1996['latestDate']));
            }
            else
            {
               
                $get1996['encoded96']  = 0;
                $get1996['latestDate']  = "n/a";
            }

        $getOthers = TempTaxDeclarations::selectRaw("COUNT(DISTINCT(current_arp)) AS others, MAX(created_at) as latestDate")
        ->where(function($query) {
            $query->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'A-')
                        ->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'B-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '96-' )
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '97-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '98-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '99-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','00-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','01-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','02-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','03-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','04-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','05-');
                        
                    })
                        ->where('pdf_filename_96','!=', null )
                    ->first();

            if ($get1996 != null){
               
                
                $getOthers['latestDate']  = date('m-d-Y g:i:s A', strtotime($getOthers['latestDate']));
            }
            else    
            {
                
                $getOthers['others']  = 0;
                $getOthers['latestDate']  = "n/a";
            }
                                 
                          
        $data[] = array(
            $get1979,
            $get1985,
            $get1996,
            $getOthers
        );        

        return $data;
    }

    private function getLoginDaily(){

        $data = [];
        $output = [];

        $queryFname = ViewAverageEncodedRanking::select('created_by', 'fname')
                                        ->get();
        
        $query =  TempTaxDeclarations::selectRaw("created_by,  COUNT(DISTINCT(pdf_filename_96)) AS pdf_filename_96")
                                            ->whereDate('created_at',  Carbon::today())
                                            ->groupBy('created_by')
                                            ->get();
                                            
        $today = substr(Carbon::today(),0,10);

        $query2 = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(pdf_filename_96)) AS pdf_filename_96")
        
                            ->where( 'status',  1)
                            ->whereRaw('created_at <> updated_at' )
                            ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $today )
                            ->groupBy('created_by')
                            ->get();

        


        foreach($query as $key => $value){
            $data[$value['created_by']] = array(
                // 'fname' =>ucwords(strtolower($value['fname'])),
                'insert' => (int)$value['pdf_filename_96'],

            );
        }

        foreach($query2 as $key => $value){

          
            $data[$value['created_by']]['updated'] =  (int)$value['pdf_filename_96'];
           
        }

        foreach($queryFname as $key => $value){
            if(isset($data[$value['created_by']])){
            $data[$value['created_by']]['fname'] = ucwords(strtolower($value['fname']));
            }
        }

        foreach($data as $key => $value){

            $data[$key]['created_by'] = $key;
        }

        foreach ($data as $key => $value) {
            $output[] = (array)$value;
        }

        return $output;

    }

    // Pick by date

    public function getDatePick(Request $request){
    
        

       $data = [];

       $expDateMonth = explode('-',$request->sel);

            $year = $expDateMonth[0];
            $month = $expDateMonth[1];
        
        $data['pick_encoded_per_day']   = self::getPickEncodedPerDay($request->sel);
        $data['pick_updated_per_day']   = self::getPickUpdatedPerDay($request->sel);
        $data['pick_encoded_per_week']  = self::getPickWeeklyEncoded($request->sel);
        $data['pick_encoded_per_month'] = self::getPickMonthlyEncoded($year, $month);
    
        $data['pick_login_daily'] = self::getPickLoginDaily($request->sel);

        
        return $data;
       
    }

    private function getPickEncodedPerDay($e){
        
        $data = [];

        // $query1 = TempTaxDeclarations::selectRaw("COUNT(DISTINCT(created_by)) AS dailyEncoder, COUNT(DISTINCT(current_arp)) AS current_arp")
        //             ->whereDate('created_at', $e)
        //             ->first();
        // $query2 = TempTaxDeclarations::selectRaw("COUNT(DISTINCT(current_arp)) AS current_arp96, 
        // count(distinct pdf_filename_96) AS pdf_file96")
        // ->where(function($query) {
        //     $query->whereRaw('SUBSTRING(current_arp,1,3) = ?',  '96-' )
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?',  '97-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?',  '98-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?',  '99-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','00-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','01-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','02-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','03-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','04-')
        //                 ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','05-');
        //             })
                 
        //             ->whereDate('created_at', $e)
        //             ->first();
        
        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                    ->where( 'status',  1)
                    ->whereDate('created_at', $e)
                    ->distinct('created_by')
                    ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                    ->where( 'status',  1)
                    ->whereDate('created_at', $e)
                    ->distinct('pdf_filename_96')
                    ->count('pdf_filename_96');


        $data['dailyTaxDec']=$queryPdfFilename96;
        
        $data['dailyEncoder'] = $queryCountCreatedBy;
        $data['today'] = date('M. d, Y', strtotime($e));
        
        return $data;

    }

    private function getPickWeeklyEncoded($e){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $data = [];

        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                    ->whereBetween('created_at', [Carbon::parse($e)->startOfWeek()->format('Y-m-d'), Carbon::parse($e)->endOfWeek()->format('Y-m-d')])
                    ->where( 'status',  1)
                    ->distinct('created_by')
                    ->count('created_by');
    
        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                    ->whereBetween('created_at', [Carbon::parse($e)->startOfWeek()->format('Y-m-d'), Carbon::parse($e)->endOfWeek()->format('Y-m-d')])
                    ->where( 'status',  1)
                    ->distinct('pdf_filename_96')
                    ->count('pdf_filename_96');
    
        
        $data['weeklyTaxDec']= $queryPdfFilename96;
        $data['weeklyEncoder'] = $queryCountCreatedBy;
        $data['week'] = Carbon::parse($e)->weekOfMonth;; 
            
        return $data;
    }

    private function getPickMonthlyEncoded($year, $month){

        $data = [];


        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                        ->where( 'status',  1)
                        ->whereMonth('created_at', '=', $month)
                        ->whereYear('created_at', '=', $year)
                        ->distinct('created_by')
                        ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                        ->where( 'status',  1)
                        ->whereMonth('created_at', '=', $month)
                        ->whereYear('created_at', '=', $year)
                        ->distinct('pdf_filename_96')
                        ->count('pdf_filename_96');
        

        $query1['month'] = 

        $data['monthlyTaxDec']=$queryPdfFilename96;
        $data['monthlyEncoder'] = $queryCountCreatedBy;
        $data['month'] = Carbon::createFromDate($month)->format('F'); 
            
        
        return $data;
    }
    
    private function getPickLoginDaily($e){

        $data = [];
        $output = [];

       

        $queryFname = ViewAverageEncodedRanking::select('created_by', 'fname')
                                        ->get();
        
        $query =  TempTaxDeclarations::selectRaw("created_by,  COUNT(DISTINCT(pdf_filename_96)) AS pdf_filename_96")
                                            ->whereDate('created_at', $e)
                                            ->groupBy('created_by')
                                            ->get();
                                            
            // $e = substr(Carbon::today(),0,10);

        $query2 = TempTaxDeclarations::selectRaw("created_by, COUNT(DISTINCT(pdf_filename_96)) AS pdf_filename_96")
        
                            ->where( 'status',  1)
                            ->whereRaw('created_at <> updated_at' )
                            ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                            ->groupBy('created_by')
                            ->get();

        


        foreach($query as $key => $value){
            $data[$value['created_by']] = array(
                // 'fname' =>ucwords(strtolower($value['fname'])),
                'insert' => (int)$value['pdf_filename_96'],

            );
        }

        foreach($query2 as $key => $value){

           
            
            $data[$value['created_by']]['updated'] =  (int)$value['pdf_filename_96'];
            
        }

        foreach($queryFname as $key => $value){
            if(isset($data[$value['created_by']])){
            $data[$value['created_by']]['fname'] = ucwords(strtolower($value['fname']));
            }
        }

        foreach($data as $key => $value){

            $data[$key]['created_by'] = $key;
        }

        foreach ($data as $key => $value) {
            $output[] = (array)$value;
        }

        

        return $output;

    }

    private function getPickUpdatedPerDay($e){

        $data = [];

        $today = substr(Carbon::today(),0,10);

        $queryCountCreatedBy = TempTaxDeclarations::select('created_by')
                    
                    ->where( 'status',  1)
                    ->whereRaw('created_at <> updated_at' )
                    ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                    ->distinct('created_by')
                    ->count('created_by');

        $queryPdfFilename96 = TempTaxDeclarations::select('pdf_filename_96')
                    
                    ->where( 'status',  1)
                    ->whereRaw('created_at <> updated_at' )
                    ->whereRaw('SUBSTRING(updated_at,1,10) = ?',  $e )
                    ->distinct('pdf_filename_96')
                    ->count('pdf_filename_96');
        
        
        $data['updatedTaxDec']= $queryPdfFilename96;
        
        $data['updateEncoder'] = $queryCountCreatedBy;
        
        

        return $data;
    }
    

    public function getMonthlyRanking(Request $request){

       

        $explodeDateMonth = explode('-',$request->dateMonth);

            $year = $explodeDateMonth[0];
            $month = $explodeDateMonth[1];

        $data = [];

        $output = [];

        $queryAverageEncodedRank = TempTaxDeclarations::selectRaw("temp_tax_declarations.created_by,
                                                    CONCAT(profiles.last_name, ', ' ,profiles.first_name) AS fname,
                                            count(distinct (temp_tax_declarations.current_arp)) AS number_works,
                                            count(distinct(substr(temp_tax_declarations.created_at,1,10))) as login")
                                            ->leftJoin('profiles', 'temp_tax_declarations.created_by', 
                                            '=', 'profiles.user_id')
                            ->where('temp_tax_declarations.status', 1)
                            ->whereMonth('temp_tax_declarations.created_at', '=', $month)
                            ->whereYear('temp_tax_declarations.created_at', '=', $year)
                            ->groupBy('created_by','fname')
                            ->get();

        $queryDiffCount96 = TempTaxDeclarations::selectRaw('created_by, count(distinct(pdf_filename_96)) as pdf_file96,
                                                    count(distinct(current_arp)) as current_arp_96')
                                    ->where(function($query) {
                                        $query->whereRaw('SUBSTRING(current_arp,1,3) = ?',  '96-' )
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?',  '97-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?',  '98-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?',  '99-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','00-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','01-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','02-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','03-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','04-')
                                                ->orWhereRaw('SUBSTRING(current_arp,1,3) = ?','05-');
                                                })
                                    ->whereMonth('created_at', '=', $month)
                                    ->whereYear('created_at', '=', $year)
                                    ->groupBy('created_by')
                                    ->get();
        
        

        foreach($queryAverageEncodedRank as $key => $value){
            $data[$value['created_by']] = array(
                'fname'             => ucwords(strtolower($value['fname'])),
                'number_works'      => (int)$value['number_works'],
                'login'             => (int)$value['login'],
                'total'             => (int)$value['number_works'],
                'average'           => number_format((int)$value['number_works']  /
                                            (int)$value['login'],2,".",",")
            );
        }

        
        foreach($queryDiffCount96 as $key => $value){

            if(isset($data[$value['created_by']])){
                

                $data[$value['created_by']]['total'] = (int)$data[$value['created_by']]['number_works'] - 
                                                    ((int)$value['current_arp_96']) + (int)$value['pdf_file96'];
                
                $data[$value['created_by']]['average'] = number_format(((int)$data[$value['created_by']]['number_works'] - 
                                                        ((int)$value['current_arp_96']) + (int)$value['pdf_file96']) /
                                                    $data[$value['created_by']]['login'],2,".",",");
                
                $data[$value['created_by']]['pdf_file96'] = (int)$value['pdf_file96'];
                $data[$value['created_by']]['current_arp_96'] = (int)$value['current_arp_96'];
            }
           

        }

        foreach($data as $key => $value){

            $data[$key]['created_by'] = $key;
        }

        
        foreach ($data as $key => $value) {
            $output[] = (array)$value;
        }

        
        
        return $output;
        
                            

    }

    public function timeline(Request $request){
        $data = [];

       
        if ($request->dateMonth == null){
        $query = TempTaxDeclarations::selectRaw("DISTINCT(SUBSTRING(created_at,1,10)) AS dates, 
                    COUNT(DISTINCT(pdf_filename_96)) AS encoded")
                                        ->where('created_by', $request->sel)
                                        ->groupBy('dates')
                                        ->orderBy('dates','DESC')
                                        ->get();
        }

        else
        {

            $explodeDateMonth = explode('-',$request->dateMonth);

            $year = $explodeDateMonth[0];
            $month = $explodeDateMonth[1];

            $query = TempTaxDeclarations::selectRaw("DISTINCT(SUBSTRING(created_at,1,10)) AS dates, 
                    COUNT(DISTINCT(pdf_filename_96)) AS encoded")
                                        ->where('created_by', $request->sel)
                                        ->whereMonth('created_at', '=', $month)
                                        ->whereYear('created_at', '=', $year)
                                        ->groupBy('dates')
                                        ->orderBy('dates','DESC')
                                        ->get();
        }
        foreach($query as $key => $value){
            $data[] = array(
                'dates' => date('M. d, Y', strtotime($value['dates'])),
                'encoded' => $value['encoded'],

            );
        }                        

        return $data;
        
    }

    private function getLineChartEncoder(){
        $data = []; 

        $query =  TempTaxDeclarations::selectRaw("count(distinct(pdf_filename_96)) as encoded, 
                                            DATE_FORMAT(from_unixtime(unix_timestamp(created_at) 
                                            - unix_timestamp(created_at) mod 1), '%H:%i') as createdAt")
                            ->where('created_by', 24)
                            ->whereRaw("SUBSTR(created_at,1,10) = '2021-11-08'")
                            ->groupBy('createdAT')
                            ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'minutes' => $value['createdAt'],
                'encoded' => $value['encoded'],

            );
        }
        
        return $data;
    }

    public function updateEncoderDashboardCEW(Request $request){

       

        $update = CheckEncoderWork::where('series',$request->series)
                                 ->where('zone',$request->zone)
                                ->where('barangay', $request->barangay)
        ->update(
            [
                    'total_pdf' => (int)$request->total_pdf,
                    
                   
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

    

        $updateRemark = CheckEncoderWork::select('work_pdf','total_pdf')
                                    ->where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->first();

                if ($updateRemark['total_pdf'] == 0)
                {
                    CheckEncoderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'no PDF']);
                }

                else if ($updateRemark['work_pdf'] == 0)
                {
                    CheckEncoderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'available']);
                }


                else if ($updateRemark['work_pdf'] < $updateRemark['total_pdf']){
                    CheckEncoderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'incomplete']);
                }
                else  if ($updateRemark['work_pdf'] > $updateRemark['total_pdf']){
                    CheckEncoderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'over the mark']);
                }
                else if ($updateRemark['work_pdf'] = $updateRemark['total_pdf'])
                {
                    CheckEncoderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'complete']);
                }
                


        

        if($update){
            return response()->json([
                "data" => [],
                'message' => "Update Succcessfully",
                'status' => 1
            ], 200);            
        }
        
        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);

        
    }

    public function existPdfFilename96(Request $request) {

        // $answer = 0;

        $query = TempTaxDeclarations::where('pdf_filename_96',$request->sel)->exists();


        if($query){
            return false;
        }

        return true;
            // else{
            //     $answer = 0;
            // }

        // return $answer;

    }

    public function sumOfAllSeries(){

        $data = [];
        
        // $query2005 = CheckEncoderWork::where('series', '2005')
        //                         ->sum('total_pdf');
        
        $query1996 = CheckEncoderWork::where('series', '1996')
                                ->sum('total_pdf');
        
        $query1985 = CheckEncoderWork::where('series', '1985')
                                ->sum('total_pdf');
        
        $query1979 = CheckEncoderWork::where('series', '1979')
                                ->sum('total_pdf');
        
        // $queryTotalPDF =  CheckEncoderWork::sum('total_pdf');

        // $others = (int)$queryTotalPDF -( (int)$query2005 + (int)$query1996 + (int)$query1985 + (int)$query1979); 
        
        // $data['totalPDF2005'] = $query2005;
        $data['totalPDF1996'] = $query1996;
        $data['totalPDF1985'] = $query1985;
        $data['totalPDF1979'] = $query1979;
        

        return $data;

    }

}
