<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivePrevious extends Model
{
    use HasFactory;

    protected $table = 'active_previous';
}
