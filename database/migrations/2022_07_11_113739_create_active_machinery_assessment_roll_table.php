<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActiveMachineryAssessmentRollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_machinery_assessment_roll', function (Blueprint $table) {
            $table->id();
            $table->char('ARP',20);
            $table->char('UpdateCode',2)->nullable();
            $table->char('kind',1)->nullable();
            $table->char('PIN',30);
            $table->string('Location',70)->nullable();
            $table->char('OwnerNo',8)->nullable();
            $table->char('class_',4)->nullable();
            $table->date('Dt_Reg')->nullable();
            $table->char('Taxability',1)->nullable();
            $table->date('Effectivity')->nullable();
            $table->string('AppraisedBy',50)->nullable();
            $table->date('AppraisedDt')->nullable();
            $table->string('AssessedBy',50)->nullable();
            $table->date('AssessedDt')->nullable();
            $table->text('Memoranda')->nullable();
            $table->string('insertedBy',50)->nullable();
            $table->date('insertedDt')->nullable();
            $table->string('updatedBy',50)->nullable();
            $table->date('updatedDt')->nullable();
            $table->char('TctNo',25)->nullable();
            $table->char('SurveyNo',40)->nullable();
            $table->char('LotNo',25)->nullable();
            $table->char('BlkNo',25)->nullable();
            $table->char('North',60)->nullable();
            $table->char('East',60)->nullable();
            $table->char('South',60)->nullable();
            $table->char('West',60)->nullable();
            $table->char('Barangay',70)->nullable();
            $table->string('Owner',250)->nullable();
            $table->string('OwnerAddress',413)->nullable();
            $table->string('CCTNO',25)->nullable();
            $table->char('BldgOwner',100)->nullable();
            $table->char('BldgArp',20)->nullable();
            $table->char('LandOwner2',100)->nullable();
            $table->char('LandArp2',20)->nullable();
            $table->char('LandLotNo',25)->nullable();
            $table->char('LandBlkNo',25)->nullable();
            $table->char('UpdateDesc',70)->nullable();
            $table->string('BldgLocation',70)->nullable();
            $table->string('MachLocation',70)->nullable();
            $table->char('Assessor',70)->nullable();
            $table->decimal('TotalAv', 20,2)->nullable();
            $table->char('MuniDistName',70)->nullable();
            $table->char('Zone',30)->nullable();
            $table->integer('BldgStorey')->nullable();
            $table->string('BldgTypeDesc',50)->nullable();
            $table->string('StrucType',10)->nullable();
            $table->char('SubClass',4)->nullable();
            $table->date('TctDate')->nullable();
            $table->date('CCTDate')->nullable();
            $table->decimal('TotalArea', 20,2)->nullable();
            $table->decimal('TotalMv', 20,2)->nullable();
            $table->char('ADDL_LOT_NO',150)->nullable();
            $table->char('ADDL_BLK_NO',150)->nullable();
            $table->char('PREV_TCT_LAND',25)->nullable();
            $table->string('PREV_TCTDATE_LAND',10)->nullable();
            $table->string('PREV_TCT_BLDG',25)->nullable();
            $table->string('PREV_TCTDATE_BLDG',10)->nullable();
            $table->char('PREV_CCT_BLDG_ADDL',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_machinery_assessment_roll');
    }
}
