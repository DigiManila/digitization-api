<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\MapPolygon;
use App\BgyPolygon;
use App\Road;
use App\DistrictPolygon;
use App\PoliticalBoundary;

class KmlController extends Controller
{

    public function extractRoadKml(){
        $directory = storage_path('app' . DIRECTORY_SEPARATOR . 'kml'. DIRECTORY_SEPARATOR . 'road');
        $path = scandir($directory);
        $scanned_directory = array_diff($path, array('..', '.'));
        
        $arr = [];
        $counter = 1;

        \Log::info("------------------------------------- NEW DATA --------------------------------------");
    
        foreach($scanned_directory as $dir){

            $data = simplexml_load_file($directory . DIRECTORY_SEPARATOR . $dir);

            foreach ($data->Document->Folder as $key => $value) {
                foreach ($value->Placemark as $k => $v) {
                    $name = trim($v->ExtendedData->Data[1]->value);
                    if(!$name){
                        $name = "NR" . $counter;
                        $counter++;
                    }

                    $coords = $v->LineString->coordinates;
                    $coords_exp = explode("\n", $coords);
    
                    $text = "[";
    
                    foreach($coords_exp as $k2 => $v2){
    
                        if(trim($v2)){
    
                            $latlong_explode = explode(",", trim($v2));
                            $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
                        }
                        
                    }
                    $text = substr($text, 0, -1);
                    $text = $text . "]";
    
                    $arr[] = array(
                        'road'      => $name,
                        'polygon'   => $text,
                    );                    
                    
                }
            }          
        }

        $chunk_count = 2000;

        foreach (array_chunk($arr,$chunk_count) as $chunk){
            $count = Road::insert($chunk);
        }

        return 'OK';
    }

    public function extractKml(){

        $directory = storage_path('app' . DIRECTORY_SEPARATOR . 'kml'. DIRECTORY_SEPARATOR . 'bgy');
        $path = scandir($directory);
        $scanned_directory = array_diff($path, array('..', '.'));
        
        $arr = [];
        $counter = 1;

        \Log::info("------------------------------------- NEW DATA --------------------------------------");
    
        foreach($scanned_directory as $dir){

            $data = simplexml_load_file($directory . DIRECTORY_SEPARATOR . $dir);

            foreach ($data->Document->Folder as $key => $value) {

                $x_district = explode(" ", $value->name);
                $district = $x_district[1];

                foreach ($value->Placemark as $k => $v) {

                    if(trim($v->ExtendedData->Data[1]->value)){
                        $bgy = trim($v->ExtendedData->Data[1]->value);
                    } else{
                        $bgy = "NB" . $counter;
                        $counter++;
                    }

                    if($v->MultiGeometry){
                        
                        foreach($v->MultiGeometry->Polygon as $k2 => $v2){
                            $coords = $v2->outerBoundaryIs->LinearRing->coordinates;
                            // \Log::info($coords);
                            // return "OK";
                            $coords_exp = explode("\n", $coords);
            
                            // return $coords_exp;
            
                            $text = "[";
            
                            foreach($coords_exp as $k3 => $v3){
            
                                if(trim($v3)){
            
                                    $latlong_explode = explode(",", trim($v3));
                                    $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
                                }
                                
                            }
                            $text = substr($text, 0, -1);
                            $text = $text . "]";
            
                            $arr[] = array(
                                'bgy'       => $bgy,
                                'polygon'   => $text,
                                'district'  => $district,
                            );
                        }

                    } else{

                        $coords = $v->Polygon->outerBoundaryIs->LinearRing->coordinates;
                        $coords_exp = explode("\n", $coords);
        
                        // return $coords_exp;
        
                        $text = "[";
        
                        foreach($coords_exp as $k2 => $v2){
        
                            if(trim($v2)){
        
                                $latlong_explode = explode(",", trim($v2));
                                $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
                            }
                            
                        }
                        $text = substr($text, 0, -1);
                        $text = $text . "]";
        
                        $arr[] = array(
                            'bgy'       => $bgy,
                            'polygon'   => $text,
                            'district'  => $district,
                        );

                    }

                }
                
            }
            
            // foreach ($data->Document->Folder->Placemark as $key => $value) {

            //     $bgy = trim($value->ExtendedData->Data[1]->value) ? trim($value->ExtendedData->Data[1]->value) : "No Brgy";

            //     if($value->MultiGeometry){
                    
            //         foreach($value->MultiGeometry->Polygon as $k => $v){
            //             $coords = $v->outerBoundaryIs->LinearRing->coordinates;
            //             // \Log::info($coords);
            //             // return "OK";
            //             $coords_exp = explode("\n", $coords);
        
            //             // return $coords_exp;
        
            //             $text = "[";
        
            //             foreach($coords_exp as $k2 => $v2){
        
            //                 if(trim($v2)){
        
            //                     $latlong_explode = explode(",", trim($v2));
            //                     $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
            //                 }
                            
            //             }
            //             $text = substr($text, 0, -1);
            //             $text = $text . "]";
        
            //             $arr[] = array(
            //                 'bgy'       => $bgy,
            //                 'polygon'   => $text,
            //                 'district'  => 0,
            //             );
            //         }

            //     } else{

            //         $coords = $value->Polygon->outerBoundaryIs->LinearRing->coordinates;
            //         $coords_exp = explode("\n", $coords);
    
            //         // return $coords_exp;
    
            //         $text = "[";
    
            //         foreach($coords_exp as $k => $v){
    
            //             if(trim($v)){
    
            //                 $latlong_explode = explode(",", trim($v));
            //                 $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
            //             }
                        
            //         }
            //         $text = substr($text, 0, -1);
            //         $text = $text . "]";
    
            //         $arr[] = array(
            //             'bgy'       => $bgy,
            //             'polygon'   => $text,
            //             'district'  => 0,
            //         );

            //     }

            // }
            
        }

        // return "OK";

        $chunk_count = 2000;

        foreach (array_chunk($arr,$chunk_count) as $chunk){
            $count = BgyPolygon::insert($chunk);
        }

        return 'OK';
    }




    // public function extractKml(){

    //     $directory = storage_path('app' . DIRECTORY_SEPARATOR . 'kml'. DIRECTORY_SEPARATOR . 'map_polygon');
    //     $path = scandir($directory);
    //     $scanned_directory = array_diff($path, array('..', '.'));
        
    //     $arr = [];
    
    //     foreach($scanned_directory as $dir){
    
    //         $data = simplexml_load_file($directory . DIRECTORY_SEPARATOR . $dir);
    //         $brgy = explode(" ", $dir)[1];
    //         // $brgy = explode(".", $brgy)[0];

    //         foreach ($data->Document->Folder->Placemark as $key => $value) {

    //             if((array) $value->ExtendedData){

    //                 $pin = (array) $value->ExtendedData->SchemaData->children()->SimpleData[0];

    //                 $parsed_lat_long = "";
    //                 $parsed_gmap_lat_long = "";

    //                 if(isset($pin[0]) && strlen(trim($pin[0])) > 15){

    //                     $get_pin = $pin[0];

    //                     if($pin[0] == "Unknown Area Type"){
    //                         if((array) $value->ExtendedData->SchemaData->children()->SimpleData[1]){
    //                             $pin = (array) $value->ExtendedData->SchemaData->children()->SimpleData[1];
    //                             if(isset($pin[0])){
    //                                 $get_pin = $pin[0];
    //                             } else{
    //                                 continue;
    //                             }
                                
    //                         } else{
    //                             continue;
    //                         }

    //                     }

    //                     $center_coords = "";
            
    //                     if(!(array) $value->Polygon){
    //                         $coords = (array) $value->MultiGeometry->Polygon->outerBoundaryIs->LinearRing->coordinates;
    //                     } else{
    //                         $coords = (array) $value->Polygon->outerBoundaryIs->LinearRing->coordinates;
    //                     }

    //                     $coords_explode = explode(" ", trim($coords[0]));
    
    //                     // Compute center_coords
    //                     $center_coords = self::computeCenter($coords_explode);
    
    //                     foreach ($coords_explode as $k => $v) {
            
    //                         if(!$v){
    //                         } else{

    //                             $latlong = explode(",", $v);

    //                             $lat = trim($latlong[1]);
    //                             $lng = trim($latlong[0]);
                
    //                             $gmap_lat = $lat / 0.99999796;
    //                 //             // $gmap_lng = $lng / 1.000000233115300;
    //                             $gmap_lng = $lng / 1.000000466230960;
                
    //                             if($k){
    //                                 $parsed_lat_long =  $parsed_lat_long . "|{ \"lat\": " . $lat . ", \"lng\": " . $lng . " }";
    //                                 $parsed_gmap_lat_long = $parsed_gmap_lat_long . "|{ \"lat\": " . $gmap_lat . ", \"lng\": " . $gmap_lng . " }";
    //                             } else{
    //                                 $parsed_lat_long =  "{ \"lat\": " . $latlong[1] . ", \"lng\": " . $lng . " }";
    //                                 $parsed_gmap_lat_long = "{ \"lat\": " . $gmap_lat . ", \"lng\": " . $gmap_lng . " }";
    //                             }
    //                         }
    //                     }

    //                     //check barangay

    //                     if($brgy == "HARRISON" || $brgy == "LUNETA" || $brgy == "PORT"){
    //                         $bgy = explode('-',$get_pin);
    //                         $brgy = $bgy[2];
    //                     }

    //                     $arr[] = array(
    //                                 'pin' => $get_pin, 
    //                                 'polygon' => "[" . $parsed_lat_long . "]", 
    //                                 'gmap_polygon' => "[" . $parsed_gmap_lat_long . "]", 
    //                                 'center_coords' => $center_coords,
    //                                 'barangay' => $brgy
    //                             );

    //                 } else{
    //                     \Log::info(json_encode($value->ExtendedData));
    //                 }

    //             } 
    //         }
            
    //     }


        
    //     $chunk_count = 2000;

    //     foreach (array_chunk($arr,$chunk_count) as $chunk){
    //         $count = MapPolygon::insert($chunk);
    //     }

    //     return 'OK';
    // }

    private function computeCenter($data){

        $arrX = [];
        $arrY = [];

        foreach ($data as $key => $value) {

            $latLng = explode(",", $value);

            if($latLng[0]){
                $arrX[] = $latLng[1] / 0.99999796;
                $arrY[] = $latLng[0] / 1.000000466230960;
            }
        }

        sort($arrX);
        sort($arrY);

        $minX = reset($arrX);
        $maxX = end($arrX);

        $minY = reset($arrY);
        $maxY = end($arrY);

        $getCenterX = ($maxX - $minX) / 2;
        $getCenterY = ($maxY - $minY) / 2;

        return "{ \"lat\": " . (string)($minX + $getCenterX) . ", \"lng\": " . (string)($minY + $getCenterY) . " }";

    }

    private function removeNewLine($coords){

        print_r($coords);

        $coordinates = explode("\r\n", $coords[0]);

        foreach($coordinates as $val){
            print_r($val);
        }

    }

    public function extractKmlBrgy(){

        $pol_boundaries = PoliticalBoundary::select('bgy', 'district')->get();

        $dist = [];

        foreach ($pol_boundaries as $key => $value) {
            $bgy = trim($value['bgy']);
            $dist[$bgy] = $value['district'];
        }


        $directory = storage_path('app' . DIRECTORY_SEPARATOR . 'kml'. DIRECTORY_SEPARATOR . 'bgy');
        $path = scandir($directory);
        $scanned_directory = array_diff($path, array('..', '.'));

        foreach($scanned_directory as $dir){
    
            $data = simplexml_load_file($directory . DIRECTORY_SEPARATOR . $dir);

            $d = [];

            foreach ($data->Document->Folder as $key => $value){
                foreach($value as $val){
                    
                    if($val->name){

                        
                        // $d[] = $val->Polygon->outerBoundaryIs;
                        

                        $bgy = explode(" ", $val->name)[2];

                        $coords = $val->Polygon->outerBoundaryIs->LinearRing->coordinates;
                        $coords_exp = explode("\n", $coords);
        
                        // return $coords_exp;
        
                        $text = "[";
        
                        foreach($coords_exp as $k => $v){
        
                            if(trim($v)){
        
                                $latlong_explode = explode(",", trim($v));
        
                                $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
                            }
                            
                        }
                        $text = substr($text, 0, -1);
                        $text = $text . "]";
        
                        $d[] = array(
                            'bgy'       => $bgy,
                            'polygon'   => $text,
                            'district'  => isset($dist[$bgy]) ? $dist[$bgy] : null,
                        );
                    }
                }
            }
        }

        // return $d;
        
        $chunk_count = 2000;

        foreach (array_chunk($d,$chunk_count) as $chunk){
            $count = BgyPolygon::insert($chunk);
        }

        return 'OK';

    }

    public function extractPoliticalBoundaries(){
        $directory = storage_path('app' . DIRECTORY_SEPARATOR . 'kml'. DIRECTORY_SEPARATOR . 'political');
        $path = scandir($directory);
        $scanned_directory = array_diff($path, array('..', '.'));

        foreach($scanned_directory as $dir){
    
            $data = simplexml_load_file($directory . DIRECTORY_SEPARATOR . $dir);

            $d = [];

            foreach ($data->Document->Folder as $key => $value){
                foreach($value as $val){
                    if($val->name){

                        $bgy = explode(" ", $val->name)[1];

                        $coords = $val->Polygon->outerBoundaryIs->LinearRing->coordinates;
                        $coords_exp = explode("\n", $coords);
        
                        // return $coords_exp;
        
                        $text = "[";
        
                        foreach($coords_exp as $k => $v){
        
                            if(trim($v)){
        
                                $latlong_explode = explode(",", trim($v));
        
                                $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
                            }
                            
                        }
                        $text = substr($text, 0, -1);
                        $text = $text . "]";
        
                        $d[] = array(
                            'bgy'       => $bgy,
                            'polygon'   => $text,
                        );
                    }
                }
            }
        }
        
        $chunk_count = 2000;

        foreach (array_chunk($d,$chunk_count) as $chunk){
            $count = PoliticalBoundary::insert($chunk);
        }

        return 'OK';

    }

    public function updateDistrict(){

        $pol_boundaries = PoliticalBoundary::select('bgy', 'district')->get();

        $dist = [];

        foreach ($pol_boundaries as $key => $value) {
            $bgy = trim($value['bgy']);
            $dist[$bgy] = $value['district'];
        }

        $data = BgyPolygon::get();

        foreach ($data as $key => $value) {
            if(isset($dist[$value['bgy']])){
                $bgypoly = BgyPolygon::find($value['id']);
                $bgypoly->district = $dist[$value['bgy']];
                $bgypoly->save();
            }
        }

    }

    public function extractDistrict(){

        $directory = storage_path('app' . DIRECTORY_SEPARATOR . 'kml'. DIRECTORY_SEPARATOR . 'district');
        $path = scandir($directory);
        $scanned_directory = array_diff($path, array('..', '.'));
        
        $arr = [];

        // \Log::info("------------------------------------- NEW DATA --------------------------------------");
    
        foreach($scanned_directory as $dir){

            $data = simplexml_load_file($directory . DIRECTORY_SEPARATOR . $dir);

            foreach ($data->Document->Folder->Placemark as $key => $value) {

                $x_district = explode(" ", $value->name);
                $district = $x_district[1];

                $coords = $value->Polygon->outerBoundaryIs->LinearRing->coordinates;
                $coords_exp = explode("\n", $coords);

                $text = "[";

                foreach($coords_exp as $k => $v){

                    if(trim($v)){
                        $latlong_explode = explode(",", trim($v));
                        $text = $text . '{"lat": ' . $latlong_explode[1] . ', "lng": ' . $latlong_explode[0] . '}|';
                    }
                    
                }
                $text = substr($text, 0, -1);
                $text = $text . "]";

                $arr[] = array(
                    'district'  => $district,
                    'polygon'   => $text,
                );

            }

        }

        $chunk_count = 2000;

        foreach (array_chunk($arr,$chunk_count) as $chunk){
            $count = DistrictPolygon::insert($chunk);
        }

        return 'OK';

    }
}
