<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_data', function (Blueprint $table) {
            $table->id();
            $table->string('pin')->nullable();
            $table->string('pin_display')->nullable();
            $table->date('date_inst')->nullable();
            $table->string('seller')->nullable();
            $table->string('buyer')->nullable();
            $table->string('location')->nullable();
            $table->string('lot')->nullable();
            $table->string('block')->nullable();
            $table->string('area')->nullable();
            $table->string('selling_price')->nullable();
            $table->string('unit_value')->nullable();
            $table->string('sale_sq_m')->nullable();
            $table->string('mv_imp')->nullable();
            $table->string('class')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_data');
    }
}
