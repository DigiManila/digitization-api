<?php

namespace App\Http\Controllers;

use App\AggBarangayD;
use App\AssessmentRoll;
use Illuminate\Http\Request;

class AggBarangayDController extends Controller
{
    public function saveAggBarangay(){
        
        $barangay = [];

        for ($i = 951; $i<=999; $i++) {
            $brgyNo = str_pad($i,3,'0',STR_PAD_LEFT);
            

            $bQuery = AssessmentRoll::select('pin','area','market_value','currentAssessValue','previousAssessValue','kind')
                                    ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                                    -> where('cancelled', 0)
                                    ->get();
            
            $LResi= 0; $LComm= 0; $LChar= 0; $LEduc= 0; $LGovt= 0; $LIndu= 0; $LReli= 0; 
            $BResi= 0; $BComm= 0; $BChar= 0; $BEduc= 0; $BGovt= 0; $BIndu= 0; $BReli= 0; 
            $MResi= 0; $MComm= 0; $MChar= 0; $MEduc= 0; $MGovt= 0; $MIndu= 0; $MReli= 0; 
            $LSpc1= 0; $LSpc2= 0; $LSpc3= 0; $LSpc4= 0; $LSpc5= 0; $LSpc6= 0; $LSpec= 0;
            $BSpc1= 0; $BSpc2= 0; $BSpc3= 0; $BSpc4= 0; $BSpc5= 0; $BSpc6= 0; $BSpec= 0;
            $MSpc1= 0; $MSpc2= 0; $MSpc3= 0; $MSpc4= 0; $MSpc5= 0; $MSpc6= 0; $MSpec= 0;
            $LOthe= 0; $BOthe= 0; $MOthe= 0; 
            
            foreach($bQuery as $key => $value){
                
                if($value['kind'] == 'L-RESI'){
                    $LResi++;
                }
                else if($value['kind'] == 'L-COMM'){
                    $LComm++;
                }
                else if($value['kind'] == 'L-CHAR'){
                    $LChar++;
                }
                else if($value['kind'] == 'L-EDUC'){
                    $LEduc++;
                }
                else if($value['kind'] == 'L-GOVT'){
                    $LGovt++;
                }
                else if($value['kind'] == 'L-INDU'){
                    $LIndu++;
                }
                else if($value['kind'] == 'L-RELI'){
                    $LReli++;
                }
               
                else if($value['kind'] == 'B-RESI'){
                    $BResi++;
                }
                else if($value['kind'] == 'B-COMM'){
                    $BComm++;
                }
                else if($value['kind'] == 'B-CHAR'){
                    $BChar++;
                }
                else if($value['kind'] == 'B-EDUC'){
                    $BEduc++;
                }
                else if($value['kind'] == 'B-GOVT'){
                    $BGovt++;
                }
                else if($value['kind'] == 'B-INDU'){
                    $BIndu++;
                }
                else if($value['kind'] == 'B-RELI'){
                    $BReli++;
                }
               
                else if($value['kind'] == 'M-RESI'){
                    $MResi++;
                }
                else if($value['kind'] == 'M-COMM'){
                    $MComm++;
                }
                else if($value['kind'] == 'M-CHAR'){
                    $MChar++;
                }
                else if($value['kind'] == 'M-EDUC'){
                    $MEduc++;
                }
                else if($value['kind'] == 'M-GOVT'){
                    $MGovt++;
                }
                else if($value['kind'] == 'M-INDU'){
                    $MIndu++;
                }
                else if($value['kind'] == 'M-RELI'){
                    $MReli++;
                }
                
                else if($value['kind'] == 'L-SPC1'){
                    $LSpc1++;
                }
                else if($value['kind'] == 'L-SPC2'){
                    $LSpc2++;
                }
                else if($value['kind'] == 'L-SPC3'){
                    $LSpc3++;
                }
                else if($value['kind'] == 'L-SPC4'){
                    $LSpc4++;
                }
                else if($value['kind'] == 'L-SPC5'){
                    $LSpc5++;
                }
                else if($value['kind'] == 'L-SPC6'){
                    $LSpc6++;
                }

                else if($value['kind'] == 'B-SPC1'){
                    $BSpc1++;
                }
                else if($value['kind'] == 'B-SPC2'){
                    $BSpc2++;
                }
                else if($value['kind'] == 'B-SPC3'){
                    $BSpc3++;
                }
                else if($value['kind'] == 'B-SPC4'){
                    $BSpc4++;
                }
                else if($value['kind'] == 'B-SPC5'){
                    $BSpc5++;
                }
                else if($value['kind'] == 'B-SPC6'){
                    $BSpc6++;
                }

                else if($value['kind'] == 'M-SPC1'){
                    $MSpc1++;
                }
                else if($value['kind'] == 'M-SPC2'){
                    $MSpc2++;
                }
                else if($value['kind'] == 'M-SPC3'){
                    $MSpc3++;
                }
                else if($value['kind'] == 'M-SPC4'){
                    $MSpc4++;
                }
                else if($value['kind'] == 'M-SPC5'){
                    $MSpc5++;
                }
                else if($value['kind'] == 'M-SPC6'){
                    $MSpc6++;
                }

                else if($value['kind'] == 'L-SPEC'){
                    $LSpec++;
                }
                else if($value['kind'] == 'B-SPEC'){
                    $BSpec++;
                }
                else if($value['kind'] == 'M-SPEC'){
                    $MSpec++;
                }

                else if($value['kind'] == 'L-OTHE'){
                    $LOthe++;
                }
                else if($value['kind'] == 'B-OTHE'){
                    $BOthe++;
                }
                else if($value['kind'] == 'M-OTHE'){
                    $MOthe++;
                }
                
            }
       
    
            $countPin = $bQuery->count();
            $sumArea = $bQuery->sum('area');
            $sumMarketValue = $bQuery->sum('market_value');
            $sumAssessValue = $bQuery->sum('currentAssessValue');
            $sumPreviousValue =$bQuery->sum('previousAssessValue');
           
        

            $barangay[] = array(
               'brgyNo'                 =>  $brgyNo,
                'countBrgy'             =>  $countPin,
                'area'                  =>  $sumArea,
                'market_value'          =>  $sumMarketValue,
                'currentAssessValue'    =>  $sumAssessValue,
                'PreviousAssessValue'   =>  $sumPreviousValue,
                'L-RESI'                =>  $LResi,
                'L-COMM'                =>  $LComm,
                'L-CHAR'                =>  $LChar,
                'L-EDUC'                =>  $LEduc,
                'L-GOVT'                =>  $LGovt,
                'L-INDU'                =>  $LIndu,
                'L-RELI'                =>  $LReli,
                
                'B-RESI'                =>  $BResi,
                'B-COMM'                =>  $BComm,
                'B-CHAR'                =>  $BChar,
                'B-EDUC'                =>  $BEduc,
                'B-GOVT'                =>  $BGovt,
                'B-INDU'                =>  $BIndu,
                'B-RELI'                =>  $BReli,
                
                'M-RESI'                =>  $MResi,
                'M-COMM'                =>  $MComm,
                'M-CHAR'                =>  $MChar,
                'M-EDUC'                =>  $MEduc,
                'M-GOVT'                =>  $MGovt,
                'M-INDU'                =>  $MIndu,
                'M-RELI'                =>  $MReli,

                'L-SPC1'                =>  $LSpc1,
                'L-SPC2'                =>  $LSpc2,
                'L-SPC3'                =>  $LSpc3,
                'L-SPC4'                =>  $LSpc4,
                'L-SPC5'                =>  $LSpc5,
                'L-SPC6'                =>  $LSpc6,

                'B-SPC1'                =>  $BSpc1,
                'B-SPC2'                =>  $BSpc2,
                'B-SPC3'                =>  $BSpc3,
                'B-SPC4'                =>  $BSpc4,
                'B-SPC5'                =>  $BSpc5,
                'B-SPC6'                =>  $BSpc6,

                'M-SPC1'                =>  $MSpc1,
                'M-SPC2'                =>  $MSpc2,
                'M-SPC3'                =>  $MSpc3,
                'M-SPC4'                =>  $MSpc4,
                'M-SPC5'                =>  $MSpc5,
                'M-SPC6'                =>  $MSpc6,

                'L-SPEC'                =>  $LSpec,
                'B-SPEC'                =>  $BSpec,
                'M-SPEC'                =>  $MSpec,
                
                'L-OTHE'                =>  $LOthe,
                'B-OTHE'                =>  $BOthe,
                'M-OTHE'                =>  $MOthe,

                     
            );
            
            // AggBarangayD::where('brgyNo',$brgyNo)->delete();
        }
        
        
        
        $status = AggBarangayD::insert($barangay);
        
        
        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }

    
        
       
    
}
