<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewIdleLandsInspBgy extends Model
{
    use HasFactory;

    protected $table = 'view_idle_lands_insp_bgy';
}
