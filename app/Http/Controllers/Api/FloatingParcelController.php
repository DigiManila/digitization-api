<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\FloatingBuilding;
use App\FloatingMachinery;
use App\AssessmentRoll;

use App\ActiveAssessmentRoll;
use App\ActiveTaxdecAssmnt;
use App\ActiveLandAssessmentRoll;
use App\ActiveBuildingAssessmentRoll;
use App\ActiveMachineryAssessmentRoll;

class FloatingParcelController extends Controller
{

    public function getAllFloating(){

        $floating_building = self::getFloatingBuilding();
        $floating_machinery = self::getFloatingMachinery();

        $count_building = self::countFloatingBuilding();
        $count_machinery = self::countFloatingMachinery();

        

        return response()->json([
            "data" => compact(
               
                'floating_building',
                'floating_machinery',
                'count_machinery',
                'count_building'
                
            ),
            'message' => 'Floating Parcel Retrieved Successfully',
            'status' => 1
        ], 200);

    }

    public function getFloatingBuilding(){
        
        $building = [];

        $query = ActiveBuildingAssessmentRoll::select('pin','Barangay')
                                ->whereRaw(' substr(pin,1,18) not in (select pin from active_land_assessment_roll)') 
                                ->where('Barangay', '!=', 000) 
                                ->orderBy('Barangay')
                                ->get();

        foreach($query as $key => $value){
            
            $building [] = array(
                'brgyNo' => $value['Barangay'],
                'pin' => $value['pin'],
                
            );
        }


        return $building;
    }

    public function getFloatingMachinery(){
        
        $machinery = [];

        $query = FloatingMachinery::get()->toArray();

        $query = ActiveMachineryAssessmentRoll::select('pin','Barangay')
                            ->whereRaw(' substr(pin,1,18) not in (select pin from active_land_assessment_roll)') 
                            ->where('Barangay', '!=', 000) 
                            ->orderBy('Barangay')
                            ->get();

        foreach($query as $key => $value){
            
            $machinery [] = array(
                'brgyNo' => $value['Barangay'],
                'pin' => $value['pin'],
            );
        }

        return $machinery;
    }

    private function countFloatingBuilding(){
        
        
        $buildingCount = ActiveBuildingAssessmentRoll::select('pin')
                        ->whereRaw(' substr(pin,1,18) not in (select pin from active_land_assessment_roll)') 
                        ->where('Barangay', '!=', 000) 
                        ->orderBy('Barangay')
                        ->count('pin');

            $buildingCount = number_format($buildingCount);  
        
        return $buildingCount;
    }

    private function countFloatingMachinery(){
    

        $machineryCount = ActiveMachineryAssessmentRoll::select('pin')
                    ->whereRaw(' substr(pin,1,18) not in (select pin from active_land_assessment_roll)') 
                    ->where('Barangay', '!=', 000) 
                    ->orderBy('Barangay')
                    ->count('pin');

            $machineryCount = number_format($machineryCount);                                
        
        return $machineryCount;
    }

}
