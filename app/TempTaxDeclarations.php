<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class TempTaxDeclarations extends Model
{
    use HasFactory, LogsActivity;
    
    protected $guarded = [];

    // protected static $logAttributes = ['*'];

    protected static $logName = 'Tax Declaration';

    protected static $logUnGuarded = true;
   
}
