<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileViewerRemark extends Model
{
    use HasFactory;
    protected $guarded = [];
}
