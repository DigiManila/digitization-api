<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZoneDistrict extends Model
{
    use HasFactory;
    
    protected $table = 'zone_district';
}
