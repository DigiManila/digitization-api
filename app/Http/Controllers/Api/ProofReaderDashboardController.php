<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ViewAverageProofreadRanking;

use App\TaxDeclaration;

use App\CheckProofreaderWork;

use Carbon\Carbon;

class ProofReaderDashboardController extends Controller
{
    public function getData(){

       
        $average_rank = self::getAverageRank();
        $top_rank = self::getTopRank();
        $max_proofread = self::getMaxProofread();
        $fastest_proofreader = self::getFastestProofReader();
        $proofread_per_day = self::getProofreadPerDay();
        $proofread_per_week = self::getWeeklyProofread();
        $proofread_per_month = self::getMonthlyProofread();
        $total_proofread = self::getTotalProofread();
        $login_daily = self::getLoginDaily();
        $check_proofreader_work = self::getCheckProofreaderWork();
        $sum_all_series = self::sumOfAllSeries();

        
        
        return response()->json([
            
            "data" => compact(
                
                
                'average_rank',
                'top_rank',
                'max_proofread',
                'fastest_proofreader',
                'proofread_per_day',
                'proofread_per_week',
                'proofread_per_month',
                'total_proofread',
                'login_daily',
                'check_proofreader_work',
                'sum_all_series'

                

                
                
            ),
            'message' => 'Dashboard Retrieved Successfully',
            'status' => 1
        ], 200);
    }

    private function getTopRank() {
        
        
        $query = ViewAverageProofreadRanking::select('fname','average')
                                        ->first();

                                    
        $query['fname'] = ucwords(strtolower($query['fname']));
        $query['average'] = number_format($query['average'],2,".",",");
        
        return $query;
    }

    private function getMaxProofread() {
        
        
        $query = ViewAverageProofreadRanking::selectRaw("fname, number_works")
                                        ->orderBy('number_works','DESC')
                                        ->first();

                                    
        $query['fname'] = ucwords(strtolower($query['fname']));
        $query['number_works'] = $query['number_works'];
        
        return $query;
    }

    private function getFastestProofReader() {
        
        
        $query = TaxDeclaration::selectRaw("SUBSTR(tax_declarations.created_at,1,10) as created_At, 
                                            COUNT(pdf_filename_96) AS highest, 
                                            CONCAT(profiles.last_name, ', ' ,profiles.first_name) AS fname")
                                        ->leftJoin('profiles', 'tax_declarations.proofread_by', 
                                        '=', 'profiles.user_id')
                                        ->groupBy('fname', 'created_At')
                                        ->orderBy('highest','DESC')
                                        ->first();

                                    
        $query['fname'] = ucwords(strtolower($query['fname']));
        $query['highest'] = $query['highest'];
        $query['created_At'] = date('F d, Y', strtotime($query['created_At']));
        
        return $query;
    }

    private function getAverageRank(){

        $averageRank = [];

        $query = ViewAverageProofreadRanking::select('proofread_by','fName','number_works','login_date','average','ranking')
                                    ->get();

        
        foreach($query as $key => $value){
            $averageRank[] = array(
                'fname' => ucwords(strtolower($value['fName'])),
                'total' => $value['number_works'],
                'login' => $value['login_date'],
                'average' => number_format($value['average'],2,".",","),
                'ranking' => $value['ranking'],
                'proofread_by' => $value['proofread_by'],

            );
        }

        return $averageRank;

    }

    private function getCheckProofreaderWork(){

        $checkProofreaderWork = [];

        $query = CheckProofreaderWork::select('series','zone','barangay','fname','work_pdf','total_pdf',
                                            'remarks','created_at')
                                    ->get();

        
        foreach($query as $key => $value){
            $checkProofreaderWork[] = array(
                'series' => $value['series'],
                'zone' => $value['zone'],
                'barangay' => $value['barangay'],
                'fname' => ucwords(strtolower($value['fname'])),
                'work_pdf' => $value['work_pdf'],
                'total_pdf' => $value['total_pdf'],
                'remarks' => $value['remarks'],
                'created_at' => date('m-d-Y g:i:s A', strtotime($value['created_at'])),
                
            
            );
        }

        return $checkProofreaderWork ;

    }

    private function getProofreadPerDay(){

        $data = [];


        $query = TaxDeclaration::selectRaw("COUNT(DISTINCT(proofread_by)) AS dailyProofreader, 
        COUNT(pdf_filename_96) AS dailyTaxDec")
                    ->whereDate('created_at', Carbon::today())
                    ->first();
        
        
        $data['dailyTaxDec']=$query['dailyTaxDec'];
        
        $data['Proofreader'] = $query['Proofreader'];
        
        return $data;
    }

    private function getWeeklyProofread(){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $data = [];

        $query = TaxDeclaration::selectRaw("COUNT(DISTINCT(proofread_by)) AS weeklyProofreader, 
        COUNT(pdf_filename_96) AS weeklyTaxDec")
            ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
            ->first();
        
        $data['weeklyTaxDec']=$query['weeklyTaxDec'];

        $data['weeklyProofreader'] = $query['weeklyProofreader'];

        return $data;
    }

    private function getMonthlyProofread(){

        $data = [];

        $month = Carbon::today()->month;

        $query = TaxDeclaration::selectRaw("COUNT(DISTINCT(proofread_by)) AS monthlyProofreader, 
        COUNT(pdf_filename_96) AS monthlyTaxDec")
                                    ->whereMonth('created_at', $month)
                                    ->first();
        
        $data['monthlyTaxDec']=$query['monthlyTaxDec'];
        $data['monthlyProofreader'] = $query['monthlyProofreader'];
       
        return $data;
    }

    public function getTotalProofread(){

        $data = [];

        $get1979 = TaxDeclaration::selectRaw("COUNT(pdf_filename_96) AS proofread79, MAX(created_at) as latestDate")
                    ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'A-' )
                    ->first();

            if ($get1979 != null){

             
                
                $get1979['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1979['latestDate']));      
            }
            else
            {
              
                $get1979['proofread79']  = 0;
                $get1979['latestDate']  = "n/a";
            }
        
        
        $get1985 = TaxDeclaration::selectRaw("COUNT(pdf_filename_96) AS proofread85, MAX(created_at) as latestDate")
                    ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'B-' )
                    ->first();
        
            if ($get1985 != null){

             
                
                $get1985['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1985['latestDate']));      
            }
            else
            {
               
                $get1985['proofread85']  = 0;
                $get1985['latestDate']  = "n/a";
            }

        
        $get1996 = TaxDeclaration::selectRaw("COUNT(pdf_filename_96) AS proofread96, MAX(created_at) as latestDate")
        ->where(function($query) {
            $query->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'A-')
                        ->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'B-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  'AA-' );
        
                    })
                    ->first();
        
            if ($get1996 != null){
             
                
                $get1996['latestDate']  = date('m-d-Y g:i:s A', strtotime($get1996['latestDate']));
            }
            else
            {
               
                $get1996['proofread96']  = 0;
                $get1996['latestDate']  = "n/a";
            }

        $getOthers = TaxDeclaration::selectRaw("COUNT(pdf_filename_96) AS others, MAX(created_at) as latestDate")
        ->where(function($query) {
            $query->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'A-')
                        ->whereRaw('not SUBSTRING(current_arp,1,2) = ?',  'B-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '96-' )
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '97-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '98-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?',  '99-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','00-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','01-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','02-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','03-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','04-')
                        ->whereRaw('not SUBSTRING(current_arp,1,3) = ?','05-');
                        
                    })
                        ->where('pdf_filename_96','!=', null )
                    ->first();

            if ($get1996 != null){
               
                
                $getOthers['latestDate']  = date('m-d-Y g:i:s A', strtotime($getOthers['latestDate']));
            }
            else    
            {
                
                $getOthers['others']  = 0;
                $getOthers['latestDate']  = "n/a";
            }
                                 
                          
        $data[] = array(
            $get1979,
            $get1985,
            $get1996,
            $getOthers
        );        

        return $data;
    }

    private function getLoginDaily(){

        $data = [];
        $output = [];

        $query =  TaxDeclaration::selectRaw("average_proofread_ranking.proofread_by, average_proofread_ranking.fname,  
                                        COUNT(pdf_filename_96) AS pdf_filename_96")
                                            ->leftJoin('average_proofread_ranking', 'tax_declarations.proofread_by', '=', 'average_proofread_ranking.proofread_by')
                                            ->whereDate('tax_declarations.created_at',  Carbon::today())
                                            ->groupBy('average_proofread_ranking.proofread_by', 'average_proofread_ranking.fname')
                                            ->orderBy('pdf_filename_96','DESC')
                                            ->get()
                                            ->toArray();

    

                


        foreach($query as $key => $value){
            $data[$value['proofread_by']] = array(
                'fname' =>ucwords(strtolower($value['fname'])),
                'total' => (int)$value['pdf_filename_96'],

            );
        }

        // foreach($query2 as $key => $value){

        //     if(isset($data[$value['created_by']])){
        //         $data[$value['created_by']]['total'] = $data[$value['created_by']]['total'] + (int)$value['current_arp'];
        //     }

        // }

        // foreach($data as $key => $value){

        //     $data[$key]['created_by'] = $key;
        // }

        foreach ($data as $key => $value) {
            $output[] = (array)$value;
        }

        

        return $output;

    }

    public function getDatePick(Request $request){
    
        

        $data = [];
 
        $expDateMonth = explode('-',$request->sel);
 
             $year = $expDateMonth[0];
             $month = $expDateMonth[1];
         
         $data['pick_proofread_per_day']   = self::getPickProofreadPerDay($request->sel);
         $data['pick_proofread_per_week']  = self::getPickWeeklyProofread($request->sel);
         $data['pick_proofread_per_month'] = self::getPickMonthlyProofread($year, $month);
     
         $data['pick_login_daily'] = self::getPickLoginDaily($request->sel);
 
         
         return $data;
        
     }

     private function getPickProofreadPerDay($e){
        
        $data = [];

        $query = TaxDeclaration::selectRaw("COUNT(DISTINCT(proofread_by)) AS dailyProofreader, COUNT(pdf_filename_96) AS pdf_filename_96")
                    ->whereDate('created_at', $e)
                    ->first();

        $query['today'] = date('M. d, Y', strtotime($e));

        $data['dailyTaxDec']=$query['pdf_filename_96'];
        $data['dailyProofreader'] = $query['dailyProofreader'];
        $data['today'] = $query['today']; 
        
        return $data;

    }

    private function getPickWeeklyProofread($e){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $data = [];

        $query = TaxDeclaration::selectRaw("COUNT(DISTINCT(proofread_by)) AS weeklyProofreader, 
                        COUNT(pdf_filename_96) AS pdf_filename_96")
            ->whereBetween('created_at', [Carbon::parse($e)->startOfWeek()->format('Y-m-d'), Carbon::parse($e)->endOfWeek()->format('Y-m-d')])
            ->first();

        $query['week'] = Carbon::parse($e)->weekOfMonth;
        
        $data['weeklyTaxDec']=$query['pdf_filename_96'] ;
        $data['weeklyProofreader'] = $query['weeklyProofreader'];
        $data['week'] = $query['week']; 
            
        return $data;
    }

    private function getPickMonthlyProofread($year, $month){

        $data = [];

        $query = TaxDeclaration::selectRaw("COUNT(DISTINCT(proofread_by)) AS monthlyProofreader, 
                                    COUNT(pdf_filename_96) AS pdf_filename_96")
                                    ->whereMonth('created_at', '=', $month)
                                    ->whereYear('created_at', '=', $year)
                                    ->first();

        $query['month'] = Carbon::createFromDate($month)->format('F');

        $data['monthlyTaxDec']=$query['pdf_filename_96'];
        $data['monthlyProofreader'] = $query['monthlyProofreader'];
        $data['month'] = $query['month']; 
            
    
        return $data;
    }

    private function getPickLoginDaily($e){

        $data = [];
        $output = [];

        $query =  TaxDeclaration::selectRaw("average_proofread_ranking.proofread_by, 
        average_proofread_ranking.fname,  
        COUNT(pdf_filename_96) AS pdf_filename_96")
            ->leftJoin('average_proofread_ranking', 'tax_declarations.proofread_by', '=', 'average_proofread_ranking.proofread_by')
            ->whereDate('created_at',  $e)
            ->groupBy('average_proofread_ranking.proofread_by', 'average_proofread_ranking.fname')
            ->orderBy('pdf_filename_96','DESC')
            ->get();
       


        foreach($query as $key => $value){
            $data[$value['proofread_by']] = array(
                'fname' => ucwords(strtolower($value['fname'])),
                'total' => (int)$value['pdf_filename_96'],

            );
        }

        // foreach($query2 as $key => $value){

        //     if(isset($data[$value['created_by']])){
        //         $data[$value['created_by']]['total'] = $data[$value['created_by']]['total'] + (int)$value['current_arp'];
        //     }

        // }

        // foreach($data as $key => $value){

        //     $data[$key]['created_by'] = $key;
        // }

        foreach ($data as $key => $value) {
            $output[] = (array)$value;
        }

        return $output;
    }

    public function getMonthlyRanking(Request $request){

        $explodeDateMonth = explode('-',$request->dateMonth);

            $year = $explodeDateMonth[0];
            $month = $explodeDateMonth[1];

        $data = [];

        $output = [];

        $queryAverageProofreadRank = TaxDeclaration::selectRaw("tax_declarations.proofread_by,
                                                    CONCAT(profiles.last_name, ', ' ,profiles.first_name) AS fname,
                                            count(tax_declarations.pdf_filename_96) AS number_works,
                                            count(distinct(substr(tax_declarations.created_at,1,10))) as login")
                                            ->leftJoin('profiles', 'tax_declarations.proofread_by', 
                                            '=', 'profiles.user_id')
                            ->whereMonth('tax_declarations.created_at', '=', $month)
                            ->whereYear('tax_declarations.created_at', '=', $year)
                            ->groupBy('proofread_by','fname')
                            ->get();


        foreach($queryAverageProofreadRank as $key => $value){
            $data[$value['proofread_by']] = array(
                'fname'             => ucwords(strtolower($value['fname'])),
                'number_works'      => (int)$value['number_works'],
                'login'             => (int)$value['login'],
                'total'             => (int)$value['number_works'],
                'average'           => number_format((int)$value['number_works']  /
                                            (int)$value['login'],2,".",",")
            );
        }

        
        // foreach($queryDiffCount96 as $key => $value){

        //     if(isset($data[$value['created_by']])){
                

        //         $data[$value['created_by']]['total'] = (int)$data[$value['created_by']]['number_works'] - 
        //                                             ((int)$value['current_arp_96']) + (int)$value['pdf_file96'];
                
        //         $data[$value['created_by']]['average'] = number_format(((int)$data[$value['created_by']]['number_works'] - 
        //                                                 ((int)$value['current_arp_96']) + (int)$value['pdf_file96']) /
        //                                             $data[$value['created_by']]['login'],2,".",",");
                
        //         $data[$value['created_by']]['pdf_file96'] = (int)$value['pdf_file96'];
        //         $data[$value['created_by']]['current_arp_96'] = (int)$value['current_arp_96'];
        //     }
           

        // }

        // foreach($data as $key => $value){

        //     $data[$key]['created_by'] = $key;
        // }

        
        foreach ($data as $key => $value) {
            $output[] = (array)$value;
        }       
        
        return $output;                     

    }

    public function timeline(Request $request){
        $data = [];

        \Log::info($request->sel);
        if ($request->dateMonth == null){
        $query = TaxDeclaration::selectRaw("DISTINCT(SUBSTRING(created_at,1,10)) AS dates, 
                    COUNT(pdf_filename_96) AS proofread")
                                        ->where('proofread_by', $request->sel)
                                        ->groupBy('dates')
                                        ->orderBy('dates','DESC')
                                        ->get();
        }

       

        else
        {

            $explodeDateMonth = explode('-',$request->dateMonth);

            $year = $explodeDateMonth[0];
            $month = $explodeDateMonth[1];

            $query = TaxDeclaration::selectRaw("DISTINCT(SUBSTRING(created_at,1,10)) AS dates, 
                    COUNT(pdf_filename_96) AS proofread")
                                        ->where('proofread_by', $request->sel)
                                        ->whereMonth('created_at', '=', $month)
                                        ->whereYear('created_at', '=', $year)
                                        ->groupBy('dates')
                                        ->orderBy('dates','DESC')
                                        ->get();
        }

        
        foreach($query as $key => $value){
            $data[] = array(
                'dates' => date('M. d, Y', strtotime($value['dates'])),
                'proofread' => $value['proofread'],

            );
        }                        

        return $data;
        
    }

    public function updateProofreaderDashboardCEW(Request $request){

       

        $update = CheckProofreaderWork::where('series',$request->series)
                                 ->where('zone',$request->zone)
                                ->where('barangay', $request->barangay)
        ->update(
            [
                    'total_pdf' => (int)$request->total_pdf,
                    
                   
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

    

        $updateRemark = CheckProofreaderWork::select('work_pdf','total_pdf')
                                    ->where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->first();

                if ($updateRemark['total_pdf'] == 0)
                {
                    CheckProofreaderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'no PDF']);
                }

                else if ($updateRemark['work_pdf'] == 0)
                {
                    CheckProofreaderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'available']);
                }


                else if ($updateRemark['work_pdf'] < $updateRemark['total_pdf']){
                    CheckProofreaderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'incomplete']);
                }
                else  if ($updateRemark['work_pdf'] > $updateRemark['total_pdf']){
                    CheckProofreaderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'over the mark']);
                }
                else if ($updateRemark['work_pdf'] = $updateRemark['total_pdf'])
                {
                    CheckProofreaderWork::where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'complete']);
                }
                
        
        if($update){
            return response()->json([
                "data" => [],
                'message' => "Update Succcessfully",
                'status' => 1
            ], 200);            
        }
        
        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);

        
    }

    public function sumOfAllSeries(){

        $data = [];
        
        // // $query2005 = CheckProofreaderWork::where('series', '2005')
        //                         ->sum('total_pdf');
        
        $query1996 = CheckProofreaderWork::where('series', '1996')
                                ->sum('total_pdf');
        
        $query1985 = CheckProofreaderWork::where('series', '1985')
                                ->sum('total_pdf');
        
        $query1979 = CheckProofreaderWork::where('series', '1979')
                                ->sum('total_pdf');
        
        // $queryTotalPDF =  CheckEncoderWork::sum('total_pdf');

        // $others = (int)$queryTotalPDF -( (int)$query2005 + (int)$query1996 + (int)$query1985 + (int)$query1979); 
        
        // $data['totalPDF2005'] = $query2005;
        $data['totalPDF1996'] = $query1996;
        $data['totalPDF1985'] = $query1985;
        $data['totalPDF1979'] = $query1979;
        

        return $data;

    }
}

