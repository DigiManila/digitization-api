<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Storage;

use App\ReleaseTaxDec;

class ReleaseTaxDecViewingController extends Controller
{
    public function getDashboardData(){

        return ReleaseTaxDec::selectRaw('id, td_no, or_no, requestor, DATE_FORMAT(created_at, "%b. %d, %Y") as date_released')
                                ->orderBy('created_at', 'desc')
                                ->get()
                                ->toArray();

    }

    public function getFiles(Request $request){

        $file_path = "";

        if($request['mode'] == 'or'){
            $data = ReleaseTaxDec::select('or_path')
                                    ->where('id', $request['id'])
                                    ->first();
            $file_path = $data['or_path'];
        } elseif($request['mode'] == 'td'){
            $data = ReleaseTaxDec::select('td_path')
                                    ->where('id', $request['id'])
                                    ->first();
            $file_path = $data['td_path'];
        } elseif($request['mode'] == 'file'){
            $data = ReleaseTaxDec::select('file_path')
                                    ->where('id', $request['id'])
                                    ->first();
            $file_path = $data['file_path'];
        }

        if($file_path){
            
            $file = Storage::disk('svnas')->get($file_path);
            $fn   = basename($file_path);
    
            $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
            file_put_contents($temp_file_path, $file);
    
            $headers = array(
                'Content-Description: File Transfer',
                'Content-Type: application/octet-stream',
                'Content-Disposition: attachment; filename="' . $fn . '"',
            );
    
            return response()->download($temp_file_path, $fn, $headers);
            
        } else{
            return null;
        }

        
    }
}
