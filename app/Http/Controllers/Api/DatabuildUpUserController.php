<?php

namespace App\Http\Controllers\Api;

use App\ViewDatabuildUpUserWorks;
use App\TempTaxDeclarations;
use Carbon\Carbon;
use DB;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DatabuildUpUserController extends Controller
{
    
    public function getDatabuildUpUser() {
        $databuildup = [];

        $query = ViewDatabuildUpUserWorks::select('user_id','company_id', 'username', 'first_name','last_name','job','number_works')
                                    ->get();

        foreach($query as $key => $value){
            $databuildup[] = array(
                'user_id' => $value['user_id'],
                'company_id' => $value['company_id'],
                'username' => $value['username'],
                'first_name' => $value['first_name'],
                'last_name' => $value['last_name'],
                'job' => $value['job'],
                // 'position' => $query['position'], 
                'number_works' => $value['number_works'],

            );
        }

        return $databuildup;
    }

    public function getLineChartData(Request $request){

        //get first day
        $first_date =  $request['date'] . "-01 00:00:00";
        
        //get last day
        $last_date =  date("Y-m-t", strtotime($first_date)) . " 23:59:59";

        $data = TempTaxDeclarations::selectRaw('DISTINCT(temp_tax_declarations.current_arp), temp_tax_declarations.created_by, temp_tax_declarations.created_at, profiles.last_name, profiles.first_name, profiles.middle_name')
                                    ->leftJoin('profiles', 'profiles.user_id', '=', 'temp_tax_declarations.created_by')
                                    ->whereBetween('temp_tax_declarations.created_at', [$first_date, $last_date])
                                    ->orderBy('temp_tax_declarations.created_by')
                                    // ->groupBy('temp_tax_declarations.current_arp')
                                    ->get();

        // return compact('first_date', 'last_date');
        // return $data;
        return self::formatData($data);

    }

    private function formatData($data){

        $temp_data  = [];
        $users      = [];
        $arp_pool   = [];
        $output     = [];

        foreach ($data as $key => $value) {

            if(!array_search($value['current_arp'], $arp_pool)){

                $arp_pool[] = $value['current_arp'];

                if(!isset($users[$value['created_by']])){
                    $name = strtoupper($value['last_name'] . '_' .  $value['first_name']);
                    $users[$value['created_by']] = str_replace(' ', '_', $name);
                    // $users[$value['created_by']] = $name;
                }
    
                $mth_yr = date('Y-m-d', strtotime($value['created_at']));
    
                if(!isset($temp_data[$mth_yr])){
                    $temp_data[$mth_yr][$value['created_by']] = 1;
                } else{
                    if(!isset($temp_data[$mth_yr][$value['created_by']])){
                        $temp_data[$mth_yr][$value['created_by']] = 1;
                    } else{
                        $temp_data[$mth_yr][$value['created_by']] = $temp_data[$mth_yr][$value['created_by']] + 1;
                    }
                }
            }
            
        }

        $ctr = 0;

        ksort($temp_data);

        foreach ($temp_data as $key => $value) {
            foreach ($users as $k => $v) {
                if(isset($value[$k])){
                    $output[$ctr][$v] = $value[$k];
                } else{
                    $output[$ctr][$v] = 0;                    
                }
            }
            $output[$ctr]['created'] = $key;
            $ctr++;
        }

        $output_users = array_values($users);

        // return array_values($users);
        return compact('output', 'output_users');

    }

    // private function getEncodedRecords(){

    //     if(date('D')!='Mon'){
    //         $mon = date('Y-m-d 00:00:00',strtotime('last Monday'));
    //         $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
    //     } else{
    //         $mon = date('Y-m-d 00:00:00');
    //         $sun = date('Y-m-d 23:59:59',strtotime($mon . ' + 6 days'));
    //     }

    //     // $labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    //     $labels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    //     $series = [];

    //     $data = RecordsTaxDecData::select('id', 'created_at')
    //                         ->whereBetween('created_at', [$mon, $sun])
    //                         ->get()
    //                         ->groupBy(function($date) {
    //                             return Carbon::parse($date->created_at)->format('D');
    //                         });

    //     $series = self::getCount($data, $labels);

    //     return compact('series');

    // }

    // private function getCount($data, $labels){

    //     $arr = [];

    //     foreach($labels as $val){

    //         if(isset($data[$val])){
    //             $arr[] = count($data[$val]);
                
    //         } else{
    //             $arr[] = 0;
    //         }
    //     }

    //     return array($arr);

    // }

    // public function getNumberOfWorksPerDate() {
    //     $NumberOfWorks = [];

    //     $query =   TempTaxDeclarations:: select('current_arp')
    //             ->where( 'created_by', $user_id)
    //             ->where('status',$status) 
    //             ->whereBetween('created_at', [$rangeDate['startDate'], $rangeDate['endDate']])
    //             ->distinct()    
    //             ->count('current_arp');
    // }
}
