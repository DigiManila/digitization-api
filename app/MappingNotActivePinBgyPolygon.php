<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingNotActivePinBgyPolygon extends Model
{
    use HasFactory;
    protected $table = 'mapping_not_active_pin_bgy_polygons';
}
