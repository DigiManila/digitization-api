<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\TempTaxDeclarations;
use App\TaxDeclaration;
use App\TaxDeclarationsAssmnt;
use App\Profile;

use Auth;

class EncoderController extends Controller
{
    public function getDataEncoded(Request $request){

        $td = $request;

        if($td['year'] == '1996'){
            $data = TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'cancelled_by_td', 'previous_arp', 'created_by', 
                                    'owner_name', 'pin')
                        ->where('pdf_filename_96', $td['td'])
                        ->first();
        } else{
            $data = TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'cancelled_by_td', 'previous_arp', 
                        'owner_name', 'location', 'lot', 'block','created_by', 'pin')
                        ->where('current_arp', $td['td'])
                        ->first();
        }

        if($data){


            $profile = Profile::select('first_name', 'last_name')->where('user_id', $data['created_by'])->first();
            $enc_name = $profile['first_name'] . " " . $profile['last_name'];
            $data['encoder'] = $enc_name;
            

        }

        return $data;

    }

    public function updateEncoded(Request $request){

       

        $status = TempTaxDeclarations::where('id', $request->id )
                    ->update([
                        'previous_arp' => $request->previous_arp,
                        'cancelled_by_td' => $request->cancelled_by_td,
                        'owner_name' => $request->owner_name, 
                        'location' => $request->location,
                        'lot' => $request->lot,
                        'block' => $request->block
                    ]);

        

        if($status){
            return response()->json([
                "data" => [],
                'message' => "Update Successfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "Unsuccessful",
            'status' => 2
        ], 200);
    }

    public function getDataProofread(Request $request){

        
        
        $data = TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'cancelled_by_td', 'previous_arp', 'proofread_by', 
                    'owner_name', 'pin','location','lot','block','tct','tct_date','cct','cct_date',
                    'cancelled_by_td', 'previous_pin','memoranda','remarks')
                    ->where('pdf_filename_96', $request->td)
                    ->first();
        
        $propertyAssessment = [];
        
        $query =TaxDeclarationsAssmnt:: select('id','kind','actual_use', 'area', 'market_value',
                                    'assessed_value',)
                                    ->where('pdf_filename', $request->td)
                                    ->get()
                                    ->toArray();
        
        foreach($query as $key => $value){

                $propertyAssessment[] = array(
                    'id'                => $value['id'],
                    'kind'              => $value['kind'],
                    'actual_use'        => $value['actual_use'],
                    'area'              => $value['area'],
                    'market_value'      => $value['market_value'],
                    'assessed_value'    => $value['assessed_value']
                );
        };

        $data['propertyAssessment'] = $propertyAssessment;
        
        
        
        if($data){


            $profile = Profile::select('first_name', 'last_name')->where('user_id', $data['proofread_by'])->first();
            $enc_name = $profile['first_name'] . " " . $profile['last_name'];
            $data['proofreader'] = $enc_name;
            

        }

        return $data;
    }

    public function saveDataEncoded(Request $request){

        $request['created_by'] = Auth::user()->id;
        $request['status'] = 1;
        $request['is_databuildup'] = 1;
        $request['created_at'] = date('Y-m-d H:i:s');
        $request['updated_at'] = date('Y-m-d H:i:s');

        // return 0;
        
        $status = TempTaxDeclarations::insert($request->toArray());

        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);
        }

        return response()->json([
            "data" => [],
            'message' => "Not Saved",
            'status' => 2
        ], 200);
    }
}
