<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\AssessmentRoll;
use Storage;

class ExcelController extends Controller
{
    public static function extractExcel(){

        $active_count       = self::saveData('Active');
        return "Total files processed: " . ($active_count);

    }

    public static function extractInitialExcel(){

        $cancelled_count    = self::saveData('Cancelled');

        $active_count       = self::saveData('Active');
        \Log::info("Active OK");
        return "Total files processed: $cancelled_count + $active_count = " . ($cancelled_count + $active_count);

    }

    private static function saveData($mode){

        $ctr = 0;
        $data = [];
        $cancelled_pins = [];

        $file_list = Storage::files("assessment" . DIRECTORY_SEPARATOR . date("Y-m-d") . DIRECTORY_SEPARATOR . $mode);



        foreach($file_list as $list){

            $app_path = "app" . DIRECTORY_SEPARATOR . $list;

            $inputFileName = storage_path($app_path);
    
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($inputFileName);
    
            $worksheet = $spreadsheet->getActiveSheet();
    
            $highestRow = $worksheet->getHighestRow();
    
            for ($row = 11; $row <= $highestRow; $row++) {

                if($mode == 'Cancelled' && $spreadsheet->getActiveSheet()->getCellByColumnAndRow(8, $row)->getValue()){
                    $cancelled_pins[] = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(8, $row)->getValue();
                }
    
                if(trim($spreadsheet->getActiveSheet()->getCellByColumnAndRow(12, $row)->getValue()) == "Grand Total" || trim($spreadsheet->getActiveSheet()->getCellByColumnAndRow(12, 13)->getValue()) == "Grand Total" ){
                    break;
                }                

                $connected_to_prev_data = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(2, $row)->getValue() ? true : false;

                //owners_name

                if(!$connected_to_prev_data){

                    $last_arr_val = end($data);

                    // if(!$spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue()){
                        // \Log::info($list . " -- " . $row);
                        // \Log::info($connected_to_prev_data);
                        // \Log::info($last_arr_val['address']);
                    // }

                    $owner_name = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, $row)->getValue() : $last_arr_val['owner_name'];
                    $address    = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue() : $last_arr_val['address'];
                    $lot        = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $row)->getValue() : $last_arr_val['lot'];
                    $block      = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $row)->getValue() : $last_arr_val['block'];
                    $status     = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(10, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(10, $row)->getValue() : $last_arr_val['status'];
                    $kind       = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(13, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(13, $row)->getValue() : $last_arr_val['kind'];

                    // \Log::info($address);

                } else{
                    $owner_name = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, $row)->getValue() : "-";
                    $address    = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue() : "-";
                    $lot        = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $row)->getValue() : "-";
                    $block      = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $row)->getValue() : "-";
                    $status     = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(10, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(10, $row)->getValue() : "-";
                    $kind       = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(13, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(13, $row)->getValue() : "-";
                }

                $tct                 = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(7, $row)->getValue();
                $pin                 = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(8, $row)->getValue();
                $tax_effectivity     = date('Y-m-d', strtotime($spreadsheet->getActiveSheet()->getCellByColumnAndRow(9, $row)->getValue()));
                $current_arp         = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(11, $row)->getValue();
                $previous_arp        = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(12, $row)->getValue();
                $area                = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(14, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(14, $row)->getValue() : 0;
                $market_value        = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(15, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(15, $row)->getValue() : 0;
                $assessment_level    = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(16, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(16, $row)->getValue() : 0;
                $currentAssessValue  = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(17, $row)->getValue() ? $spreadsheet->getActiveSheet()->getCellByColumnAndRow(17, $row)->getValue() : 0;
                $previousAssessValue = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(18, $row)->getValue();
                $taxable             = strpos($list, "taxable") !== false ? 1 : 0;
                $cancelled           = $mode == 'Cancelled' ? 1 : 0;
                $district_id         = 0;
                $zone_id             = 0;
                $created_by          = "Created by command script";
                $updated_by          = "Created by command script";

                // \Log::info($current_arp);

                $data[] = array(
                    'owner_name'          => $owner_name,
                    'address'             => $address, 
                    'lot'                 => $lot,
                    'block'               => $block,
                    'tct'                 => $tct,
                    'pin'                 => $pin,
                    'tax_effectivity'     => $tax_effectivity,
                    'status'              => $status,
                    'current_arp'         => $current_arp,
                    'previous_arp'        => $previous_arp,
                    'cancelled_by_td'     => 'none',
                    'kind'                => $kind,
                    'area'                => $area,
                    'market_value'        => $market_value,
                    'assessment_level'    => $assessment_level,
                    'currentAssessValue'  => $currentAssessValue,
                    'previousAssessValue' => $previousAssessValue,
                    'taxable'             => $taxable,
                    'cancelled'           => $cancelled,
                    'district_id'         => $district_id,
                    'zone_id'             => $zone_id,
                    'created_by'          => $created_by,
                    'updated_by'          => $updated_by,
                );

                $ctr++;

                // \Log::info($spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, $row)->getValue());
            }
        }

        // $active_pins = AssessmentRoll::select('cancelled')->whereIn('pin', $cancelled_pins)->get();

        // return 0;

        $chunk_count = 10;

        \Log::info("Before Loop");

        foreach (array_chunk($data,$chunk_count) as $chunk){

            // foreach($chunk as $address){
            //     if(!$address['address']){
            //         \Log::info($address);
            //     }
            // }

            $count = AssessmentRoll::insert($chunk);
            // \Log::info($count);
        }

        return $ctr;

    }

}
