<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReleaseCancellationVerification extends Model
{
    use HasFactory;

    protected $guarded = [];
}
