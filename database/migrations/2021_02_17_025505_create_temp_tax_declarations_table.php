<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempTaxDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_tax_declarations', function (Blueprint $table) {
            $table->id();
            $table->string("pin")->nullable();
            $table->string("current_arp")->nullable();
            $table->text("owner_name");
            $table->string("cancelled_by_td")->nullable();
            $table->string("previous_arp")->nullable();
            $table->string('ref_tax_dec')->nullable();
            $table->date('effectivity_date')->nullable();
            $table->text("location")->nullable();
            $table->string("lot")->nullable();
            $table->string("block")->nullable();
            $table->string("tct")->nullable();
            $table->date("tct_date")->nullable();
            $table->string("cct")->nullable();
            $table->date("cct_date")->nullable();
            
            
            $table->string("kind");
            $table->decimal("area",20,2);
            $table->decimal("market_value",20,2);
           
            $table->decimal("currentAssessValue",20,2);
            $table->boolean('is_databuildup');
            $table->text('created_by')->nullable();
            $table->text('proofread_by')->nullable();
            $table->date('proofread_date')->nullable();
            
                        
            
            $table->string("verified_by")->nullable();
            $table->date('verified_date')->nullable();
            $table->text('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_tax_declarations');
    }
}
