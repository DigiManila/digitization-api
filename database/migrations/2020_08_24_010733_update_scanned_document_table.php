<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateScannedDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scanned_documents', function (Blueprint $table) {
            $table->bigInteger('assessment_roll_id')->change();
            $table->boolean('is_assessment_roll')->after('assessment_roll_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scanned_documents', function (Blueprint $table) {
            //
        });
    }
}
