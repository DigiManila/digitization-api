<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\TempTaxDeclarations;

use App\User;

use Auth;

use Spatie\Activitylog\Models\Activity;

class TempTaxDecController extends Controller
{
    public function saveTempTaxDec(Request $request){
        $saveArray = [];
        if($request['save96'] == 1){
            $current_arp = $request['current_arp'];
            $current_arp_96 = $request['current_arp_96'];
            $pdf_filename_96 = $request['pdf_filename_96'];
            $previous_arp = $request['previous_arp'];
            $pin = $request['pin'];
            $cancelled_by_td = $request['cancelled_by_td'];
            $owner_name = $request['owner_name'];
            $kind = 'L-';
            $actual_use = 'RESI';


            
                
                   
                $saveArray[] = array(
                    'pin' => $pin,
                    'current_arp_96' => $current_arp_96,
                    'pdf_filename_96' => $pdf_filename_96,
                    'current_arp' => $current_arp,
                    'owner_name' => $owner_name,
                    'cancelled_by_td' => $cancelled_by_td,
                    'previous_arp' => $previous_arp,
                    'kind' => $kind.$actual_use,
                    
                    
                    
                    'status' => 1,
                    'is_databuildup' => 1,
                    'created_by' => Auth::user()->id,

                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),


                );

            
            
                 
            
    
        }
        
        else {
        $block = $request['block'];
        $cancelled_by_td = $request['cancelled_by_td'];
        $cct = $request['cct'];
        $cct_date = $request['cct_date'];
        $current_arp = $request['current_arp'];
        $current_arp_96 = $request['current_arp_96'];
        $pdf_filename_96 = $request['pdf_filename_96'];
        $location = $request['location'];
        $lot = $request['lot'];
        $owner_name = $request['owner_name'];
        $pin = $request['pin'];
        $previous_arp = $request['previous_arp'];
        $tct = $request['tct'];
        $tct_date = $request['tct_date'];
        $ref_tax_dec = $request['ref_tax_dec'];
        $effectivity_date = $request['effectivity_date'];
        $is_databuildup = $request['is_databuildup'];
        $created_by = $request['created_by'];
        $status = $request['status'];

        foreach($request['property_assessment'] as $key => $value){
            if($value['kind'] <> null ){
                $saveArray[] = array(
                    'pin' => $pin,
                    'current_arp_96' => $current_arp_96,
                    'pdf_filename_96' => $pdf_filename_96,
                    'current_arp' => $current_arp,
                    'owner_name' => $owner_name,
                    'cancelled_by_td' => $cancelled_by_td,
                    'previous_arp' => $previous_arp,
                    'location' => $location,
                    'lot' => $lot,
                    'block' => $block,
                    'tct' => $tct,
                    'tct_date' => $tct_date,
                    'cct' => $cct,
                    'cct_date' => $cct_date,
                    'kind' => $value['kind'].$value['actual_use'],
                    'area' => $value['area'],
                    'market_value' => $value['market_value'],
                    'currentAssessValue' => $value['assessment_value'],
                    
                    'ref_tax_dec' => $ref_tax_dec,
                    'effectivity_date' => $effectivity_date,
                    'status' => 1,
                    'is_databuildup' => 1,
                    'created_by' => Auth::user()->id,

                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),


                );

            }

            
        }
    }
        

        $status = TempTaxDeclarations::insert($saveArray);

        $activity = Activity::all()->last();

        $activity->description; //returns 'created'
        $activity->subject; //returns the instance of NewsItem that was created
        $activity->changes; //returns ['attributes' => ['name' => 'original name', 'text' => 'Lorum']];

        //call Logs
        Controller::addLog('Encode', $new_data = "" , $prev_data = "",  $remarks = "save to TempTaxDec");
    
        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);

    //  return $saveArray;

        

    }

    // private function getKind($kind){
    //     if ($kind == 2) {
    //         return 'B-';
    //     }
    //     else if($kind == 3) {
    //         return 'M-';
    //     }
    //     else {
    //         return 'L-';
    //     }
    // }

   

    private function getDataBuildUp($buildUp) {
        if ($buildUp == 1) {
            return "Data Build-up";
        }
        else  {
            return  "Reconstruction";
        }
    }

    private function getStatus($status) {
        if ($status == 1) {
            return "Created";
        }
        else if($status == 2)  {
            return  "Proofed";
        }
        else {
            return "Verified";
        }
    }

    public function getSingleTempTaxDec(Request $request){
        $id = $request['id'];
       
        $data = TempTaxDeclarations::select(
            'current_arp_96',  'pdf_filename_96',
            'current_arp', 'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 
            'location', 'lot', 'block', 'cct', 'tct', 'tct_date', 'cct_date', 
            'kind', 'area', 'market_value', 'currentAssessValue', 'is_databuildup',
            'created_by'
            )
        ->where('id',$id)
        ->first();

        $actual_use = $data['kind'];

        $data['kind'] = self::getKindType($data['kind']);
        $data['actual_use'] = self::getActualUse($actual_use);
            
        return $data;
    }

    public function getTraceAssessment(Request $request) {
        $assessment_data=[];
        $getTraceAssessmentReturn=[];

        $getTraceAssessmentReturn['current_arp'] = $request['current_arp'];

        $getTraceAssessment_data = TempTaxDeclarations::where('current_arp',  $request['current_arp'])
        ->where('status', 1)
        
        ->get()
        ->toArray();

        foreach($getTraceAssessment_data as $key => $value){

            $assessment_data[] = array(
                
                'pin'                   => $value['pin'],
                'cancelled_by_td'       => $value['cancelled_by_td'],
                'owner_name'            => $value['owner_name'],
                'location'              => $value['location'],
                'lot'                   => $value['lot'],
                'block'                 => $value['block'],
                'tct'                   => $value['tct'],
                'tct_date'              => $value['tct_date'],
                'cct'                   => $value['cct'],
                'cct_date'              => $value['cct_date'],
                'effectivity_date'      => $value['effectivity_date'],
                'status'                => $value['status'],
                'current_arp'           => $value['current_arp'],
                'current_arp_96'        => $value['current_arp_96'],
                'pdf_filename_96'       => $value['pdf_filename_96'],
                'previous_arp'          => $value['previous_arp'],
                'kind'                  => self::getTraceKindType($value['kind']),
                'actual_use'            => self::getTraceActualUse($value['kind']),
                'area'                  => number_format($value['area'],2,".",",") . ".sqm",
                'market_value'          => "₱ " . number_format($value['market_value'],2,".",","),
                'currentAssessValue'    => "₱ " .number_format($value['currentAssessValue'],2,".",","),
               
                

            );

        }

        $getTraceAssessmentReturn['data'] = $assessment_data;

        
        return $getTraceAssessmentReturn;
    }

    private function getKindType($type){

        $exploded_type = explode("-", $type);
        
        return $exploded_type[0];

    }

    private function getActualUse($use){

        $exploded_type = explode("-", $use);
        
       return $exploded_type[1];
    }


    private function getTraceKindType($type){

        $exploded_type = explode("-", $type);
        
        switch ($exploded_type[0]) {
            case "L":
                return "Land";
                break;
            case "B":
                return "Building";
                break;
            case "M":
                return "Machinery";
                break;
            default:
                return null;
        }

    }

    private function getTraceActualUse($use){

        $exploded_type = explode("-", $use);
        
        switch ($exploded_type[1]) {
            case "CHAR":
                return "Charitable";
                break;
            case "COMM":
                return "Commercial";
                break;
            case "EDUC":
                return "Education";
                break;
            case "GOVT":
                return "Government";
                break;
            case "INDU":
                return "Industrial";
                break;
            case "MINE":
                return "Mine";
                break;
            case "OTHE":
                return "Others";
                break;
            case "RELI":
                return "Religion";
                break;
            case "RESI":
                return "Residential";
                break;
            case "SPC1" || "SPC1" || "SPC2" || "SPC3" || "SPC4" || "SPC5" || "SPC6" || "SPEC":
                return "Special";
                break;
            default:
                return null;
        }

    }
}
