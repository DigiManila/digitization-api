<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasActiveArpMappingMachineryDetailsView extends Model
{
    use HasFactory;

    protected $table = 'rptas_active_arp_mapping_machinery_details';
}
