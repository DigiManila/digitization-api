<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxDelinquenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_delinquencies', function (Blueprint $table) {
            $table->id();
            $table->string("pin");
            $table->string("barangay");
            $table->string("barangay_origin")->nullable();
            $table->string("district");
            $table->string("zone");
            $table->text("owner");
            $table->string("kind");
            $table->string("year_month_from");
            $table->string("year_month_to");
            $table->decimal("basic", 15, 2);
            $table->decimal("sef", 15, 2);
            $table->decimal("total_penalty", 15, 2);
            $table->decimal("grand_total", 15, 2);
            $table->date("date_as_of");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_delinquencies');
    }
}
