<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAggSingleRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agg_single_records', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('total_records')->default(0);
            $table->bigInteger('total_active_pin')->default(0);
            $table->bigInteger('total_cancelled_records')->default(0);
            $table->bigInteger('total_archive_records')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agg_single_records');
    }
}
