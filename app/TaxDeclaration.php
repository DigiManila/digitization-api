<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class TaxDeclaration extends Model
{
    use HasFactory, LogsActivity;

    protected static $logUnGuarded = true;
    
    protected $guarded = [];

    
}
