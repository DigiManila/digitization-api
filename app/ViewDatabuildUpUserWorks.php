<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewDatabuildUpUserWorks extends Model
{
    use HasFactory;
    protected $table = 'databuild_up_user';
}
