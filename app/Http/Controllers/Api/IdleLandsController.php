<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\AssessmentRoll;
use App\IdleLand;
use App\IdleLandBarangay;
use App\IdleLandsTagging;

use App\ViewIdleLandsTagged;
use App\ViewIdleLandsTaggedBgy;

use App\ViewIdleLandsInsp;
use App\ViewIdleLandsInspBgy;

use Storage;
use Response;

class IdleLandsController extends Controller
{

    public function getBarangay(){

        $idle = IdleLandBarangay::get();
        $tagged = ViewIdleLandsTaggedBgy::get();
        $insp = ViewIdleLandsInspBgy::get();
        $by_pin = IdleLand::select('pin', 'area', 'kind')->get();
        $count = 0;

        $sum = 0;

        foreach ($idle as $key => $value) {
            $sum = $sum + $value['area'];
            $count = $count + $value['sum_pin'];
        }

        $count = number_format($count);
        $sum = number_format($sum);

        return compact('idle', 'sum', 'count', 'tagged', 'insp', 'by_pin');

    }

    public function getBarangayKind(Request $request){

        $bgy = $request['bgyNo'];

        $out = [];

        $data = IdleLand::select('area', 'kind')
                        ->whereRaw("substring_index(substring_index(`view_idle_lands`.`pin`,'-',3),'-',-(1)) = $bgy")
                        ->get();

        foreach($data as $key => $val){
            if(isset($out[$val['kind']])){
                $out[$val['kind']]['total'] = $out[$val['kind']]['total'] + $val['area'];
            } else{
                $out[$val['kind']]['total'] = $val['area'];
            }
        }

        foreach($out as $key => $val){
            $out[$key]['total'] = number_format($val['total'], 2);
            $out[$key] = array_merge($out[$key], self::getIcon($key));
        }

        return $out;

    }

    public function getIdlePin(Request $request){

        $data = IdleLand::whereRaw("substring_index(substring_index(`view_idle_lands`.`pin`,'-',3),'-',-(1)) = " . $request['bgyNo'])
                        ->where("kind", $request['kind'])
                        ->get();

        return $data;

    }

    public function getIdleReportsAll(){

        $data = IdleLand::get()->toArray();

        return self::createExcel($data, "A", null);

    }

    public function listPerBarangay(Request $request){
        $data = IdleLand::select('PIN', 'area', 'kind')
                        ->whereNotNull('kind')
                        ->whereRaw("substring_index(substring_index(`pin`,'-',3),'-',-(1)) = " . $request['bgyNo'])
                        ->orderBy("PIN", "asc")
                        ->orderBy("kind", "asc")
                        ->get();

        return $data;        
    }

    public function listPerBarangayTagged(Request $request){
        $data = ViewIdleLandsTagged::select('PIN', 'TotalArea as area', 'kind')
                        ->whereNotNull('kind')
                        ->where("bgy" , $request['bgyNo'])
                        ->orderBy("PIN", "asc")
                        ->orderBy("kind", "asc")
                        ->get();

        return $data;        
    }

    public function listPerBarangayInsp(Request $request){
        $data = ViewIdleLandsInsp::select('PIN', 'TotalArea as area', 'kind')
                        ->whereNotNull('kind')
                        ->where("bgy" , $request['bgyNo'])
                        ->orderBy("PIN", "asc")
                        ->orderBy("kind", "asc")
                        ->get();

        return $data;        
    }

    public function breakdownPerBgy(){

        $newData = [];

        $data = IdleLand::select('PIN', 'area', 'kind', 'market_value', 'currentAssessValue')
                        ->get();
        

        foreach ($data as $key => $value) {
            
            $bgy = explode('-', $value['pin'])[2];

            if(isset($newData[$bgy][$value['kind']])){
                $newData[$bgy][$value['kind']]['total_area']                = $newData[$bgy][$value['kind']]['total_kind'] + $value['area'];
                $newData[$bgy][$value['kind']]['total_market_value']        = $newData[$bgy][$value['kind']]['total_market_value'] + $value['market_value'];
                $newData[$bgy][$value['kind']]['total_currentAssessValue']  = $newData[$bgy][$value['kind']]['total_currentAssessValue'] + $value['currentAssessValue'];
            } else{
                $newData[$bgy][$value['kind']]['total_kind']                = $value['area'];
                $newData[$bgy][$value['kind']]['total_market_value']        = $value['market_value'];
                $newData[$bgy][$value['kind']]['total_currentAssessValue']  = $value['currentAssessValue'];
            }

        }

        return self::createExcelBreakdown($newData);

    }

    public function exportPerBrgy(Request $request){

        $data = IdleLand::whereRaw("substring_index(substring_index(`PIN`,'-',3),'-',-(1)) = " . $request['bgyNo'])
                                        ->get();

        return self::createExcel($data, "P", $request['bgyNo']);
        
    }

    public function checkOcular(Request $request){

        $pin = $request['pin'];

        $exists = Storage::disk('sftp')->exists("/OCULAR INSPECTION REPORT/$pin.pdf");

        return $exists;

    }

    public function showOcular(Request $request){

        $pin = $request['pin'];

        $fs = Storage::disk(config('fsconf.cf'))->getDriver();
        $stream = $fs->readStream("/OCULAR INSPECTION REPORT/$pin.pdf");

        return Response::stream(function() use($stream) {
            fpassthru($stream);
        }, 200, [
            "Content-Type" => "application/pdf",
            "Content-disposition" => "attachment; filename=\"" . basename($pin . ".pdf" ) . "\"",
        ]);
    }

    public function checkTag(Request $request){

        $pin = $request['pin'];

        $status = IdleLandsTagging::where('pin', $pin )->exists();

        if($status){
            return 'V';
        } else{
            return 'A';
        }

    }

    public function getTag(Request $request){

        $pin            = $request['pin'];
        $toggle_status  = true;
        $remarks_text   = "";

        $data = IdleLandsTagging::select('remarks')
                                    ->where('pin', $pin )
                                    ->first();

        $remarks_list = IdleLandsTagging::select('remarks')
                                    ->where('remarks', '<>', 'Lot Vacant')
                                    ->pluck('remarks')
                                    ->toArray();


        if($data['remarks'] !== 'Lot Vacant'){
            $toggle_status = false;
            $remarks_text  = $data['remarks'];
        }

        return compact('toggle_status', 'remarks_text', 'remarks_list');

    }

    public function saveTag(Request $request){

        $command = $request['command'];

        $save_data = array(
            'pin'           => $request['pin'],
            'remarks'       => $request['remarks']
        );

        if($command === 'A'){

            $save_data['created_at'] = date('Y-m-d H:i:s');
            $save_data['created_by'] = $request['user'];
            $result                  = IdleLandsTagging::insert($save_data);

        } elseif($command === 'V'){

            $id = IdleLandsTagging::select('id')
                                ->where('pin', $request['pin'])
                                ->first();

            $tag = IdleLandsTagging::find($id['id']);
            $tag->remarks            = $request['remarks'];
            $tag->updated_by         = $request['user'];
            $tag->updated_at         = date('Y-m-d H:i:s');

            $result = $tag->save();

        }

        if($result){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "Saving Failed",
            'status' => 2
        ], 200);

    }

    private function createExcel($data, $param, $bgy){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->getColumnDimension('A')->setWidth(19);
        $worksheet->getColumnDimension('B')->setWidth(15);
        $worksheet->getColumnDimension('C')->setWidth(75);
        $worksheet->getColumnDimension('D')->setWidth(16);
        $worksheet->getColumnDimension('E')->setWidth(12);
        $worksheet->getColumnDimension('F')->setWidth(9);
        $worksheet->getColumnDimension('G')->setWidth(18);

        if($param == "A"){
            $worksheet->setCellValue("A2", "Idle Lands (All Barangays)");
        } else{
            $worksheet->setCellValue("A2", "Idle Lands (Barangay $bgy)");
        }

        // $worksheet->setCellValue("A2", "Idle Lands (All Barangays)");
    
        $worksheet
                ->setCellValue("A3", "As of " . date('F j, Y'))
                ->setCellValue("A5", "PIN")
                ->setCellValue("B5", "ARP Number")
                ->setCellValue("C5", "Owner's Name")
                ->setCellValue("D5", "Market Value")
                ->setCellValue("E5", "Area")
                ->setCellValue("F5", "Kind")
                ->setCellValue("G5", "Assessment Value");


        $startRow = 7;
        $row = $startRow;
        
        foreach ($data as $key => $val) {
            $worksheet
                ->setCellValue("A{$row}", $val["pin"])
                ->setCellValue("B{$row}", $val["arp"])
                ->setCellValue("C{$row}", $val["Owner"])
                ->setCellValue("D{$row}", number_format($val["market_value"], 2))
                ->setCellValue("E{$row}", number_format($val["area"], 2))
                ->setCellValue("F{$row}", $val["kind"])
                ->setCellValue("G{$row}", number_format($val["currentAssessValue"]));

            $row++;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;
    }

    private function createExcelBreakdown($data){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->getColumnDimension('A')->setWidth(20);
        $worksheet->getColumnDimension('B')->setWidth(10);
        $worksheet->getColumnDimension('C')->setWidth(2);
        $worksheet->getColumnDimension('D')->setWidth(20);
        $worksheet->getColumnDimension('E')->setWidth(2);
        $worksheet->getColumnDimension('F')->setWidth(30);
        $worksheet->getColumnDimension('G')->setWidth(2);
        $worksheet->getColumnDimension('H')->setWidth(36);

        $worksheet->setCellValue("A2", "Idle Lands (Breakdown Per Barangays)");
    
        $worksheet
                ->setCellValue("A3", "As of " . date('F j, Y'))
                ->setCellValue("A5", "Barangay")
                ->setCellValue("B5", "Kind")
                ->setCellValue("D5", "Total Area per Kind")
                ->setCellValue("F5", "Total Market Value per Kind")
                ->setCellValue("H5", "Total Current Assess Value per Kind");


        $startRow = 7;
        $row = $startRow;

        $row_bgy_start = 0;
        
        foreach ($data as $key => $val) {

            $worksheet->setCellValue("A{$row}", "Barangay " . $key);
            $row++;
            $row_bgy_start = $row;
            $total = 0;
            $total_mv = 0;
            $total_av = 0;

            foreach ($val as $k => $v) {
                $worksheet->setCellValue("B{$row}", $k);
                $worksheet->setCellValue("D{$row}", number_format($v['total_kind'], 2));
                $worksheet->setCellValue("F{$row}", number_format($v['total_market_value'], 2));
                $worksheet->setCellValue("H{$row}", number_format($v['total_currentAssessValue'], 2));
                $worksheet->getStyle("D{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $worksheet->getStyle("F{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $worksheet->getStyle("H{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $total      = $total + $v['total_kind'];
                $total_mv   = $total_mv + $v['total_market_value'];
                $total_av   = $total_av + $v['total_currentAssessValue'];
                $row++;
            }

            $worksheet->getStyle("D{$row}")
                        ->getBorders()
                        ->getTop()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $worksheet->getStyle("F{$row}")
                        ->getBorders()
                        ->getTop()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $worksheet->getStyle("H{$row}")
                        ->getBorders()
                        ->getTop()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

            $worksheet->setCellValue("D{$row}",  number_format($total, 2));
            $worksheet->setCellValue("F{$row}",  number_format($total_mv, 2));
            $worksheet->setCellValue("H{$row}",  number_format($total_av, 2));
            $worksheet->getStyle("D{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $worksheet->getStyle("F{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $worksheet->getStyle("H{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                
            $row = $row + 2;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

    private function getIdleLands(){

        $idles = [];

        $data = IdleLand::get()->toArray();

        foreach ($data as $value) {

            if($value['area'] > 1000){
                $idles[] = $value;
            }
        }

        return $idles;

    }

    private function getIcon($kind){

        switch ($kind) {
            case 'L-CHAR':
                return array(
                    "icon" => "mdi-charity",
                    "title" => "Charitable"
                );
                break;

            case 'L-COMM':
                return array(
                    "icon" => "mdi-office-building",
                    "title" => "Commercial"
                );
                break;

            case 'L-EDUC':
                return array(
                    "icon" => "mdi-school",
                    "title" => "Education"
                );
                break;

            case 'L-GOVT':
                return array(
                    "icon" => "mdi-bank",
                    "title" => "Government"
                );
                break;
            
            case 'L-INDU':
                return array(
                    "icon" => "mdi-factory",
                    "title" => "Industrial"
                );
                break;

            case 'L-OTHE':
                return array(
                    "icon" => "mdi-abugida-thai",
                    "title" => "Others"
                );
                break;

            case 'L-RELI':
                return array(
                    "icon" => "mdi-church",
                    "title" => "Religion"
                );
                break;

            case 'L-RESI':
                return array(
                    "icon" => "mdi-home-group",
                    "title" => "Residential"
                );
                break;

            case 'L-SPC1':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 1"
                );
                break;

            case 'L-SPC2':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 2"
                );
                break;

            case 'L-SPC3':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 3"
                );
                break;


            case 'L-SPC4':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 4"
                );
                break;

            case 'L-SPC5':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 5"
                );
                break;

            case 'L-SPC6':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "Special 6"
                );
                break;

            case 'L-SPEC':
                return array(
                    "icon" => "mdi-decagram",
                    "title" => "SPEC"
                );
                break;

            default:
                # code...
                break;
        }


    }



}
