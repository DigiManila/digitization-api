<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\PdfToImage\Pdf as PdfToImg;

use App\RecordsCertification;
use App\RecordsCertificationData;

use App\RecordsCertificationLetterNationalArchive;
use App\RecordsCertificationLetterNationalArchivesData;

use App\RecordsCertificationsNoTd;

use App\Profile;

use PDF;
use Storage;

class RecordsCertificationController extends Controller
{

    public function getVerifier(){

        $output = [];

        $format_name = "";

        $profiles = Profile::select('id', 'first_name', 'middle_name', 'last_name')->where('user_type', 'Records')->get();

        foreach ($profiles as $key => $value) {
            $format_name = $value['first_name'] . " " . substr($value['middle_name'], 0, 1) . ". " . $value['last_name'];

            $margin_left = 23;
    
            if(!$value['middle_name']){
                str_replace(".", "", $format_name);
            }

            $output[] = array("text" => $format_name, "value" => $value['id']);
        }

        return $output;
    }

    public function saveData(Request $request){

        $ctr_no = "";

        if(!$request['id']){
            
            $ctr_no = self::getControlNumber($request['type']);

        }

        $user = $request->user['first_name'] . " " . substr($request->user['middle_name'], 0, 1) . ". " . $request->user['last_name'];
        $created = null;

        if($request['type'] == "Property Ownership"){

            if($request['id']){

                $created = RecordsCertification::find($request['id']);

                $created->type = $request['type'];
                $created->owner = $request['details']['owner_name'];
                $created->requestor = $request['details']['requestor'];
                $created->verifier = $request['verifier'];
                $created->purpose = $request['details']['purpose'];
                $created->copies = $request['copies'];
                $created->or = $request['or'];
                $created->or_date = $request['orDate'];
                $created->amount = $request['amount'];

                $created->save();

                $delete_query = RecordsCertificationData::select('id')->where('cert_id', $request['id'])->pluck('id');

                RecordsCertificationData::destroy($delete_query);

                foreach ($request['details']['properties'] as $k => $v) {
                    $rec_data           = new RecordsCertificationData();
                    $rec_data->cert_id  = $request['id'];
                    $rec_data->kind     = $v['kind'];
                    $rec_data->pin      = $v['pin'];
                    $rec_data->lot      = $v['lot'];
                    $rec_data->blk      = $v['blk'];
                    $rec_data->area     = $v['area'];
                    $rec_data->yr_fr    = $v['yr_fr'];
                    $rec_data->yr_to    = $v['yr_to'];
                    $rec_data->mv       = $v['mv'];
                    $rec_data->av       = $v['av'];
                    $rec_data->save();
                }

                $created_id = $request['id'];
                $control_no = $created['ctr_no'];

            } else{
    
                $created = RecordsCertification::create([
                    'ctr_no' => $ctr_no,
                    'type' => $request['type'],
                    'owner' => $request['details']['owner_name'],
                    'requestor' => $request['details']['requestor'],
                    'purpose' => $request['details']['purpose'],
                    'printed_by' => "JOSE BONAPARTE HARON MAMUTUK",
                    'verifier' => $request['verifier'],
                    'user_id' => $request['user']['user_id'],
                    'prepared_by' => $user,
                    'copies' => $request['copies'],
                    'or' => $request['or'],
                    'or_date' => $request['orDate'],
                    'amount' => $request['amount'],
                ]);
    
                foreach ($request['details']['properties'] as $k => $v) {
                    $rec_data           = new RecordsCertificationData();
                    $rec_data->cert_id  = $created['id'];
                    $rec_data->kind     = $v['kind'];
                    $rec_data->pin      = $v['pin'];
                    $rec_data->lot      = $v['lot'];
                    $rec_data->blk      = $v['blk'];
                    $rec_data->area     = $v['area'];
                    $rec_data->yr_fr    = $v['yr_fr'];
                    $rec_data->yr_to    = $v['yr_to'];
                    $rec_data->mv       = $v['mv'];
                    $rec_data->av       = $v['av'];
                    $rec_data->save();
                }
                
                $created_id = $created['id'];
                $control_no = $ctr_no;

            }

        } elseif($request['type'] == "No Property Holdings"){

            if($request['id']){

                $created = RecordsCertification::find($request['id']);

                $created->type = $request['type'];
                $created->owner = $request['details']['owner_name'];
                $created->owner_addr = $request['details']['address'];
                $created->requestor = $request['details']['requestor'];
                $created->requestor_addr = $request['details']['requestor_address'];
                $created->purpose = $request['details']['purpose'];
                $created->verifier = $request['verifier'];
                $created->copies = $request['copies'];
                $created->or = $request['or'];
                $created->or_date = $request['orDate'];
                $created->amount = $request['amount'];

                $created->save();

                $created_id = $request['id'];
                $control_no = $created['ctr_no'];

            } else{

                $created = RecordsCertification::create([
                    'ctr_no' => $ctr_no,
                    'type' => $request['type'],
                    'owner' => $request['details']['owner_name'],
                    'owner_addr' => $request['details']['address'],
                    'requestor' => $request['details']['requestor'],
                    'requestor_addr' => $request['details']['requestor_address'],
                    'purpose' => $request['details']['purpose'],
                    'printed_by' => "JOSE BONAPARTE HARON MAMUTUK",
                    'verifier' => $request['verifier'],
                    'user_id' => $request['user']['user_id'],
                    'prepared_by' => $user,
                    'copies' => $request['copies'],
                    'or' => $request['or'],
                    'or_date' => $request['orDate'],
                    'amount' => $request['amount'],
                ]);

                $created_id = $created['id'];
                $control_no = $ctr_no;

            }

        } elseif($request['type'] == 'No Tax Declaration'){

            if($request['id']){

                $created = RecordsCertificationsNoTd::find($request['id']);

                $created->bgy = $request['details']['bgy'];
                $created->owner = $request['details']['owner_name'];
                $created->owner_addr = $request['details']['address'];
                $created->pin = $request['details']['pin'];
                $created->lot = $request['details']['lot'];
                $created->blk = $request['details']['block'];
                $created->year = $request['details']['impYear'];
                $created->imp_yr = $request['details']['impEffectYear'];
                $created->imp_text = $request['details']['impText'];
                $created->revision =$request['details']['revision'];
                $created->requestor = $request['details']['requestor'];
                $created->verifier = $request['verifier'];
                $created->copies = $request['copies'];
                $created->or_num = $request['or'];
                $created->or_date = $request['orDate'];
                $created->amount = $request['amount'];

                $created->save();

                $created_id = $request['id'];
                $control_no = $created['ctr_no'];

            } else{

                $certifier_1 = "JOSE BONAPARTE HARON MAMUTUK";
                $certifier_2 = "EMILIA MALONG";

                $created = RecordsCertificationsNoTd::create([
                    'ctr_no' => $ctr_no,
                    'type' => $request['type'],
                    'bgy' => $request['details']['bgy'],
                    'owner' => $request['details']['owner_name'],
                    'owner_addr' => $request['details']['address'],
                    'pin' => $request['details']['pin'],
                    'lot' => $request['details']['lot'],
                    'blk' => $request['details']['block'],
                    'year' => $request['details']['impYear'],
                    'imp_yr' => $request['details']['impEffectYear'],
                    'imp_text' => $request['details']['impText'],
                    'revision' => $request['details']['revision'],
                    'requestor' => $request['details']['requestor'],
                    'certifier_1' => $certifier_1,
                    'certifier_2' => $certifier_2,
                    'verifier' =>  $request['verifier'],
                    'prepared_by' => $request['user']['user_id'],
                    'copies' => $request['copies'],
                    'or_num' => $request['or'],
                    'or_date' => $request['orDate'],
                    'amount' => $request['amount'],
                ]);

                $created_id = $created['id'];
                $control_no = $ctr_no;

            }
        } else{

            if($request['id']){

                $created = RecordsCertificationLetterNationalArchive::find($request['id']);

                $created->type = $request['type'];
                $created->requestor = $request['details']['requestor'];
                $created->copies = $request['copies'];
                $created->or = $request['or'];
                $created->or_date = $request['orDate'];
                $created->amount = $request['amount'];

                $created->save();

                $delete_query = RecordsCertificationLetterNationalArchivesData::select('id')->where('letter_id', $request['id'])->pluck('id');

                RecordsCertificationLetterNationalArchivesData::destroy($delete_query);

                foreach ($request['details']['properties'] as $k => $v) {
                    $rec_data                   = new RecordsCertificationLetterNationalArchivesData();
                    $rec_data->letter_id        = $request['id'];
                    $rec_data->owner            = $v['owner_name'];
                    $rec_data->lot_blk          = $v['lotBlock'];
                    $rec_data->classification   = $v['classification'];
                    $rec_data->location         = $v['location'];
                    $rec_data->save();
                }

                $created_id = $request['id'];
                $control_no = $created['ctr_no'];

            } else{
    
                $created = RecordsCertificationLetterNationalArchive::create([
                    'ctr_no' => $ctr_no,
                    'type' => $request['type'],
                    'requestor' => $request['details']['requestor'],
                    'printed_by' => "JOSE BONAPARTE HARON MAMUTUK",
                    'user_id' => $request['user']['user_id'],
                    'prepared_by' => $user,
                    'copies' => $request['copies'],
                    'or' => $request['or'],
                    'or_date' => $request['orDate'],
                    'amount' => $request['amount'],
                ]);
    
                foreach ($request['details']['properties'] as $k => $v) {
                    $rec_data                   = new RecordsCertificationLetterNationalArchivesData();
                    $rec_data->letter_id        = $created['id'];
                    $rec_data->owner            = $v['owner_name'];
                    $rec_data->lot_blk          = $v['lotBlock'];
                    $rec_data->classification   = $v['classification'];
                    $rec_data->location         = $v['location'];
                    $rec_data->save();
                }
                
                $created_id = $created['id'];
                $control_no = $ctr_no;

            }
        }

        return compact('created_id', 'control_no');
        
    }

    public function updateLog(Request $request){

        $cert = RecordsCertification::find($request->id);
        $cert->printed = 1;
        $cert->save();

        return "OK";
    }

    public function getPreview(Request $request){

        if($request['type'] == 'No Tax Declaration'){
            return self::previewNoTaxDeclaration($request);
        } elseif($request['type'] == 'Letter to National Archives'){
            return self::previewLetterNationalArchives($request);
        }

        $user       = $request->user['first_name'] . " " . substr($request->user['middle_name'], 0, 1) . ". " . $request->user['last_name'];
        $verifier   = self::getVerifierName($request->verifier);
        $margin_left = 23;

        if(!$request->user['middle_name']){
            str_replace(".", "", $user);
        }

        PDF::setPrintHeader(false);
        PDF::AddPage('P', 'A4');
        PDF::SetAutoPageBreak(false, 0);

        // Create PDF

        self::backgroundPicture();
        self::createHeader($margin_left, 1);
        self::createBody($request->type, $request->details, $margin_left);

        $pos = 210;

        self::createFooter($user, $pos, $request->cert_no, $margin_left, $verifier, $request->or, $request->orDate, $request->amount);

        $cert_dir = 'records_cert';

        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //
        // $cert_dir = 'records_cert_test';
        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //


        // Send PDF

        $date = date("Y-m-d");

        $path_db = RecordsCertification::select('file_path')->where('id', $request->id)->first();

        $path_temp = "";

        if($path_db['file_path']){
            $fn =  basename($path_db['file_path']);
            $path_temp = dirname($path_db['file_path']);
        } else{
            $fn = time() . ".pdf";
        }


        $path = storage_path("app/temp/rec_cert/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        $file = Storage::get("temp/rec_cert/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        if($path_db['file_path']){
            Storage::disk('svnas')->putFileAs($path_temp , $temp_file_path, $fn);
        } else{
            Storage::disk('svnas')->putFileAs("/$cert_dir/$date" , $temp_file_path, $fn);
        }
        

        Storage::delete("temp/rec_cert/$fn");

        if(!$path_db['file_path']){
            $cert = RecordsCertification::find($request->id);
            $cert->file_path = "/$cert_dir/$date/$fn" ;
            $cert->save();
        }

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);

    }

    private function backgroundPicture(){
        $logo_doa = public_path('img') . DIRECTORY_SEPARATOR . 'DOA_tr.png';
        PDF::Image($logo_doa, 55, 80, 100, 100 , 'png', "", "C", 0, 10);
    }

    private function createHeader($margin_left, $with_date){

        $font_size = 11;

        $row = 20;

        PDF::setFontStretching(90);

        PDF::SetFont('helveticaB', '', $font_size, '', false);

        PDF::SetTextColor(54,69,79); 
        PDF::MultiCell(210, $margin_left, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, $row, true);

        $row = $row + 5;

        PDF::MultiCell(210, $margin_left, 'City of Manila', 0, 'C', 0, 0, 0, $row, true);

        $row = $row + 5;

        PDF::SetTextColor(255, 0, 0);
        PDF::MultiCell(210, $margin_left, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, $row, true);

        $logo_row = 17;

        $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
        PDF::Image($logo1, $margin_left, $logo_row, 23, 22, '', '', '', false, 300, '', false, false, 0);

        $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
        PDF::Image($logo2, 167, $logo_row, 24, 23, '', '', '', false, 300, '', false, false, 0);

        $logo3 = public_path('img') . DIRECTORY_SEPARATOR . 'bagong_pilipinas.png';
        PDF::Image($logo3, 167-34, $logo_row, 24, 23, '', '', '', false, 300, '', false, false, 0);

        $line_pos = 45;
        $style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        PDF::Line($margin_left, $line_pos, 191, $line_pos, $style);

        if($with_date){
            PDF::setFontStretching(95);
            $row = 50;
            PDF::SetTextColor(0, 0, 0);
            PDF::SetFont('helveticaB', '', 11, '', false);
            PDF::MultiCell(50, '',  date('F j, Y'), 0, 'R', 0, 0, 142, $row, true);
            PDF::setFontStretching(85);
        }
    }

    private function createFooter($user, $pos, $ctr_no, $margin_left, $verifier, $or, $or_date, $amount){

        $start_pos = $pos;

        $records = "JOSE BONAPARTE HARON MAMUTUK";

        PDF::SetFont('helvetica', '', 10, '', false);
        PDF::MultiCell('', '', "By Authority of the City Assessor:", 0, '', 0, 0, 110, $start_pos, true);
        
        $start_pos = $start_pos + 15;
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::MultiCell('', '', $records, 0, 'C', 0, 0, 112, $start_pos, true);

        PDF::SetFont('helvetica', '', 10, '', false);
        $start_pos = $start_pos + 5;
        PDF::MultiCell('', '', "Chief of Records Management Division", 0, 'C', 0, 0, 113, $start_pos, true);

        $start_pos = $start_pos + 5;
        PDF::MultiCell('', '', "PREPARED BY:", 0, '', 0, 0, $margin_left, $start_pos, true);

        $start_pos = $start_pos + 10;
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::MultiCell('', '', $user, 0, '', 0, 0, $margin_left, $start_pos, true);

        $start_pos = $start_pos + 15;
        PDF::SetFont('helvetica', '', 10, '', false);
        PDF::MultiCell('', '', "VERIFIED BY:", 0, '', 0, 0, $margin_left, $start_pos, true);

        $start_pos = $start_pos + 10;
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::MultiCell('', '', $verifier, 0, '', 0, 0, $margin_left, $start_pos, true);

        $laoo = "MA.TERESA M. ATIENZA";
        $ver1 = "ROMMEL C. TABUCOL";

        if($verifier == $laoo){
            $start_pos = $start_pos + 5;
            PDF::SetFont('helvetica', '', 10, '', false);
            PDF::MultiCell(47, '', "LAOO III", 0, 'C', 0, 0, $margin_left, $start_pos, true);
            $start_pos = $start_pos + 5;
        } else if($verifier == $ver1){
            $start_pos = $start_pos + 5;
            PDF::SetFont('helvetica', '', 10, '', false);
            PDF::MultiCell(43, '', "Administrative Officer I", 0, 'C', 0, 0, $margin_left, $start_pos, true);
            $start_pos = $start_pos + 5;
        } else{
            $start_pos = $start_pos + 10;
        }

        PDF::SetFont('helveticaB', '', 7, '', false);
        PDF::MultiCell(50, '',  "CTR No: $ctr_no", 0, 'R', 0, 0, 146, 285, true);
        PDF::MultiCell(50, '',  "OR No: $or", 0, 'L', 0, 0, 23, 285, true);

        PDF::MultiCell(50, '',  "Amount: $amount", 0, 'L', 0, 0, 60, 285, true);
        PDF::MultiCell(50, '',  "OR Date: $or_date", 0, 'L', 0, 0, 100, 285, true);

    }

    private function createBody($title, $data, $margin_left){
        
        $start = 65;
        $center = 60;
        
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::setFontStretching(100);

        $title = strtoupper($title);

        if($title == "PROPERTY OWNERSHIP"){
            PDF::MultiCell(210, '',  "CERTIFICATION OF PROPERTY HOLDINGS", 0, 'C', 0, 0, 0, $start, true);
            $start = 80;
        } else{
            PDF::MultiCell(210, '',  "CERTIFICATION OF NO PROPERTY HOLDINGS", 0, 'C', 0, 0, 0, $start, true);
            $start = 90;
        }

        PDF::SetFont('helvetica', '', 11, '', false);
        PDF::MultiCell(100, '',  "To Whom it May Concern;", 0, '', 0, 0, $margin_left, $start, true);

        if($title == "PROPERTY OWNERSHIP"){
            self::propertyOwnershipBody($data, $margin_left);
        } else{
            self::noPropertyBody($data, $margin_left);
        }

    }

    private function propertyOwnershipBody($data, $margin_left){
        $owner_name = $data['owner_name'];
        $requestor = $data['requestor'];
        $purpose = $data['purpose'];

        PDF::SetFont('helvetica', '', 12, '', false);

        $start = 90;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">This is to certify that as per record of this Office, hereunder are the real property unit/s of <b>$owner_name</b> derived from the existing hard copy of tax declarations under different revision periods to wit:</p>");

        PDF::SetFont('helveticaB', '', 8.5, '', false);
        $start = 115;
        PDF::SetXY($margin_left, $start);
        $tbl = self::getTable($data['properties']);
        PDF::writeHTML($tbl, true, false, false, false, '');

        PDF::SetFont('helvetica', '', 12, '', false);
        $start = 183;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">This certification is issued upon the request of $requestor for whatever legal purpose this may serve and it is understood that this tax declaration is for $purpose purposes only.</p>");
    }

    private function noPropertyBody($data, $margin_left){

        $owner_name = $data['owner_name'];
        $address = $data['address'];
        $requestor = $data['requestor'];
        $requestor_address = $data['requestor_address'];
        $purpose = $data['purpose'];

        PDF::SetFont('helvetica', '', 12, '', false);

        $start = 105;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; line-height: 2; text-indent: 50px;\">This is to certify that as per record of this Office, the name of <b>$owner_name</b> with address at $address has no real properties (Land and Improvement) found in the computer base of real property units in the Department of Assessment, Manila City Hall, Manila.</p>");

        $start = 140;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; line-height: 2; text-indent: 50px;\">This certification is issued upon the request of $requestor with address $requestor_address.</p>");

        $start = 160;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; line-height: 2; text-indent: 50px;\">This certification is being issued for $purpose purpose.</p>");
    }

    private function getTable($data){

        $html = <<<EOD
        <table border="0.5" cellpadding="4">
            <tr>
                <td style="width: 55px">KIND OF<br>PROPERTY</td>
                <td style="width: 88px;text-align:center;">PIN/TD</td>
                <td style="width: 45px;text-align:center;">LOT<br>NO.</td>
                <td style="width: 45px;text-align:center;">BLOCK<br>NO.</td>
                <td style="width: 57px;text-align:center;">AREA<br>(Sq. m.)</td>
                <td style="width: 51px;text-align:center;">YEAR</td>
                <td style="width: 70px;text-align:center;">MARKET VALUE</td>
                <td style="width: 70px;text-align:center;">ASSESSED VALUE</td>
            </tr>


        EOD;

        foreach ($data as $data => $value) {
            $html = $html . "<tr>";
            
                $html = $html . "<td style=\"text-align: center;\">" . $value['kind']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['pin']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['lot']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['blk']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['area']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['yr_fr'] . "-". $value['yr_to'] . "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['mv']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['av']. "</td>";

            $html = $html . "</tr>";
                
        }

        $html = $html . "</table>";

        return $html;
    }

    private function getTestData(){

        // return array(
        //     array(
        //         'kind' => "IMP.",
        //         'pin' => "117-14-801-001-042-B0001",
        //         'lot' => "8, 12-15",
        //         'blk' => "7",
        //         'area' => "22.22",
        //         'yr_fr' => "1996",
        //         'yr_to' => "2004",
        //         'mv' => "399,000",
        //         'av' => "39,900",
        //     ),
        // );

        
        return array(
            array(
                'kind' => "IMP.",
                'pin' => "117-14-801-001-042-B0001",
                'lot' => "8, 12-15",
                'blk' => "7",
                'area' => "22.22",
                'yr_fr' => "1996",
                'yr_to' => "2004",
                'mv' => "399,000",
                'av' => "39,900",
            ),
            array(
                'kind' => "IMP.",
                'pin' => "117-14-801-001-042-B0001",
                'lot' => "8, 12-15",
                'blk' => "7",
                'area' => "22.22",
                'yr_fr' => "1996",
                'yr_to' => "2004",
                'mv' => "399,000",
                'av' => "39,900",
            ),
            array(
                'kind' => "IMP.",
                'pin' => "117-14-801-001-042-B0001",
                'lot' => "8, 12-15",
                'blk' => "7",
                'area' => "22.22",
                'yr_fr' => "1996",
                'yr_to' => "2004",
                'mv' => "399,000",
                'av' => "39,900",
            ),
            array(
                'kind' => "IMP.",
                'pin' => "117-14-801-001-042-B0001",
                'lot' => "8, 12-15",
                'blk' => "7",
                'area' => "22.22",
                'yr_fr' => "1996",
                'yr_to' => "2004",
                'mv' => "399,000",
                'av' => "39,900",
            ),
            array(
                'kind' => "IMP.",
                'pin' => "117-14-801-001-042-B0001",
                'lot' => "8, 12-15",
                'blk' => "7",
                'area' => "22.22",
                'yr_fr' => "1996",
                'yr_to' => "2004",
                'mv' => "399,000",
                'av' => "39,900",
            ),
        );
    }

    private function getControlNumber($type){

        $year = date('y');
        $query_string = "N$year";

        if($type == "Property Ownership"){
            $query_string = "P$year";
        } elseif($type == "No Tax Declaration"){
            $query_string = "T$year";
        } elseif($type == "Letter to National Archives"){
            $query_string = "L$year";
        }

        if($type == "No Tax Declaration"){
            $data = RecordsCertificationsNoTd::select("ctr_no")
                                            ->where('ctr_no', 'like', "$query_string%")
                                            ->orderBy('ctr_no', 'desc')
                                            ->first();
        } elseif ($type == "Letter to National Archives") {
            $data = RecordsCertificationLetterNationalArchive::select("ctr_no")
                                            ->where('ctr_no', 'like', "$query_string%")
                                            ->orderBy('ctr_no', 'desc')
                                            ->first();
        } else{
            $data = RecordsCertification::select("ctr_no")
                                            ->where('ctr_no', 'like', "$query_string%")
                                            ->orderBy('ctr_no', 'desc')
                                            ->first();
        }

        if($data){

            $last_no = (int) substr($data['ctr_no'], 3); 

            return $query_string . sprintf('%05d', ($last_no + 1));

        } else{

            return $query_string . "00001";
        }

    }

    private function getVerifierName($id){

        $data = Profile::select('first_name', 'middle_name', 'last_name')->where('id', $id)->first();

        $user = $data['first_name'] . " " . substr($data['middle_name'], 0, 1) . ". " . $data['last_name'];

        if(!$data['middle_name']){
            str_replace(".", "", $user);
        }

        return strtoupper($user);
    }

    private function previewNoTaxDeclaration($request){
    
        $impLandOrBoth = $request['details']['impOrLandBoth'];
        $data = RecordsCertificationsNoTd::find($request->id);
        $margin_left = 23;

        PDF::setPrintHeader(false);
        PDF::AddPage('P', 'A4');
        PDF::SetAutoPageBreak(false, 0);

        self::backgroundPicture();
        self::createHeader($margin_left, 0);
        self::createNoTdBody($impLandOrBoth, $data, $margin_left);

        $pos = 190;
        self::createNoTdFooter($data, $pos, $margin_left);

        $cert_dir = 'records_cert';

        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //
        // $cert_dir = 'records_cert_test';
        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //

        // Send PDF

        $date = date("Y-m-d");

        $path_db = $data['file_path'];

        $path_temp = "";

        if($path_db){
            $fn =  basename($path_db);
            $path_temp = dirname($path_db);
        } else{
            $fn = time() . ".pdf";
        }


        $path = storage_path("app/temp/rec_cert/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        $file = Storage::get("temp/rec_cert/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        if($path_db){
            Storage::disk('svnas')->putFileAs($path_temp , $temp_file_path, $fn);
        } else{
            Storage::disk('svnas')->putFileAs("/$cert_dir/$date" , $temp_file_path, $fn);
        }
        

        Storage::delete("temp/rec_cert/$fn");

        if(!$path_db){
            $cert = RecordsCertificationsNoTd::find($request->id);
            $cert->file_path = "/$cert_dir/$date/$fn" ;
            $cert->save();
        }

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);


    }

    private function createNoTdBody($impLandOrBoth, $data, $margin_left){
        
        $start = 55;
        $center = 60;

        PDF::SetTextColor(0, 0, 0);
        PDF::SetFont('timesB', '', 20, '', false);
        PDF::setFontStretching(100);
        PDF::MultiCell(210, '',  "CERTIFICATION", 0, 'C', 0, 0, 0, $start, true);
        $start = $start + 8;
        PDF::SetFont('times', '', 10, '', false);
        PDF::MultiCell(210, '',  strtoupper($data['type']), 0, 'C', 0, 0, 0, $start, true);
        $start = $start + 20;
        
        PDF::SetFont('helvetica', '', 12, '', false);

        $html = "<p style=\"text-align:justify; text-indent: 50px;\">
            This is to certify that, as per Control Book of Brgy. " . $data['bgy'] ." " . $data['revision'] . " and record(s) of this office, the land property in the name of <b>" 
            . $data['owner'] . "</b> located at <b>" . $data['owner_addr'] . "</b> under PIN <b>" . $data['pin'] . "</b> ";
            
        if($data['lot'] && $data['blk']){
            $html = $html . "with Lot No." . $data['lot'] . " and Block No." . $data['blk'] . ", ";
        } elseif($data['lot']){
            $html = $html . "with Lot No." . $data['lot'] . ", ";
        } elseif($data['blk']){
            $html = $html . "with Block No." . $data['blk'] . ", ";
        }

        $html = $html . "has no declared tax declaration for " .$impLandOrBoth.  " for the year " . $data['year'] . ".</p>";

        PDF::writeHTMLCell(172, '', $margin_left, $start, $html);

        $start = $start + 38;

        $html = "";

        if($data['imp_yr'] && $data['imp_text']){
            $html = "<p style=\"text-align:justify; text-indent: 50px;\">
                    However, the Tax Declaration of the improvement for the same property took effect in the year " 
                    . $data['imp_yr'] . " under the name of " . $data['imp_text'] . ".</p>";
                    
            PDF::writeHTMLCell(172, '', $margin_left, $start, $html);

            $start = $start + 20;
        }

        $html = "<p style=\"text-align:justify; text-indent: 50px;\">
                This certification is being issued upon the request of <b>" 
                . $data['requestor'] . "</b> for whatever legal purpose it may serve them best.</p>";

        PDF::writeHTMLCell(172, '', $margin_left, $start, $html);

        $start = $start + 24;

        $html = "<p style=\"text-align:justify; text-indent: 50px;\">
                Issued this " . date('j') . "<sup>" . date('S') . "</sup> day of " . date('F') . " " . date('Y') . " at the city of Manila.</p>";

        PDF::writeHTMLCell(172, '', $margin_left, $start, $html);

    }

    private function createNoTdFooter($data, $pos, $margin_left){

        $start_pos = $pos;

	$prepared = Profile::select('first_name', 'middle_name', 'last_name', 'position')
			->where('user_id', $data['prepared_by'])->first();

	$prepared_middle = $prepared['middle_name'] ? substr($prepared['middle_name'], 0, 1) . "." : "";
	$prepared_name	 = $prepared['first_name'] .  " " . $prepared_middle . " " . $prepared['last_name']; 

        PDF::SetFont('helvetica', '', 10, '', false);
        PDF::MultiCell('', '', "By Authority of the City Assessor:", 0, '', 0, 0, 110, $start_pos, true);
        
        $start_pos = $start_pos + 15;
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::MultiCell('', '', $data['certifier_1'], 0, 'C', 0, 0, 112, $start_pos, true);

        PDF::SetFont('helvetica', '', 9, '', false);
        $start_pos = $start_pos + 5;
        PDF::MultiCell('', '', "LAOO V", 0, 'C', 0, 0, 113, $start_pos, true);
	PDF::MultiCell('', '', "Prepared by:", 0, '', 0, 0, $margin_left, $start_pos, true);
        $start_pos = $start_pos + 5;
        PDF::MultiCell('', '', "Chief, Assessment Records Management Division", 0, 'C', 0, 0, 113, $start_pos, true);

        $start_pos = $start_pos + 15;
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::MultiCell('', '', $data['certifier_2'], 0, 'C', 0, 0, 112, $start_pos, true);
	PDF::Multicell(90, '', $prepared_name, 0, 'C', 0, 0, $margin_left, $start_pos, true);

        PDF::SetFont('helvetica', '', 9, '', false);
        $start_pos = $start_pos + 5;
        PDF::MultiCell('', '', "LAOO V", 0, 'C', 0, 0, 113, $start_pos, true);
	PDF::MultiCell(90, '', $prepared['position'], 0, 'C', 0, 0, $margin_left, $start_pos, true);
        $start_pos = $start_pos + 5;
        PDF::MultiCell('', '', "Chief, Data Division", 0, 'C', 0, 0, 113, $start_pos, true);


        // $start_pos = $start_pos + 15;
        PDF::SetFont('helvetica', '', 9, '', false);
        PDF::MultiCell('', '', "Verified by:", 0, '', 0, 0, $margin_left, $start_pos, true);



        $verifier = Profile::select('first_name', 'middle_name', 'last_name', 'position')
                                ->where('id', $data['verifier'])->first();

        $verifier_middle = $verifier['middle_name'] ? substr($verifier['middle_name'], 0, 1) . "."  : "";
        $verifier_name = $verifier['first_name'] . " " . $verifier_middle  . " " . $verifier['last_name'];

        $start_pos = $start_pos + 15;
        PDF::SetFont('helveticaB', '', 11, '', false);
        PDF::MultiCell(90, '', $verifier_name , 0, 'C', 0, 0, $margin_left, $start_pos, true);

        // PDF::SetFont('helvetica', '', 9, '', false);
        // $start_pos = $start_pos + 5;
        // PDF::MultiCell(90, '', $verifier['position'], 0, 'C', 0, 0, $margin_left, $start_pos, true);

        if($data['verifier'] == 94){
            PDF::SetFont('helvetica', '', 9, '', false);
            $start_pos = $start_pos + 5;
            PDF::MultiCell(90, '', "LAOO III", 0, 'C', 0, 0, $margin_left, $start_pos, true);
            $start_pos = $start_pos + 5;
            PDF::MultiCell(90, '', "Actg. Asst. Chief, Assessment Records Management Division", 0, 'C', 0, 0, $margin_left, $start_pos, true);
    
        } else{
            PDF::SetFont('helvetica', '', 9, '', false);
            $start_pos = $start_pos + 5;
            PDF::MultiCell(90, '', $verifier['position'], 0, 'C', 0, 0, $margin_left, $start_pos, true);

        }

        PDF::SetFont('helveticaB', '', 7, '', false);
        PDF::MultiCell(50, '',  "CTR No: " . $data['ctr_no'], 0, 'R', 0, 0, 146, 285, true);
        PDF::MultiCell(50, '',  "OR No: " . $data['or_num'], 0, 'L', 0, 0, 23, 285, true);

        PDF::MultiCell(50, '',  "Amount: " . $data['amount'], 0, 'L', 0, 0, 60, 285, true);
        PDF::MultiCell(50, '',  "OR Date: " . $data['or_date'], 0, 'L', 0, 0, 100, 285, true);

    }

    private function previewLetterNationalArchives($request){

        $data = RecordsCertificationLetterNationalArchive::with('letter_data')->find($request->id);
        $margin_left = 23;

        PDF::setPrintHeader(false);
        PDF::AddPage('P', 'A4');
        PDF::SetAutoPageBreak(false, 0);

        self::backgroundPicture();
        self::createHeader($margin_left, 1);
        self::createLetterNationalArchiveBody($data, $margin_left);

        $pos = 235;
        self::createLetterNationalArchiveFooter($data, $pos, $margin_left);

        $cert_dir = 'records_cert';

        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //
        // $cert_dir = 'records_cert_test';
        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //

        // Send PDF

        $date = date("Y-m-d");

        $path_db = $data['file_path'];

        $path_temp = "";

        if($path_db){
            $fn =  basename($path_db);
            $path_temp = dirname($path_db);
        } else{
            $fn = time() . ".pdf";
        }


        $path = storage_path("app/temp/rec_cert/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        $file = Storage::get("temp/rec_cert/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        if($path_db){
            Storage::disk('svnas')->putFileAs($path_temp , $temp_file_path, $fn);
        } else{
            Storage::disk('svnas')->putFileAs("/$cert_dir/$date" , $temp_file_path, $fn);
        }
        

        Storage::delete("temp/rec_cert/$fn");

        if(!$path_db){
            $cert = RecordsCertificationLetterNationalArchive::find($request->id);
            $cert->file_path = "/$cert_dir/$date/$fn" ;
            $cert->save();
        }

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);


    }

    private function createLetterNationalArchiveBody($data, $margin_left){

        $requestor = $data['requestor'];

        PDF::setFontStretching(100);

        $start = 65;

        PDF::SetFont('helveticaB', '', 12, '', false);
        PDF::MultiCell(210, '',  "National Archives of the Philippines (NAP)", 0, 'L', 0, 0, $margin_left, $start, true);
        $start = $start + 5;
        PDF::MultiCell(210, '',  "Cristobal St., Paco,", 0, 'L', 0, 0, $margin_left, $start, true);
        $start = $start + 5;
        PDF::MultiCell(210, '',  "Manila", 0, 'L', 0, 0, $margin_left, $start, true);
        
        $start = $start + 15;
        PDF::MultiCell(210, '',  "Sir / Madam:", 0, 'L', 0, 0, $margin_left, $start, true);

        PDF::SetFont('helvetica', '', 12, '', false);
        $start = $start + 15;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">This pertains to the request of <b>$requestor</b> to secure certified copies of Tax Declaration for the period 1960-1978 (at the time of death) for the following real property units on LAND AND BUILDING, to wit:</p>");


        PDF::SetFont('helveticaB', '', 8.5, '', false);
        $start = $start + 20;
        PDF::SetXY($margin_left, $start);
        $tbl = self::getLetterNationalArchiveTable($data['letter_data']);
        PDF::writeHTML($tbl, true, false, false, false, '');

        PDF::SetFont('helvetica', '', 12, '', false);
        $start = $start + 70;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">Further certify that herein request by <b>$requestor</b> authorized by the above-mentioned owner.</p>");

        $start = $start + 20;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">It is understood that this request is for taxation purpose only.</p>");
        $start = $start + 10;
        PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">Thank You.</p>");
        // $tbl = self::getTable($data['properties']);

        // PDF::SetFont('helvetica', '', 12, '', false);
        // $start = 183;
        // PDF::writeHTMLCell(172, '', $margin_left, $start, "<p style=\"text-align:justify; text-indent: 50px;\">This certification is issued upon the request of $requestor for whatever legal purpose this may serve and it is understood that this tax declaration is for $purpose purposes only.</p>");
    }

    private function getLetterNationalArchiveTable($data){
        $html = <<<EOD
        <table border="0.5" cellpadding="4">
            <tr>
                <td style="width: 160px;text-align:center;">NAME</td>
                <td style="width: 75px;text-align:center;">LOT & BLOCK</td>
                <td style="width: 80px;text-align:center;">CLASSIFICATION</td>
                <td style="width: 170px;text-align:center;">LOCATION</td>
            </tr>


        EOD;

        foreach ($data as $data => $value) {
            $html = $html . "<tr>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['owner']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['lot_blk']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['classification']. "</td>";
                $html = $html . "<td style=\"text-align: center;\">" . $value['location']. "</td>";
            $html = $html . "</tr>";
                
        }

        $html = $html . "</table>";

        return $html;
    }

    private function createLetterNationalArchiveFooter($data, $pos, $margin_left){

        $start_pos = $pos;

        PDF::SetFont('helvetica', '', 11, '', false);
        PDF::MultiCell('', '', "Very truly yours,", 0, '', 0, 0, 136, $start_pos, true);
        
        $start_pos = $start_pos + 20;
        PDF::SetFont('helveticaB', '', 12, '', false);
        PDF::MultiCell('', '', $data['printed_by'], 0, 'C', 0, 0, 104, $start_pos, true);
        $start_pos = $start_pos + 5;
        PDF::SetFont('helveticaB', '', 10, '', false);
        PDF::MultiCell('', '', "Chief, Assessment Records Management Division", 0, 'C', 0, 0, 105, $start_pos, true);

        // PDF::SetFont('helvetica', '', 9, '', false);
        // $start_pos = $start_pos + 5;
        // PDF::MultiCell('', '', "LAOO V", 0, 'C', 0, 0, 113, $start_pos, true);
        // $start_pos = $start_pos + 5;
        // PDF::MultiCell('', '', "Chief, Assessment Records Management Division", 0, 'C', 0, 0, 113, $start_pos, true);

        // $start_pos = $start_pos + 15;
        // PDF::SetFont('helveticaB', '', 11, '', false);
        // PDF::MultiCell('', '', $data['certifier_2'], 0, 'C', 0, 0, 112, $start_pos, true);

        // PDF::SetFont('helvetica', '', 9, '', false);
        // $start_pos = $start_pos + 5;
        // PDF::MultiCell('', '', "LAOO V", 0, 'C', 0, 0, 113, $start_pos, true);
        // $start_pos = $start_pos + 5;
        // PDF::MultiCell('', '', "Chief, Data Division", 0, 'C', 0, 0, 113, $start_pos, true);



        PDF::SetFont('helveticaB', '', 7, '', false);
        PDF::MultiCell(50, '',  "CTR No: " . $data['ctr_no'], 0, 'R', 0, 0, 146, 285, true);
        PDF::MultiCell(50, '',  "OR No: " . $data['or'], 0, 'L', 0, 0, 23, 285, true);

        PDF::MultiCell(50, '',  "Amount: " . $data['amount'], 0, 'L', 0, 0, 60, 285, true);
        PDF::MultiCell(50, '',  "OR Date: " . $data['or_date'], 0, 'L', 0, 0, 100, 285, true);



    }

}
