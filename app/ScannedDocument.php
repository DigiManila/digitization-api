<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScannedDocument extends Model
{
    protected $guarded = [];
}
