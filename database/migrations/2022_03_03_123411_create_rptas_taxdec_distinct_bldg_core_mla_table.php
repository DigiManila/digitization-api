<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRptasTaxdecDistinctBldgCoreMlaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rptas_taxdec_distinct_bldg_core_mla', function (Blueprint $table) {
            $table->char('ARP',20);
            $table->string('BldgType',10);
            $table->string('BldgTypeDesc',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxdec_distinct_bldg_core_mla');
    }
}
