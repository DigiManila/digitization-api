<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDataReconsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_recons', function (Blueprint $table) {
            $table->string('effectivity')->nullable()->after('PIN');
            $table->string('struct_type')->nullable()->after('block');
            $table->string('prev_td_no')->nullable()->after('prev_arp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_recons', function (Blueprint $table) {
            //
        });
    }
}
