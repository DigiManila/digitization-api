<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRptasFaasMastextnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rptas_faas_mastextn', function (Blueprint $table) {
            $table->string('RecId');
            $table->char('Arp',20);
            $table->char('Pin',30)->nullable();
            $table->char('Prev_Arp',20);
            $table->char('Prev_Pin',30)->nullable();
            $table->char('Prev_Owner',100)->nullable();
            $table->decimal('Prev_Av', 20,2)->nullable();
            $table->integer('Prev_ARPageNo')->nullable();
            $table->string('InsertedBy',50)->nullable();
            $table->date('InsertedDt')->nullable();
            $table->string('UpdatedBy',50)->nullable();
            $table->date('UpdatedDt')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faas_mastextn');
    }
}
