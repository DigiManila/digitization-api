<?php

namespace App\Http\Controllers\Api;

use App\RptasTaxdecMastMla;
use App\RptasTaxdecAssmnt;
use App\RptasTaxdecMastExtn;
use App\RptasOwnerextnConcat;
use App\MapPolygon;

use App\ActiveAssessmentRoll;
use App\ActiveTaxdecAssmnt;
use App\ActiveLandAssessmentRoll;
use App\ActiveBuildingAssessmentRoll;
use App\ActiveMachineryAssessmentRoll;

use App\CancelledAssessmentRoll;
use App\CancelledBarangayPin;
use App\CancelledTaxdecAssmnt;

use App\RecordsDailyTransaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
class RPTASAssessmentController extends Controller
{
    

    public function postRptasActiveOrCancelled(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);
        

        if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

    

        //active table search
        $active = 0;

        $queryActiveOrCancelled = ActiveAssessmentRoll::select('ARP' )
                                ->where(function($query) use ($search) {
                                    $query->where('pin', "like", "%".$search."%");
                                    $query->orWhere('arp', "like", "%".$search."%");
                                    $query->orWhere('Owner', "like", "%".$search."%");
                                    $query->orWhere('TctNo', "like", "%".$search."%");
                                    $query->orWhere('CCTNO', "like", "%".$search."%");
                                })
                            
                            ->first();
        
        //cancelled table search
        if ($queryActiveOrCancelled === null) {
        
            $active = 1;

            $queryActiveOrCancelled = CancelledAssessmentRoll::select('ARP' )
                                ->where(function($query) use ($search) {
                                    $query->where('pin', "like", "%".$search."%");
                                    $query->orWhere('arp', "like", "%".$search."%");
                                    $query->orWhere('Owner', "like", "%".$search."%");
                                    $query->orWhere('TctNo', "like", "%".$search."%");
                                    $query->orWhere('CCTNO', "like", "%".$search."%");
                                })
                                ->first();
        }

     
        if ($active == 0){
            
            return $this->rptasActiveAssessmentRoll($queryActiveOrCancelled['ARP']);
        }
        else {
            
            return $this->rptasCancelledAssessmentRoll($queryActiveOrCancelled['ARP']);
        }
                        
    }


    private function rptasActiveAssessmentRoll($arp){

        $data = [];

        //searching the active assessment roll

        $queryMasterTaxdec = ActiveAssessmentRoll::select('UpdateCode','OwnerNo','ARP','PIN','Owner','OwnerAddress',
                                    'Location','BldgLocation','Barangay','Zone','MuniDistName',
                                    'SurveyNo','LotNo', 'BlkNo',  'kind', 'BldgStorey', 'BldgTypeDesc',
                                    'StrucType','SubClass',  'Taxability',
                                    'TctNo','TctDate','CCTNO','CCTDate','PREV_TCT_LAND','PREV_TCTDATE_LAND','PREV_TCT_BLDG',
                                    'PREV_TCTDATE_BLDG','PREV_CCT_BLDG_ADDL', 'SurveyNo',
                                    'North', 'South', 'East','West',
                                    'Effectivity', 'AppraisedBy', 'AppraisedDt', 'Assessor', 'Memoranda','UpdateDesc',
                                    'insertedDt','updatedDt'  )
                    ->where('ARP', $arp)
                    ->first();
    
        

        
        $queryEtAl = RptasOwnerextnConcat::select('AcctNameConcat')
                    ->where('acctNo', $queryMasterTaxdec['OwnerNo'])
                    ->first();

            if ($queryEtAl == null){
                $queryEtAl = "None";
            }
        
        $assmntTaxdec = [];
        
        $queryAssmntTaxdec = ActiveTaxdecAssmnt::select('AUDesc', 'area', 'mv','al', 'av', 'taxability')
                    ->where('arp', $queryMasterTaxdec['ARP'])
                    ->get()
                    ->toArray();

            foreach($queryAssmntTaxdec as $key => $value){

                $assmntTaxdec[] = array(
            
                'actualUse'             => $value['AUDesc'],
                'area'                  => number_format($value['area'],2,".",","). ".sqm",
                'assessmentLevel'       => number_format($value['al']),
                'marketValue'           => "₱ " . number_format($value['mv'],2,".",","),
                'assessedValue'         => "₱ " .number_format($value['av'],2,".",","),
                'taxability'            => $value['taxability']
                );

            }
        

        $queryAssmntTaxdecPrevious = RptasTaxdecMastExtn::select('Prev_Arp', 'Prev_Owner', 'Prev_Av','Prev_Pin','InsertedDt')
                    ->where('arp', $queryMasterTaxdec['ARP'])
                    ->first();
        
            if ($queryAssmntTaxdecPrevious == null){
                $queryAssmntTaxdecPrevious['Prev_Arp'] = "None";
                $queryAssmntTaxdecPrevious['Prev_Owner'] = "None";
                $queryAssmntTaxdecPrevious['Prev_Av'] = "₱ ". 0.00;
                $queryAssmntTaxdecPrevious['Prev_Pin'] = "None";
            }
            else{
                $queryAssmntTaxdecPrevious['Prev_Av'] = "₱ " . number_format($queryAssmntTaxdecPrevious['Prev_Av'],2,".",",");
            }
            
        $queryCountForward = RptasTaxdecMastExtn::select('Arp')
                ->where('Prev_Arp',  $queryMasterTaxdec['ARP']  )
                ->count();
        
        $queryCountPrevArp = RptasTaxdecMastExtn::select('Prev_Arp')
                ->where('arp',  $queryAssmntTaxdecPrevious['Prev_Arp'])
                ->count();
            if ($queryCountPrevArp > 0){
                $queryCountBackward = RptasTaxdecMastExtn::select('Prev_Arp')
                ->where('arp',  $queryMasterTaxdec['ARP'])
                ->count();
            }
            else {
                $queryCountBackward = 0;
            }        
        
        $queryCountCurrentPin = RptasTaxdecMastExtn::select('Prev_Arp')
                ->where('Prev_Arp',  $queryMasterTaxdec['ARP'] )
                ->count();
        
        $queryCountMapPolygon = MapPolygon::select('pin')
                ->where('pin', $queryMasterTaxdec['PIN'])
                ->count();

       
        
        $queryCountPrevPin = RptasTaxdecMastMla::select('pin')
                    ->where('pin', $queryAssmntTaxdecPrevious['Prev_Pin'])
                    ->count();
        
        $queryCountTitle = 0;
        if ($queryMasterTaxdec['TctNo'] != null || $queryMasterTaxdec['CCTNO'] != null )
        {
            $queryCountTitle = RptasTaxdecMastMla::select('TctNo','CCTNO')
                        ->where('TctNo', $queryMasterTaxdec['TctNo'])
                        ->where('CCTNO', $queryMasterTaxdec['CCTNO'])
                        ->count();
        }

        $pictures = self::getPicturesUrl($queryMasterTaxdec['PIN']);

        
        $queryRecordsDailyTransaction = RecordsDailyTransaction::select('td_no')
                ->where('ARP',$queryMasterTaxdec['ARP'])
                ->first();

        if ($queryRecordsDailyTransaction == null)
        {
            $queryRecordsDailyTransaction['td_no'] = '';
        }


        $data['masterTaxdec']           = $queryMasterTaxdec;
        $data['etAl']                   = $queryEtAl;
        $data['assmntTaxdec']           = $assmntTaxdec;
        $data['taxdecPrevious']         = $queryAssmntTaxdecPrevious;
        $data['countBackward']          = $queryCountBackward;
        $data['countForward']           = $queryCountForward;
        $data['countCurrentPin']        = $queryCountCurrentPin;
        $data['countMapPolygon']        = $queryCountMapPolygon;
        $data['countPrevPin']           = $queryCountPrevPin;
        $data['countTitle']             = $queryCountTitle;
        $data['pictures']               = $pictures;
        $data['td_no']                  = $queryRecordsDailyTransaction['td_no'];
        
        return $data;

    }

    private function rptasCancelledAssessmentRoll($arp){

        $data = [];

        //searching the active assessment roll

        $queryMasterTaxdec = CancelledAssessmentRoll::select('UpdateCode','OwnerNo','ARP','PIN','Owner','OwnerAddress',
                                    'Location','BldgLocation','Barangay','Zone','MuniDistName',
                                    'SurveyNo','LotNo', 'BlkNo',  'kind', 'BldgStorey', 'BldgTypeDesc',
                                    'StrucType','SubClass',  'Taxability',
                                    'TctNo','TctDate','CCTNO','CCTDate','PREV_TCT_LAND','PREV_TCTDATE_LAND','PREV_TCT_BLDG',
                                    'PREV_TCTDATE_BLDG','PREV_CCT_BLDG_ADDL', 'SurveyNo',
                                    'North', 'South', 'East','West',
                                    'Effectivity', 'AppraisedBy', 'AppraisedDt', 'Assessor', 'Memoranda', 'UpdateDesc',
                                    'insertedDt','updatedDt'  )
                    ->where('ARP', $arp)
                    ->first();
    
        

        
        $queryEtAl = RptasOwnerextnConcat::select('AcctNameConcat')
                    ->where('acctNo', $queryMasterTaxdec['OwnerNo'])
                    ->first();

            if ($queryEtAl == null){
                $queryEtAl = "None";
            }
        
        $assmntTaxdec = [];
        
        $queryAssmntTaxdec = CancelledTaxdecAssmnt::select('AUDesc', 'area', 'mv','al', 'av', 'taxability')
                    ->where('arp', $queryMasterTaxdec['ARP'])
                    ->get()
                    ->toArray();

            foreach($queryAssmntTaxdec as $key => $value){

                $assmntTaxdec[] = array(
            
                'actualUse'             => $value['AUDesc'],
                'area'                  => number_format($value['area'],2,".",","). ".sqm",
                'assessmentLevel'       => number_format($value['al']),
                'marketValue'           => "₱ " . number_format($value['mv'],2,".",","),
                'assessedValue'         => "₱ " .number_format($value['av'],2,".",","),
                'taxability'            => $value['taxability']
                );

            }
        

        $queryAssmntTaxdecPrevious = RptasTaxdecMastExtn::select('Prev_Arp', 'Prev_Owner', 'Prev_Av','Prev_Pin', 'InsertedDt')
                    ->where('arp', $queryMasterTaxdec['ARP'])
                    ->first();
        
            if ($queryAssmntTaxdecPrevious == null){
                $queryAssmntTaxdecPrevious['Prev_Arp'] = "None";
                $queryAssmntTaxdecPrevious['Prev_Owner'] = "None";
                $queryAssmntTaxdecPrevious['Prev_Av'] = "₱ ". 0.00;
                $queryAssmntTaxdecPrevious['Prev_Pin'] = "None";
            }
            else{
                $queryAssmntTaxdecPrevious['Prev_Av'] = "₱ " . number_format($queryAssmntTaxdecPrevious['Prev_Av'],2,".",",");
            }
            
        $queryCountForward = RptasTaxdecMastExtn::select('Arp')
                ->where('Prev_Arp',  $queryMasterTaxdec['ARP']  )
                ->count();
        
        $queryCountPrevArp = RptasTaxdecMastExtn::select('Prev_Arp')
                ->where('arp',  $queryAssmntTaxdecPrevious['Prev_Arp'])
                ->count();
            if ($queryCountPrevArp > 0){
                $queryCountBackward = RptasTaxdecMastExtn::select('Prev_Arp')
                ->where('arp',  $queryMasterTaxdec['ARP'])
                ->count();
            }
            else {
                $queryCountBackward = 0;
            }        
        
        $queryCountCurrentPin = RptasTaxdecMastExtn::select('Prev_Arp')
                ->where('Prev_Arp',  $queryMasterTaxdec['ARP'] )
                ->count();
        
        $queryCountMapPolygon = MapPolygon::select('pin')
                ->where('pin', $queryMasterTaxdec['PIN'])
                ->count();
        
        $queryCountPrevPin = RptasTaxdecMastMla::select('pin')
                    ->where('pin', $queryAssmntTaxdecPrevious['Prev_Pin'])
                    ->count();
        
        $queryCountTitle = 0;
        if ($queryMasterTaxdec['TctNo'] != null || $queryMasterTaxdec['CCTNO'] != null )
        {
            $queryCountTitle = RptasTaxdecMastMla::select('TctNo','CCTNO')
                        ->where('TctNo', $queryMasterTaxdec['TctNo'])
                        ->where('CCTNO', $queryMasterTaxdec['CCTNO'])
                        ->count();
        }

        $pictures = self::getPicturesUrl($queryMasterTaxdec['PIN']);

        $queryRecordsDailyTransaction = RecordsDailyTransaction::select('cancellation_number','td_no')
                ->where('ARP',$queryMasterTaxdec['ARP'])
                ->first();

            if ($queryRecordsDailyTransaction == null)
            {
                $queryRecordsDailyTransaction['td_no'] = '';
                $queryRecordsDailyTransaction['cancellation_number'] = '';
            }


        $data['masterTaxdec']           = $queryMasterTaxdec;
        $data['etAl']                   = $queryEtAl;
        $data['assmntTaxdec']           = $assmntTaxdec;
        $data['taxdecPrevious']         = $queryAssmntTaxdecPrevious;
        $data['countBackward']          = $queryCountBackward;
        $data['countForward']           = $queryCountForward;
        $data['countCurrentPin']        = $queryCountCurrentPin;
        $data['countMapPolygon']        = $queryCountMapPolygon;
        $data['countPrevPin']           = $queryCountPrevPin;
        $data['countTitle']             = $queryCountTitle;
        $data['pictures']               = $pictures;
        $data['cancellation_number']    = $queryRecordsDailyTransaction['cancellation_number'];
        $data['td_no']                  = $queryRecordsDailyTransaction['td_no'];
        
        return $data;

    }

    public function getPicturesUrl($pin){
        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data["BLDG IMAGE"];

        $list = [];

        foreach($path as $paths){
            if(gettype($paths) == 'string'){
                if(strpos($paths, $pin) !== false){
                    $list[] = $paths;
                }
            }
        }

        if(count($list)){

            sort($list, 4);
            
            return array(
                "listCount" => count($list),
                "list"      => $list,
            );
        }

        $list[] = "imagenf.jpg";

        return array(
            "listCount" => count($list),
            "list"      => $list,
        );
    }

    public function rptasDialogForwardArp(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);

        if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

        $data = [];

        $query = RptasTaxdecMastExtn::select('arp','Prev_Arp','Pin', 'Prev_Pin','Prev_Owner')
                        ->where('Prev_Arp', 'like', '%'.$search.'%')
                        ->orderBy('arp','desc')
                        ->get()
                        ->toArray(); 
        

            foreach($query as $key => $value){
                

                $data[] = array(
                
                'arp'                       => $value['arp'],
                'prevArp'                   => $value['Prev_Arp'],
                'prevPin'                   => $value['Prev_Pin'],
                'pin'                       => $value['Pin'],
                'prevOwner'                 => $value['Prev_Owner'],
                
                );

            }

        return $data;
    }

    public function rptasDialogSearchPin(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);

        if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

        $data = [];

        $query = RptasTaxdecMastMla::select('arp','pin','Owner','Effectivity')
                        ->where('pin', 'like', '%'.$search.'%')
                        ->orderBy('arp','desc')
                        ->get()
                        ->toArray();

            foreach($query as $key => $value){

                $data[] = array(
            
                'arp'                   => $value['arp'],
                'pin'                   => $value['pin'],
                'Owner'                 => $value['Owner'],
                'Effectivity'            => $value['Effectivity'],
                );

            }

        return $data;
    }

    public function rptasDialogSearchOwnerNo(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);

        if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

        $data = [];

        $query = RptasTaxdecMastMla::select('arp','pin','Owner','Effectivity')
                        ->where('ownerNo', $search)
                        ->orderBy('arp','desc')
                        ->get()
                        ->toArray();

            foreach($query as $key => $value){

                $data[] = array(
            
                'arp'                   => $value['arp'],
                'pin'                   => $value['pin'],
                'Owner'                 => $value['Owner'],
                'Effectivity'            => $value['Effectivity'],
                );

            }

        return $data;
    }

    public function rptasDialogSearchOwner(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);

          if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

        $data = [];

        $query = RptasTaxdecMastMla::select('arp','pin','Owner','Effectivity')
                        ->where('Owner', 'like', '%'.$search.'%'  )
                        ->orderBy('arp','desc')
                        ->get()
                        ->toArray();

            foreach($query as $key => $value){

                $data[] = array(
            
                'arp'                   => $value['arp'],
                'pin'                   => $value['pin'],
                'Owner'                 => $value['Owner'],
                'Effectivity'            => $value['Effectivity'],
                );

            }

        return $data;
    }

    public function rptasDialogSearchTitle(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);

        if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

        $data = [];

        $query = RptasTaxdecMastMla::select('TCTNo','CCTNO','arp','pin','Owner','Effectivity')
                        ->where(function($query) use ($search) {
                            $query->orWhere('TctNo', $search);
                            $query->orWhere('CCTNO', $search);
                        })
                        ->orderBy('arp','desc')
                        ->get()
                        ->toArray();

            foreach($query as $key => $value){

                $data[] = array(
                'TCTNo'                   => $value['TCTNo'],
                'CCTNO'                   => $value['CCTNO'],
                'arp'                   => $value['arp'],
                'pin'                   => $value['pin'],
                'Owner'                 => $value['Owner'],
                'Effectivity'            => $value['Effectivity'],
                );

            }

        return $data;
    }

    public function rptasDialogBackward(Request $request){

        $search = $request->search;
        $exp = explode("-", $search);

        if (strtoupper($exp[0]) == 'C' || strtoupper($exp[0]) == 'D' ) {
            
            $record = RecordsDailyTransaction::select('ARP')
                ->where('td_no',$search)
                ->first();   

            $search = $record ? $record->ARP : null;
        }

        $data = [];

        $query = RptasTaxdecMastExtn::select('arp','Prev_Arp','Pin','Prev_Pin','Prev_Owner')
                        ->where('arp',  $search)
                        ->orderBy('arp','desc')
                        ->get()
                        ->toArray();
        

            foreach($query as $key => $value){
                

                $data[] = array(
            
                'arp'                   => $value['arp'],
                'prevArp'               => $value['Prev_Arp'],
                'pin'                   => $value['Pin'],
                'prevPin'              => $value['Prev_Pin'],
                'prevOwner'             => $value['Prev_Owner'],
                
                );

            }

        return $data;
    }

    public function rptasMuniDistName(){
        // $data = [];

        $query = RptasTaxdecMastMla::select('MuniDistName')
                    ->whereNotNull('MuniDistName')
                    ->distinct()
                    ->orderBy('MuniDistName')
                    ->get();
    

        return $query;
    }

    public function rptasMuniToBarangay(Request $request){
        
        $query = RptasTaxdecMastMla::select('Barangay')
                ->where('MuniDistName', $request->muni)
                ->where('Barangay', '!=', 000)
                ->distinct()
                ->orderBy('Barangay')
                ->get();
        
        return $query;
        
    }

    public function rptasMuniToBlock(Request $request){
        
        if ($request->barangay === null){

            $query = RptasTaxdecMastMla::select('BlkNo')
                    ->where('MuniDistName', $request->muni)
                    ->where('Barangay', '!=', 000)
                    ->where('BlkNo', '!=', '')
                    ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                    ->distinct()
                    ->orderBy('BlkNo')
                    ->get();
        }
        else {
            $query = RptasTaxdecMastMla::select('BlkNo')
                    ->where('Barangay', $request->barangay)
                    ->where('BlkNo', '!=', '')
                    ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                    ->distinct()
                    ->orderBy('BlkNo')
                    ->get();
        }
    
        return $query;
        
    }

    public function rptasMuniToLot(Request $request){
        if ($request->barangay == ''){
            
            $query = RptasTaxdecMastMla::select('LotNo')
                    
                    ->where('BlkNo',  $request->blk)
                    ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                    
                    ->distinct()
                    ->orderBy('LotNo')
                    ->get();
        }
        else {
           
            $query = RptasTaxdecMastMla::select('LotNo')
                    
                    ->where('Barangay', $request->barangay)
                    ->where('BlkNo',  $request->blk)
                    ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                    
                    ->distinct()
                    ->orderBy('LotNo')
                    ->get();
        }
     
        return $query;
        
    }

    public function rptasLocationWithDetails(Request $request){

        $data = [];
        if ($request->barangay == '' && $request->blk == '' && $request->lot == '' ){
            $query =RptasTaxdecMastMla::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                ->where('MuniDistName', $request->muni)
                ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                ->get()
                ->toArray();
        }
        else if ($request->blk == '' && $request->lot == ''){
            $query =RptasTaxdecMastMla::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                ->where('Barangay', $request->barangay)
                ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                ->get()
                ->toArray();    
        }
        else if ($request->barangay == '' && $request->lot == ''){
            $query =RptasTaxdecMastMla::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                ->where('MuniDistName', $request->muni)
                ->where('BlkNo', $request->blk)
                ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                ->get()
                ->toArray();    
        }
        else if ($request->lot == ''){
            $query =RptasTaxdecMastMla::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                ->where('Barangay', $request->barangay)
                ->where('BlkNo', $request->blk)
                ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
                ->get()
                ->toArray();    
        }
        // else   {
        // $query =RptasTaxdecMastMla::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
        //         ->where('MuniDistName', $request->muni)
        //         ->where('Barangay', $request->barangay)
        //         ->where('BlkNo', $request->blk)
        //         ->where('LotNo', $request->lot)
        //         ->whereRaw('SUBSTRING(arp,1,2) = ?',  $request->adOrAA )
        //         ->get()
        //         ->toArray();
        // }
            foreach($query as $key => $value){

                $data[] = array(
                'Location'          => $value['Location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'arp'               => $value['arp'],
                'pin'               => $value['pin'],
                'owner'             => $value['owner'],
                );

            }
    
        return $data;
    }
}
