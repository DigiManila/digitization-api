<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterReleaseTaxDecsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_tax_decs', function (Blueprint $table) {
            $table->string('or_no')->nullable()->change();
            $table->string('or_path')->nullable()->change();
            $table->string('file_path')->nullable()->change();
            $table->string('barcode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_tax_decs', function (Blueprint $table) {
            //
        });
    }
}
