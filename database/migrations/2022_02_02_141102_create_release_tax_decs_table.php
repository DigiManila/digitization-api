<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReleaseTaxDecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('release_tax_decs', function (Blueprint $table) {
            $table->id();
            $table->string("or_no");
            $table->string("td_no");
            $table->string("requestor");
            $table->string("or_path");
            $table->string("file_path");
            $table->string("certifier");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('release_tax_decs');
    }
}
