<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingStatus extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'created_by');
    }

    public function trackname()
    {
        return $this->hasOne(TrackingProcess::class, 'process_rank', 'returned_from_process');
    }
}
