<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ViewRptasAssessmentRoll;

use App\RptasTaxdecMastMla;
use App\RptasTaxdecAssmnt;
use App\RptasTaxdecMastExtn;
use App\RptasOwnerextnConcat;

use App\RptasActiveArpAllDetails;
use App\RptasActiveArpTaxdecAssmnt;

use App\ActiveAssessmentRoll;
use App\ActiveTaxdecAssmnt;
use App\ActiveLandAssessmentRoll;
use App\ActiveBuildingAssessmentRoll;
use App\ActiveMachineryAssessmentRoll;

use App\MapPolygon;
use App\BgyPolygon;
use App\DistrictPolygon;
use App\PoliticalBoundary;
use App\MappingBgyPolygon;
use App\MappingPoliticalBoundary;
use App\MappingActiveMap;
use App\MappingNotActivePinMap;
use App\Road;

use Illuminate\Support\Facades\Redis;

use File;
use Storage;

class RptasPolygonController extends Controller
{

    public function RptasSinglePolygon(Request $request){
        
        
        
        //find the parcel polygon and barangays
        $data = MapPolygon::select('gmap_polygon', 'barangay', 'center_coords')
                    ->where('pin', $request->pin)->first();
        
        
            if($data){

                $data['gmap_polygon'] = str_replace("|", ",", $data['gmap_polygon']);
                $data['gmap_polygon'] = str_replace("\\", "", $data['gmap_polygon']);

                $all_barangay = MapPolygon::select('pin', 'gmap_polygon','center_coords')
                                ->where('barangay', $data['barangay'])
                                ->where('pin', '!=', $request->pin)
                                ->get();

                $data['all_barangay'] = self::parseBarangay($all_barangay);

                // \Log::info($data['all_barangay']);

            }
        //find the current arp of the pin
        $queryUpdatedARP = ActiveLandAssessmentRoll::select('ARP','PIN' )
                                ->where('PIN', $request->pin)
                                ->orderBy('ARP', 'desc')
                                ->first();

            if ($queryUpdatedARP !== null){
                     //select the master data of the current arp
                $queryMasterTaxdec = ActiveLandAssessmentRoll::select('ARP','PIN','Owner','OwnerAddress',
                                    'Location','BldgLocation','Barangay', 'MuniDistName',
                                    'LotNo', 'BlkNo', 'TCTNo', 'CCTNO',
                                    'kind', 'BldgStorey', 'BldgTypeDesc',
                                    'StrucType','SubClass')
                                    ->where('ARP', $queryUpdatedARP['ARP'])
                                    ->first();

                
                //select the taxdecasssmnt of the current arp
                $taxdecAssmnt = [];

                $queryTaxdecAssmnt = ActiveTaxdecAssmnt::select('kind','AUDesc', 'area',
                                            'MV','AL', 'AV','Taxability')
                                    ->where('ARP', $queryUpdatedARP['ARP'])
                                    ->get()
                                    ->toArray();

                    foreach($queryTaxdecAssmnt as $key => $value){

                        $taxdecAssmnt[] = array(
                                
                            'kind'                  => self::getKindType($value['kind']),
                            'AUDesc'                => $value['AUDesc'],
                            'area'                  => number_format($value['area'],2,".",","). ".sqm",
                            'marketValue'           => "₱ " . number_format($value['MV'],2,".",","),
                            'assessedLevel'         => number_format($value['AL']).' %',
                            'assessedValue'         => "₱ " .number_format($value['AV'],2,".",","),
                            'taxability'            => self::getTaxable($value['Taxability']),
            
                        );
                    }
            }
            else {
                $queryMasterTaxdec = [];
                $taxdecAssmnt = [];
            }
        
        
        $queryBuildingMachinery = ActiveAssessmentRoll::select('PIN')
                                    ->where('PIN', 'like',  $request->pin.'%')
                                    ->whereRaw('LENGTH(PIN) > 18')
                                    ->distinct('PIN')
                                    ->orderBy('PIN', 'ASC')
                                    ->get()
                                    ->toArray();
                                    
        
        $building = [];
        $machinery = [];

            foreach ($queryBuildingMachinery as $bm_data){
                if (strpos($bm_data['PIN'],'-B') !== false){
                    $building[] = $bm_data['PIN'];
                }
                else if (strpos($bm_data['PIN'],'-M') !== false){
                    $machinery[] = $bm_data['PIN'];
                }
            }

        $data['building_list'] = $building;
        $data['machinery_list'] = $machinery;
        
        
        $pictures = self::getPicturesUrl($request->pin);

        $mapping = $request['mapping'];
        
        $data['bgy_polygon'] = [];

        // Get all unused barangay
        if(isset($data['barangay'])){

            $district = BgyPolygon::select('district')
                            ->where('bgy', (int)$data['barangay'])
                            ->first();


            if($mapping == 1){
                $unused_bgy = MappingBgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$data['barangay'])
                                ->where('district', $district['district'])
                                ->get();
            } else{
            
                $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$data['barangay'])
                                ->where('district', $district['district'])
                                ->get();
            }

            $unused_district = DistrictPolygon::select('district', 'polygon')
                                ->where('district', '!=', $district['district'])
                                ->get();
           
            $data['bgy_polygon'] = self::parseUnusedBarangay($unused_bgy);
            $data['dist_polygon'] = self::parseUnusedDistrict($unused_district);
            $data['district'] = $district;
        }

       $data['masterTaxdec'] =  $queryMasterTaxdec;
       $data['taxdecAssmnt'] =  $taxdecAssmnt;
       $data['pictures']    =   $pictures;

       
       return $data;
        
    }

    public function postBuildingMachineryPin(Request $request){
        //find the current arp of the pin
        $queryUpdatedARP = ActiveAssessmentRoll::select('ARP','PIN' )
                                ->where('PIN', $request->pin)
                                ->orderBy('ARP', 'desc')
                                ->first();
        
        //select the master data of the current arp

        $queryMasterTaxdec = ActiveAssessmentRoll::select('ARP','PIN','Owner','OwnerAddress',
                            'Location','BldgLocation','Barangay', 'MuniDistName',
                            'LotNo', 'BlkNo', 'TCTNo', 'CCTNO',
                            'kind', 'BldgStorey', 'BldgTypeDesc',
                            'StrucType','SubClass')
                            ->where('ARP', $queryUpdatedARP['ARP'])
                            ->first();

        
        //select the taxdecasssmnt of the current arp
        $taxdecAssmnt = [];

        $queryTaxdecAssmnt = ActiveTaxdecAssmnt::select('kind','AUDesc', 'area',
                                    'MV','AL', 'AV','Taxability')
                            ->where('ARP', $queryUpdatedARP['ARP'])
                            ->get()
                            ->toArray();

            foreach($queryTaxdecAssmnt as $key => $value){

                $taxdecAssmnt[] = array(
                        
                    'kind'                  => self::getKindType($value['kind']),
                    'AUDesc'                => $value['AUDesc'],
                    'area'                  => number_format($value['area'],2,".",","). ".sqm",
                    'marketValue'           => "₱ " . number_format($value['MV'],2,".",","),
                    'assessedLevel'         => number_format($value['AL']).' %',
                    'assessedValue'         => "₱ " .number_format($value['AV'],2,".",","),
                    'taxability'            => self::getTaxable($value['Taxability']),
    
                );
            }
        
        $data['masterTaxdec'] =  $queryMasterTaxdec;
        $data['taxdecAssmnt'] =  $taxdecAssmnt;

        return $data;
    }

    public function postDistrict(Request $request){

        $district = $request['district'];

        $bgy = BgyPolygon::select('bgy')
                ->where('district', $district)
                ->orderBy('id', 'asc')
                ->first();
        
        $brgyNo = str_pad($bgy['bgy'],3,'0',STR_PAD_LEFT);

        $pin = ActiveLandAssessmentRoll::select('PIN')
                ->where('Barangay',  $brgyNo )
                ->whereRaw('LENGTH(PIN) = 18')
                ->orderBy('PIN')
                ->get();
        

        foreach($pin as $p){

            $request = new \Illuminate\Http\Request();
            $request->replace(['pin' => $p['PIN']]);

            $res = self::RptasSinglePolygon($request);

            if($res['bgy_polygon']){
                return self::RptasSinglePolygon($request);
            }
        }
        
    }

    public function postSinglePolygonNoSelection(Request $request){

        $bgy = $request['bgy'];
        $brgyNo = str_pad($bgy,3,'0',STR_PAD_LEFT);
        
        $pin = MapPolygon::select('pin')
                ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                ->first();
        
            
        // $pin = RptasTaxdecMastMla::select('PIN')
        //         ->where('BarangayCode',  $brgyNo )
        //         ->whereRaw('LENGTH(PIN) = 18')
        //         ->orderBy('PIN')
        //         ->first();


        $request = new \Illuminate\Http\Request();
        $request->replace(['pin' => $pin['pin']]);
        return self::RptasSinglePolygon($request);
        
    }


    

    public function getPicturesUrl($pin){
        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data["BLDG IMAGE"];

        $list = [];

        foreach($path as $paths){
            if(gettype($paths) == 'string'){
                if(strpos($paths, $pin) !== false){
                    $list[] = $paths;
                }
            }
        }

        if(count($list)){

            sort($list, 4);
            
            return array(
                "listCount" => count($list),
                "list"      => $list,
            );
        }

        $list[] = "imagenf.jpg";

        return array(
            "listCount" => count($list),
            "list"      => $list,
        );
    }

    public function postInfoTextUnusedParcel(Request $request){
        
        $query = ActiveLandAssessmentRoll::select('ARP','PIN','Owner','Location','LotNo', 'BlkNo')
                                    ->where('PIN', $request->pin)
                                    ->first();

        return $query;
    }

    private function parseUnusedBarangay($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'bgy'       => $value['bgy'],
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function parseUnusedDistrict($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }



    private function getKindType($type){

        switch ($type) {
            case "L":
                return "Land";
                break;
            case "B":
                return "Building";
                break;
            case "M":
                return "Machinery";
                break;
            default:
                return null;
        }

    }

 

    private function parseBarangay($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'pin'   => $value['pin'],
                'path'  => str_replace("|", ",", $value['gmap_polygon']),
                'marker'=> str_replace("|", ",", $value['center_coords'])
            );

           
        }
        return $data_arr;
        // return $str;

    }

    private function getTaxable($tax) {
        if ($tax === 'T') {
            return 'Taxable';
        }
        else {
            return  'Exempted';
        }
    }

    private function parseRoad($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'name'   => $value['road'],
                'path'   => json_decode(str_replace("|", ",", $value['polygon']))
            );

        }

        return $data_arr;
    }

    
}
