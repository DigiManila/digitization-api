<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasNotActivePinPoliticalBoundariesView extends Model
{
    use HasFactory;
    protected $table = 'rptas_not_active_pin_political_boundaries';
}
