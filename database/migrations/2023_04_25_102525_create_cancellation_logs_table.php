<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCancellationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_logs', function (Blueprint $table) {
            $table->id();
            $table->string("or_no")->nullable();
            $table->string("amount")->nullable();
            $table->date("or_date")->nullable();
            $table->string("can_no")->nullable();
            $table->string("name")->nullable();
            $table->string("pin")->nullable();
            $table->string("requestor")->nullable();
            $table->string("or_path")->nullable();
            $table->string("can_path")->nullable();
            $table->string("log_path")->nullable();
            $table->string("certifier")->nullable();
            $table->string('verifier')->nullable();
            $table->string('printed_by')->nullable();
            $table->string('processed_by')->nullable();
            $table->integer("copies")->default(1);
            $table->string("barcode")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_logs');
    }
}
