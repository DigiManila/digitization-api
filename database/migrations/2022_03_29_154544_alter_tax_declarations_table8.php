<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTaxDeclarationsTable8 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_declarations', function (Blueprint $table) {
            $table->string('district_name')->after('pdf_filename_96')->nullable();
            $table->string('previous_pin')->after('cancelled_by_td')->nullable();
            $table->string('remarks')->after('previous_pin')->nullable();
            $table->text('memoranda')->after('remarks')->nullable();

            $table->string('kind')->nullable()->change();
            $table->decimal('area',20,2)->nullable()->change();
            $table->decimal('market_value',20,2)->nullable()->change();
            $table->decimal('currentAssessValue',20,2)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_declarations', function (Blueprint $table) {
            //
        });
    }
}
