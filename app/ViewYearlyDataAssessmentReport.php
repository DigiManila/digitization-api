<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewYearlyDataAssessmentReport extends Model
{
    use HasFactory;
    protected $table = 'view_yearly_data_assessment_report';
}
