<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCancelledBarangayPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancelled_barangay_pins', function (Blueprint $table) {
            
            $table->char('Barangay',70)->nullable();
            $table->decimal('sumArea', 20,2)->nullable();
            $table->decimal('sumMV', 20,2)->nullable();
            $table->decimal('sumAV', 20,2)->nullable();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancelled_barangay_pins');
    }
}
