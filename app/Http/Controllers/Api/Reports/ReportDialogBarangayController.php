<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\RecordsDailyTransaction;

use App\ReportAssessmentRoll;

class ReportDialogBarangayController extends Controller
{
    public function exportToExcel(Request $request){

        $data = [];

        $AA = 'AA-';
        $AD = 'AD-';
        $ARPtype = $request->selectARPType;
        $current = 1;

        if ($request->tab == 3){
            $current = 0;
        }
        


        $queryReport = ReportAssessmentRoll::select(
            'report_assessment_rolls.pin',
            'report_assessment_rolls.ARP',
            'report_assessment_rolls.Owner',
            'report_assessment_rolls.location',
            'report_assessment_rolls.LotNo',
            'report_assessment_rolls.BlkNo',
            'report_assessment_rolls.area',
            'report_assessment_rolls.MV',
            'report_assessment_rolls.AV',
            'records_daily_transactions.td_no',
            'records_daily_transactions.cancellation_number',
            )
                                    ->leftJoin('records_daily_transactions', 'report_assessment_rolls.ARP', '=', 'records_daily_transactions.ARP')
                                    ->where(function ($queryReport) {
                                        $queryReport->whereDate('report_assessment_rolls.AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('report_assessment_rolls.AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(report_assessment_rolls.ARP) > ?", 3)
                                    ->where('report_assessment_rolls.BarangayCode',  $request->barangay)
                                    ->where('report_assessment_rolls.Current', $current)  
                                    ->when($ARPtype == 'AD only', function($query) use ( $AD){
                                        return $query->where('report_assessment_rolls.ARP', "like", "%" . $AD . "%");
                                    })
                                    ->when($ARPtype == 'AA only', function($query) use ( $AA){
                                        return $query->where('report_assessment_rolls.ARP', "like", "%" . $AA . "%");
                                    })
                                    ->get();

        foreach($queryReport as $key => $value){
            $data[] = array(
                'PIN'                   => $value['pin'],
                'ARP'                   => $value['ARP'],
                'Owner'                 => $value['Owner'],
                'Location'              => $value['location'],
                'LotNo'                 => str_replace("/", ",",$value['LotNo']),
                'BlkNo'                 => str_replace("/", ",",$value['BlkNo']),
                'area'                  => $value['area'],
                'MV'                    => $value['MV'],
                'AV'                    => $value['AV'],
                'td_no'                 => $value['td_no'],
                'cancellation_number'   => $value['cancellation_number'],
            );    
        }
                                
        return self::createExcel($data,$request->barangay, $current, $ARPtype, $request->tab);

    }

    private function createExcel( $data, $barangay, $current, $ARPtype, $tab) {
        if ($current == 1){
            if ($tab==0){
                $title = "Report of Barangay ". $barangay. " Active - ".$ARPtype;
            }
            if ($tab==1){
                $title = "Report of Barangay ". $barangay. " Active - Residential - ".$ARPtype;
            }
            if ($tab==2){
                $title = "Report of Barangay ". $barangay. " Active - Commercial - ".$ARPtype;
            }
            
        }
        else {
            $title = "Report of Barangay ". $barangay. " Cancelled - ".$ARPtype;
        }


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        // $worksheet->setTitle($title);
        $spreadsheet->getProperties()
                ->setTitle($title);
        $spreadsheet->getSheetByName($title);
        $worksheet = $spreadsheet->getActiveSheet();

        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates('I1')
                ->setOffsetX(50)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates('K1')
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
         //REPUBLIC OF THE PHILIPPINES
         $worksheet->mergeCells('A1:K1');
         $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
         // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
         $worksheet->getStyle("A1")->getFont()->setSize(12);
         $worksheet->getStyle('A1:K1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A1:K1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
 
         //CITY OF MANILA
         $worksheet->mergeCells('A2:K2');
         $worksheet->setCellValue("A2", "CITY OF MANILA");
         $worksheet->getStyle("A2:K2")->getFont()->setBold(true);
         $worksheet->getStyle("A2")->getFont()->setSize(14);
         $worksheet->getStyle('A2:K2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A2:K2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
         
         //DEPARTMENT OF ASSESSMENT
         $worksheet->mergeCells('A3:K3');
         $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
         $worksheet->getStyle("A3:K3")->getFont()->setBold(true);
         $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
         $worksheet->getStyle('A3:K3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A3:K3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
 
         //*Title of Report
         $worksheet->mergeCells('A5:K6');
         $worksheet->setCellValue("A5", $title);
         $worksheet->getStyle("A5:K6")->getFont()->setBold(true);
         $worksheet->getStyle("A5")->getFont()->setSize(18);
         $worksheet->getStyle('A5:K6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         $worksheet->getStyle('A5:K6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
         
         
         
         $worksheet->mergeCells('A8:K8');
         $worksheet->getStyle('A8:K8')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
         $worksheet->setCellValue("A8", "printed date:  ". date("F j, Y"));
         $worksheet->getStyle("A8")->getFont()->setSize(8);
 
         //---------------------setup width
         $worksheet->getColumnDimension('A')->setWidth(30);
         $worksheet->getColumnDimension('B')->setWidth(20);
         $worksheet->getColumnDimension('C')->setWidth(20);
         $worksheet->getColumnDimension('D')->setWidth(20);
         $worksheet->getColumnDimension('E')->setWidth(30);
         $worksheet->getColumnDimension('F')->setWidth(15);
         $worksheet->getColumnDimension('G')->setWidth(15);
         $worksheet->getColumnDimension('H')->setWidth(24);
         $worksheet->getColumnDimension('I')->setWidth(10);
         $worksheet->getColumnDimension('J')->setWidth(10);
         $worksheet->getColumnDimension('K')->setWidth(11);

           //-----------------------setup merge cell, style and text for header
 
        $worksheet->setCellValue('A10', "PIN");
        $worksheet->setCellValue('B10', "ARP");
        $worksheet->setCellValue('C10', "TD");
        $worksheet->setCellValue('D10', "CANCELLATION");
        $worksheet->setCellValue('E10', "OWNER");
        $worksheet->setCellValue('F10', "LOCATION");
        $worksheet->setCellValue('G10', "BLOCK");
        $worksheet->setCellValue('H10', "LOT");
        $worksheet->setCellValue('I10', "AREA");
        $worksheet->setCellValue('J10', "MARKET VALUE");
        $worksheet->setCellValue('K10', "ASSESSED VALUE");

        $styleHeader = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
            'font' => [
                'bold' => true,
            ],

        ];

        $worksheet->getStyle('A10:K10')->applyFromArray($styleHeader);

        $styleBordersData = [
            
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                ],
            ],
        ];
        $row = 11;

        foreach ($data as $key => $val){
            $worksheet->setCellValue("A{$row}", $val['PIN']);
            $worksheet->setCellValue("B{$row}", $val['ARP']);
            $worksheet->setCellValue("C{$row}", $val['td_no']);
            $worksheet->setCellValue("D{$row}", $val['cancellation_number']);
            $worksheet->setCellValue("E{$row}", $val['Owner']);
            $worksheet->setCellValue("F{$row}", $val['Location']);
            $worksheet->setCellValue("G{$row}", $val['LotNo']);
            $worksheet->setCellValue("H{$row}", $val['BlkNo']);
            $worksheet->setCellValue("I{$row}", $val['area']);
            $worksheet->setCellValue("J{$row}", $val['MV']);
            $worksheet->setCellValue("K{$row}", $val['AV']);

            $worksheet->getStyle("A{$row}:K{$row}")->applyFromArray($styleBordersData);
            $worksheet->getStyle("I{$row}:K{$row}")->getNumberFormat()->setFormatCode('#,###,###,###');
            
            $row++;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;
    }
}
