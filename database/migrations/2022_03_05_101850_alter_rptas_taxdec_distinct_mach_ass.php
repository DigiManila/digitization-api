<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRptasTaxdecDistinctMachAss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rptas_taxdec_distinct_mach_ass', function (Blueprint $table) {
            $table->string('Kind',50)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rptas_taxdec_distinct_mach_ass', function (Blueprint $table) {
            //
        });
    }
}
