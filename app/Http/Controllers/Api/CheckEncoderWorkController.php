<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\CheckEncoderWork;
use App\TempTaxDeclarations;
use App\ViewAverageEncodedRanking;
use Carbon\Carbon;
use Auth;
use App\Profile;

class CheckEncoderWorkController extends Controller
{
    public function getZone(Request $request){

        $data = [];
        $query = CheckEncoderWork::select('zone')
                            ->where('series','=',$request->sel)
                            ->distinct('zone')
                            ->get();
                            
                            
          
        foreach($query as $key => $val){
            $data[] = $val['zone'];
        } 
        
        return $data;
    }

    public function getBarangay(Request $request){

        $data = [];

        $query = CheckEncoderWork::select('barangay')
                            ->where('zone','=',$request->sel)
                            ->get()
                            ->toArray();

        foreach($query as $key => $val){
            $data[] = $val['barangay'];
        } 

        return $data;
    }

    public function getRemark(Request $request){

        

        $query = CheckEncoderWork::select('remarks')
                            ->where('zone','=',$request->zone)
                            ->where('barangay','=',$request->barangay)
                            ->first();
                            

        return $query;
    }

    public function getCEWDashboard(Request $request){

       
        if ($request->year == '1996'){

            $query = CheckEncoderWork::select('series','zone','work_pdf','total_pdf','remarks','fname')
                    ->where('barangay','=',  $request->brgy )
                    ->first();
        }


        else{

            $query = CheckEncoderWork::select('series','zone','work_pdf','total_pdf','remarks','fname')
                    ->where('series','=',  $request->year )
                    ->where('zone','=',  $request->zone )
                    ->first();

            if ($query === null) {
                $query = 'none';
            }
            
        }

        \Log::info($query);
        

        return $query;


    }

    public function updateCEW(Request $request){

        

        $username = Profile::selectRaw("CONCAT(last_name, ', ' ,first_name) AS fname")
                                    ->where('user_id', Auth::user()->id)
                                    ->first();

        if ($request->series == '1996'){                                    
            $countWorkPDF = TempTaxDeclarations::select('pdf_filename_96')
                                        ->whereRaw('SUBSTRING(pdf_filename_96,1,3) = ?',  $request->barangay)
                                        ->count();
            
             

            $update = CheckEncoderWork::where('series',$request->series)
                                ->where('zone',$request->zone)
                                ->where('barangay', $request->barangay)
                ->update(
                    [
                            'work_pdf' => (int)$countWorkPDF,
                            'total_pdf' => $request->total_pdf,
                            'fname' => $username['fname'],
                            'created_by' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
        
        }
        else if ($request->barangay === null) {

            if ($request->type == 'insert'){
                
                $saveArray= [];

                $saveArray[] = array(
                    'series'        => $request->series,
                    'zone'          => $request->zone,
                    'barangay'      => $request->barangay,
                    'work_pdf'      => $request->work_pdf,
                    'total_pdf' => $request->total_pdf,
                    'fname'         => $username['fname'],
                    'created_by'    => Auth::user()->id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                );  

                CheckEncoderWork::insert($saveArray);

              
                
                    return response()->json([
                        "data" => [],
                        'message' => "New data has been added succcessfully",
                        'status' => 1
                    ], 200);            
                
            }
            
            else if ($request->type == 'update') {

                if ($request->series == '1985'){


                    $countWorkTD = TempTaxDeclarations::select('current_arp')
                    ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'B')
                    ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $request->zone)
                    ->count();

                    \Log::info($countWorkTD);

                    $update = CheckEncoderWork::where('series',$request->series)
                         ->where('zone',$request->zone)
                
                    ->update(
                        [
                                'work_pdf' => (int)$countWorkTD,
                                'total_pdf' => $request->total_pdf,
                                'fname' => $username['fname'],
                                'created_by' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }

                else if ($request->series == '1979'){
                    
                    $countWorkTD = TempTaxDeclarations::select('current_arp')
                    ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'A')
                    ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $request->zone)
                    ->count();

                    // \Log::info($countWorkTD);

                    $update = CheckEncoderWork::where('series',$request->series)
                         ->where('zone',$request->zone)
                
                    ->update(
                        [
                                'work_pdf' => (int)$countWorkTD,
                                'total_pdf' => $request->total_pdf,
                                'fname' => $username['fname'],
                                'created_by' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }

               
            }
        }
        
        

        $updateRemark = CheckEncoderWork::select('work_pdf','total_pdf')
                                    ->where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->first();

                if ($updateRemark['work_pdf'] < $updateRemark['total_pdf']){
                    CheckEncoderWork::select('work_pdf','total_pdf')
                                    ->where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'incomplete']);
                }
                else  if ($updateRemark['work_pdf'] > $updateRemark['total_pdf']){
                    CheckEncoderWork::select('work_pdf','total_pdf')
                                    ->where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'over the mark']);
                }
                else if ($updateRemark['work_pdf'] = $updateRemark['total_pdf'])
                {
                    CheckEncoderWork::select('work_pdf','total_pdf')
                                    ->where('series',$request->series)
                                    ->where('zone',$request->zone)
                                    ->where('barangay',$request->barangay)
                                    ->update(['remarks' => 'complete']);
                }


               

        if($update){
            return response()->json([
                "data" => [],
                'message' => "Update Succcessfully",
                'status' => 1
            ], 200);            
        }
        
        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);



    }

  


}
