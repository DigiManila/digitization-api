<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\LandUsePolygon;
use App\BusinessPermit;

class ActualUseController extends Controller
{
    public function getLandUse(){

        $output = [];
        
        $land_use = LandUsePolygon::select('polygon', 'class', 'desc')
                                        ->get()
                                        ->toArray();

        foreach ($land_use as $key => $value) {

            $output[] = array(
                'path'      => str_replace("|", ",", $value['polygon']),
                'class'     => $value['class'],
                'desc'      => $value['desc'],
                'options'   => self::createOptions($value['class'])
            );
            
        }

        return $output;

    }

    public function getBusinessList(Request $request){
        return BusinessPermit::select('id', 'pin', 'acct_no', 'business_name', 'business_owner', 'line_of_business', 'no_of_employees', 'area')
                                ->where('pin', $request['pin'])
                                ->get()
                                ->toArray();
    }

    public function savePermit(Request $request){

        $save_data = array(
            'pin'               => $request['pin'],
            'business_name'     => $request['businessName'],
            'business_owner'    => $request['businessOwner'],
            'line_of_business'  => $request['lineOfBusiness'],
            'no_of_employees'   => $request['noOfEmployees'],
            'area'              => $request['area'],
            'created_by'        => $request['user'],
            'created_at'        => date('Y-m-d H:i:s'),
        );

        $acctNo = $request['acctNo'];

        if($request['mode'] === 'A'){

            if(!$acctNo){
                $acctNo = self::getLastNoAcctNo();
            }

            $save_data['acct_no']       = $acctNo;
            $save_data['created_by']    = $request['user'];
            $save_data['created_at']    = date('Y-m-d H:i:s');

            $result = BusinessPermit::insert($save_data);

        } elseif($request['mode'] === 'V'){

            $hasDuplicate = BusinessPermit::select('acct_no')
                                ->where('id', '!=', $request['id'])
                                ->where('acct_no',  '=' , $acctNo)
                                ->get();

            if (count($hasDuplicate)) {
                return response()->json([
                    "data" => [],
                    'message' => "Cannot have duplicate Account Number",
                    'status' => 2
                ], 200);
             }

            $permit = BusinessPermit::find($request['id']);
            $permit->acct_no            = $acctNo;
            $permit->pin                = $request['pin'];
            $permit->business_name      = $request['businessName'];
            $permit->business_owner     = $request['businessOwner'];
            $permit->line_of_business   = $request['lineOfBusiness'];
            $permit->no_of_employees    = $request['noOfEmployees'];
            $permit->area               = $request['area'];
            $permit->updated_by         = $request['user'];
            $permit->updated_at         = date('Y-m-d H:i:s');

            $result = $permit->save();

        }

        if($result){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "Saving Failed",
            'status' => 2
        ], 200);

    }

    private function parseLandUse($data){

        $data_arr = [];

        foreach ($data as $key => $value) {



            // if($value['polygon']){
            //     $data_arr[] = array(
            //         'path'  => str_replace("|", ",", $value['polygon']),
            //         'class' => $value['class'],
            //         'desc'  => $value['desc']
            //     );
            // }

            \Log::info($value);

        }

        return $data_arr;

    }

    private function createOptions($class){

        switch ($class) {
            case 'C-2/MXD':
                return self::createJson("#ffa500", "#ffa500");
                return '{ strokeColor:"#ffa500", fillColor:"#ffa500", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'C-3/MXD':
                return self::createJson("#e41a1c", "#e41a1c");
                return '{ strokeColor:"#e41a1c", fillColor:"#e41a1c", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'I-1':
                return self::createJson("#9e8de7", "#9e8de7");
                return '{ strokeColor:"#9e8de7", fillColor:"#9e8de7", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'INS-G':
                return self::createJson("#45c4f7", "#45c4f7");
                return '{ strokeColor:"#45c4f7", fillColor:"#45c4f7", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'INS-U':
                return self::createJson("#0f65f9", "#0f65f9");
                return '{ strokeColor:"#0f65f9", fillColor:"#0f65f9", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'POS-CEM':
                return self::createJson("#729b6f", "#729b6f");
                return '{ strokeColor:"#729b6f", fillColor:"#729b6f", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'POS-GEN':
                return self::createJson("#5aff2d", "#5aff2d");
                return '{ strokeColor:"#5aff2d", fillColor:"#5aff2d", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'R-3/MXD':
                return self::createJson("#eaff00", "#eaff00");
                return '{ strokeColor:"#eaff00", fillColor:"#eaff00", strokeOpacity:0.9, strokeWeight:4 }';
                break;

            case 'UTIL':
                return self::createJson("#8b8b8b", "#8b8b8b");
                return '{ strokeColor:"#8b8b8b", fillColor:"#8b8b8b", strokeOpacity:0.9, strokeWeight:4 }';
                break;
            
            default:
                # code...
                break;
        }

    }

    private function createJson($d1, $d2){

        $output = array(
            'strokeColor'   => $d1,
            'fillColor'     => $d2,
            'strokeOpacity' => 0.5,
            'strokeWeight'  => 4,
            'fillOpacity'   => 0.2,
            'zIndex'        => -10,
        );

        return json_encode($output);

    }

    private function getLastNoAcctNo(){

        $pattern = "NAN" . date('Y') . "-";
        $acct_no = "";

        $data = BusinessPermit::select('acct_no')
                                ->where('acct_no', 'like', $pattern . '%')
                                ->orderBy('acct_no', 'desc')
                                ->first();
        if($data){
            $exp = explode('-', $data['acct_no']);
            $last = (int)$exp[1] + 1;
            return $pattern .  sprintf('%07d', $last);

        } else{
            return $pattern .  sprintf('%07d', 1);
        }

    }



}
