<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Api\SalesDataController;

class SalesDataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sales Data Import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sales = new SalesDataController();
        $status = $sales->importSalesDataExcel();
        print $status;
    }
}
