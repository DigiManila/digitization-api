<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveMachineryAssessmentRoll extends Model
{
    use HasFactory;
    protected $table = 'active_machinery_assessment_roll';
}
