<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Cache;

class StatusCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $pin = "false";
        $assessment = "false";

        if(isset(Cache::get('pin')[0])){
            $pin = "true";
        }

        // if(Cache::get('assessment')[0]){
        //     $assessment = "true";
        // }


        $output_str = "\n\nStatus of cache 'pin': $pin";
        // $output_str = $output_str . "\nStatus of cache 'assessment': $assessment";
        $output_str = $output_str . "\n\n";
        print $output_str;

    }
}
