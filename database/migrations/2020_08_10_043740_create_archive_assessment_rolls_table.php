<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchiveAssessmentRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_assessment_rolls', function (Blueprint $table) {
            $table->id();
            $table->string("arp_no")->nullable();
            $table->string("pin_no")->nullable();
            $table->string("scanned_document_id")->nullable();
            $table->text("owner_name");
            $table->text("previous_owner_name")->nullable();
            $table->text("address");
            $table->string("barangay");
            $table->string("lot");
            $table->string("block");
            $table->string("disrict")->nullable();
            $table->integer("zone")->nullable();

             $table->string("created_by")->nullable();
            $table->string("updated_by")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_assessment_rolls');
    }
}
