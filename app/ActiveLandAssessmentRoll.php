<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveLandAssessmentRoll extends Model
{
    use HasFactory;
    protected $table = 'active_land_assessment_roll';
}
