<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DistrictPolygon;
use App\ActiveAssessmentRoll;
use App\MapPolygonWithSection;

class BarangayMapController extends Controller
{
    public function getSectionMapPolygons(Request $request){

        $bgy = $request['bgy'];
        $sec = $request['sec'];

        $sections = self::getSections($bgy);

        $map_active = MapPolygonWithSection::select('gmap_polygon')
                                        ->where('barangay', $bgy)
                                        ->where('section', $sec)
                                        ->get()
                                        ->toArray();

        $map_inactive = MapPolygonWithSection::select('gmap_polygon')
                                        ->where('barangay', $bgy)
                                        ->where('section', '!=', $sec)
                                        ->get()
                                        ->toArray();

        $active     = self::parseSections($map_active);
        $inactive   = self::parseSections($map_inactive);

        return response()->json([
            "data" => compact("active", "inactive", "sections"),
            'message' => "Saved Succcessfully",
            'status' => 1
        ], 200);            
        
    }

    public function getMarkers(Request $request){

        $marker = [];
        $active_pin     = $request['active_pin'];
        $active_latlng  = '{ "lat": ' . $request['active_center']['lat'] . ', "lng": ' . $request['active_center']['lng'] . ' }';

        $marker[$active_pin] = array(
            'pin'       => $active_pin,
            'center'    => $active_latlng
        );

        foreach ($request['unused'] as $key => $value) {
            $marker[$value['pin']] = array(
                'pin'       => $value['pin'],
                'center'    => $value['marker']
            );
        }

        ksort($marker);

        $output = array_values($marker);

        return $output;

    }

    public function downloadExcel(Request $request){

        $list = self::getAllActive($request);

        return self::createExcel($list, $request['bgy']);

    }

    private function getSections($bgy){

        $list = [];
        $centroid = "";

        $latlng = [];

        $data = MapPolygonWithSection::select('section', 'center_coords')
                                        ->where('barangay', $bgy)                     
                                        ->get()
                                        ->toArray();

        foreach($data as $key => $val){

            $list[] = $val['section'];

            $tmp = json_decode($val['center_coords'], 1);

            $latlng[] = $tmp["lng"] . "," . $tmp["lat"];
        }

        $centroid = self::computeCenter($latlng);

        return compact('centroid', 'list');

    }

    private function computeCenter($data){

        $arrX = [];
        $arrY = [];

        foreach ($data as $key => $value) {

            $latLng = explode(",", $value);

            if($latLng[0]){
                // $arrX[] = $latLng[1] / 0.99999796;
                // $arrY[] = $latLng[0] / 1.000000466230960;

                $arrX[] = $latLng[1];
                $arrY[] = $latLng[0];
            }
        }

        sort($arrX);
        sort($arrY);

        $minX = reset($arrX);
        $maxX = end($arrX);

        $minY = reset($arrY);
        $maxY = end($arrY);

        $getCenterX = ($maxX - $minX) / 2;
        $getCenterY = ($maxY - $minY) / 2;

        return "{ \"lat\": " . (string)($minX + $getCenterX) . ", \"lng\": " . (string)($minY + $getCenterY) . " }";

    }

    private function parseSections($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['gmap_polygon']),
            );
        }
        return $data_arr;
    }

    private function getAllActive($data){

        $group  = [];
        $join   = [];

        $active = ActiveAssessmentRoll::select("PIN", "Owner")
                                    ->where("Barangay", $data['bgy'])
                                    ->get()
                                    ->toArray();

        $group = self::groupActive($active);

        foreach ($data['data'] as $key => $value) {

            $pin = $value['pin'];

            if(isset($group[$pin])){
                $join[$key] = $group[$pin];
            } else{
                $join[$key][] = array(
                    'PIN' => $pin,
                    'Owner' => ""
                );
            }

        }
        
        return $join;

    }

    private function groupActive($active){

        $data = [];

        foreach ($active as $key => $value) {

            $base_pin = substr($value['PIN'], 0, 18);

            $data[$base_pin][] = $value;

        }

        return $data;

    }

    private function createExcel($data, $bgy){

        $spreadsheet    = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet      = $spreadsheet->getActiveSheet();

        $worksheet->setTitle($bgy);
        $spreadsheet->getSheetByName($bgy);

        $worksheet      = $spreadsheet->getActiveSheet();

        $worksheet->getColumnDimension('A')->setWidth(6);
        $worksheet->getColumnDimension('B')->setWidth(34);
        $worksheet->getColumnDimension('C')->setWidth(38);
        $worksheet->getColumnDimension('D')->setWidth(11);
        $worksheet->getColumnDimension('E')->setWidth(11);
        $worksheet->getColumnDimension('F')->setWidth(11);
        $worksheet->getColumnDimension('G')->setWidth(15);
        $worksheet->getColumnDimension('H')->setWidth(17);
        $worksheet->getColumnDimension('I')->setWidth(28);
        $worksheet->getColumnDimension('J')->setWidth(42);

        $worksheet->mergeCells('A1:J2');
        $worksheet->mergeCells('A3:A4');
        $worksheet->mergeCells('B3:B4');
        $worksheet->mergeCells('C3:C4');
        $worksheet->mergeCells('D3:F3');
        $worksheet->mergeCells('G3:G4');
        $worksheet->mergeCells('H3:H4');
        $worksheet->mergeCells('I3:I4');
        $worksheet->mergeCells('J3:J4');

        $worksheet
                ->setCellValue("A1", "BARANGAY " . $bgy)
                ->setCellValue("A3", "No.")
                ->setCellValue("B3", "PIN")
                ->setCellValue("C3", "Owner's Name")
                ->setCellValue("D3", "Structure Type")
                ->setCellValue("D4", "Wood")
                ->setCellValue("E4", "Concrete")
                ->setCellValue("F4", "Mixed")
                ->setCellValue("G3", "No. of Storeys")
                ->setCellValue("H3", "Has Machineries")
                ->setCellValue("I3", "Date Constructed (Estimate)")
                ->setCellValue("J3", "Remarks");

        $worksheet->getStyle("A1:J4")->getFont()->setBold(true);
        $worksheet->getStyle('A1:J4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A1:J4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->getStyle('A1:J4')->getAlignment()->setWrapText(true);

        $row = 5;
       
        foreach ($data as $key => $value) {

            $count = count($value);

            $end_merge = $row + ($count - 1);

            if($count > 1){
                $worksheet->mergeCells("A{$row}:A{$end_merge}");
            }

            $worksheet->setCellValue("A{$row}", $key + 1);
            $worksheet->getStyle("A{$row}")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle("A{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            foreach ($value as $key => $val) {

                $worksheet->setCellValue("B{$row}", $val["PIN"]);
                $worksheet->getStyle("B{$row}")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("B{$row}")->getAlignment()->setWrapText(true);

                $worksheet->setCellValue("C{$row}", $val["Owner"]);
                $worksheet->getStyle("C{$row}")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("C{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle("C{$row}")->getAlignment()->setWrapText(true);

                $row = $row + 1;
            }

        }


        $endRow = $row - 1;

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        
        $worksheet->getStyle("A1:J{$endRow}")->applyFromArray($styleArray);

        $worksheet->setSelectedCells('A1');
        $worksheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,4);
        $worksheet->getPageSetup()->setPrintArea("A1:J{$endRow}");

        $worksheet = $spreadsheet->setActiveSheetIndex(0);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        // $writer->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;




    }
}
