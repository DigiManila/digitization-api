<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\OrTaxdec;
use App\Profile;
use App\RecordsTaxDecData;
use Auth;

class PaymentController extends Controller
{
    public function savePayment(Request $request){

        $data = $request->toArray();
        $data['released_by'] = Auth::user()->id;

        $status = OrTaxdec::create($data);

        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);

    }

    public function getPayments(){

        $certifier = [];

        $payments = OrTaxdec::select('id', 'or_num', 'tax_declaration','printed')->orderBy('id', 'DESC')->get();
        $certifier_data = Profile::select('id', 'first_name', 'middle_name', 'last_name')->where('position', 'LOCAL ASSESSMENT OPERATION OFFICER III')->get();

        foreach($certifier_data as $key => $val){
            $certifier[$key]['text'] = $val['first_name'] . " " . $val['middle_name'] . " " . $val['last_name'];
            $certifier[$key]['value'] = $val['id'];
        }

        return compact('payments', 'certifier');
    }
}