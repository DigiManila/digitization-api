<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ActiveAssessmentRoll;
use App\RptasTaxdecMastMla;
use App\ReportAssessmentRoll;
use App\ViewYearlyDataAssessmentReport;

class ReportChartController extends Controller
{
    public function getData(){
        
        $yearlyAssessedValueReport = self::getYearlyAssessedValueReport();
        $yearlyDataAssessmentValueReport = self::getYearlyDataAssessmentValueReport();
      

        return response()->json([
            "data" => compact(
                'yearlyAssessedValueReport',
                'yearlyDataAssessmentValueReport',
                
            ),
            'message' => 'Chart Data Retrieved Successfully',
            'status' => 1
        ],200);
    }

    private function getYearlyAssessedValueReport(){
       
        $currentYear = date('Y');

        $tenYears = date('Y', strtotime('-9 year'));

        $query = ActiveAssessmentRoll::selectRaw('Year(AppraisedDt) as appYear, sum(TotalAv) as totalAv, count(ARP) as totalRPU')
                                    ->where('Taxability', 'T')
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->whereRaw('Year(AppraisedDt) >= ?', $tenYears)
                                    ->whereRaw('Year(AppraisedDt) <= ?', $currentYear)
                                    ->groupBy('appYear')
                                    ->orderBy('appYear')
                                    ->get();

            foreach($query as $key => $value){

                $value['AV'] = self::abbreviateNumber($value['totalAv']);
                $value['abbreviateAV'] = self::abbreviateNumberText($value['totalAv']);

            }
        
                
        return $query;
    }

    
    private function getYearlyDataAssessmentValueReport(){

        $query = ReportAssessmentRoll::selectRaw("
        SUM(CASE WHEN year(AppraisedDt) <= '2013' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2013',
        SUM(CASE WHEN year(AppraisedDt) <= '2014' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2014',
        SUM(CASE WHEN year(AppraisedDt) <= '2015' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2015',
        SUM(CASE WHEN year(AppraisedDt) <= '2016' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2016',
        SUM(CASE WHEN year(AppraisedDt) <= '2017' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2017',
        SUM(CASE WHEN year(AppraisedDt) <= '2018' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2018',
        SUM(CASE WHEN year(AppraisedDt) <= '2019' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2019',
        SUM(CASE WHEN year(AppraisedDt) <= '2020' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2020',
        SUM(CASE WHEN year(AppraisedDt) <= '2021' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2021',
        SUM(CASE WHEN year(AppraisedDt) <= '2022' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2022',
        SUM(CASE WHEN year(AppraisedDt) <= '2023' And Taxability = 'T' AND CURRENT = 1 THEN AV ELSE 0 END) AS '2023'
        ")
                    ->first();

            $query = json_decode(json_encode($query), false);
           
            $data = [];
            $firstYear = 2013;
           
            foreach($query as $key => $value){
                $data[] = array(
                    'appYear' =>  $firstYear,
                    'AV' =>  self::abbreviateNumber($value),
                    'abbreviateAV' =>  self::abbreviateNumberText($value)
                );
                $firstYear++;
            }
                
        return $data;
    }

    private function abbreviateNumberText($num) {
        if ($num >= 0 && $num < 1000) {
          $format = floor($num);
          $suffix = '';
        } 
        else if ($num >= 1000 && $num < 1000000) {
          $format = floor($num / 1000);
          $suffix = 'K+';
        } 
        else if ($num >= 1000000 && $num < 1000000000) {
          $format = floor($num / 1000000);
          $suffix = 'M+';
        } 
        else if ($num >= 1000000000 && $num < 1000000000000) {
          $format = floor($num / 1000000000);
          $suffix = 'B+';
        } 
        else if ($num >= 1000000000000) {
          $format = floor($num / 1000000000000);
          $suffix = 'T+';
        }
        
        return !empty($format . $suffix) ? $format . $suffix : 0;
      }

      private function abbreviateNumber($num) {
        if ($num >= 0 && $num < 1000) {
          $format = floor($num);
         
        } 
        else if ($num >= 1000 && $num < 1000000) {
          $format = floor($num / 1000);
         
        } 
        else if ($num >= 1000000 && $num < 1000000000) {
          $format = floor($num / 1000000);
         
        } 
        else if ($num >= 1000000000 && $num < 1000000000000) {
          $format = floor($num / 1000000000);
          
        } 
        else if ($num >= 1000000000000) {
          $format = floor($num / 1000000000000);
         
        }
        
        return $format;
      }
}
