<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingActiveMap extends Model
{
    use HasFactory;
    protected $table = 'mapping_active_map';
}
