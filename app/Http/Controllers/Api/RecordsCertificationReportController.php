<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\RecordsCertification;
use App\RecordsCertificationLetterNationalArchive;
use App\RecordsCertificationsNoTd;
use App\Profile;

use Storage;
use DB;

class RecordsCertificationReportController extends Controller
{

    public function getDashboardData(){

        $default = "All";

        return self::getData(date("Y"), $default);

    }

    public function updateDashboardData(Request $request){
        return self::getData(date($request['year']), $request['cert']);
    }

    private function getData($y, $cert){

        $year = substr($y, 0, 2);

        if($cert == 'No Tax Declaration'){

            $total = RecordsCertificationsNoTd::select('copies')
                                    ->whereNotNull('created_at')
                                    ->whereBetween('created_at', ["$y-01-01 00:00:00", "$y-12-31 23:59:59"])
                                    ->sum('copies');
    
            $count = RecordsCertificationsNoTd::select(DB::raw('sum(copies) as sums'), DB::raw("DATE_FORMAT(created_at,'%m') as months"))
                                    ->whereNotNull('created_at')
                                    ->whereYear('created_at', $y)
                                    ->groupBy('months')
                                    ->get();
        } elseif($cert == 'Letter to National Archives'){

            $total = RecordsCertificationLetterNationalArchive::select('copies')
                                    ->whereNotNull('created_at')
                                    ->whereBetween('created_at', ["$y-01-01 00:00:00", "$y-12-31 23:59:59"])
                                    ->sum('copies');
    
            $count = RecordsCertificationLetterNationalArchive::select(DB::raw('sum(copies) as sums'), DB::raw("DATE_FORMAT(created_at,'%m') as months"))
                                    ->whereNotNull('created_at')
                                    ->whereYear('created_at', $y)
                                    ->groupBy('months')
                                    ->get();
        } else{

            $total = RecordsCertification::select('copies')
                                    ->whereNotNull('created_at')
                                    ->whereBetween('created_at', ["$y-01-01 00:00:00", "$y-12-31 23:59:59"])
                                    ->where(function($query) use ($cert, $year) {
                                        if($cert == "Property Ownership"){
                                            return $query->where('ctr_no', 'like', 'P%');
                                        } elseif ($cert == "No Property Holdings") {
                                            return $query->where('ctr_no', 'like', 'N%');
                                        }
                                    })
                                    ->where('printed', 1)
                                    ->sum('copies');
    
            $count = RecordsCertification::select(DB::raw('sum(copies) as sums'), DB::raw("DATE_FORMAT(created_at,'%m') as months"))
                                    ->whereNotNull('created_at')
                                    ->whereYear('created_at', $y)
                                    ->where(function($query) use ($cert) {
                                        if($cert == "Property Ownership"){
                                            return $query->where('ctr_no', 'like', 'P%');
                                        } elseif ($cert == "No Property Holdings") {
                                            return $query->where('ctr_no', 'like', 'N%');
                                        }
                                    })
                                    ->where('printed', 1)
                                    ->groupBy('months')
                                    ->get();
        }


        $countByMonth   = self::countByMonth($count);
        $output         = self::formatData($countByMonth, $y);

        $output['total_count']  = $total;
        $output['table_data']   = self::getTableData($output['curr_year'], $cert);
        $output['year_list']    = self::getYearList();
        $output['month_list']   = self::getMonthList($y);
        $output['day_list']     = self::getAllDays($output['table_data']);

        return $output;

    }

    // public function getYearMonth(Request $request){
        
    //     list($year, $month) = explode("-", $request['yearMonth']);

    //     $list = Storage::disk('svnas')->allDirectories("/released_taxdecs");
    //     $files = Storage::disk('svnas')->allFiles("/released_taxdecs");

    //     \Log::info($files);

    // }

    public function exportToExcel(Request $request){

        $year   = $request['year'];
        $month  = $request['month'];
        $day    = $request['day'];
        $type   = $request['type'];
        $cert   = $request['cert'];

        $total = $request['total'];

        if($type == 'Yearly'){

            if($cert == 'No Tax Declaration'){
                $data = RecordsCertificationsNoTd::selectRaw("ctr_no, type, requestor, certifier_1, certifier_2, prepared_by, verifier, DATE_FORMAT(records_certifications_no_tds.created_at, '%M %e, %Y') as date_released, copies")
                        ->whereYear('records_certifications_no_tds.created_at', $year)
                        ->get()
                        ->toArray();

                foreach ($data as $key => $value) {
                    $get_names = self::getNames([$value['prepared_by'], $value['verifier']]);
                    $data[$key]['prepared_by_name']   = $get_names[0];
                    $data[$key]['verifier_name']      = $get_names[1];
                }

            } elseif($cert == 'Letter to National Archives'){
                $data = RecordsCertificationLetterNationalArchive::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, user_id, DATE_FORMAT(records_certification_letter_national_archives.created_at, '%M %e, %Y') as date_released, copies")
                        ->get()
                        ->toArray();

                foreach ($data as $key => $value) {
                    $get_names = self::getNames([$value['user_id']]);
                    $data[$key]['printed_name']   = $get_names[0];
                }

            } elseif($cert == 'Property Ownership'){
                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->where('printed', 1)
                        ->where('type', 'Property Ownership')
                        ->get()
                        ->toArray();

            } elseif($cert == 'No Property Holdings'){
                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->where('printed', 1)
                        ->where('type', 'No Property Holdings')
                        ->get()
                        ->toArray();

            } else{
                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->where('printed', 1)
                        ->get()
                        ->toArray();
            }

            $newData = self::groupByMonthReport($data);

            $total = count($data);
            
        } elseif ($type == 'Monthly') {

            if($cert == 'No Tax Declaration'){

                $data = RecordsCertificationsNoTd::selectRaw("ctr_no, type, requestor, certifier_1, certifier_2, prepared_by, verifier, DATE_FORMAT(records_certifications_no_tds.created_at, '%M %e, %Y') as date_released, copies")
                        ->leftJoin('profiles', 'records_certifications_no_tds.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications_no_tds.created_at', $year)
                        ->whereMonth('records_certifications_no_tds.created_at', self::getMonthValueByNumber($month))
                        ->get()
                        ->toArray();

                foreach ($data as $key => $value) {
                    $get_names = self::getNames([$value['prepared_by'], $value['verifier']]);
                    $data[$key]['prepared_by_name']   = $get_names[0];
                    $data[$key]['verifier_name']      = $get_names[1];
                }


            } elseif($cert == 'Letter to National Archives'){

                $data = RecordsCertificationLetterNationalArchive::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, user_id, DATE_FORMAT(records_certification_letter_national_archives.created_at, '%M %e, %Y') as date_released, copies")
                        ->whereYear('records_certification_letter_national_archives.created_at', $year)
                        ->whereMonth('records_certification_letter_national_archives.created_at', self::getMonthValueByNumber($month))
                        ->get()
                        ->toArray();

                foreach ($data as $key => $value) {
                    $get_names = self::getNames([$value['user_id']]);
                    $data[$key]['printed_name']   = $get_names[0];
                }

            } elseif($cert == 'Property Ownership'){

                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->whereMonth('records_certifications.created_at', self::getMonthValueByNumber($month))
                        ->where('printed', 1)
                        ->where('type', 'Property Ownership')
                        ->get()
                        ->toArray();

            } elseif($cert == 'No Property Holdings'){

                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->whereMonth('records_certifications.created_at', self::getMonthValueByNumber($month))
                        ->where('printed', 1)
                        ->where('type', 'No Property Holdings')
                        ->get()
                        ->toArray();

            } else{

                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->whereMonth('records_certifications.created_at', self::getMonthValueByNumber($month))
                        ->where('printed', 1)
                        ->get()
                        ->toArray();

            }

            $newData = self::groupByDayReport($data);
            
            $total = count($data);

        } else{

            if($cert == 'No Tax Declaration'){

                $data = RecordsCertificationsNoTd::selectRaw("ctr_no, type, requestor, certifier_1, certifier_2, prepared_by, verifier, DATE_FORMAT(records_certifications_no_tds.created_at, '%M %e, %Y') as date_released, copies")
                        ->leftJoin('profiles', 'records_certifications_no_tds.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications_no_tds.created_at', $year)
                        ->whereMonth('records_certifications_no_tds.created_at', self::getMonthValueByNumber($month))
                        ->whereDay('records_certifications_no_tds.created_at', sprintf('%02d', $day))
                        ->get()
                        ->toArray();

                foreach ($data as $key => $value) {
                    $get_names = self::getNames([$value['prepared_by'], $value['verifier']]);
                    $data[$key]['prepared_by_name']   = $get_names[0];
                    $data[$key]['verifier_name']      = $get_names[1];
                }


            } elseif($cert == 'Letter to National Archives'){

                $data = RecordsCertificationLetterNationalArchive::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, records_certification_letter_national_archives.user_id, DATE_FORMAT(records_certification_letter_national_archives.created_at, '%M %e, %Y') as date_released, copies")
                        ->whereYear('records_certification_letter_national_archives.created_at', $year)
                        ->whereMonth('records_certification_letter_national_archives.created_at', self::getMonthValueByNumber($month))
                        ->whereDay('records_certification_letter_national_archives.created_at', sprintf('%02d', $day))
                        ->get()
                        ->toArray();

                foreach ($data as $key => $value) {
                    $get_names = self::getNames([$value['user_id']]);
                    $data[$key]['printed_name']   = $get_names[0];
                }

            } else{

                $data = RecordsCertification::selectRaw("ctr_no, type, requestor, printed_by, prepared_by, DATE_FORMAT(records_certifications.created_at, '%M %e, %Y') as date_released, profiles.first_name, profiles.middle_name, profiles.last_name, copies")
                        ->leftJoin('profiles', 'records_certifications.verifier', '=', 'profiles.id')
                        ->whereYear('records_certifications.created_at', $year)
                        ->whereMonth('records_certifications.created_at', self::getMonthValueByNumber($month))
                        ->whereDay('records_certifications.created_at', sprintf('%02d', $day))
                        ->where('printed', 1)
                        ->get()
                        ->toArray();
                }

            $newData = self::groupByDayReport($data);
            
            $total = count($data);   

        }

        return self::createExcel($newData, $total, $type, $month, $day, $year, $cert);

    }

    public function getFile(Request $request){

        $cert = $request['cert'];

        if($cert == 'No Tax Declaration'){
            $path = RecordsCertificationsNoTd::select('file_path')->where('id', $request->id)->first();
        } elseif($cert == 'Letter to National Archives'){
            $path = RecordsCertificationLetterNationalArchive::select('file_path')->where('id', $request->id)->first();
        } else {
            $path = RecordsCertification::select('file_path')->where('id', $request->id)->first();
        }

        return Storage::disk('svnas')->get($path->file_path);

    }

    private function getNames($data){
        
        $names = [];

        foreach ($data as $key => $value) {
            $name = Profile::select('first_name', 'middle_name', 'last_name')->where('user_id', $value)->first();
            $names[] = $name['first_name'] . " " . $name['middle_name'] . " " . $name['last_name'];
        }

        return $names;


    }

    private function groupByFolder($data){

        $output = [];

        foreach ($data as $key => $value) {
            
            $exp = explode("/", $value);

            if(strpos($exp[3], ".pdf") !== false){
                $output[$exp[2]][] = $value;
            }
            
        }

        return $output;

    }

    private function groupByMonthReport($data){

        $output = [];

        foreach ($data as $key => $value) {
            
            $exp = explode(" ", $value['date_released']);
            $output[$exp[0]][] = $value;
            
        }

        return $output;

    }

    private function groupByDayReport($data){

        $output = [];

        foreach ($data as $key => $value) {
            
            $exp = explode(" ", $value['date_released']);
            $day = str_replace(",", "", $exp[1]);
            
            $output[$day][] = $value;
            
        }

        return $output;        
    }

    private function countByMonth($data){

        $output = [
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        ];

        $month_arr = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec',
        ];

        foreach ($data as $key => $value) {

            // $exp_key = explode("-", $value['monthyear']);

            $month_word             = $month_arr[sprintf('%02d', $value['months'])];
            $output[$month_word]    = $value['sums'];

        }

        // foreach ($data as $key => $value) {

        //     $exp_key = explode("-", $key);

        //     $month_word             = $month_arr[$exp_key[1]];
        //     $output[$month_word]    = $output[$month_word] + count($value);

        // }

        return $output;

    }

    private function formatData($data, $y){

        $chart_data = [];
        $curr_year  = $y;

        foreach ($data as $key => $value) {
            $chart_data[] = array(
                'month' => $key,
                'count' => $value
            );
        }

        return compact('chart_data', 'curr_year');

    }

    private function getTableData($year, $cert){

        if($cert == 'No Tax Declaration'){
            $data = RecordsCertificationsNoTd::selectRaw("id, ctr_no, requestor, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies")
                                    ->whereYear('created_at', $year)
                                    ->get();
        }

        elseif($cert == 'Letter to National Archives'){
            $data = RecordsCertificationLetterNationalArchive::selectRaw("id, ctr_no, requestor, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies")
                                    ->whereYear('created_at', $year)
                                    ->get();
        }

        else{
            $data = RecordsCertification::selectRaw("id, ctr_no, requestor, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies")
                                    ->whereYear('created_at', $year)
                                    ->where(function($query) use ($cert) {
                                        if($cert == "Property Ownership"){
                                            return $query->where('ctr_no', 'like', 'P%');
                                        } elseif ($cert == "No Property Holdings") {
                                            return $query->where('ctr_no', 'like', 'N%');
                                        }
                                    })
                                    ->where('printed', 1)
                                    ->get();
        }

        return $data;

    }

    private function getYearList(){

        $list = [];

        $currYear = (int) date('Y');

        while ($currYear >= 2022) {
            $list[] = $currYear;
            $currYear--;
        }

        return $list;

    }

    private function getMonthList($y){

        $month_arr = [
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];

        $output = [];

        $list = RecordsCertification::selectRaw("Distinct month(created_at) as month")
                            ->whereYear('created_at', '=', $y)
                            ->get();

        foreach ($list as $key => $value) {
            $output[] = $month_arr[$value['month']];
        }

        return $output;

    }
    
    private function getAllDays($data){
        
        $output = [];

        foreach ($data as $key => $value) {
            
            $date = $value['date_released'];

            $exp = explode(" ", $date);

            $month  = $exp[0];
            $day    = str_replace(",", "", $exp[1]);

            if(!isset($output[$month])){
                $output[$month] = [];
            }

            if(!in_array($day, $output[$month])){
                $output[$month][] = str_replace(",", "", $day);
            }

        }

        return $output;

    }

    private function getMonthValueByNumber($month){

        switch ($month) {
            case 'January':
                return "01";
                break;

            case 'February':
                return "02";
                break;

            case 'March':
                return "03";
                break;

            case 'April':
                return "04";
                break;

            case 'May':
                return "05";
                break;

            case 'June':
                return "06";
                break;

            case 'July':
                return "07";
                break;

            case 'August':
                return "08";
                break;

            case 'September':
                return "09";
                break;

            case 'October':
                return "10";
                break;

            case 'November':
                return "11";
                break;

            case 'December':
                    return "12";
                    break;
            
            default:
                return 0;
                break;
        }

    }

    private function getTotalByProcessor($data, $cert){

        $output = [];

        foreach ($data as $key => $value) {
            foreach ($value as $k => $v) {

                if($cert == 'No Tax Declaration'){
                    $name = strtoupper($v['prepared_by_name']);
                } else{
                    $name = strtoupper($v['prepared_by']);
                }
                
                
                if(!isset($output[$name])){
                    $output[$name] = $v['copies'];
                } else{
                    $output[$name] = $output[$name] + $v['copies'];
                }
            }
        }

        ksort($output);

        return $output;

    }

    private function createExcel($data, $t, $type, $month, $day, $year, $cert){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle("Summary");
        $spreadsheet->getSheetByName("Summary");

        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->mergeCells('B2:M3');
        $worksheet->getStyle("B2:M3")->getFont()->setBold(true);

        $worksheet->getStyle('B2:M3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('B2:M3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $worksheet->setCellValue("B2", "$type Report of Released Certification");
        $worksheet->getStyle("B2")->getFont()->setSize(16);

        $worksheet->mergeCells('B4:M4');
        $worksheet->getStyle('B4:M4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("B4", $cert == 'All' ? "Property Ownership + No Property Holdings" : $cert);
        $worksheet->getStyle("B4")->getFont()->setSize(12);

        $worksheet->mergeCells('B5:M5');
        $worksheet->getStyle('B5:M5')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("B5", "as of:  " . date("F j, Y"));
        $worksheet->getStyle("B5")->getFont()->setSize(8);

        // Set Borders
        $worksheet->getStyle('B2:M5')
                ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $worksheet->getStyle('B2:M5')
                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $worksheet->getStyle('B2:M5')
                ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $worksheet->getStyle('B2:M5')
                ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                

        $worksheet->mergeCells('D8:F8');
        $worksheet->mergeCells('D9:F9');
        $worksheet->mergeCells('D10:F10');
        $worksheet->mergeCells('D11:F11');

        $worksheet->mergeCells('I8:K8');
        $worksheet->mergeCells('I9:K9');
        $worksheet->mergeCells('I10:K10');
        $worksheet->mergeCells('I11:K11');

        $worksheet->getColumnDimension('F')->setWidth(40);
        $worksheet->getColumnDimension('K')->setWidth(40);

        if($cert == 'No Tax Declaration'){
            $worksheet->setCellValue("D8", "Total Request for No Tax Declaration: ");
            $worksheet->setCellValue("I8", "Total Released for No Tax Declaration (Per Copy): ");

            $worksheet->mergeCells('D15:F15');
            $worksheet->setCellValue("D15", "Total Processed By: ");

            $total_processed_by = self::getTotalByProcessor($data, $cert);

            $row = 17;

            foreach($total_processed_by as $key => $value){
                $worksheet->mergeCells("D{$row}:F{$row}");
                $worksheet->setCellValue("D{$row}", $key)
                            ->setCellValue("G{$row}", $value);
                $row++;
            }

            $g_total    = 0;

            foreach ($data as $key => $value) {

                if($type == 'Yearly'){
                    $tab_name = $key;
                } elseif ($type == 'Monthly') {
                    $tab_name = $month . " " . $key;
                } else{
                    $tab_name = $month . " " . $day . " " . $year;
                }

                $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $tab_name);
                $spreadsheet->addSheet($myWorkSheet);
                $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);
                
                $worksheet = $spreadsheet->getActiveSheet();
        
                $worksheet->getColumnDimension('A')->setWidth(20);
                $worksheet->getColumnDimension('B')->setWidth(30);
                $worksheet->getColumnDimension('C')->setWidth(60);
                $worksheet->getColumnDimension('D')->setWidth(50);
                $worksheet->getColumnDimension('E')->setWidth(30);
                $worksheet->getColumnDimension('F')->setWidth(30);
                $worksheet->getColumnDimension('G')->setWidth(30);
                $worksheet->getColumnDimension('H')->setWidth(20);
                $worksheet->getColumnDimension('I')->setWidth(10);
                $worksheet
                        ->setCellValue("A9", "Control Number")
                        ->setCellValue("B9", "Type")
                        ->setCellValue("C9", "Requestor")
                        ->setCellValue("D9", "Certified By")
                        ->setCellValue("E9", "Certified By")
                        ->setCellValue("F9", "Prepared By")
                        ->setCellValue("G9", "Verified By")
                        ->setCellValue("H9", "Date Released")
                        ->setCellValue("I9", "Copies");
        
                $startRow = 10;
                $row = $startRow;

                $total = 0;

                foreach ($value as $k => $val) {

                    $total = $total + $val['copies'];

                    $worksheet->setCellValue("A{$row}", $val['ctr_no']);
                    $worksheet->setCellValue("B{$row}", $val['type']);
                    $worksheet->setCellValue("C{$row}", $val['requestor']);
                    $worksheet->setCellValue("D{$row}", $val['certifier_1']);
                    $worksheet->setCellValue("E{$row}", $val['certifier_2']);
                    $worksheet->setCellValue("F{$row}", $val['prepared_by_name']);
                    $worksheet->setCellValue("G{$row}", $val['verifier_name']);
                    $worksheet->setCellValue("H{$row}", $val['date_released']);
                    $worksheet->setCellValue("I{$row}", $val['copies']);

                    $worksheet->getStyle("B{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $row++;
                    
                }

                $append_date = "";

                if($type == 'Yearly'){
                    $append_date = $key;
                } elseif ($type == 'Monthly') {
                    $append_date = $month . " " . $key;
                } else{
                    $append_date = $month . " " . $day . ", " . $year;   
                }


                $worksheet->setCellValue("A3", "Total Released for {$append_date}: ")
                            ->setCellValue("C3", $total);

                $worksheet->getStyle('C3:C6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $worksheet->setSelectedCells('A1');

                $g_total = $g_total + $total;

            }

            
            $worksheet = $spreadsheet->setActiveSheetIndex(0);
    
            $worksheet->setCellValue("G8", $t)
                        ->setCellValue("L8", $g_total);


        } elseif($cert == 'Letter to National Archives'){

            $worksheet->setCellValue("D8", "Total Request for Letter to National Archives: ");
            $worksheet->setCellValue("I8", "Total Released for Letter to National Archives (Per Copy): ");

            $worksheet->mergeCells('D15:F15');
            $worksheet->setCellValue("D15", "Total Processed By: ");

            $total_processed_by = self::getTotalByProcessor($data, $cert);

            $row = 17;

            foreach($total_processed_by as $key => $value){
                $worksheet->mergeCells("D{$row}:F{$row}");
                $worksheet->setCellValue("D{$row}", $key)
                            ->setCellValue("G{$row}", $value);
                $row++;
            }

            $g_total    = 0;

            foreach ($data as $key => $value) {

                if($type == 'Yearly'){
                    $tab_name = $key;
                } elseif ($type == 'Monthly') {
                    $tab_name = $month . " " . $key;
                } else{
                    $tab_name = $month . " " . $day . " " . $year;
                }

                $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $tab_name);
                $spreadsheet->addSheet($myWorkSheet);
                $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);
                
                $worksheet = $spreadsheet->getActiveSheet();
        
                $worksheet->getColumnDimension('A')->setWidth(20);
                $worksheet->getColumnDimension('B')->setWidth(30);
                $worksheet->getColumnDimension('C')->setWidth(60);
                $worksheet->getColumnDimension('D')->setWidth(50);
                $worksheet->getColumnDimension('E')->setWidth(30);
                $worksheet->getColumnDimension('F')->setWidth(30);
                $worksheet->getColumnDimension('G')->setWidth(20);
                $worksheet->getColumnDimension('H')->setWidth(10);

                $worksheet
                        ->setCellValue("A9", "Control Number")
                        ->setCellValue("B9", "Type")
                        ->setCellValue("C9", "Requestor")
                        ->setCellValue("D9", "Printed By")
                        ->setCellValue("E9", "Certified By")
                        ->setCellValue("F9", "Prepared By")
                        ->setCellValue("G9", "Date Released")
                        ->setCellValue("H9", "Copies");
        
                $startRow = 10;
                $row = $startRow;

                $total = 0;

                foreach ($value as $k => $val) {

                    $total = $total + $val['copies'];

                    $worksheet->setCellValue("A{$row}", $val['ctr_no']);
                    $worksheet->setCellValue("B{$row}", $val['type']);
                    $worksheet->setCellValue("C{$row}", $val['requestor']);
                    $worksheet->setCellValue("D{$row}", $val['printed_name']);
                    $worksheet->setCellValue("E{$row}", $val['printed_by']);
                    $worksheet->setCellValue("F{$row}", $val['prepared_by']);
                    $worksheet->setCellValue("G{$row}", $val['date_released']);
                    $worksheet->setCellValue("H{$row}", $val['copies']);

                    $worksheet->getStyle("B{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $row++;
                    
                }

                $append_date = "";

                if($type == 'Yearly'){
                    $append_date = $key;
                } elseif ($type == 'Monthly') {
                    $append_date = $month . " " . $key;
                } else{
                    $append_date = $month . " " . $day . ", " . $year;   
                }


                $worksheet->setCellValue("A3", "Total Released for {$append_date}: ")
                            ->setCellValue("C3", $total);

                $worksheet->getStyle('C3:C6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $worksheet->setSelectedCells('A1');

                $g_total = $g_total + $total;

            }

            
            $worksheet = $spreadsheet->setActiveSheetIndex(0);
    
            $worksheet->setCellValue("G8", $t)
                        ->setCellValue("L8", $g_total);
        } else{

            if($cert == "Property Ownership"){
                $worksheet->setCellValue("D8", "Total Request for Property Ownership: ");
                $worksheet->setCellValue("I8", "Total Released for Property Ownership (Per Copy): ");
            } elseif($cert == "No Property Holdings"){
                $worksheet->setCellValue("D8", "Total Request for No Property Holdings: ");
                $worksheet->setCellValue("I8", "Total Released for No Property Holdings (Per Copy): ");
            } else{
                $worksheet->setCellValue("D8", "Total Request for Property Ownership: ")
                            ->setCellValue("D9", "Total Request for No Property Holdings: ");

                $worksheet->setCellValue("I8", "Total Released for Property Ownership (Per Copy): ")
                            ->setCellValue("I9", "Total Released for No Property Holdings (Per Copy): ");
            }

            if($cert == "All"){
                $worksheet->setCellValue("D11", "Total Request (Property Ownership + No Property Holdings): ")
                            ->setCellValue("G11", $t);

                $worksheet->setCellValue("I11", "Total Released (Property Ownership + No Property Holdings): ");

            }

            $worksheet->mergeCells('D15:F15');
            $worksheet->setCellValue("D15", "Total Processed By: ");

            $total_processed_by = self::getTotalByProcessor($data, $cert);

            $row = 17;

            foreach($total_processed_by as $key => $value){
                $worksheet->mergeCells("D{$row}:F{$row}");
                $worksheet->setCellValue("D{$row}", $key)
                            ->setCellValue("G{$row}", $value);
                $row++;
            }

            $total_p = 0;
            $total_n = 0;

            $total_r_p = 0;
            $total_r_n = 0;

            foreach ($data as $key => $value) {

                $p = 0;
                $n = 0;

                if($type == 'Yearly'){
                    $tab_name = $key;
                } elseif ($type == 'Monthly') {
                    $tab_name = $month . " " . $key;
                } else{
                    $tab_name = $month . " " . $day . " " . $year;
                }

                $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $tab_name);
                $spreadsheet->addSheet($myWorkSheet);
                $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);
                
                $worksheet = $spreadsheet->getActiveSheet();
        
                $worksheet->getColumnDimension('A')->setWidth(20);
                $worksheet->getColumnDimension('B')->setWidth(30);
                $worksheet->getColumnDimension('C')->setWidth(60);
                $worksheet->getColumnDimension('D')->setWidth(50);
                $worksheet->getColumnDimension('E')->setWidth(30);
                $worksheet->getColumnDimension('F')->setWidth(30);
                $worksheet->getColumnDimension('G')->setWidth(20);
                $worksheet->getColumnDimension('H')->setWidth(10);
            
                $worksheet
                        ->setCellValue("A9", "Control Number")
                        ->setCellValue("B9", "Type")
                        ->setCellValue("C9", "Requestor")
                        ->setCellValue("D9", "Printed By")
                        ->setCellValue("E9", "Prepared By")
                        ->setCellValue("F9", "Verified By")
                        ->setCellValue("G9", "Date Released")
                        ->setCellValue("H9", "Copies");
        
                $startRow = 10;
                $row = $startRow;

                foreach ($value as $k => $val) {

                    if(strpos($val['type'], "Property Ownership") !== false){
                        $p = $p + $val['copies'];
                        $total_r_p++;
                    } else{
                        $n = $n + $val['copies'];
                        $total_r_n++;
                    }
        
                    $worksheet->setCellValue("A{$row}", $val['ctr_no']);
                    $worksheet->setCellValue("B{$row}", $val['type']);
                    $worksheet->setCellValue("C{$row}", $val['requestor']);
                    $worksheet->setCellValue("D{$row}", $val['printed_by']);
                    $worksheet->setCellValue("E{$row}", $val['prepared_by']);
                    $worksheet->setCellValue("F{$row}", $val['first_name'] . " " . $val['middle_name'] . " " . $val['last_name']);
                    $worksheet->setCellValue("G{$row}", $val['date_released']);
                    $worksheet->setCellValue("H{$row}", $val['copies']);


                    $worksheet->getStyle("B{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $row++;
                    
                }

                $append_date = "";

                if($type == 'Yearly'){
                    $append_date = $key;
                } elseif ($type == 'Monthly') {
                    $append_date = $month . " " . $key;
                } else{
                    $append_date = $month . " " . $day . ", " . $year;   
                }

                $total_sheet = $n + $p;

                if($cert == "Property Ownership"){
                    $worksheet->setCellValue("A3", "Total Released for {$append_date}: ")
                                ->setCellValue("C3", $total_sheet);
                } elseif($cert == "No Property Holdings"){
                    $worksheet->setCellValue("A3", "Total Released for {$append_date}: ")
                                ->setCellValue("C3", $total_sheet);
                } else{
                    $worksheet->setCellValue("A3", "Total Released Property Ownership for {$append_date}: ")
                                ->setCellValue("C3", $p)
                                ->setCellValue("A4", "Total Released No Property Holdings for {$append_date}: ")
                                ->setCellValue("C4", $n)
                                ->setCellValue("A6", "Total Released for {$append_date}: ")
                                ->setCellValue("C6", $total_sheet);
                }
                            

                $worksheet->getStyle('C3:C6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $worksheet->setSelectedCells('A1');

                $total_p = $total_p + $p;
                $total_n = $total_n + $n;

            }

            $worksheet = $spreadsheet->setActiveSheetIndex(0);

            if($cert == "Property Ownership"){
                $worksheet->setCellValue("G8", $total_r_p)
                            ->setCellValue("L8", $total_p);
            } elseif($cert == "No Property Holdings"){
                $worksheet->setCellValue("G8", $total_r_n)
                            ->setCellValue("L8", $total_n);
            } else{
                $worksheet->setCellValue("G8", $total_r_p)
                            ->setCellValue("G9", $total_r_n)
                            ->setCellValue("L8", $total_p)
                            ->setCellValue("L9", $total_n)
                            ->setCellValue("L11", $total_p + $total_n);
            } 

        }

        $worksheet->setSelectedCells('A1');

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

}
