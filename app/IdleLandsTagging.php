<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdleLandsTagging extends Model
{
    use HasFactory;
    
    protected $guarded = [];
}
