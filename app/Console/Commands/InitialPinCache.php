<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Api\CacheController;

class InitialPinCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initial:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initial caching of data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cache = new CacheController();
        $status = $cache->createCache();
        print $status;
    }
}
