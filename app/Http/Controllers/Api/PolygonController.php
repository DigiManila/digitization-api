<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\AssessmentRoll;

use App\MapPolygon;
use App\BgyPolygon;
use App\DistrictPolygon;
use App\PoliticalBoundary;
use App\MappingBgyPolygon;
use App\MappingPoliticalBoundary;
use App\MappingActiveMap;
use App\MappingNotActivePinMap;
use App\Road;
use App\ZoneDistrict;

use Illuminate\Support\Facades\Redis;

use File;
use Storage;

class PolygonController extends Controller
{

    public function getSinglePolygon(Request $request){

        // $pin = substr($request['pin'],0,18);
        $pin = $request['pin'];

        $data = MapPolygon::select('gmap_polygon', 'barangay', 'center_coords')->where('pin', $pin)->first();

        if($data){

            $data['gmap_polygon'] = str_replace("|", ",", $data['gmap_polygon']);
            $data['gmap_polygon'] = str_replace("\\", "", $data['gmap_polygon']);

            $all_barangay = MapPolygon::select('pin', 'gmap_polygon')
                            ->where('barangay', $data['barangay'])
                            ->where('pin', '!=', $pin)
                            ->get();

            $data['all_barangay'] = self::parseBarangay($all_barangay);

        }

        $assessment_data = [];

        $buildingMachinery_data = AssessmentRoll::select('pin')
                                    ->distinct('pin')
                                    ->where('pin', 'like',  $request['pin'].'%')
                                    ->where('cancelled', 0)
                                    ->whereRaw('LENGTH(pin) > 18')
                                    ->orderBy('pin', 'ASC')
                                    ->get()
                                    ->toArray();
        
       
        $building = [];
        $machinery = [];

        foreach ($buildingMachinery_data as $bm_data){
            if (strpos($bm_data['pin'],'-B') !== false){
                $building[] = $bm_data['pin'];
            }
            else if (strpos($bm_data['pin'],'-M') !== false){
                $machinery[] = $bm_data['pin'];
            }
        }

        $data['building_list'] = $building;
        $data['machinery_list'] = $machinery;
            
        $assessment_query = AssessmentRoll::where('pin', $request['pin'])
                        ->where('cancelled', 0)
                        ->get()
                        ->toArray();


        $pin = $request['pin'];

        foreach($assessment_query as $key => $value){

            $assessment_data[] = array(
                 
                'pin'                   => $value['pin'],
                'owner_name'            => $value['owner_name'],
                'address'               => $value['address'],
                'lot'                   => $value['lot'],
                'block'                 => $value['block'],
                'tct'                   => $value['tct'],
                'tax_effectivity'       => $value['tax_effectivity'],
                'status'                => $value['status'],
                'current_arp'           => $value['current_arp'],
                'previous_arp'          => $value['previous_arp'],
                'kind'                  => self::getKindType($value['kind']),
                'actual_use'            => self::getActualUse($value['kind']),
                'area'                  => number_format($value['area'],2,".",","). ".sqm",
                'market_value'          => "₱ " . number_format($value['market_value'],2,".",","),
                'assessment_level'      => $value['assessment_level'],
                'currentAssessValue'    => "₱ " .number_format($value['currentAssessValue'],2,".",","),
                'previousAssessValue'   => "₱ ".number_format($value['previousAssessValue'],2,".",","),
                'taxable'               => self::getTaxable($value['taxable']),

            );

        }

        $pictures = self::getPicturesUrl($pin);

        $data['pictures']       = $pictures;
        $data['assessment']     = array(
                                        'pin' => $pin,
                                        'data' => $assessment_data
                                    );

        $mapping = $request['mapping'];

        $data['bgy_polygon'] = [];

        // Get all unused barangay
        if(isset($data['barangay'])){

            $district = BgyPolygon::select('district')
                            ->where('bgy', (int)$data['barangay'])
                            ->first();
            // $district = ZoneDistrict::select('district')
            //                 // ->where('bgy', (int)$data['barangay'])
            //                 ->where('barangay', $data['barangay'])
            //                 ->first();


            if($mapping == 1){
                $unused_bgy = MappingBgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$data['barangay'])
                                ->where('district', $district['district'])
                                ->get();
            } else{
                $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$data['barangay'])
                                ->where('district', $district['district'])
                                ->get();
            }

            $unused_district = DistrictPolygon::select('district', 'polygon')
                                ->where('district', '!=', $district['district'])
                                ->get();
           
            $data['bgy_polygon'] = self::parseUnusedBarangay($unused_bgy);
            $data['dist_polygon'] = self::parseUnusedDistrict($unused_district);
            $data['district'] = $district;
        }


        // Get District
        return $data;
    }

    public function getSinglePolygonNoSelection(Request $request){

        $bgy = $request['bgy'];
        $brgyNo = str_pad($bgy,3,'0',STR_PAD_LEFT);

        $pin = MapPolygon::select('pin')
                ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                ->first();

        $request = new \Illuminate\Http\Request();
        $request->replace(['pin' => $pin['pin']]);
        return self::getSinglePolygon($request);
        
    }

    public function getDistrictClick(Request $request){

        $district = $request['district'];

        $bgy = BgyPolygon::select('bgy')
                ->where('district', $district)
                ->orderBy('id', 'asc')
                ->first();

        $brgyNo = str_pad($bgy['bgy'],3,'0',STR_PAD_LEFT);

        $pin = MapPolygon::select('pin')
                ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                ->first();

        $request = new \Illuminate\Http\Request();
        $request->replace(['pin' => $pin['pin']]);
        return self::getSinglePolygon($request);
        
    }

    public function getBuildingMachineryPin(Request $request) {
        $assessment_data=[];
        $buildingMachnineryReturn=[];

        $buildingMachnineryReturn['pin'] = $request['pin'];

        $buildingMachinery_data = AssessmentRoll::where('pin',  $request['pin'])
        ->where('cancelled', 0)
        ->orderBy('pin', 'ASC')
        ->get()
        ->toArray();

        \Log::info($buildingMachinery_data);

        foreach($buildingMachinery_data as $key => $value){

            $assessment_data[] = array(

                'pin'                   => $value['pin'],
                'owner_name'            => $value['owner_name'],
                'address'               => $value['address'],
                'lot'                   => $value['lot'],
                'block'                 => $value['block'],
                'tct'                   => $value['tct'],
                'tax_effectivity'       => $value['tax_effectivity'],
                'status'                => $value['status'],
                'current_arp'           => $value['current_arp'],
                'previous_arp'          => $value['previous_arp'],
                'kind'                  => self::getKindType($value['kind']),
                'actual_use'            => self::getActualUse($value['kind']),
                'area'                  => number_format($value['area'],2,".",","). ".sqm",
                'market_value'          => "₱ " . number_format($value['market_value'],2,".",","),
                'assessment_level'      => $value['assessment_level'],
                'currentAssessValue'    => "₱ " .number_format($value['currentAssessValue'],2,".",","),
                'previousAssessValue'   => "₱ ".number_format($value['previousAssessValue'],2,".",","),
                'taxable'               => self::getTaxable($value['taxable']),

            );

        }

        $buildingMachnineryReturn['data'] = $assessment_data;

        // $faasFiles = Storage::files('faas');
        // $matchingFiles = preg_grep('/faas\/' . $request['pin'] . '\-\d{10}\.pdf/', $faasFiles);

        // $faas_files = [];

        // foreach($matchingFiles as $val){
        //     $faas_files = base64_encode(file_get_contents( storage_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . $val));
        // }

        // $buildingMachnineryReturn['faas'] = $faas_files;

        // \Log::info($buildingMachinery_data);
        
        return $buildingMachnineryReturn;
    }

    public function getTraceAssessment20052014(Request $request) {
        $assessment_data=[];
       $getTraceAssessmentReturn=[];

       $getTraceAssessmentReturn['current_arp'] = $request['current_arp'];

    $getTraceAssessment_data = AssessmentRoll::where('current_arp',  $request['current_arp'])
       ->get()
       ->toArray();

       foreach($getTraceAssessment_data as $key => $value){

           $assessment_data[] = array(
               
               'pin'                   => $value['pin'],
           
               'cancelled_by_td'       => $value['cancelled_by_td'],
               'owner_name'            => $value['owner_name'],
            //    'location'              => $value['location'],
               'lot'                   => $value['lot'],
               'block'                 => $value['block'],
               'tct'                   => $value['tct'],
            //    'tct_date'              => $value['tct_date'],
            //    'cct'                   => $value['cct'],
            //    'cct_date'              => $value['cct_date'],
               'tax_effectivity'      => $value['tax_effectivity'],
               'status'                => $value['status'],
               'current_arp'           => $value['current_arp'],
               'previous_arp'          => $value['previous_arp'],
               'kind'                  => self::getKindType($value['kind']),
               'actual_use'            => $value['actualUse'],
               'area'                  => number_format($value['area'],2,".",",") . ".sqm",
               'market_value'          => "₱ " . number_format($value['market_value'],2,".",","),
               'currentAssessValue'    => "₱ " .number_format($value['currentAssessValue'],2,".",","),
              
               

           );

       }

       $getTraceAssessmentReturn['data'] = $assessment_data;

       
       return $getTraceAssessmentReturn;
   }

    public function getTraceAssessmentPin20052014(Request $request) {

        $assessment_data = [];
        $getTraceAssessmentReturn = [];

        $getTraceAssessmentReturn['pin'] = $request['pin'];

        $getTraceAssessment_data = AssessmentRoll::where('pin',  $request['pin'])
                                                    ->orderBy('current_arp', 'DESC')
                                                    ->get()
                                                    ->toArray();
        
       

        foreach($getTraceAssessment_data as $key => $value){

            $assessment_data[] = array(
                
                'pin'                   => $value['pin'],
                //comment ko muna wala kasi to sa bagong assessment roll
                // 'cancelled_by_td'       => $value['cancelled_by_td'],
                'owner_name'            => $value['owner_name'],
                //    'location'              => $value['location'],
                'lot'                   => $value['lot'],
                'block'                 => $value['block'],
                'tct'                   => $value['tct'],
                //    'tct_date'              => $value['tct_date'],
                //    'cct'                   => $value['cct'],
                //    'cct_date'              => $value['cct_date'],
                'tax_effectivity'      => $value['tax_effectivity'],
                'status'                => $value['status'],
                'current_arp'           => $value['current_arp'],
                'previous_arp'          => $value['previous_arp'],
                'kind'                  => self::getKindType($value['kind']),
                'actual_use'            => $value['actualUse'],
                'area'                  => number_format($value['area'],2,".",",") . ".sqm",
                'market_value'          => "₱ " . number_format($value['market_value'],2,".",","),
                'currentAssessValue'    => "₱ " .number_format($value['currentAssessValue'],2,".",","),

            );

        }

        $getTraceAssessmentReturn['data'] = $assessment_data;

        return $getTraceAssessmentReturn;
    }

    public function updateCancelledBy (Request $request){

            $current_arp = $request['current_arp'];

            //comment ko muna wala kasi to sa bagong assessment roll

            $status = AssessmentRoll::where('current_arp', $current_arp )
            ->update(
                [
                    'cancelled_by_td' => $request['cancelled_by_td']
                ]
            );

            if($status){
                return response()->json([
                    "data" => [],
                    'message' => "Update Succcessfully",
                    'status' => 1
                ], 200);            
            }

            return response()->json([
                "data" => [],
                'message' => "No data found",
                'status' => 2
            ], 200);
    }

    private function getPicturesUrl($pin){

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data["BLDG IMAGE"];

        $list = [];

        foreach($path as $paths){
            if(gettype($paths) == 'string'){
                if(strpos($paths, $pin) !== false){
                    $list[] = $paths;
                }
            }
        }

        if(count($list)){

            sort($list, 4);
            
            return array(
                "listCount" => count($list),
                "list"      => $list,
            );
        }

        $list[] = "imagenf.jpg";

        return array(
            "listCount" => count($list),
            "list"      => $list,
        );

        // return $path;

        // $matchingFiles = preg_grep("/" . $pin . "*.png/", $path);

        // rsort($matchingFiles);

        // return $matchingFiles;
        

        // if (!File::isDirectory("Pictures/$pin")) {
        //     return [];
        // }

        // // $pin = "117-01-001-005-015";

        // $url_pic = [];

        // $files = File::allFiles(public_path("Pictures/$pin"));

        // foreach ($files as $filename) {
        //     $url_pic[]['src'] = url("Pictures/$pin/" . basename($filename->getPathname()));
        // }
        // return $url_pic;


    }

    // POLYGONS
    public function getPolygon(Request $request){
        $data = AssessmentRoll::with('polygon')->where('id', $request['id'])->first();

        if($data['polygon']){

            $data['polygon']['gmap_polygon'] = str_replace("|", ",", $data['polygon']['gmap_polygon']);

            if($data['polygon']['center_coords']){
                $data['polygon']['center_coords'] = str_replace("|", ",\n", $data['polygon']['center_coords']);
            } else{
                $data['polygon']['center_coords'] = "";
            }
            
            $all_barangay = MapPolygon::select('gmap_polygon')
                            ->where('barangay', $data['polygon']['barangay'])
                            ->where('pin', '!=', $data['pin'])
                            ->orderBy('pin', 'ASC')
                            ->get();

            $data['all_barangay'] = self::parseBarangay($all_barangay);

            return $data;
        } else{
            return 0;
        }
    }

    private function parseBarangay($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'pin'   => $value['pin'],
                'path'  => str_replace("|", ",", $value['gmap_polygon'])
            );

            // if($key == 0){
            //     $str = str_replace("|", ",", $value['gmap_polygon']);
            // } else{
            //     $str = $str . "," . str_replace("|", ",", $value['gmap_polygon']);
            // }
        }
        return $data_arr;
        return $str;

    }

    private function parseUnusedBarangay($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'bgy'       => $value['bgy'],
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function parseUnusedDistrict($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }


    public function selectBgyBoundaries(Request $request){
        // return $request;


        if($request->sel == "1" && $request->mapping == "1"){
            $unused_district = DistrictPolygon::select('district', 'polygon')
                                ->where('district', '!=', $request['district'])
                                ->get();
            
            return self::parseUnusedDistrict($unused_district);
        } else if ($request->sel == "1" && $request->mapping == "1") {
            $unused_district = DistrictPolygon::select('district', 'polygon')
                                ->where('district', '!=', $request['district'])
                                ->get();
            return self::parseUnusedDistrict($unused_district);
        }
        else if ($request->sel == "2" && $request->mapping == "0") {
            $unused_bgy = PoliticalBoundary::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$request->bgy)->where('district', '=', $request->district)->get();
            return self::parseUnusedBarangay($unused_bgy);
        }
        else {
            $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$request->bgy)
                                ->where('district', $request->district)
                                ->get();
            return self::parseUnusedBarangay($unused_bgy);
        }

       
    }
     

     // ----- Mapping Module ----- //

     public function getActiveMap(Request $request){
         
        $brgyNo = $request['brgyNo'];
        $active_map = [];

        $query = MappingActiveMap::select('pin','owner_name','lot','block','tct')
                            ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                            ->get();
            
            foreach ($query as $key => $value) {
                $active_map [] = array(
                    'pin'                   =>      $value['pin'],
                    'owner_name'           =>      $value['owner_name'],
                    'lot'                   =>      $value['lot'],
                    'block'                 =>      $value['block'],
                    'tct'                   =>      $value['tct']
                );
            }
        
        
        return $active_map;
     }

     public function getNotActiveMap(Request $request){
         
        $brgyNo = $request['brgyNo'];
        $notActive_map = [];

        $query = MappingNotActivePinMap::select('pin')
                            ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                            ->get();
            
            foreach ($query as $key => $value) {
                $notActive_map [] = array(
                    'pin'                   =>      $value['pin'],
                );
            }
        
        
        return $notActive_map;
     }

     public function getRoads(){
        
        $data = Road::select('road', 'polygon')->get();
        return self::parseRoad($data);

    }

    private function getKindType($type){

        $exploded_type = explode("-", $type);
        
        switch ($type) {
            case "L":
                return "Land";
                break;
            case "B":
                return "Building";
                break;
            case "M":
                return "Machinery";
                break;
            default:
                return null;
        }

    }


    
    private function getActualUse($use){

        $exploded_type = explode("-", $use);
        
        switch ($exploded_type[1]) {
            case "CHAR":
                return "Charitable";
                break;
            case "COMM":
                return "Commercial";
                break;
            case "EDUC":
                return "Education";
                break;
            case "GOVT":
                return "Government";
                break;
            case "INDU":
                return "Industrial";
                break;
            case "MINE":
                return "Mine";
                break;
            case "OTHE":
                return "Others";
                break;
            case "RELI":
                return "Religion";
                break;
            case "RESI":
                return "Residential";
                break;
            case "SPC1" || "SPC1" || "SPC2" || "SPC3" || "SPC4" || "SPC5" || "SPC6" || "SPEC":
                return "Special";
                break;
            default:
                return null;
        }

    }

    private function getTaxable($tax) {
        if ($tax === 'T') {
            return 'Taxable';
        }
        else {
            return  'Exempted';
        }
    }

    private function parseRoad($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'name'   => $value['road'],
                'path'   => json_decode(str_replace("|", ",", $value['polygon']))
            );

        }

        return $data_arr;
    }

    
}
