<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ReportAssessmentRoll;

use App\CheckEncoderWork;
use App\CheckProofreaderWork;

class AssessmentRollController extends Controller
{
    public function getData(){


        $totalRptasPin = self::getTotalRptasPin();
        $activeRptasPin = self::getActiveRptasPin();
        $cancelledRptasPin = $totalRptasPin - $activeRptasPin;
        $totalArchiveTd = self::getTotalArchiveTd();
        $barangayActivePin = self::getBarangayActivePin();
        $activeAppraiser = self::getActiveAppraiser();
        $resiActive = self::getBarangayResidentialActivePin();
        $commActive = self::getBarangayCommercialActivePin();
        $barangayCancelledPin = self::getBarangayCancelledPin();

        $encodedTd = self::getEncodedTd();
        $proofreadTd = self::getProofreadTd();

        return response()->json([
            "data" => compact(
                'totalRptasPin',
                'activeRptasPin',
                'cancelledRptasPin',
                'totalArchiveTd',
                'barangayActivePin',
                'activeAppraiser',
                'resiActive',
                'commActive',
                'barangayCancelledPin',
                'encodedTd',
                'proofreadTd',
                
            ),
            'message' => 'Dashboard Retrieved Successfully',
            'status' => 1
        ], 200);
    }

    private function getTotalRptasPin(){

        $query = ReportAssessmentRoll::select('ARP')
                    ->distinct('ARP')
                    ->count();

        return $query;
    }

    private function getActiveRptasPin(){

        $query = ReportAssessmentRoll::select('ARP')
                    ->where('Current',1)
                    ->distinct('ARP')
                    ->count();

        return $query;
    }

    private function getTotalArchiveTd(){

        $query = CheckEncoderWork::sum('total_pdf');

        return number_format($query);
    }
    
    public function getBarangayActivePin(){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as count, SUM(area) AS sumArea, SUM(MV) AS sumMV,SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current',1)
                                ->where('BarangayCode','!=', null)
                                ->groupBy('BarangayCode')
                                ->orderBy('BarangayCode')
                                ->get();

        
            foreach($query as $key => $value){
                $data[] = array(
                    'Barangay'      => $value['Barangay'],
                    'countBarangay' => $value['count'],
                    'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                    'sumMV'         => number_format($value['sumMV'],2,".",","),
                    'sumAV'         => number_format($value['sumAV'],2,".",","),
                );    
            }

        return $data;

    }

    public function getBarangayResidentialActivePin(){
        
        $data = [];
        
        $queryResidential = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as countBarangay, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current',1)
                                ->where('AUDesc', 'RESIDENTIAL')
                                ->groupBy('BarangayCode')
                                ->orderBy('BarangayCode')
                                ->get();


        
            foreach($queryResidential as $key => $value){
                $data[] = array(
                    'Barangay'      => $value['Barangay'],
                    'countBarangay' => $value['countBarangay'],
                    'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                    'sumMV'         => number_format($value['sumMV'],2,".",","),
                    'sumAV'         => number_format($value['sumAV'],2,".",","),
                );    
            }

        return $data;
    }

    public function getBarangayCommercialActivePin(){
        
        $data = [];
        
        $queryCommercial = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as countBarangay, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current',1)
                                ->where('AUDesc', 'COMMERCIAL')
                                ->groupBy('BarangayCode')
                                ->orderBy('BarangayCode')
                                ->get();

        
            foreach($queryCommercial as $key => $value){
                $data[] = array(
                    'Barangay'      => $value['Barangay'],
                    'countBarangay' => $value['countBarangay'],
                    'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                    'sumMV'         => number_format($value['sumMV'],2,".",","),
                    'sumAV'         => number_format($value['sumAV'],2,".",","),
                );    
            }

        return $data;
    }

    public function getActiveAppraiser(){
        
        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AppraisedBy, COUNT(AppraisedBy) as count, SUM(area) AS sumArea, SUM(MV) AS sumMV,SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current',1)
                                ->where('AppraisedBy','!=', null)
                                ->where('AppraisedBy','!=', '')
                                ->where('AppraisedBy','!=', '.')
                                ->where('AppraisedBy','!=', '13780')
                                ->groupBy('AppraisedBy')
                                ->orderBy('AppraisedBy')
                                ->get();
            foreach($query as $key => $value){
                $data[] = array(
                    'AppraisedBy'      => $value['AppraisedBy'],
                    'countAppraised' => $value['count'],
                    'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                    'sumMV'         => number_format($value['sumMV'],2,".",","),
                    'sumAV'         => number_format($value['sumAV'],2,".",","),
                );    
            }
            

        return $data;

    }

    public function getBarangayCancelledPin(){
        
        $data = [];

        // $query = CancelledBarangayPin::get();

        $query = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as count, SUM(area) AS sumArea, SUM(MV) AS sumMV,SUM(AV) AS sumAV")
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('Current',0)
                                    ->where('BarangayCode','!=', null)
                                    ->groupBy('BarangayCode')
                                    ->orderBy('BarangayCode')
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'Barangay'      => $value['Barangay'],
                'countBarangay' => $value['count'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }

        return $data;

    }

    public function getEncodedTd(){

        $query = CheckEncoderWork::selectRaw("series, sum(work_pdf) as work_pdf, sum(total_pdf) as total_pdf")
                                    ->groupBy('series')
                                    ->orderBy('series')
                                    ->get();

        return $query;
    }

    public function getProofreadTd(){

        $query = CheckProofreaderWork::selectRaw("series, sum(work_pdf) as work_pdf, sum(total_pdf) as total_pdf")
                                    ->groupBy('series')
                                    ->orderBy('series')
                                    ->get();

        return $query;
    }

     //------------------- active pin -----------------//
    
    //for DialogBarangay
    

    public function postBarangay(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('Current', 1) 
                                ->groupBy('AUDesc')
                                ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                        
        
                                
        return $data;

    }

    public function postBarangayResidential(Request $request){

        $data = [];
        

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('Current', 1)  
                                ->where('AUDesc', 'RESIDENTIAL')
                                ->groupBy('AUDesc')
                                ->get();

                
                foreach($query as $key => $value){
                    $data[] = array(
                        'AUDesc'        => $value['AUDesc'],
                        'count'         => $value['COUNT'],
                        'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                        'sumMV'         => number_format($value['sumMV'],2,".",","),
                        'sumAV'         => number_format($value['sumAV'],2,".",","),
                    );    
                }
                        
        
                                
        return $data;

    }

    public function postBarangayCommercial(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('Current', 1)   
                                ->where('AUDesc', 'COMMERCIAL')
                                ->groupBy('AUDesc')
                                ->get();

        
                    foreach($query as $key => $value){
                        $data[] = array(
                            'AUDesc'        => $value['AUDesc'],
                            'count'         => $value['COUNT'],
                            'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                            'sumMV'         => number_format($value['sumMV'],2,".",","),
                            'sumAV'         => number_format($value['sumAV'],2,".",","),
                        );    
                    }
        
                        
        
                                
        return $data;

    }

    public function postMultipleAUBarangay(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('Current', 1) 
                                ->groupBy('AUDesc')
                                ->get();
     
        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                                
        return $data;

    }

    public function postAppraiser(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('AppraisedBy', $request->appraiser) 
                                ->where('Current', 1) 
                                ->groupBy('AUDesc')
                                ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                        
        
                                
        return $data;

    }

    public function postMultipleAppraiser(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('AppraisedBy', $request->appraiser) 
                                ->where('Current', 1) 
                                ->groupBy('AUDesc')
                                ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                        
        
                                
        return $data;

    }

    //searching Barangay PIN

    public function postBarangayPin(Request $request){

        $data = []; 

        

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('Current', 1)
                                    ->where('AUDesc', $request->AU)     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }


    //searching Multiple use Barangay PIN
    public function postMultipleBarangayPin(Request $request){

        $data = []; 

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('Current', 1)
                                    ->where('AUDesc', $request->AU)
                                    ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }

    public function postAppraiserPin(Request $request){

        $data = []; 

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('AppraisedBy',  $request->appraiser)
                                    ->where('Current', 1)
                                    ->where('AUDesc', $request->AU)     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }

    public function postMultipleAppraiserPin(Request $request){

        $data = []; 

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                    ->whereRaw("LENGTH(ARP) > ?", 3)                        
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('AppraisedBy',  $request->appraiser)
                                    ->where('Current', 1)
                                    ->where('AUDesc', $request->AU)
                                    ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }
    
    //------------------- cancelled pin -----------------//
    
    
    
     //for DialogBarangay
    

     public function postCancelledBarangay(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('Current', 0) 
                                ->groupBy('AUDesc')
                                ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                        
        
                                
        return $data;

    }

    public function postCancelledAppraiser(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('AppraisedBy', $request->appraiser) 
                                ->where('Current', 0) 
                                ->groupBy('AUDesc')
                                ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                        
        
                                
        return $data;

    }

    public function postCancelledMultipleAUBarangay(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('Current', 0) 
                                ->groupBy('AUDesc')
                                ->get();
     
        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                                
        return $data;

    }

    public function postCancelledMultipleAUAppraiser(Request $request){

        $data = [];

        $query = ReportAssessmentRoll::selectRaw("AUDesc, COUNT(AUDesc) AS COUNT, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')
                                ->where('BarangayCode', $request->barangay)
                                ->where('kind', $request->kind) 
                                ->where('AppraisedBy', $request->appraiser) 
                                ->where('Current', 0) 
                                ->groupBy('AUDesc')
                                ->get();
     
        foreach($query as $key => $value){
            $data[] = array(
                'AUDesc'        => $value['AUDesc'],
                'count'         => $value['COUNT'],
                'sumArea'       => number_format($value['sumArea'],2,'.',',') . ".sqm",
                'sumMV'         => number_format($value['sumMV'],2,".",","),
                'sumAV'         => number_format($value['sumAV'],2,".",","),
            );    
        }
                                
        return $data;

    }

    //searching Barangay PIN

    public function postCancelledBarangayPin(Request $request){

        $data = []; 

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('Current', 0)
                                    ->where('AUDesc', $request->AU)     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }

    public function postCancelledAppraiserPin(Request $request){

        $data = []; 

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('AppraisedBy',  $request->appraiser)
                                    ->where('Current', 0)
                                    ->where('AUDesc', $request->AU)     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }

    //searching Multiple use Barangay PIN
    public function postCancelledMultipleBarangayPin(Request $request){

        $data = []; 

        $query = ReportAssessmentRoll::select('pin','ARP','Owner', 'location', 'LotNo', 'BlkNo', 'area', 'MV', 'AV')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('BarangayCode',  $request->barangay)
                                    ->where('kind',  $request->kind)
                                    ->where('Current', 0)
                                    ->where('AUDesc', $request->AU)
                                    ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_rolls GROUP BY ARP HAVING COUNT(ARP) > 1)')     
                                    ->get();

        foreach($query as $key => $value){
            $data[] = array(
                'pin'               => $value['pin'],
                'ARP'               => $value['ARP'],
                'Owner'             => $value['Owner'],
                'location'          => $value['location'],
                'LotNo'             => $value['LotNo'],
                'BlkNo'             => $value['BlkNo'],
                'TotalArea'         => number_format($value['area'],2,'.',',') . ".sqm",
                'TotalMV'           => number_format($value['MV'],2,".",","),
                'TotalAV'           => number_format($value['AV'],2,".",","),
            );    
        }
                                
        return $data;
    }
    

    //utility api function

    public function selectDialogBarangay(Request $request){
        
        if ($request->mu != 'Mu')
        {

            $query = ReportAssessmentRoll::select('BarangayCode as Barangay')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('AppraisedBy', $request->appraiser)
                                    ->where('kind', $request->kind)
                                    ->where('BarangayCode', '!=', 000)
                                    ->where('BarangayCode', '!=', null)
                                    ->where('Current', 0)
                                    ->distinct('BarangayCode')
                                    ->orderBy('BarangayCode')
                                    ->get();
        }
        else {
            $query = ReportAssessmentRoll::select('BarangayCode as Barangay')
                                    ->where(function ($query) {
                                        $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                            ->orWhereNull('AppraisedDt');
                                    })
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('AppraisedBy', $request->appraiser)
                                    ->where('kind', $request->kind)
                                    ->whereRaw('ARP IN (SELECT ARP FROM report_assessment_roll GROUP BY ARP HAVING COUNT(ARP) > 1)')     
                                    ->where('BarangayCode', '!=', 000)
                                    ->where('BarangayCode', '!=', null)
                                    ->where('Current', 0)
                                    ->distinct('BarangayCode')
                                    ->orderBy('BarangayCode')
                                    ->get();
        }

        return $query;
    }

   // ----------------------------------------------------------------
   // Excel report
    public function exportToExcel(Request $request){

        
        if ($request->tab == 'ACTIVE'){
            $totalAv = ReportAssessmentRoll::whereDate('AppraisedDt', '<', date("Y-m-d"))
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('Current', 1)
                                    ->sum('AV');
            
            $totalPinCount = ReportAssessmentRoll::whereDate('AppraisedDt', '<', date("Y-m-d"))
                            ->whereRaw("LENGTH(ARP) > ?", 3)
                            ->where('Current', 1)
                            ->distinct("ARP")
                            ->count("ARP");

            $queryReport = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as countBarangay, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                            ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                            ->where('Current', 1)
                            ->whereRaw("LENGTH(ARP) > ?", 3)
                            ->groupBy('BarangayCode')
                            ->orderBy('BarangayCode')
                            ->get();
        }
        else if ($request->tab == 'RESIDENTIAL' || $request->tab == 'COMMERCIAL' ){
        
        $totalAv = ReportAssessmentRoll::where('AUDesc', $request->tab)
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current', 1)
                                ->sum('AV');
                                
        $totalPinCount = ReportAssessmentRoll::where('AUDesc', $request->tab)
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->distinct("ARP")
                                ->count("ARP");
       

        $queryReport = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as countBarangay, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where('AUDesc', $request->tab)
                                ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current', 1)
                                ->groupBy('BarangayCode')
                                ->orderBy('BarangayCode')
                                ->get();
        
        }
        else if ($request->tab == 'CANCELLED'){
           
                $totalAv = ReportAssessmentRoll::where('Current', 0)
                                        ->where(function ($query) {
                                            $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                                ->orWhereNull('AppraisedDt');
                                        })
                                        ->whereRaw("LENGTH(ARP) > ?", 3)
                                        ->sum('AV');
                
                $totalPinCount = ReportAssessmentRoll::where('Current', 0)
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->distinct("ARP")
                                ->count("ARP");

                $queryReport = ReportAssessmentRoll::selectRaw("BarangayCode as Barangay, COUNT(BarangayCode) as countBarangay, SUM(area) AS sumArea, SUM(MV) AS sumMV, SUM(AV) AS sumAV")
                                ->where(function ($query) {
                                    $query->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                        ->orWhereNull('AppraisedDt');
                                })
                                ->whereRaw("LENGTH(ARP) > ?", 3)
                                ->where('Current', 0)
                                ->groupBy('BarangayCode')
                                ->orderBy('BarangayCode')
                                ->get();         
        }

        else if($request->tab == 'APPRAISER'){
            $totalAv = ReportAssessmentRoll::where('Current', 1)
                                    ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->groupBy('AppraisedBy')
                                    ->orderBy('AppraisedBy')
                                    ->sum('AV');

            $totalPinCount = ReportAssessmentRoll::where('Current', 1)
                                    ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->groupBy('AppraisedBy')
                                    ->orderBy('AppraisedBy')
                                    ->distinct("ARP")
                                    ->count("ARP");

           
        $queryReport = ReportAssessmentRoll::selectRaw("AppraisedBy, COUNT(AppraisedBy) as count, SUM(area) AS sumArea, SUM(MV) AS sumMV,SUM(AV) AS sumAV")
                                    ->where('Current',1)
                                    ->whereDate('AppraisedDt', '<', date("Y-m-d"))
                                    ->whereRaw("LENGTH(ARP) > ?", 3)
                                    ->where('AppraisedBy','!=', null)
                                    ->where('AppraisedBy','!=', '')
                                    ->where('AppraisedBy','!=', '.')
                                    ->where('AppraisedBy','!=', '13780')
                                    ->groupBy('AppraisedBy')
                                    ->orderBy('AppraisedBy')
                                    ->get();

        }

        

            return self::createExcel($totalAv, $totalPinCount, $queryReport, $request->tab);
    }
    
    private function createExcel( $tAv, $totalPinCount, $queryReport, $type)    {

        if ($type == 'APPRAISER'){
            $titleReport = "Report of All ". $type;
        }
        else {
            $titleReport = "Report of Barangay ". $type;
        }
        
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

          // Summary Tab

        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle("Summary");
        $spreadsheet->getSheetByName("Summary");
        $worksheet = $spreadsheet->getActiveSheet();

       
        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates('E1')
                ->setOffsetX(20)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates('E1')
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
        //REPUBLIC OF THE PHILIPPINES
        $worksheet->mergeCells('A1:E1');
        $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
        // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
        $worksheet->getStyle("A1")->getFont()->setSize(12);
        $worksheet->getStyle('A1:E1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A1:E1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //CITY OF MANILA
        $worksheet->mergeCells('A2:E2');
        $worksheet->setCellValue("A2", "CITY OF MANILA");
        $worksheet->getStyle("A2:E2")->getFont()->setBold(true);
        $worksheet->getStyle("A2")->getFont()->setSize(14);
        $worksheet->getStyle('A2:E2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A2:E2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //DEPARTMENT OF ASSESSMENT
        $worksheet->mergeCells('A3:E3');
        $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
        $worksheet->getStyle("A3:E3")->getFont()->setBold(true);
        $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
        $worksheet->getStyle('A3:E3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A3:E3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //*Title of Report
        $worksheet->mergeCells('A5:E6');
        $worksheet->setCellValue("A5", $titleReport);
        $worksheet->getStyle("A5:E6")->getFont()->setBold(true);
        $worksheet->getStyle("A5")->getFont()->setSize(18);
        $worksheet->getStyle('A5:E6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A5:E6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //*Date of Report
        $worksheet->mergeCells('A7:E7');
        $worksheet->getStyle('A7:E7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("A7", "as of:  " . date("F j, Y"));
        $worksheet->getStyle("A7")->getFont()->setSize(8);


        //setup Label
        
        // $worksheet->mergeCells('D9:F9');
        // $worksheet->mergeCells('D10:F10');
        // $worksheet->mergeCells('D11:F11');
        // $worksheet->mergeCells('D12:F12');
        // $worksheet->mergeCells('D13:F13');
        // $worksheet->mergeCells('D14:F14');
        //place label and value
        
        $worksheet->getColumnDimension('A')->setWidth(20);
        $worksheet->getColumnDimension('B')->setWidth(20);
        $worksheet->getColumnDimension('C')->setWidth(30);
        $worksheet->getColumnDimension('D')->setWidth(30);
        $worksheet->getColumnDimension('E')->setWidth(30);

        $resTaxRate = .0150;
        $comTaxRate = .02;
        $taxRateText = "";

        if ($type == 'RESIDENTIAL' || $type == 'COMMERCIAL' ){
            
            if ($type == "RESIDENTIAL"){
                $totalBasicTax = $tAv * $resTaxRate;
                $taxRateText = "1.50%";
            }
            else{
                $taxRateText = "2%";
                $totalBasicTax = $tAv * $comTaxRate;
            }

            $worksheet->setCellValue("B9", "TOTAL PIN COUNT: ")
                        ->setCellValue("D9", $totalPinCount)
                        ->setCellValue("B10", "TOTAL ASSESSED VALUE: ")
                        ->setCellValue("D10", $tAv)
                        ->setCellValue("B11", "TAX RATE: ")
                        ->setCellValue("D11", $taxRateText)
                        ->setCellValue("B12", "TOTAL BASIC TAX: ")
                        ->setCellValue("D12", $totalBasicTax)
                        ->setCellValue("B13", "GENERAL FUND: ")
                        ->setCellValue("D13", "70%")
                        ->setCellValue("B14", "TOTAL GENERAL FUND: ")
                        ->setCellValue("D14", $totalBasicTax * .7);

            
            $worksheet->getStyle('D10')->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $worksheet->getStyle('D11')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $worksheet->getStyle('D12')->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $worksheet->getStyle('D13')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $worksheet->getStyle('D14')->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        }
        else {
            $worksheet->setCellValue("B9", "TOTAL PIN COUNT: ")
                        ->setCellValue("D9", $totalPinCount)
                        ->setCellValue("B10", "TOTAL ASSESSED VALUE: ")
                        ->setCellValue("D10", $tAv);
            
            $worksheet->getStyle('D9')->getNumberFormat()->setFormatCode('#,###,###,###');
            $worksheet->getStyle('D10')->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $worksheet->getStyle('D11')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        }
   
        // $allBarangay = self::getBarangayResidentialActivePin();
        // \Log::info($allBarangay);
        // return 0;
        // \Log::info($allBarangayPin);
        // return 0;
       
            
            $tab_name = "All Barangay - ".$type;
            $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $tab_name);
            $spreadsheet->addSheet($myWorkSheet);
            $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);

            //headings
            $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
            $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
            $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
            
            $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawingLogoManila->setName('Manila City Hall')
                ->setDescription('Logo')
                ->setPath($manila_city_logo) // put your path and image here
                ->setCoordinates('A1')
                ->setOffsetX(10)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
            $drawingLogoManila->setWorksheet($worksheet);

            $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawingLogoDOA->setName('Department of Assessment')
                    ->setDescription('Logo')
                    ->setPath($doa_logo) // put your path and image here
                    ->setCoordinates('E1')
                    ->setOffsetX(20)
                    ->setOffsetY(10)
                    ->setHeight(75)
                    ->getShadow()->setVisible(true)->setDirection(45);
            $drawingLogoDOA->setWorksheet($worksheet);

            $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawingLogoDIGI->setName('Digitization')
                ->setDescription('Logo')
                ->setPath($digi_logo) // put your path and image here
                ->setCoordinates('E1')
                ->setOffsetX(130)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
            $drawingLogoDIGI->setWorksheet($worksheet);
            
            //REPUBLIC OF THE PHILIPPINES
            $worksheet->mergeCells('A1:E1');
            $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
            // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
            $worksheet->getStyle("A1")->getFont()->setSize(12);
            $worksheet->getStyle('A1:E1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A1:E1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            //CITY OF MANILA
            $worksheet->mergeCells('A2:E2');
            $worksheet->setCellValue("A2", "CITY OF MANILA");
            $worksheet->getStyle("A2:E2")->getFont()->setBold(true);
            $worksheet->getStyle("A2")->getFont()->setSize(14);
            $worksheet->getStyle('A2:E2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A2:E2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            
            //DEPARTMENT OF ASSESSMENT
            $worksheet->mergeCells('A3:E3');
            $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
            $worksheet->getStyle("A3:E3")->getFont()->setBold(true);
            $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
            $worksheet->getStyle('A3:E3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A3:E3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            //*Title of Report
            $worksheet->mergeCells('A5:E6');
            $worksheet->setCellValue("A5", $tab_name);
            $worksheet->getStyle("A5:E6")->getFont()->setBold(true);
            $worksheet->getStyle("A5")->getFont()->setSize(18);
            $worksheet->getStyle('A5:E6')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A5:E6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            
            //*Date of Report
            $worksheet->mergeCells('A7:E7');
            $worksheet->getStyle('A7:E7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $worksheet->setCellValue("A7", "as of:  " . date("F j, Y"));
            $worksheet->getStyle("A7")->getFont()->setSize(8);

            
            $worksheet = $spreadsheet->getActiveSheet();
            $worksheet->getColumnDimension('A')->setWidth(20);
            $worksheet->getColumnDimension('B')->setWidth(20);
            $worksheet->getColumnDimension('C')->setWidth(30);
            $worksheet->getColumnDimension('D')->setWidth(30);
            $worksheet->getColumnDimension('E')->setWidth(30);


            //*style content Header
            $styleBorderHeaders = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                    ],
                ],
            ];

            
            $worksheet->getStyle('A9:E9')->applyFromArray($styleBorderHeaders);

           

            if($type == "APPRAISER"){
                $aColumn = "Appraiser";
                $aValue = "AppraisedBy";
                $bValue = "count";
            }
            else {
                $aColumn = "Barangay";
                $aValue = "Barangay";
                $bValue = "countBarangay";
            }

            $worksheet
                ->setCellValue("A9", $aColumn)
                ->setCellValue("B9", "No. of PINs")
                ->setCellValue("C9", "Area")
                ->setCellValue("D9", "Market Value")
                ->setCellValue("E9", "Assessed Value");
                
                $row = 10;

            foreach ($queryReport as $key => $value){

                    $worksheet->setCellValue("A{$row}", $value[$aValue]);
                    $worksheet->setCellValue("B{$row}", $value[$bValue]);
                    $worksheet->setCellValue("C{$row}", $value['sumArea']);
                    $worksheet->setCellValue("D{$row}", $value['sumMV']);
                    $worksheet->setCellValue("E{$row}", $value['sumAV']);
                    
                    //*style  content
                    $styleBorderContentsAB = [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                            ],
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ];

                    $styleBorderContentsCE = [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                        
                            ],
                        ],
                        
                    ];
                    
                    $worksheet->getStyle("A{$row}:B{$row}")->applyFromArray($styleBorderContentsAB);
                    $worksheet->getStyle("C{$row}:E{$row}")->applyFromArray($styleBorderContentsCE);
                    
                    $worksheet->getStyle("A{$row}:B{$row}")->getNumberFormat()
                    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
                    $worksheet->getStyle("C{$row}:E{$row}")->getNumberFormat()
                    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                
                    
                    $row++;

            }
        

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

    private function findingConsolidation($query){
        $arrayArp = [];
        $oldKey = 0;   
        foreach($query as $key => $value){
    
            if($key > 0){
                if ($value['ARP'] == $query[$oldKey]['ARP'] && $value['AUDesc'] == $query[$oldKey]['AUDesc'] ){
                    $query[$key]['Current'] = 2;
                }

                if ($value['Current'] != 2){
                    array_push($arrayArp, $value['ARP']);
                }
            }

            if ($key < 1){
                $oldKey = 0;
            
            }
            else {
                $oldKey++;
                
            }
            
        }

        return $arrayArp;
    }


}
