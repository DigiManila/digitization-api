<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingNoPinMap extends Model
{
    use HasFactory;
    protected $table = 'mapping_no_map_pin';
}
