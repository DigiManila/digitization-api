<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTaxDecDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_tax_dec_data', function (Blueprint $table) {
            $table->id();
            $table->string("arp");
            $table->text("owner_name");
            $table->string("address")->nullable();
            $table->string("lot")->nullable();
            $table->string("block")->nullable();
            $table->bigInteger('verified_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_tax_dec_data');
    }
}
