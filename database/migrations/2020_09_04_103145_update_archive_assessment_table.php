<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateArchiveAssessmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archive_assessment_rolls', function (Blueprint $table) {
            $table->text("owner_name")->nullable()->change();
            $table->text("address")->nullable()->change();
            $table->string("barangay")->nullable()->change();
            $table->string("lot")->nullable()->change();
            $table->string("block")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archive_assessment_rolls', function (Blueprint $table) {
            //
        });
    }
}
