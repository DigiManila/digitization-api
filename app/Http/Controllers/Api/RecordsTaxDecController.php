<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\RecordsTaxDecData;
use App\ReleaseTaxDec;
use App\Profile;
use Auth;
use Storage;
use Illuminate\Support\Facades\Redis;

class RecordsTaxDecController extends Controller
{
    public function saveTaxDec(Request $request){

        $data = $request->form;

        $data['year'] = 1979;
        $data['param'] = $request->param == "Sta. Ana" ? "STAANA" : strtoupper($request->param);
        $data['verified_by'] = Auth::user()->id;

        if($request->param){
            $check = RecordsTaxDecData::select('arp')
                                    ->where('arp', $data['arp'])
                                    ->where('param', $request->param)
                                    ->first();            
        } else{
            $check = RecordsTaxDecData::select('arp')
                                    ->where('arp', $data['arp'])
                                    ->first();            
        }


        if(isset($check['arp'])){
            return response()->json([
                "data" => [],
                'message' => "Tax Declaration already encoded",
                'status' => 3
            ], 200);              
        }

        $status = RecordsTaxDecData::create($data);

        if($status){
            return response()->json([
                "data" => [],
                 'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }

    public function checkTdIfExist(Request $request){

        $param = null;

        if($request->param){
            $param = $request->param == "Sta. Ana" ? "STAANA" : strtoupper($request->param);

            $data = RecordsTaxDecData::select('arp')
                                    ->where('arp', $request->arp)
                                    ->where('param', $param)
                                    ->first();
        } else{

            $data = RecordsTaxDecData::select('arp')
                                    ->where('arp', $request->arp)
                                    ->first();

        }

        if($data){
            return response()->json([
                "data" => [],
                'message' => "TD number exist",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                "data" => [],
                'message' => "TD number doesn't exist",
                'status' => 0
            ], 200);
        }
    }

    public function verifyTdExist(Request $request){

        $found  = false;
        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        if($request['year'] == "1996"){

            $bgy = explode("-", $request['td']);

            $search_name = "B-" . (int)$bgy[0] . " " . $request['year'];
            $data = $redis_data[$request['year']];

            foreach ($data as $key => $val) {
                if(strpos($key, $search_name) !== false){
                    $folder_name = $key;
                    break;
                }
            }

            if(in_array($request['td'] . ".pdf",$data[$folder_name])){
                $found = true;
            }

        } else{
            $zone   = explode("-", $request['td']);
            $folder_name = "ZONE " . intval($zone[1]);
    
            if($request['has_district']){
                $folder_name = $folder_name . " " . $request['district'];
            }

            $data = $redis_data[$request['year']][$folder_name];
    
            if(in_array($request['td'] . ".pdf",$data)){
                $found = true;
            } elseif(in_array("1" . $request['td'] . ".jpg",$data) && in_array("2" . $request['td'] . ".jpg",$data)){
                $found = true;
            }
        }



        if($found){
            return response()->json([
                "data" => [],
                'message' => "TD number exist",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                "data" => [],
                'message' => "File doesn't exist. Please contact administrator for file verification.",
                'status' => 0
            ], 200);
        }
    }

    public function getCertifiers(){

        $certifier = [];

        $certifier_data = Profile::select('id', 'first_name', 'middle_name', 'last_name')->where('position', 'LOCAL ASSESSMENT OPERATION OFFICER IV')->where('user_type', 'RecordsHead')->get();

        foreach($certifier_data as $key => $val){
            $certifier[$key]['text'] = $val['first_name'] . " " . $val['middle_name'] . " " . $val['last_name'];
            $certifier[$key]['value'] = $val['id'];
        }

        return compact('certifier');        
    }

    public function printTdLogs(Request $request){
        return Controller::addLog('Taxdec', $new_data = "" , $prev_data = "",  $remarks = $request['remarks']);
    }

    public function getReleasedTd(Request $request){
        return ReleaseTaxDec::select('id', 'or_no', 'td_no', 'requestor')->get();
    }

    public function getZoneBgy(){

        $zone = [];
        $brgy = [];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $data = $redis_data['1996'];

        foreach ($data as $key => $value) {

            $exp = explode(" ", $key);

            $zone_val = explode("-", $exp[0])[1];

            if(!in_array($zone_val, $zone)){
                $zone[] = explode("-", $exp[0])[1];
            }
            
            $brgy[] = explode("-", $exp[1])[1];
            
        }

        sort($zone, 6);
        sort($brgy, 6);

        return response()->json([
            "data" => compact('zone', 'brgy'),
            'message' => "Zone Barangay List returned",
            'status' => 1
        ], 200);

    }

}
