<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveAssessmentRoll extends Model
{
    use HasFactory;

    protected $table = 'active_assessment_roll';
}


