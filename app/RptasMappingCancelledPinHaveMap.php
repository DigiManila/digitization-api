<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasMappingCancelledPinHaveMap extends Model
{
    use HasFactory;

    protected $table = 'rptas_mapping_cancelled_pin_have_map';
}

