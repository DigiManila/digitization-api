<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrivateTempTaxdec extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('private_temp_taxdec', function (Blueprint $table) {
            $table->id();
            $table->string("pin")->nullable();
            $table->string("current_arp")->nullable();
            $table->string('current_arp_96')->nullable();
            $table->string('owner_name')->nullable();
            $table->string("cancelled_by_td")->nullable();
            $table->string("previous_arp")->nullable();
            $table->string('ref_tax_dec')->nullable();
            $table->date('effectivity_date')->nullable();
            $table->text("location")->nullable();
            $table->string("lot")->nullable();
            $table->string("block")->nullable();
            $table->string("tct")->nullable();
            $table->date("tct_date")->nullable();
            $table->string("cct")->nullable();
            $table->date("cct_date")->nullable();
            
            
            $table->string("kind")->nullable();
            $table->decimal("area",20,2)->nullable();
            $table->decimal("market_value",20,2)->nullable();
           
            $table->decimal("currentAssessValue",20,2)->nullable();
            $table->boolean('is_databuildup')->nullable();
            $table->string('created_by')->nullable();
            $table->string('proofread_by')->nullable();
            $table->dateTime('proofread_date')->nullable();
            
                        
            
            $table->string("verified_by")->nullable();
            $table->dateTime('verified_date')->nullable();
            $table->Integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('private_temp_taxdec');
    }
}
