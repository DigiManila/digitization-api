<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewReportBarangayKind extends Model
{
    use HasFactory;

    protected $table = 'report_barangay_kind';
}
