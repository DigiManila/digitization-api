<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ActiveAssessmentRoll;
use App\CancelledAssessmentRoll;
use App\ActiveTaxdecAssmnt;
use App\CancelledTaxdecAssmnt;
use App\ActivePrevious;
use App\DataRecon;
use App\UnionAllAd;
use App\RptasTaxdecMastExtn;
use App\RptasTaxdecMastMla;

use App\Helper\Helper;

class DataReconciliationController extends Controller
{
    public function getData(Request $request){

        $controlBook = [];

        $query = UnionAllAd::select('union_all_ad.ARP', 'union_all_ad.PIN','rptas_taxdec_mastextn.Prev_Arp', 'union_all_ad.status')
                ->leftJoin('rptas_taxdec_mastextn', 'union_all_ad.ARP', '=','rptas_taxdec_mastextn.Arp')
                ->where('barangay', $request->barangay)
                ->where('union_all_ad.Taxability', $request->taxability)
                ->orderBy('PIN', 'asc')
                ->get();

        $new_query = self::joinDataRecons($query, $request->barangay, false);

        foreach($new_query as $key => $value){

            $variance = "";

            if(!$value['status_desc']){
                $variance = "N/C";
            } else {
                $variance = $value['status_desc'];
            }

            $controlBook[] = array(
                'ARP'           => $value['ARP'],
                // 'TDNo'          => substr($value['ARP'],9,5),
                'PIN'           => $value['PIN'],
                'Prev_Arp'      => $value['Prev_Arp'],
                'variance'      => $variance,
                'status'        => $value['status']
            );

        }

        if($request->addData){
            $nrptas = DataRecon::select('PIN', 'active_cancel', 'status_desc')
                                ->whereRaw('SUBSTRING(PIN,8,3) = ?',  $request->barangay)
                                ->where('status_desc',  'Not on RPTAS')
                                ->get()->toArray();

            foreach ($nrptas as $key => $value) {
                $controlBook[] = array(
                    'ARP'           => "",
                    // 'TDNo'          => substr($value['ARP'],9,5),
                    'PIN'           => $value['PIN'],
                    'Prev_Arp'      => "",
                    'variance'      => $value['status_desc'],
                    'status'        => $value['active_cancel']
                );
            }
        }

        return $controlBook;
    }

    public function getTaxdecAssmnt(Request $request){

        $main_data      = [];
        $active_td      = [];
        $prev_td        = [];
        $active_prev    = [];

        if($request['status'] == 'Active'){
            
            $active_prev = ActivePrevious::where('ARP', $request['arp'])->first();
    
            $main_data = ActiveAssessmentRoll::select('Owner', 'Location', 'LotNo', 'BlkNo', 'Memoranda', 'StrucType')
                            ->where('PIN', $active_prev['PIN'])
                            ->first();
    
            $active_td = ActiveTaxdecAssmnt::select('AU', 'AUDesc', 'area', 'MV', 'AV', 'Kind', 'Taxability')
                            ->where('ARP', $active_prev['ARP'])
                            ->get();
    
            $prev_td = CancelledTaxdecAssmnt::select('AU', 'AUDesc', 'AV', 'Kind', 'Taxability')
                            ->where('ARP', $active_prev['Prev_Arp'])
                            ->get();

        } else{

            $active_prev = [];

            $main_data = CancelledAssessmentRoll::select('PIN', 'Owner', 'Location', 'LotNo', 'BlkNo', 'Memoranda', 'StrucType')
                            ->where('ARP', $request['arp'])
                            ->first();

            $active_td = CancelledTaxdecAssmnt::select('AU', 'AUDesc', 'area', 'MV', 'AV', 'Kind', 'Taxability')
                ->where('ARP', $request['arp'])
                ->get();


            $active_prev['PIN'] = $main_data['PIN'];
            $active_prev['ARP'] = $request['arp'];

            if($request->prev_arp){

                $prev_td = CancelledTaxdecAssmnt::select('ARP', 'AU', 'AUDesc', 'AV', 'Kind', 'Taxability')
                                ->where('ARP', $request->prev_arp)
                                ->get();
                                
                $active_prev['Prev_Arp'] = $prev_td[0]->ARP;

            } else{
                $prev_td = [json_decode('{"ARP":null,"AU":null,"AUDesc":null,"AV":null,"Kind":null,"Taxability":null}', 1)];
                $active_prev['Prev_Arp'] = null;
            }
    
        }

        $main_data['Owner'] = trim($main_data['Owner']);
        $main_data['Location'] = trim($main_data['Location']);

        $effectivity = RptasTaxdecMastMla::select("Effectivity")->where('PIN', substr($active_prev['PIN'], 0, 18))->first();

        $main_data['Effectivity'] = "";

        if($effectivity){
            $main_data['Effectivity'] = date('Y', strtotime($effectivity['Effectivity']));
        }

        $output = self::formatOutput($main_data, $active_td, $prev_td, $active_prev);

        return $output;

    }

    private function formatOutput($main, $curr, $prev, $arp){

        $out    = [];
        $class  = self::getClassification($arp['PIN']);

        $out['arp']             = $arp['ARP'];
        $out['td']              = substr($arp['ARP'],9,5);
        $out['owner']           = $main['Owner'];
        $out['location']        = $main['Location'];
        $out['pin']             = $arp['PIN'];
        $out['effectivity']     = $main['Effectivity'];
        $out['lot']             = $main['LotNo'];
        $out['blk']             = $main['BlkNo'];
        $out['blk']             = $main['BlkNo'];
        $out['strucType']       = $main['StrucType'];

        $out['land']['res']     = self::getMvFormat();
        $out['land']['com']     = self::getMvFormat();

        $out['imp']['res']      = self::getMvFormat();
        $out['imp']['com']      = self::getMvFormat();

        $out['mach']            = self::getMvFormat();
        $out['spl']             = self::getMvFormat();

        $out['prev_av']['res']  = self::getPrevAvFormat();
        $out['prev_av']['com']  = self::getPrevAvFormat();
        $out['prev_av']['mach'] = self::getPrevAvFormat();
        $out['prev_av']['spl']  = self::getPrevAvFormat();

        $out['prev_arp']        = $arp['Prev_Arp'];
        $out['memo']            = $main['Memoranda'];

        foreach ($curr as $key => $value) {

            $val = json_decode($value, 1);

            if($class == 'mach'){
                $out['mach']['actual_use'] = $value['AU'];
                $out['mach']['au_desc'] = $value['AUDesc'];
                $out['mach']['area'] = $value['area'];
                $out['mach']['mv'] = $value['MV'];
                $out['mach']['av'] = $value['AV'];
            } elseif($value['AU'] == 'RESI'){
                $out[$class]['res']['actual_use'] = $value['AU'];
                $out[$class]['res']['au_desc'] = $value['AUDesc'];
                $out[$class]['res']['area'] = $value['area'];
                $out[$class]['res']['mv'] = $value['MV'];
                $out[$class]['res']['av'] = $value['AV'];
            } elseif($value['AU'] == 'COMM'){
                $out[$class]['com']['actual_use'] = $value['AU'];
                $out[$class]['com']['au_desc'] = $value['AUDesc'];
                $out[$class]['com']['area'] = $value['area'];
                $out[$class]['com']['mv'] = $value['MV'];
                $out[$class]['com']['av'] = $value['AV'];
            } else{
                $out['spl']['actual_use'] = $value['AU'];
                $out['spl']['au_desc'] = $value['AUDesc'];
                $out['spl']['area'] = $value['area'];
                $out['spl']['mv'] = $value['MV'];
                $out['spl']['av'] = $value['AV'];
            }
        }


        foreach ($prev as $key => $value) {

            if($class == 'mach'){
                $out['prev_av']['mach']['actual_use'] = $value['AU'];
                $out['prev_av']['mach']['au_desc'] = $value['AUDesc'];
                $out['prev_av']['mach']['av'] = $value['AV'];
            } elseif($value['AU'] == 'RESI'){
                $out['prev_av']['res']['actual_use'] = $value['AU'];
                $out['prev_av']['res']['au_desc'] = $value['AUDesc'];
                $out['prev_av']['res']['av'] = $value['AV'];
            } elseif($value['AU'] == 'COMM'){
                $out['prev_av']['com']['actual_use'] = $value['AU'];
                $out['prev_av']['com']['au_desc'] = $value['AUDesc'];
                $out['prev_av']['com']['av'] = $value['AV'];
            } else{
                $out['prev_av']['spl']['actual_use'] = $value['AU'];
                $out['prev_av']['spl']['au_desc'] = $value['AUDesc'];
                $out['prev_av']['spl']['av'] = $value['AV'];
            }
        }

        return $out;

    }

    private function joinDataRecons($data, $bgy, $report){

        $arr = [];

        if($report){
            $query = DataRecon::whereRaw('SUBSTRING(PIN,8,3) = ?',  $bgy)
                                ->get()->toArray();
        } else{
            $query = DataRecon::select('arp', 'prev_arp', 'status_desc')
                                ->whereRaw('SUBSTRING(PIN,8,3) = ?',  $bgy)
                                ->get()->toArray();
        }


        foreach ($query as $key => $value) {
            $arr[$value['arp'] . "_" . $value['prev_arp']] = $value['status_desc'];
        }

        foreach ($data as $key => $value) {

            if(isset($arr[$value['ARP'] . "_" . $value['Prev_Arp']])){
                $data[$key]['status_desc'] = $arr[$value['ARP'] . "_" . $value['Prev_Arp']];
            } else{
                $data[$key]['status_desc'] = null;
                // \Log::info($value->toArray());
                // \Log::info($value['ARP'] . "_" . $value['Prev_Arp']);
            }
        }

        return $data;

    }

    public function saveData(Request $request){

        $status = self::getStatus($request['statusDesc']);
        $tax    = $request['taxability']; 

        $save_data = $request['data'];

        if($request['changeTaxability']){
            if($tax == 'Taxable'){
                $tax = 'Exempt';
            } else{
                $tax = 'Taxable';
            }
        }

        $save_data['changed_tax']   = $request['changeTaxability'];
        $save_data['taxability']    = $tax;

        $save_data['status']        = $status;
        $save_data['status_desc']   = $request['statusDesc'];
        $save_data['active_cancel'] = $request['activeCancel'];
        $save_data['created_by']    = $request['user'];


        if($request['statusDesc'] == 'Not on Control Book'){
            $st = DataRecon::create(
                array(
                    'arp'           => $save_data['arp'],
                    'PIN'           => $save_data['PIN'],
                    'changed_tax'   => $request['changeTaxability'],
                    'taxability'    => $tax,
                    'status'        => $status,
                    'prev_arp'      => $save_data['prev_arp'],
                    'status_desc'   => $request['statusDesc'],
                    'created_by'    => $request['user'],
                )
            );
        } else{

            // if cancellation

            $td_cancel = explode('-', $save_data['td_no']);

            if(strlen($td_cancel[0]) > 2){
                $save_data['cancellation_no'] = $save_data['td_no'];
                $save_data['td_no'] = "";
            }

            $st = DataRecon::create($save_data);
        }

        if($st){
            return "Saved";
        } else{
            return "Saving Failed";
        }


    }

    public function getComparison(Request $request){

        if(!$request->arp && !$request->prev_arp){
            $rptas = [];
        } else{
            $rptas = self::formatComparison(self::getTaxdecAssmnt($request));
        }

        $control_book   = self::trimData(DataRecon::where('arp', $request->arp)->where('prev_arp', $request->prev_arp)->first()->toArray());

        return compact('rptas', 'control_book');
    }

    private function getMvFormat(){
        return array(
            'actual_use' => "",
            'au_desc' => "",
            'area' => 0,
            'mv' => 0,
            'av' => 0
        );
    }

    private function getPrevAvFormat(){
        return array(
            'actual_use' => "",
            'au_desc' => "",
            'av' => 0
        );
    }

    private function getClassification($pin){

        if (strpos($pin, 'B') !== false) {
            return 'imp';
        } elseif (strpos($pin, 'M') !== false) {
            return 'mach';
        } else{
            return 'land';
        }
    }

    private function getStatus($desc){
        if ($desc == 'With Variance') {
            return 'CV';
        } elseif ($desc == 'No Variance') {
            return 'CN';
        } elseif ($desc == 'Not on RPTAS') {
            return 'NR';
        } elseif ($desc == 'Not on Control Book') {
            return 'NC';
        }      
    }

    private function formatComparison($data){

        return array(
            'PIN'           => trim($data['pin']),
            'arp'           => trim($data['arp']),
            'block'         => trim($data['blk']),
            'effectivity'   => trim($data['effectivity']),
            'ic_area'       => trim($data['imp']['com']['area']),
            'ic_au'         => trim($data['imp']['com']['actual_use']),
            'ic_aud'        => trim($data['imp']['com']['au_desc']),
            'ic_av'         => trim($data['imp']['com']['av']),
            'ic_mv'         => trim($data['imp']['com']['mv']),
            'ir_area'       => trim($data['imp']['res']['area']),
            'ir_au'         => trim($data['imp']['res']['actual_use']),
            'ir_aud'        => trim($data['imp']['res']['au_desc']),
            'ir_av'         => trim($data['imp']['res']['av']),
            'ir_mv'         => trim($data['imp']['res']['mv']),
            'lc_area'       => trim($data['land']['com']['area']),
            'lc_au'         => trim($data['land']['com']['actual_use']),
            'lc_aud'        => trim($data['land']['com']['au_desc']),
            'lc_av'         => trim($data['land']['com']['av']),
            'lc_mv'         => trim($data['land']['com']['mv']),
            'location'      => trim($data['location']),
            'lot'           => trim($data['lot']),
            'lr_area'       => trim($data['land']['res']['area']),
            'lr_au'         => trim($data['land']['res']['actual_use']),
            'lr_aud'        => trim($data['land']['res']['au_desc']),
            'lr_av'         => trim($data['land']['res']['av']),
            'lr_mv'         => trim($data['land']['res']['mv']),
            'm_area'        => trim($data['mach']['area']),
            'm_au'          => trim($data['mach']['actual_use']),
            'm_aud'         => trim($data['mach']['au_desc']),
            'm_av'          => trim($data['mach']['av']),
            'm_mv'          => trim($data['mach']['mv']),
            'memoranda'     => trim($data['memo']),
            'owner'         => trim($data['owner']),
            'prev_arp'      => trim($data['prev_arp']),
            'pvc_au'        => trim($data['prev_av']['com']['actual_use']),
            'pvc_aud'       => trim($data['prev_av']['com']['au_desc']),
            'pvc_av'        => trim($data['prev_av']['com']['av']),
            'pvm_au'        => trim($data['prev_av']['mach']['actual_use']),
            'pvm_aud'       => trim($data['prev_av']['mach']['au_desc']),
            'pvm_av'        => trim($data['prev_av']['mach']['av']),
            'pvr_au'        => trim($data['prev_av']['res']['actual_use']),
            'pvr_aud'       => trim($data['prev_av']['res']['au_desc']),
            'pvr_av'        => trim($data['prev_av']['res']['av']),
            'pvs_au'        => trim($data['prev_av']['spl']['actual_use']),
            'pvs_aud'       => trim($data['prev_av']['spl']['au_desc']),
            'pvs_av'        => trim($data['prev_av']['spl']['av']),
            's_area'        => trim($data['spl']['area']),
            's_au'          => trim($data['spl']['actual_use']),
            's_aud'         => trim($data['spl']['au_desc']),
            's_av'          => trim($data['spl']['av']),
            's_mv'          => trim($data['spl']['mv']),
            'struct_type'   => trim($data['strucType']),
        );

    }

    private function trimData($data){

        $output = [];

        foreach ($data as $key => $value) {
            $output[$key] = trim($value);
        }

        return $output;
    }

    // ------------------------------------ Data Recon Table -------------------------------------------- //

    public function tableGetData(){

        $data = DataRecon::select('td_no', 'arp', 'PIN', 'status_desc', 'taxability')->get();
        $bgy_list = DataRecon::selectRaw('distinct substring(PIN,8,3) as bgy')->pluck('bgy')->toArray();

        return compact('data', 'bgy_list');

    }

    // ------------------------------------ Report ------------------------------------------------------ //

    public function generateReport(Request $request){

        $taxability = $request->taxability == "Taxable" ? "T" : "E";

        $query = UnionAllAd::select('union_all_ad.ARP', 'union_all_ad.PIN','rptas_taxdec_mastextn.Prev_Arp', 'union_all_ad.status')
                ->leftJoin('rptas_taxdec_mastextn', 'union_all_ad.ARP', '=','rptas_taxdec_mastextn.Arp')
                ->where('barangay', $request->bgy)
                ->where('union_all_ad.Taxability', $taxability)
                ->orderBy('PIN', 'asc')
                ->orderBy('union_all_ad.ARP', 'ASC')
                ->get();

        $new_query = self::joinDataRecons($query, $request->bgy, true);

        $rptas = self::formatForReport($new_query);

        $list = [];

        foreach ($new_query as $key => $value){
            $list[] = $value;
        }

        $data_recons = DataRecon::whereRaw('SUBSTRING(PIN,8,3) = ?',  $request->bgy)
                                ->orderBy('PIN', 'asc')
                                ->orderBy('arp', 'ASC')
                                ->get()
                                ->toArray();

        foreach($data_recons as $key => $val){
            $data_recons[$key]['total_area'] = $val['lr_area'] + $val['lc_area'] + $val['ir_area'] + $val['ic_area'] + $val['m_area'] + $val['s_area'];
        }
        
        return self::exportReport($rptas, $data_recons, $taxability);

    }

    private function formatForReport($data){

        $format = [];

        $curr_active = [];
        $curr_cancelled = [];

        $prev_active = [];
        $prev_cancelled = [];

        foreach ($data as $key => $value) {

            if($value['status_desc']){

                if($value['status'] == 'Active'){
                    $curr_active[] = $value['ARP'];
                    $prev_active[] = $value['Prev_Arp'];
                } else{
                    $curr_cancelled[] = $value['ARP'];
                    $prev_cancelled[] = $value['Prev_Arp'];
                }

            }

        }

        $active_details = ActiveAssessmentRoll::select('ARP','Owner', 'Location', 'LotNo', 'BlkNo', 'Memoranda', 'StrucType', 'Effectivity')
                                                ->whereIn('ARP', $curr_active)
                                                ->get()->toArray();

        $cancelled_details = CancelledAssessmentRoll::select('ARP','Owner', 'Location', 'LotNo', 'BlkNo', 'Memoranda', 'StrucType', 'Effectivity')
                                                ->whereIn('ARP', $curr_cancelled)
                                                ->get()->toArray();

        

        $query_curr_active = ActiveTaxdecAssmnt::select('ARP','AU', 'AUDesc', 'area', 'MV', 'AV', 'Kind', 'Taxability')->whereIn('ARP', $curr_active)->get()->toArray();
        $query_curr_cancelled = CancelledTaxdecAssmnt::select('ARP','AU', 'AUDesc', 'area', 'MV', 'AV', 'Kind', 'Taxability')->whereIn('ARP', $curr_cancelled)->get()->toArray();

        $query_prev_active = CancelledTaxdecAssmnt::select('ARP','AU', 'AUDesc', 'area', 'MV', 'AV', 'Kind', 'Taxability')->whereIn('ARP', $prev_active)->get()->toArray();
        $query_prev_cancelled = CancelledTaxdecAssmnt::select('ARP','AU', 'AUDesc', 'area', 'MV', 'AV', 'Kind', 'Taxability')->whereIn('ARP', $prev_cancelled)->get()->toArray();

        $arp_active_details = self::arpKey($active_details, 0);
        $arp_cancelled_details = self::arpKey($cancelled_details, 0);
        $arp_query_curr_active = self::arpKey($query_curr_active, 1);
        $arp_query_curr_cancelled = self::arpKey($query_curr_cancelled, 1);
        $arp_query_prev_active = self::arpKey($query_prev_active, 1);
        $arp_query_prev_cancelled = self::arpKey($query_prev_cancelled, 1);
    
        foreach ($data as $key => $value) {

            $details    = [];
            $current    = [];
            $previous   = [];

            if($value['status_desc']){

                if($value['status'] == 'Active'){
                    $details    = isset($arp_active_details[$value['ARP']]) ? $arp_active_details[$value['ARP']] : [];
                    $current    = isset($arp_query_curr_active[$value['ARP']]) ? $arp_query_curr_active[$value['ARP']] : [];
                    $previous   = isset($arp_query_prev_active[$value['Prev_Arp']]) ? $arp_query_prev_active[$value['Prev_Arp']] : [];
                } else{
                    $details    = isset($arp_cancelled_details[$value['ARP']]) ? $arp_cancelled_details[$value['ARP']] : [];
                    $current    = isset($arp_query_curr_cancelled[$value['ARP']]) ? $arp_query_curr_cancelled[$value['ARP']] : [];
                    $previous   = isset($arp_query_prev_cancelled[$value['Prev_Arp']]) ? $arp_query_prev_cancelled[$value['Prev_Arp']] : [];
                }
    
                $format[] = array(
                    'pin'       => $value->toArray(),
                    'details'   => $details,
                    'current'   => $current,
                    'previous'  => $previous,
                );

            }


        }

        $output = self::formatRptasData($format);

        return $output;

    }

    private function formatRptasData($data){

        $output = [];

        foreach($data as $key => $value){

            $consolidated = [];

            $consolidated['Owner'] = $value['details']['Owner'];
            $consolidated['Location'] = $value['details']['Location'];
            $consolidated['PIN'] = $value['pin']['PIN'];
            $consolidated['ARP'] = $value['details']['ARP'];
            $consolidated['Effectivity'] = date('Y', strtotime($value['details']['Effectivity']));
            $consolidated['LotNo'] = $value['details']['LotNo'];
            $consolidated['BlkNo'] = $value['details']['BlkNo'];
            $consolidated['StrucType'] = $value['details']['StrucType'];
            $consolidated['Memoranda'] = $value['details']['Memoranda'];

            $rptas_format_values = self::rptasValues($value);

            $consolidated['Area'] = $rptas_format_values['total_area'];

            $consolidated['mvlr']   = $rptas_format_values['land']['res']['mv'];
            $consolidated['mvlc']   = $rptas_format_values['land']['com']['mv'];
            $consolidated['mvir']   = $rptas_format_values['imp']['res']['mv'];
            $consolidated['mvic']   = $rptas_format_values['imp']['com']['mv'];
            $consolidated['mvm']    = $rptas_format_values['mach']['mv'];
            $consolidated['mvs']    = $rptas_format_values['spl']['mv'];
            $consolidated['avlr']   = $rptas_format_values['land']['res']['av'];
            $consolidated['avlc']   = $rptas_format_values['land']['com']['av'];
            $consolidated['avir']   = $rptas_format_values['imp']['res']['av'];
            $consolidated['avic']   = $rptas_format_values['imp']['com']['av'];
            $consolidated['avm']    = $rptas_format_values['mach']['av'];
            $consolidated['avs']    = $rptas_format_values['spl']['av'];

            $consolidated['PreviousAV'] = $rptas_format_values['prev_assess'];
            
            $output[] = $consolidated;
            
        }

        return $output;
    }

    private function rptasValues($data){

        $output = array(
            'land' => array(
                'res' => array(
                    'mv' => 0,
                    'av' => 0
                ),
                'com' => array(
                    'mv' => 0,
                    'av' => 0
                )
            ),
            'imp' => array(
                'res' => array(
                    'mv' => 0,
                    'av' => 0
                ),
                'com' => array(
                    'mv' => 0,
                    'av' => 0
                )
            ),
            'mach' => array(
                'mv' => 0,
                'av' => 0
            ),
            'spl' => array(
                'mv' => 0,
                'av' => 0
            ),
            'prev_assess' => "",
            'total_area' => 0,
        );


        krsort($data['current']);
        krsort($data['previous']);

        $area = 0;

        foreach($data['current'] as $key => $value){

            $area = $area + $value['area'];

            if($key == 'RESI'){
                if($value['Kind'] == 'L'){
                    $output['land']['res']['mv'] = $value['MV'];
                    $output['land']['res']['av'] = $value['AV'];
                } elseif($value['Kind'] == 'B'){
                    $output['imp']['res']['mv'] = $value['MV'];
                    $output['imp']['res']['av'] = $value['AV'];
                } else{
                    $output['mach']['mv'] = $value['MV'];
                    $output['mach']['av'] = $value['AV'];
                }
            } elseif($key == 'COMM'){
                if($value['Kind'] == 'L'){
                    $output['land']['com']['mv'] = $value['MV'];
                    $output['land']['com']['av'] = $value['AV'];
                } elseif($value['Kind'] == 'B'){
                    $output['imp']['com']['mv'] = $value['MV'];
                    $output['imp']['com']['av'] = $value['AV'];
                } else{
                    $output['mach']['mv'] = $value['MV'];
                    $output['mach']['av'] = $value['AV'];
                }
            } else{
                $output['spl']['mv'] = $value['MV'];
                $output['spl']['av'] = $value['AV'];
            }
        }

        $str = "";

        foreach($data['previous'] as $key => $value){

            if(count($data['previous']) > 1){
                $str = $str . "$key-" . $value['AV'] . "\n";
            } else{
                $str = $value['AV'];
            }

        }

        $output['prev_assess'] = rtrim($str);

        $output['total_area'] = $area;

        return $output;

    }

    private function arpKey($data, $filter){

        $output = [];

        foreach ($data as $key => $value) {

            if($filter){
                $output[$value['ARP']][$value['AU']] = $value;
            } else{
                $output[$value['ARP']] = $value;
            }

        }

        return $output;


    }

    private function exportReport($rptas, $control_book, $taxability){

        $header = new Helper;

        $excel = $header->excelHeader("Data Reconcilliation Report", date('F j, Y'), 'AB', 'Z', 'AA', 'Z');

        $spreadsheet    = $excel['spreadsheet'];
        $worksheet      = $excel['worksheet'];

        $spreadsheet->getActiveSheet()->setTitle("Report");

        $worksheet->getColumnDimension('A')->setWidth(7);
        $worksheet->getColumnDimension('B')->setWidth(23);
        $worksheet->getColumnDimension('C')->setWidth(34);
        $worksheet->getColumnDimension('D')->setWidth(30);
        $worksheet->getColumnDimension('E')->setWidth(27);
        $worksheet->getColumnDimension('F')->setWidth(17);
        $worksheet->getColumnDimension('G')->setWidth(12);
        $worksheet->getColumnDimension('H')->setWidth(11);
        $worksheet->getColumnDimension('I')->setWidth(22);
        $worksheet->getColumnDimension('J')->setWidth(15);
        $worksheet->getColumnDimension('K')->setWidth(12);
        $worksheet->getColumnDimension('L')->setWidth(15);
        $worksheet->getColumnDimension('M')->setWidth(17);
        $worksheet->getColumnDimension('N')->setWidth(15);
        $worksheet->getColumnDimension('O')->setWidth(15);
        $worksheet->getColumnDimension('P')->setWidth(15);
        $worksheet->getColumnDimension('Q')->setWidth(14);
        $worksheet->getColumnDimension('R')->setWidth(14);
        $worksheet->getColumnDimension('S')->setWidth(14);
        $worksheet->getColumnDimension('T')->setWidth(15);
        $worksheet->getColumnDimension('U')->setWidth(14);
        $worksheet->getColumnDimension('V')->setWidth(15);
        $worksheet->getColumnDimension('W')->setWidth(15);
        $worksheet->getColumnDimension('X')->setWidth(15);
        $worksheet->getColumnDimension('Y')->setWidth(13);
        $worksheet->getColumnDimension('Z')->setWidth(20);
        $worksheet->getColumnDimension('AA')->setWidth(13);
        $worksheet->getColumnDimension('AB')->setWidth(20);

        $worksheet->mergeCells('A8:A10');
        $worksheet->setCellValue("A8", "No.");

        $worksheet->mergeCells('B8:B10');
        $worksheet->setCellValue("B8", "TD No. / \n Cancellation No.");        

        $worksheet->mergeCells('C8:C10');
        $worksheet->setCellValue("C8", "Owner's Name"); 

        $worksheet->mergeCells('D8:D10');
        $worksheet->setCellValue("D8", "Location"); 

        $worksheet->mergeCells('E8:E10');
        $worksheet->setCellValue("E8", "PIN");

        $worksheet->mergeCells('F8:F10');
        $worksheet->setCellValue("F8", "ARP");

        $worksheet->mergeCells('G8:G10');
        $worksheet->setCellValue("G8", "Effectivity");

        $worksheet->mergeCells('H8:H10');
        $worksheet->setCellValue("H8", "Lot No.");

        $worksheet->mergeCells('I8:I10');
        $worksheet->setCellValue("I8", "Block No.");

        $worksheet->mergeCells('J8:J10');
        $worksheet->setCellValue("J8", "Area");

        $worksheet->mergeCells('K8:K10');
        $worksheet->setCellValue("K8", "Structure\nType");


        // Market Value

        $worksheet->mergeCells('L8:Q8');
        $worksheet->setCellValue("L8", "Market Value");

        $worksheet->mergeCells('L9:M9');
        $worksheet->setCellValue("L9", "Land");

        $worksheet->mergeCells('N9:O9');
        $worksheet->setCellValue("N9", "Improvement");

        $worksheet->setCellValue("P9", "Machinery");

        $worksheet->setCellValue("L10", "Residential");
        $worksheet->setCellValue("M10", "Commercial");
        $worksheet->setCellValue("N10", "Residential");
        $worksheet->setCellValue("O10", "Commercial");
        $worksheet->setCellValue("P10", "ResCom");

        $worksheet->mergeCells('Q9:Q10');
        $worksheet->setCellValue("Q9", "SPL");


        // Assess Value
        $worksheet->mergeCells('R8:W8');
        $worksheet->setCellValue("R8", "Assess Value");

        $worksheet->mergeCells('R9:S9');
        $worksheet->setCellValue("R9", "Land");

        $worksheet->mergeCells('T9:U9');
        $worksheet->setCellValue("T9", "Improvement");

        $worksheet->setCellValue("V9", "Machinery");

        $worksheet->setCellValue("R10", "Residential");
        $worksheet->setCellValue("S10", "Commercial");
        $worksheet->setCellValue("T10", "Residential");
        $worksheet->setCellValue("U10", "Commercial");
        $worksheet->setCellValue("V10", "ResCom");

        $worksheet->mergeCells('W9:W10');
        $worksheet->setCellValue("W9", "SPL");

        $worksheet->mergeCells('X8:X10');
        $worksheet->setCellValue("X8", "Previous AV");

        $worksheet->mergeCells('Y8:Y10');
        $worksheet->setCellValue("Y8", "Previous TD");

        $worksheet->mergeCells('Z8:Z10');
        $worksheet->setCellValue("Z8", "Memoranda");

        $worksheet->mergeCells('AA8:AA10');
        $worksheet->setCellValue("AA8", "Taxability");

        $worksheet->mergeCells('AB8:AB10');
        $worksheet->setCellValue("AB8", "Status");

        $worksheet->getStyle('A8:AB10')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('A8:AB10')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $start_row = 12;
        $ctr = 1;

        $for_report = self::pairData($rptas, $control_book);

        foreach ($for_report as $key => $value) {

            $worksheet->mergeCells('A' . $start_row . ':A' . $start_row + 1);
            $worksheet->getStyle('A' . $start_row . ':A' . $start_row + 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A' . $start_row . ':A' . $start_row + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $worksheet->setCellValue('A' . $start_row, $ctr);

            $worksheet->mergeCells("B$start_row:B" . $start_row + 1);
            $worksheet->getStyle('B' . $start_row . ':B' . $start_row + 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('B' . $start_row . ':B' . $start_row + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $td_cancellation = "";

            if($value['cb']){
                $td_cancellation = $value['cb']['cancellation_no'] ? $value['cb']['cancellation_no'] : $value['cb']['td_no'];
            }

            $worksheet->setCellValue('B' . $start_row, $td_cancellation);

            $worksheet->setCellValue('C' . $start_row, $value['rptas'] ? $value['rptas']['Owner'] : "");
            $worksheet->setCellValue('D' . $start_row, $value['rptas'] ? $value['rptas']['Location'] : "");
            $worksheet->setCellValue('E' . $start_row, $value['rptas'] ? $value['rptas']['PIN'] : "");
            $worksheet->setCellValue('F' . $start_row, $value['rptas'] ? $value['rptas']['ARP'] : "");
            $worksheet->setCellValue('G' . $start_row, $value['rptas'] ? $value['rptas']['Effectivity'] : "");

            $worksheet->setCellValue('C' . $start_row + 1, $value['cb'] ? $value['cb']['owner'] : "");
            $worksheet->setCellValue('D' . $start_row + 1, $value['cb'] ? $value['cb']['location'] : "");
            $worksheet->setCellValue('E' . $start_row + 1, $value['cb'] ? $value['cb']['PIN'] : "");
            $worksheet->setCellValue('F' . $start_row + 1, $value['cb'] ? $value['cb']['arp'] : "");
            $worksheet->setCellValue('G' . $start_row + 1, $value['cb'] ? $value['cb']['effectivity'] : "");

            $worksheet->getStyle('H' . $start_row . ":" . 'I' . $start_row + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $worksheet->setCellValue('H' . $start_row, $value['rptas'] ? $value['rptas']['LotNo'] : "");
            $worksheet->setCellValue('I' . $start_row, $value['rptas'] ? $value['rptas']['BlkNo'] : "");
            $worksheet->setCellValue('J' . $start_row, $value['rptas'] ? $value['rptas']['Area'] : "");
            $worksheet->setCellValue('K' . $start_row, $value['rptas'] ? $value['rptas']['StrucType'] : "");

            $worksheet->setCellValue('H' . $start_row + 1, $value['cb'] ? $value['cb']['lot'] : "");
            $worksheet->setCellValue('I' . $start_row + 1, $value['cb'] ? $value['cb']['block'] : "");
            $worksheet->setCellValue('J' . $start_row + 1, $value['cb'] ? $value['cb']['total_area'] : "");
            $worksheet->setCellValue('K' . $start_row + 1, $value['cb'] ? $value['cb']['struct_type'] : "");

            $worksheet->setCellValue('L' . $start_row, $value['rptas'] ? $value['rptas']['mvlr'] : "0");
            $worksheet->setCellValue('M' . $start_row, $value['rptas'] ? $value['rptas']['mvlc'] : "0");
            $worksheet->setCellValue('N' . $start_row, $value['rptas'] ? $value['rptas']['mvir'] : "0");
            $worksheet->setCellValue('O' . $start_row, $value['rptas'] ? $value['rptas']['mvic'] : "0");
            $worksheet->setCellValue('P' . $start_row, $value['rptas'] ? $value['rptas']['mvm'] : "0");
            $worksheet->setCellValue('Q' . $start_row, $value['rptas'] ? $value['rptas']['mvs'] : "0");
            $worksheet->setCellValue('R' . $start_row, $value['rptas'] ? $value['rptas']['avlr'] : "0");
            $worksheet->setCellValue('S' . $start_row, $value['rptas'] ? $value['rptas']['avlc'] : "0");
            $worksheet->setCellValue('T' . $start_row, $value['rptas'] ? $value['rptas']['avir'] : "0");
            $worksheet->setCellValue('U' . $start_row, $value['rptas'] ? $value['rptas']['avic'] : "0");
            $worksheet->setCellValue('V' . $start_row, $value['rptas'] ? $value['rptas']['avm'] : "0");
            $worksheet->setCellValue('W' . $start_row, $value['rptas'] ? $value['rptas']['avs'] : "0");
            $worksheet->setCellValue('X' . $start_row, $value['rptas'] ? $value['rptas']['PreviousAV'] : "0");

            $worksheet->setCellValue('L' . $start_row + 1, $value['cb'] ? $value['cb']['lr_mv'] : "0");
            $worksheet->setCellValue('M' . $start_row + 1, $value['cb'] ? $value['cb']['lc_mv'] : "0");
            $worksheet->setCellValue('N' . $start_row + 1, $value['cb'] ? $value['cb']['ir_mv'] : "0");
            $worksheet->setCellValue('O' . $start_row + 1, $value['cb'] ? $value['cb']['ic_mv'] : "0");
            $worksheet->setCellValue('P' . $start_row + 1, $value['cb'] ? $value['cb']['m_mv'] : "0");
            $worksheet->setCellValue('Q' . $start_row + 1, $value['cb'] ? $value['cb']['s_mv'] : "0");
            $worksheet->setCellValue('R' . $start_row + 1, $value['cb'] ? $value['cb']['lr_av'] : "0");
            $worksheet->setCellValue('S' . $start_row + 1, $value['cb'] ? $value['cb']['lc_av'] : "0");
            $worksheet->setCellValue('T' . $start_row + 1, $value['cb'] ? $value['cb']['ir_av'] : "0");
            $worksheet->setCellValue('U' . $start_row + 1, $value['cb'] ? $value['cb']['ic_av'] : "0");
            $worksheet->setCellValue('V' . $start_row + 1, $value['cb'] ? $value['cb']['m_av'] : "0");
            $worksheet->setCellValue('W' . $start_row + 1, $value['cb'] ? $value['cb']['s_av'] : "0");

            if($value['cb']){
                $prev_arp_cb = self::formatPrevAvCB(array(
                    array($value['cb']['pvr_au'], $value['cb']['pvr_av']),
                    array($value['cb']['pvc_au'], $value['cb']['pvc_av']),
                    array($value['cb']['pvm_au'], $value['cb']['pvm_av']),
                    array($value['cb']['pvs_au'], $value['cb']['pvs_av']),
                ));
                $worksheet->setCellValue('X' . $start_row + 1, $prev_arp_cb);
            } else{
                $worksheet->setCellValue('X' . $start_row + 1, "");
            }
       
            $worksheet->mergeCells("Y$start_row:Y" . $start_row + 1);
            $worksheet->getStyle('Y' . $start_row . ':Y' . $start_row + 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('Y' . $start_row . ':Y' . $start_row + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            if($value['cb']){
                $worksheet->setCellValue('Y' . $start_row, $value['cb'] ? $value['cb']['prev_td_no'] : "");
            }

            $worksheet->setCellValue('Z'  . $start_row, $value['rptas'] ? $value['rptas']['Memoranda'] : "");
            $worksheet->setCellValue('AA' . $start_row, $taxability == 'T' ? 'Taxable' : 'Exempt');

            $worksheet->setCellValue('Z'  . $start_row + 1, $value['cb'] ? $value['cb']['memoranda'] : "");
            $worksheet->setCellValue('AA' . $start_row + 1, $value['cb'] ? $value['cb']['taxability'] : "");

            $worksheet->mergeCells("AB$start_row:AB" . $start_row + 1);
            $worksheet->getStyle('AB' . $start_row . ':AB' . $start_row + 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('AB' . $start_row . ':AB' . $start_row + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $worksheet->setCellValue('AB' . $start_row, $value['cb'] ? $value['cb']['status_desc'] : "Not on Control Book");

            $start_row  = $start_row + 3;
            $ctr        = $ctr + 1;
        }

        $summary_data = self::excelTotalAndColorFormat($worksheet, 12, $start_row);

        $worksheet->setSelectedCells('A1');

        $worksheet2 = $spreadsheet->createSheet();
        $worksheet2->setTitle('Summary');

        $worksheet2->getColumnDimension('A')->setWidth(20);
        $worksheet2->getColumnDimension('B')->setWidth(20);
        $worksheet2->getColumnDimension('C')->setWidth(2);
        $worksheet2->getColumnDimension('D')->setWidth(25);
        $worksheet2->getColumnDimension('E')->setWidth(25);
        $worksheet2->getColumnDimension('F')->setWidth(30);

        $worksheet2->setCellValue('D2', 'RPTAS Data');
        $worksheet2->setCellValue('E2', 'Control BooK Data');
        $worksheet2->setCellValue('F2', "Variance\n(RPTAS Data - Control Book Data)");

        $worksheet2->setCellValue('A3', 'Total Market Value');
        $worksheet2->setCellValue('B4', 'Land');
        $worksheet2->setCellValue('B5', '---  Residential');
        $worksheet2->setCellValue('B6', '---  Commercial');
        $worksheet2->setCellValue('B7', 'Improvement');
        $worksheet2->setCellValue('B8', '---  Residential');
        $worksheet2->setCellValue('B9', '---  Commercial');
        $worksheet2->setCellValue('B10', 'Machinery');
        $worksheet2->setCellValue('B11', 'Special');

        $worksheet2->setCellValue('A13', 'Total Assess Value');
        $worksheet2->setCellValue('B14', 'Land');
        $worksheet2->setCellValue('B15', '---  Residential');
        $worksheet2->setCellValue('B16', '---  Commercial');
        $worksheet2->setCellValue('B17', 'Improvement');
        $worksheet2->setCellValue('B18', '---  Residential');
        $worksheet2->setCellValue('B19', '---  Commercial');
        $worksheet2->setCellValue('B20', 'Machinery');
        $worksheet2->setCellValue('B21', 'Special');

        $worksheet2->setCellValue('A23', 'Total Assess Value');

        // Market Value

        $worksheet2->setCellValue('D5', $summary_data['rptas']['mv']['land']['res']);
        $worksheet2->setCellValue('D6', $summary_data['rptas']['mv']['land']['com']);
        $worksheet2->setCellValue('D8', $summary_data['rptas']['mv']['imp']['res']);
        $worksheet2->setCellValue('D9', $summary_data['rptas']['mv']['imp']['com']);
        $worksheet2->setCellValue('D10', $summary_data['rptas']['mv']['mach']);
        $worksheet2->setCellValue('D11', $summary_data['rptas']['mv']['spl']);

        $worksheet2->setCellValue('E5', $summary_data['cb']['mv']['land']['res']);
        $worksheet2->setCellValue('E6', $summary_data['cb']['mv']['land']['com']);
        $worksheet2->setCellValue('E8', $summary_data['cb']['mv']['imp']['res']);
        $worksheet2->setCellValue('E9', $summary_data['cb']['mv']['imp']['com']);
        $worksheet2->setCellValue('E10', $summary_data['cb']['mv']['mach']);
        $worksheet2->setCellValue('E11', $summary_data['cb']['mv']['spl']);

        $worksheet2->setCellValue('F5', $worksheet2->getCell('D5')->getValue() - $worksheet2->getCell('E5')->getValue());
        $worksheet2->setCellValue('F6', $worksheet2->getCell('D6')->getValue() - $worksheet2->getCell('E6')->getValue());
        $worksheet2->setCellValue('F8', $worksheet2->getCell('D8')->getValue() - $worksheet2->getCell('E8')->getValue());
        $worksheet2->setCellValue('F9', $worksheet2->getCell('D9')->getValue() - $worksheet2->getCell('E9')->getValue());
        $worksheet2->setCellValue('F10', $worksheet2->getCell('D10')->getValue() - $worksheet2->getCell('E10')->getValue());
        $worksheet2->setCellValue('F11', $worksheet2->getCell('D11')->getValue() - $worksheet2->getCell('E11')->getValue());

        // Assess Value

        $worksheet2->setCellValue('D15', $summary_data['rptas']['av']['land']['res']);
        $worksheet2->setCellValue('D16', $summary_data['rptas']['av']['land']['com']);
        $worksheet2->setCellValue('D18', $summary_data['rptas']['av']['imp']['res']);
        $worksheet2->setCellValue('D19', $summary_data['rptas']['av']['imp']['com']);
        $worksheet2->setCellValue('D20', $summary_data['rptas']['av']['mach']);
        $worksheet2->setCellValue('D21', $summary_data['rptas']['av']['spl']);

        $worksheet2->setCellValue('E15', $summary_data['cb']['av']['land']['res']);
        $worksheet2->setCellValue('E16', $summary_data['cb']['av']['land']['com']);
        $worksheet2->setCellValue('E18', $summary_data['cb']['av']['imp']['res']);
        $worksheet2->setCellValue('E19', $summary_data['cb']['av']['imp']['com']);
        $worksheet2->setCellValue('E20', $summary_data['cb']['av']['mach']);
        $worksheet2->setCellValue('E21', $summary_data['cb']['av']['spl']);

        $worksheet2->setCellValue('F15', $worksheet2->getCell('D15')->getValue() - $worksheet2->getCell('E15')->getValue());
        $worksheet2->setCellValue('F16', $worksheet2->getCell('D16')->getValue() - $worksheet2->getCell('E16')->getValue());
        $worksheet2->setCellValue('F18', $worksheet2->getCell('D18')->getValue() - $worksheet2->getCell('E18')->getValue());
        $worksheet2->setCellValue('F19', $worksheet2->getCell('D18')->getValue() - $worksheet2->getCell('E19')->getValue());
        $worksheet2->setCellValue('F20', $worksheet2->getCell('D20')->getValue() - $worksheet2->getCell('E20')->getValue());
        $worksheet2->setCellValue('F21', $worksheet2->getCell('D21')->getValue() - $worksheet2->getCell('E21')->getValue());


        // Previouys AV

        $worksheet2->setCellValue('D23', $summary_data['rptas']['prev_av']);
        $worksheet2->setCellValue('E23', $summary_data['cb']['prev_av']);
        $worksheet2->setCellValue('F23', $worksheet2->getCell('D23')->getValue() - $worksheet2->getCell('E23')->getValue());


        $worksheet2->setSelectedCells('A1');
        $spreadsheet->setActiveSheetIndexByName('Report');

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

    private function pairData($rptas, $cb){
        
        $arr_rptas      = [];
        $arr_control    = [];
        $joined         = [];

        foreach ($rptas as $key => $value) {
            $k = $value['PIN'] . "_" . $value['ARP'];
            $arr_rptas[$k] = $value;
        }

        foreach ($cb as $key => $value) {
            $k = $value['PIN'] . "_" . $value['arp'];
            $arr_control[$k] = $value;
        }

        foreach ($cb as $key => $value) {
            
            $k = $value['PIN'] . "_" . $value['arp'];

            if($value['status_desc'] == 'Not on RPTAS'){
                $joined[] = array(
                    'rptas' => [],
                    'cb'    => $arr_control[$k],
                );
            } elseif ($value['status_desc'] == 'Not on Control Book') {
                $joined[] = array(
                    'rptas' => $arr_rptas[$k],
                    'cb'    => [],
                );
            } else{
                $joined[] = array(
                    'rptas' => $arr_rptas[$k],
                    'cb'    => $arr_control[$k],
                );
            }
        }

        return $joined;

    }

    private function excelTotalAndColorFormat($ws, $start, $end){

        $totals = array(
            'rptas' => array(
                'mv' => array(
                    'land' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'imp' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'mach' => 0,
                    'spl' => 0,
                ),
                'av' => array(
                    'land' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'imp' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'mach' => 0,
                    'spl' => 0,
                ),
                'prev_av' => 0,
            ),
            'cb' => array(
                'mv' => array(
                    'land' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'imp' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'mach' => 0,
                    'spl' => 0,
                ),
                'av' => array(
                    'land' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'imp' => array(
                        'res' => 0,
                        'com' => 0
                    ),
                    'mach' => 0,
                    'spl' => 0,
                ),
                'prev_av' => 0,
            ),
        );

        for ($i=$start; $i < $end; $i+=3) { 

            if($ws->getCell('AB' . $i) == "Not on Control Book"){
                $ws->getStyle('C' . $i + 1 . ':X' . $i + 1)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
                $ws->getStyle('Z' . $i + 1 . ':AA' . $i + 1)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            } elseif($ws->getCell('AB' . $i) == "Not on RPTAS"){
                $ws->getStyle('C' . $i . ':X' . $i)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
                $ws->getStyle('Z' . $i . ':AA' . $i)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF0000');
            } else{
                foreach (self::arrColumns() as $key => $value) {
                    if(self::checkIfMatch($ws->getCell($value . $i), $ws->getCell($value . $i + 1))){
                        $ws->getStyle($value . $i . ':' . $value . '' . $i + 1)->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000');
                    }
                }
            }

            $totals['rptas']['mv']['land']['res']   = $totals['rptas']['mv']['land']['res'] + $ws->getCell('L' . $i)->getValue();
            $totals['cb']['mv']['land']['res']      = $totals['cb']['mv']['land']['res'] + $ws->getCell('L' . $i + 1)->getValue();

            $totals['rptas']['mv']['land']['com']   = $totals['rptas']['mv']['land']['com'] + $ws->getCell('M' . $i)->getValue();
            $totals['cb']['mv']['land']['com']      = $totals['cb']['mv']['land']['com'] + $ws->getCell('M' . $i + 1)->getValue();

            $totals['rptas']['mv']['imp']['res']    = $totals['rptas']['mv']['imp']['res'] + $ws->getCell('N' . $i)->getValue();
            $totals['cb']['mv']['imp']['res']       = $totals['cb']['mv']['imp']['res'] + $ws->getCell('N' . $i + 1)->getValue();

            $totals['rptas']['mv']['imp']['com']    = $totals['rptas']['mv']['imp']['com'] + $ws->getCell('O' . $i)->getValue();
            $totals['cb']['mv']['imp']['com']       = $totals['cb']['mv']['imp']['com'] + $ws->getCell('O' . $i + 1)->getValue();

            $totals['rptas']['mv']['mach']          = $totals['rptas']['mv']['mach'] + $ws->getCell('P' . $i)->getValue();
            $totals['cb']['mv']['mach']             = $totals['cb']['mv']['mach'] + $ws->getCell('P' . $i + 1)->getValue();

            $totals['rptas']['mv']['spl']           = $totals['rptas']['mv']['spl'] + $ws->getCell('Q' . $i)->getValue();
            $totals['cb']['mv']['spl']              = $totals['cb']['mv']['spl'] + $ws->getCell('Q' . $i + 1)->getValue();


            $totals['rptas']['av']['land']['res']   = $totals['rptas']['av']['land']['res'] + $ws->getCell('R' . $i)->getValue();
            $totals['cb']['av']['land']['res']      = $totals['cb']['av']['land']['res'] + $ws->getCell('R' . $i + 1)->getValue();

            $totals['rptas']['av']['land']['com']   = $totals['rptas']['av']['land']['com'] + $ws->getCell('S' . $i)->getValue();
            $totals['cb']['av']['land']['com']      = $totals['cb']['av']['land']['com'] + $ws->getCell('S' . $i + 1)->getValue();

            $totals['rptas']['av']['imp']['res']    = $totals['rptas']['av']['imp']['res'] + $ws->getCell('T' . $i)->getValue();
            $totals['cb']['av']['imp']['res']       = $totals['cb']['av']['imp']['res'] + $ws->getCell('T' . $i + 1)->getValue();
            
            $totals['rptas']['av']['imp']['com']    = $totals['rptas']['av']['imp']['com'] + $ws->getCell('U' . $i)->getValue();
            $totals['cb']['av']['imp']['com']       = $totals['cb']['av']['imp']['com'] + $ws->getCell('U' . $i + 1)->getValue();

            $totals['rptas']['av']['mach']          = $totals['rptas']['av']['mach'] + $ws->getCell('V' . $i)->getValue();
            $totals['cb']['av']['mach']             = $totals['cb']['av']['mach'] + $ws->getCell('V' . $i + 1)->getValue();

            $totals['rptas']['av']['spl']          = $totals['rptas']['av']['spl'] + $ws->getCell('W' . $i)->getValue();
            $totals['cb']['av']['spl']             = $totals['cb']['av']['spl'] + $ws->getCell('W' . $i + 1)->getValue();

            $totals['rptas']['prev_av']             = $totals['rptas']['prev_av'] + self::prevAvTotal($ws->getCell('X' . $i)->getValue());
            $totals['cb']['prev_av']                = $totals['cb']['prev_av'] + self::prevAvTotal($ws->getCell('X' . $i + 1)->getValue());
          
        }

        return $totals;

    }

    private function prevAvTotal($data){

        if($data){
            if(strpos($data, "\n") !== false){
    
                $sum = 0;
                $exp = explode("\n", $data);
                foreach($exp as $value){
                    $prev = explode("-", $value);
                    $sum = $sum + $prev[1];
                }
    
                return $sum;
            } else{
                return $data;
            }
        } else{
            return 0;
        }

    }

    private function checkIfMatch($s1, $s2){
        return !(trim($s1) == trim($s2));
    }

    private function formatPrevAvCB($data){

        $output = "";
        $vals = 0;

        foreach($data as $key => $value){
            if($value[0]){
                $vals = $vals + 1;
            }
        }

        if($vals === 0){
            $output = "0";
        } elseif ($vals === 1) {
            foreach($data as $key => $value){
                if($value[0]){
                    $output = "" . $value[1];
                }
            }
        } else{
            foreach($data as $key => $value){
                if($value[0]){
                    $output = $output . $value[0] . "-" . $value[1] . "\n";
                }
            }            
        }

        return rtrim($output);
    }

    private function arrColumns(){
        return array(
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Z',
            'AA',
        );
    }

    public function editGetBgy(){

        return DataRecon::selectRaw("DISTINCT substring_index(substring_index(PIN,'-',3),'-',-(1)) as bgy")
                            ->orderBy('bgy')
                            ->pluck('bgy');

    }

    public function editGetList(Request $request){

        return DataRecon::select('id', 'PIN', 'arp', 'prev_arp', 'status_desc')
                        ->whereRaw("substring_index(substring_index(PIN,'-',3),'-',-(1)) = '" . $request['barangay'] . "'")
                        ->where('taxability', $request['taxability'])
                        ->orderBy('PIN')
                        ->get();

    }

    public function editGetData(Request $request){

        return DataRecon::where('id', $request['id'])->first();

    }
    
    public function editUpdateData(Request $request){

        $data = $request['data'];
        $data['taxability']         = $request['taxability'];
        $data['active_cancel']      = $request['activeCancel'];
        $data['changed_tax']        = $request['changeTaxability'];
        $data['updated_by']         = $request['user'];

        $td_cancel = explode('-', $data['td_no']);

        if(strlen($td_cancel[0]) > 2){
            $data['cancellation_no'] = $data['td_no'];
            $data['td_no'] = "";
        }


        DataRecon::find($request['id'])->update($data);

        return "Record Updated";
    }

}
