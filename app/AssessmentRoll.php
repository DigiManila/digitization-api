<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentRoll extends Model
{
    public function scanned()
    {
        return $this->hasMany('App\ScannedDocument', 'assessment_roll_id', 'id')
                    ->where('is_assessment_roll', true);
    }

    public function polygon()
    {
        return $this->hasOne('App\MapPolygon', 'pin', 'pin')->select('pin', 'polygon', 'gmap_polygon', 'center_coords', 'barangay');
    }

}
