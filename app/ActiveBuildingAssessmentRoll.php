<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveBuildingAssessmentRoll extends Model
{
    use HasFactory;

    protected $table = 'active_building_assessment_roll';
}
