<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\CancellationLog;

use Storage;
use DB;

class ReleaseCancellationController extends Controller
{

    public function getDashboardData(){

        return self::getData(date("Y"));

    }

    public function updateDashboardData(Request $request){
        return self::getData(date($request['year']));
    }

    private function getData($y){

        $total = CancellationLog::select('copies')
                                ->whereNotNull('created_at')
                                ->whereBetween('created_at', ["$y-01-01 00:00:00", "$y-12-31 23:59:59"])
                                ->sum('copies');

        // $count = ReleaseTaxDec::select(DB::raw('count(id) as `data`'),DB::raw("CONCAT_WS('-',MONTH(created_at),YEAR(created_at)) as monthyear"))
        //                             ->whereNotNull('created_at')
        //                             ->whereYear('created_at', $y)
        //                             ->groupby('monthyear')
        //                             ->get();

        $count = CancellationLog::select(DB::raw('sum(copies) as sums'), DB::raw("DATE_FORMAT(created_at,'%m') as months"))
                                ->whereNotNull('created_at')
                                ->whereYear('created_at', $y)
                                ->groupBy('months')
                                ->get();
        // \Log::info($count);

        // $grouped        = self::groupByFolder($files);
        $countByMonth   = self::countByMonth($count);
        $output         = self::formatData($countByMonth, $y);
        // $output         = self::formatData($count, $y);

        $output['total_count']  = $total;
        $output['table_data']   = self::getTableData($output['curr_year']);
        $output['year_list']    = self::getYearList();
        $output['month_list']   = self::getMonthList($y);
        $output['day_list']     = self::getAllDays($output['table_data']);

        return $output;

    }

    // public function getYearMonth(Request $request){
        
    //     list($year, $month) = explode("-", $request['yearMonth']);

    //     $list = Storage::disk('svnas')->allDirectories("/released_taxdecs");
    //     $files = Storage::disk('svnas')->allFiles("/released_taxdecs");

    //     \Log::info($files);

    // }

    public function exportToExcel(Request $request){

        $year   = $request['year'];
        $month  = $request['month'];
        $day    = $request['day'];
        $type   = $request['type'];

        $total = $request['total'];

        if($type == 'Yearly'){
            $data = CancellationLog::selectRaw("or_no, can_no, requestor, printed_by, certifier, processed_by, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies, verifier")
                    ->whereYear('created_at', $year)
                    ->get()
                    ->toArray();

            $newData = self::groupByMonthReport($data);
            
        } elseif ($type == 'Monthly') {

            $data = CancellationLog::selectRaw("or_no, can_no, requestor, printed_by, certifier, processed_by, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies, verifier")
                    ->whereYear('created_at', $year)
                    ->whereMonth('created_at', self::getMonthValueByNumber($month))
                    ->get()
                    ->toArray();

            $newData = self::groupByDayReport($data);
            
            $total = count($data);

        } else{

            $data = CancellationLog::selectRaw("or_no, can_no, requestor, printed_by, certifier, processed_by, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies, verifier")
                    ->whereYear('created_at', $year)
                    ->whereMonth('created_at', self::getMonthValueByNumber($month))
                    ->whereDay('created_at', sprintf('%02d', $day))
                    ->get()
                    ->toArray();

            $newData = self::groupByDayReport($data);
            
            $total = count($data);   

        }

        return self::createExcel($newData, $total, $type, $month, $day, $year);

    }

    public function getFilePath(Request $request){

        $output = [];

        $data = CancellationLog::select('or_path', 'log_path', 'can_path')->where('id', $request->id)->first()->toArray();
        
        foreach ($data as $key => $value) {
            $output[] = array(
                'key' => $key,
                'val' => $value
            );
        }

        return $output;

    }

    public function getFile(Request $request){

        return Storage::disk('svnas')->get($request->path);

    }

    private function groupByFolder($data){

        $output = [];

        foreach ($data as $key => $value) {
            
            $exp = explode("/", $value);

            if(strpos($exp[3], ".pdf") !== false){
                $output[$exp[2]][] = $value;
            }
            
        }

        return $output;

    }

    private function groupByMonthReport($data){

        $output = [];

        foreach ($data as $key => $value) {
            
            $exp = explode(" ", $value['date_released']);
            $output[$exp[0]][] = $value;
            
        }

        return $output;

    }

    private function groupByDayReport($data){

        $output = [];

        foreach ($data as $key => $value) {
            
            $exp = explode(" ", $value['date_released']);
            $day = str_replace(",", "", $exp[1]);
            
            $output[$day][] = $value;
            
        }

        return $output;        
    }

    private function countByMonth($data){

        $output = [
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        ];

        $month_arr = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec',
        ];

        foreach ($data as $key => $value) {

            // $exp_key = explode("-", $value['monthyear']);

            $month_word             = $month_arr[sprintf('%02d', $value['months'])];
            $output[$month_word]    = $value['sums'];

        }

        // foreach ($data as $key => $value) {

        //     $exp_key = explode("-", $key);

        //     $month_word             = $month_arr[$exp_key[1]];
        //     $output[$month_word]    = $output[$month_word] + count($value);

        // }

        return $output;

    }

    private function formatData($data, $y){

        $chart_data = [];
        $curr_year  = $y;

        foreach ($data as $key => $value) {
            $chart_data[] = array(
                'month' => $key,
                'count' => $value
            );
        }

        return compact('chart_data', 'curr_year');

    }

    private function getTableData($year){

        $data = CancellationLog::selectRaw("id, or_no, can_no, requestor, DATE_FORMAT(created_at, '%M %e, %Y') as date_released, copies")
                                ->whereYear('created_at', $year)
                                ->get();


        return $data;

    }

    private function getYearList(){

        $list = [];

        $currYear = (int) date('Y');

        while ($currYear >= 2022) {
            $list[] = $currYear;
            $currYear--;
        }

        return $list;

    }

    private function createExcel($data, $t, $type, $month, $day, $year){

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Summary Tab

        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle("Summary");
        $spreadsheet->getSheetByName("Summary");
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->mergeCells('B2:M3');
        $worksheet->getStyle("B2:M3")->getFont()->setBold(true);

        $worksheet->getStyle('B2:M3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle('B2:M3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $worksheet->setCellValue("B2", "$type Report of Released Cancellation");
        $worksheet->getStyle("B2")->getFont()->setSize(16);

        $worksheet->mergeCells('B4:M4');
        $worksheet->getStyle('B4:M4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("B4", "as of:  " . date("F j, Y"));
        $worksheet->getStyle("B4")->getFont()->setSize(8);

        // Set Borders
        $worksheet->getStyle('B2:M4')
                ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $worksheet->getStyle('B2:M4')
                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $worksheet->getStyle('B2:M4')
                ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $worksheet->getStyle('B2:M4')
                ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $worksheet->mergeCells('D8:F8');
        $worksheet->mergeCells('D9:F9');
        $worksheet->mergeCells('D10:F10');
        $worksheet->mergeCells('D11:F11');

        $worksheet->setCellValue("D11", "Total: ")
                    ->setCellValue("G11", $t)
                    ->setCellValue("D8", "Total Request with OR: ")
                    ->setCellValue("D9", "Total Special Request: ");


        $worksheet->mergeCells('D15:G15');
        $worksheet->setCellValue("D15", "Total Processed By: ");

        $total_processed_by = self::getTotalByProcessor($data);

        $row = 17;

        foreach($total_processed_by as $key => $value){
            $worksheet->mergeCells("D{$row}:G{$row}");
            $worksheet->setCellValue("D{$row}", $key)
                        ->setCellValue("H{$row}", $value);
            $row++;
        }

        $total_or = 0;
        $total_sr = 0;

        foreach ($data as $key => $value) {

            $or = 0;
            $sr = 0;

            if($type == 'Yearly'){
                $tab_name = $key;
            } elseif ($type == 'Monthly') {
                $tab_name = $month . " " . $key;
            } else{
                $tab_name = $month . " " . $day . " " . $year;
            }

            $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $tab_name);
            $spreadsheet->addSheet($myWorkSheet);
            $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);
            
            $worksheet = $spreadsheet->getActiveSheet();
    
            $worksheet->getColumnDimension('A')->setWidth(20);
            $worksheet->getColumnDimension('B')->setWidth(20);
            $worksheet->getColumnDimension('C')->setWidth(60);
            $worksheet->getColumnDimension('D')->setWidth(50);
            $worksheet->getColumnDimension('E')->setWidth(30);
            $worksheet->getColumnDimension('F')->setWidth(30);
            $worksheet->getColumnDimension('G')->setWidth(30);
            $worksheet->getColumnDimension('H')->setWidth(20);
            $worksheet->getColumnDimension('I')->setWidth(10);
    
    
            // $worksheet->setCellValue("A2", "Idle Lands (Breakdown Per Barangays)");
        
            $worksheet->setCellValue("A9", "Cancellation Number")
                    ->setCellValue("B9", "OR Number")
                    ->setCellValue("C9", "Requestor")
                    ->setCellValue("D9", "Printed By")
                    ->setCellValue("E9", "Certifier")
                    ->setCellValue("F9", "Processed By")
                    ->setCellValue("G9", "Verifier")
                    ->setCellValue("H9", "Date Released")
                    ->setCellValue("I9", "Copies");
    
            $startRow = 10;
            $row = $startRow;

            foreach ($value as $k => $val) {

                if(strpos($val['or_no'], "SR") !== false || $val['or_no'] == '7132022'){
                    // $sr++;
                    $sr = $sr + $val['copies'];
                } else{
                    // $or++;
                    $or = $or + $val['copies'];
                }
    
                $worksheet->setCellValue("A{$row}", $val['can_no']);
                $worksheet->setCellValue("B{$row}", $val['or_no']);
                $worksheet->setCellValue("C{$row}", $val['requestor']);
                $worksheet->setCellValue("D{$row}", $val['printed_by']);
                $worksheet->setCellValue("E{$row}", $val['certifier']);
                $worksheet->setCellValue("F{$row}", $val['processed_by']);
                $worksheet->setCellValue("G{$row}", $val['verifier']);
                $worksheet->setCellValue("H{$row}", $val['date_released']);
                $worksheet->setCellValue("I{$row}", $val['copies']);

                $worksheet->getStyle("B{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $row++;
                
            }

            // $worksheet->setCellValue("F10", $or)
            //             ->setCellValue("F11", $sr);

            $append_date = "";

            if($type == 'Yearly'){
                $append_date = $key;
            } elseif ($type == 'Monthly') {
                $append_date = $month . " " . $key;
            } else{
                $append_date = $month . " " . $day . ", " . $year;   
            }

            $total_sheet = $or + $sr;

            $worksheet->setCellValue("A3", "Total Released with OR for {$append_date}: ")
                        ->setCellValue("A6", "Total Released for {$append_date}: ")
                        ->setCellValue("C6", $total_sheet)
                        ->setCellValue("C3", $or)
                        ->setCellValue("A4", "Total Released SR for {$append_date}: ")
                        ->setCellValue("C4", $sr);

            $worksheet->getStyle('C3:C6')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            $worksheet->setSelectedCells('A1');

            $total_or = $total_or + $or;
            $total_sr = $total_sr + $sr;

        }

        $worksheet = $spreadsheet->setActiveSheetIndex(0);

        $worksheet->setCellValue("G8", $total_or)
                ->setCellValue("G9", $total_sr);

        $worksheet->setSelectedCells('A1');

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

    private function getMonthList($y){

        $month_arr = [
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];

        $output = [];

        $list = CancellationLog::selectRaw("Distinct month(created_at) as month")
                            ->whereYear('created_at', '=', $y)
                            ->get();

        foreach ($list as $key => $value) {
            $output[] = $month_arr[$value['month']];
        }

        return $output;

    }
    
    private function getAllDays($data){
        
        $output = [];

        foreach ($data as $key => $value) {
            
            $date = $value['date_released'];

            $exp = explode(" ", $date);

            $month  = $exp[0];
            $day    = str_replace(",", "", $exp[1]);

            if(!isset($output[$month])){
                $output[$month] = [];
            }

            if(!in_array($day, $output[$month])){
                $output[$month][] = str_replace(",", "", $day);
            }

        }

        return $output;

    }

    private function getMonthValueByNumber($month){

        switch ($month) {
            case 'January':
                return "01";
                break;

            case 'February':
                return "02";
                break;

            case 'March':
                return "03";
                break;

            case 'April':
                return "04";
                break;

            case 'May':
                return "05";
                break;

            case 'June':
                return "06";
                break;

            case 'July':
                return "07";
                break;

            case 'August':
                return "08";
                break;

            case 'September':
                return "09";
                break;

            case 'October':
                return "10";
                break;

            case 'November':
                return "11";
                break;

            case 'December':
                    return "12";
                    break;
            
            default:
                return 0;
                break;
        }

    }

    private function getTotalByProcessor($data){

        $output = [];

        foreach ($data as $key => $value) {
            foreach ($value as $k => $v) {

                $name = strtoupper($v['processed_by']);

                if(!isset($output[$name])){
                    $output[$name] = $v['copies'];
                } else{
                    $output[$name] = $output[$name] + $v['copies'];
                }
            }
        }

        ksort($output);

        return $output;

    }


}
