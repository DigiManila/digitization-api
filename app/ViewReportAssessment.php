<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewReportAssessment extends Model
{
    use HasFactory;

    protected $table = 'reports_assessment_active';
}
