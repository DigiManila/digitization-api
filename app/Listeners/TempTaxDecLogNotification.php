<?php

namespace App\Listeners;

use App\Events\TempTaxDecLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TempTaxDecLogNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TempTaxDecLog  $event
     * @return void
     */
    public function handle(TempTaxDecLog $event)
    {
        $event->subject = 'TempTaxDec';
        $event->description = 'Save';

        Session::activity($event->subject)
            ->by($event->user)
            ->log($event->description);
    }
}
