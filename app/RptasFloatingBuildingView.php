<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasFloatingBuildingView extends Model
{
    use HasFactory;
    protected $table = 'rptas_floating_building';
}
