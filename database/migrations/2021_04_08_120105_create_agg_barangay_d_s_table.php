<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAggBarangayDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agg_barangay_d_s', function (Blueprint $table) {
            $table->id();
            $table->string('brgyNo')->nullable();
            $table->bigInteger('countBrgy')->nullable();
            $table->decimal('area',20,2)->nullable();
            $table->decimal('market_value',20,2)->nullable();
            $table->decimal('CurrentAssessValue',20,2)->nullable();
            $table->decimal('PreviousAssessValue',20,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agg_barangay_d_s');
    }
}
