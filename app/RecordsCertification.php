<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordsCertification extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function cert_data()
    {
        return $this->hasMany('App\RecordsCertificationData', 'cert_id', 'id');
    }

}
