<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasTaxdecDistinctBldgCoreMla extends Model
{
    use HasFactory;

    protected $table = 'rptas_taxdec_distinct_bldg_core_mla';
}
