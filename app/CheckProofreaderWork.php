<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckProofreaderWork extends Model
{
    use HasFactory;

    protected $table = 'check_proofreader_work';
}
