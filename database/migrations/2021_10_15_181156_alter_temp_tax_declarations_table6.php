<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTempTaxDeclarationsTable6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_tax_declarations', function (Blueprint $table) {
            $table->string("kind")->nullable()->change();
            $table->decimal("currentAssessValue",20,2)->nullable()->change();
            $table->decimal("area",20,2)->nullable()->change();
            $table->decimal("market_value",20,2)->nullable()->change();
           
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_tax_declarations', function (Blueprint $table) {
            //
        });
    }
}
