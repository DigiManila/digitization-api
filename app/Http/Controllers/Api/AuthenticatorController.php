<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Api\EncryptDecryptController;
use App\OrTaxdec;
use App\RecordsTaxDecData;
use App\ReleaseTaxDec;
use App\ReleaseTdVerification;
use App\ReleaseCancellationVerification;
use App\CancellationLog;

class AuthenticatorController extends Controller
{
    public function getData(Request $request){

        $decryptIt = new EncryptDecryptController;
        $id = $decryptIt->decryptString($request['barcode_text'], self::returnKey());
        $data = [];

        if(strpos(".",$id) !== false || !is_numeric($id)){

            if($request['flag'] === "TD"){
                $data = ReleaseTaxDec::where('barcode', $request['barcode_text'])->first();
            } else{
                $data = CancellationLog::where('barcode', $request['barcode_text'])->first();
            }

            if(!$data){
                return response()->json([
                    "data" => [],
                    'message' => "Invalid Barcode!!\nError Code : 2",
                    'status' => 2
                ], 200);
            }

        } else{
            $id = intval($id);
            if($request['flag'] === "TD"){
                $data = ReleaseTaxDec::where('id', $id)->first();
            } else{
                $data = CancellationLog::where('id', $id)->first();
            }
            
        }

        if($data){

            $data['date_released'] = date("F j, Y", strtotime($data['created_at']));

            return response()->json([
                "data" => $data,
                'message' => "Authenticated Data found",
                'status' => 1
            ], 200);   
        }

        return response()->json([
            "data" => [],
            'message' => "No Data Found!!\nError Code : 4",
            'status' => 4
        ], 200);        

    }

    private function returnKey(){
        return "DigitizationTeam=array(Bong,King,Jc);";
    }

    public function logData(Request $request){

        $data = $request->all();

        if($request['flag'] === "TD"){
            unset($data['cancellation_no']);
            unset($data['flag']);
            ReleaseTdVerification::create($data);
        } else{
            unset($data['td_no']);
            unset($data['flag']);
            ReleaseCancellationVerification::create($data);
        }

        return "OK";

    }
}
