<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Profile;

class LoginController extends Controller
{
    public function login(Request $request){

        $data = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        if(!auth()->attempt($data)){
            return response()->json([
                "data" => [],
                'message' => 'Invalid Credentials',
                'status' => 0
            ], 200);
        }

        $profile = Profile::select('first_name', 'middle_name', 'last_name', 'user_id')
                    ->where('user_id', auth()->user()->id)
                    ->first();

        $token = auth()->user()->createToken('token')->accessToken;

        // $path = storage_path("app" . DIRECTORY_SEPARATOR . $profile["profile_pic_path"]);
        // $type = pathinfo($path, PATHINFO_EXTENSION);
        // $data = file_get_contents($path);
        // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        return response()->json([
            "data" => [
                'profile' => $profile,
                'access_token' => $token,
                // 'image' => "$base64"
            ],
            'message' => "Login Successful",
            'status' => 1
        ], 200);

        return response();

    }
}
