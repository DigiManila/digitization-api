<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FloatingMachinery extends Model
{
    use HasFactory;
    protected $table = 'floating_machinery_active';
        
}
