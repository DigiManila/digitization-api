<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\AssessmentRoll;

use App\TempTaxDeclarations;

use Cache;

class CacheController extends Controller
{
    public function createCache(){

        $data   = [];
        $pin    = [];


        $data = AssessmentRoll::cursor()->filter(function ($pin) {
            return $pin->cancelled != 1;
        });

      

        foreach($data as $val){
            if (strlen($val->pin) <= 18)
                $pin[]  = $val->pin;
        }

        $pin = array_unique($pin);

        Cache::forget('pin');
        $pinCache = Cache::forever('pin', $pin);

        return 'OK';
    }

    public function getPinCache(){
        return Cache::get('pin');
    }

    public function getDataCache($pin){
        return Cache::get('assessment')[$pin];
    }

    public function updateCache(){
        return 0;
    }

    public function retrieveCache(){

        // $data = AssessmentRoll::get();
        $data = AssessmentRoll::get();
        $arr = [];

        foreach($data as $val){
            $arr[$val['pin']] =  $val;
        }

        $value = Cache::rememberForever('assessment', function () {
            return $arr;
        });

        return $value;

    }

    
}
