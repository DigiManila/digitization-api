<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Base32\Base32;

class EncryptDecryptController extends Controller
{
    public function createEncryptedBarcode($id){

        $encrypted_str = "";

        $or_id = sprintf('%06d', $id);

        $generated_str = $or_id;

        // \Log::info($generated_str);

        $encrypted_str = self::encryptString($generated_str, self::returnKey());

        // \Log::info(self::decryptString($encrypted_str, self::returnKey()));

        return $encrypted_str;

    }

    private function returnKey(){
        return "D1g1t1z@t10n";
    }

    private function returnEncryptionMethod(){
        // return "AES-192-CBC";
        return "aes-256-cbc";
    }

    public function encryptString($data) {
        $key = self::returnKey();
        $prefix = "A" . rand(0, 9);
        // $suffix = sprintf(rand(0, 9));
        $suffix = sprintf("%02d" ,rand(0, 9));
        $suffix = sprintf("%02d" ,dechex($suffix));

        // $encryption_key = base64_decode($key);

        // $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::returnEncryptionMethod()));
        // $encrypted = openssl_encrypt($data, self::returnEncryptionMethod(), $encryption_key, 1, $iv);

        // $output = Base32::encode($encrypted . ':' . $iv);

        $output = sprintf("%15d", $data);

        return $prefix . Base32::encode($output) . $suffix;

        return str_replace("=", "", $output);
    }

    public function decryptString($data) {

        $data = substr($data, 2);
        $data = substr($data, 0, -2);
        
        $output = Base32::decode($data);

        return str_replace(" ", "", $output);

        return Base32::decode($data);
        $key = self::returnKey();

        $encryption_key = base64_decode($key);

        list($encrypted_data, $iv) = array_pad(explode(':', Base32::decode($data . '='), 2),2,null);

        return openssl_decrypt($encrypted_data, self::returnEncryptionMethod(), $encryption_key, 1, $iv);

    }

    public function encryptor($data){
        $key = self::returnKey();
        $ivlen = openssl_cipher_iv_length($cipher=self::returnEncryptionMethod());
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
        return base64_encode( $iv.$hmac.$ciphertext_raw );
    }

    public function decryptor($data){
        $key = self::returnKey();
        $c = base64_decode($data);
        $ivlen = openssl_cipher_iv_length($cipher=self::returnEncryptionMethod());
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
        if (hash_equals($hmac, $calcmac))
            return $original_plaintext."\n";
    }

}
