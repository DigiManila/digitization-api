<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckEncoderWork extends Model
{
    use HasFactory;

    protected $table = 'check_encoder_work';
}
