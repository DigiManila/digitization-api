<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasMappingBrgynoView extends Model
{
    use HasFactory;

    protected $table = 'rptas_mapping_brgyno';
}
