<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTaxDeclarationsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_declarations', function (Blueprint $table) {
            $table->text('created_by')->nullable()->after('is_databuildup');
            $table->text('proofread_by')->nullable()->after('created_by');
            $table->date('proofread_date')->nullable()->after('proofread_by');
            $table->date('verified_date')->nullable()->after('verified_by');
            $table->text('status')->after('verified_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_declarations', function (Blueprint $table) {
            //
        });
    }
}
