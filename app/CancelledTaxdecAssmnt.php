<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CancelledTaxdecAssmnt extends Model
{
    use HasFactory;

    protected $table = 'cancelled_taxdec_assmnt';
}
