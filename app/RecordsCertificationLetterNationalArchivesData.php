<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordsCertificationLetterNationalArchivesData extends Model
{
    use HasFactory;

    protected $guarded = [];
}
