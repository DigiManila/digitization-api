<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportAssessmentRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_assessment_rolls', function (Blueprint $table) {
            $table->string('Owner')->nullable();
            $table->string('Location')->nullable();
            $table->string('LotNo')->nullable();
            $table->string('BlkNo')->nullable();
            $table->string('TctNo')->nullable();
            $table->string('PIN')->nullable();
            $table->date('Effectivity')->nullable();
            $table->date('InsertedDt')->nullable();
            $table->string('Taxability')->nullable();
            $table->string('UpdateDesc')->nullable();
            $table->string('ARP')->nullable();
            $table->string('Prev_Arp')->nullable();
            $table->char('kind',1)->nullable();
            $table->string('AUDesc')->nullable();
            $table->decimal('area', 20,2)->nullable();
            $table->decimal('MV', 20,2)->nullable();
            $table->decimal('AL', 20,2)->nullable();
            $table->decimal('AV', 20,2)->nullable();
            $table->decimal('Prev_Av', 20,2)->nullable();
            $table->text('Memoranda')->nullable();
            $table->string('AppraisedBy')->nullable();
            $table->date('AppraisedDt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_assessment_rolls');
    }
}
