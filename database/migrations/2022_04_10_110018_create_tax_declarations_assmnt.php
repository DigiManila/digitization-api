<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxDeclarationsAssmnt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_declarations_assmnt', function (Blueprint $table) {
            $table->id();
            $table->string('current_arp');
            $table->string('pdf_filename')->nullable();
            $table->string('pin')->nullable();
            $table->string('kind');
            $table->string('actual_use');
            $table->decimal('area',20,2)->nullable();
            $table->decimal('market_value',20,2)->nullable();
            $table->integer('assessment_level')->nullable();
            $table->decimal('assessed_value',20,2)->nullable();
            $table->string('taxability')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_declarations_assmnt');
    }
}
