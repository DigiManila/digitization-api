<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ReportAssessmentRoll;
use App\ActiveAssessmentRoll;
use App\CancelledAssessmentRoll;
use App\ActiveTaxdecAssmnt;
use App\CancelledTaxdecAssmnt;
use App\RptasTaxdecMastMla;
use App\RptasTaxdecMastExtn;
use App\RecordsDailyTransaction;
use App\TdPin;
use App\Profile;
use App\MiscRecordTransaction;
use Carbon\Carbon;
use Auth;

class RecordsDivisionController extends Controller
{

    public function postData(Request $request){

        $dailyUpdateDescTransaction = self::postDailyUpdateDescTransaction($request);
        $dailyTaxDecTransaction = self::postDailyTaxDecTransaction($request);
        $countDailyCancelledTransaction = self::postCountDailyCancelledTransaction($request);
        $dailyCancelledTaxDecTransaction = self::postDailyCancelledTaxDecTransaction($request);
        // $chartProfileWeekly = self::chartProfileWeekly($request);

        return response()->json([
            "data" => compact(
                'dailyUpdateDescTransaction',
                'dailyTaxDecTransaction',
                'countDailyCancelledTransaction',
                'dailyCancelledTaxDecTransaction',
                // 'chartProfileWeekly'
            ),
            'message' => 'Transactions Retrieved Successfull',
            'status' => 1
        ],200);

    }

    private function postDailyUpdateDescTransaction($request){
        
        $query = ActiveAssessmentRoll::selectRaw('UpdateDesc, COUNT(UpdateDesc) as count')
            ->when($request->switch == true, function($query) use ($request){
                return $query->where('PIN', $request->searchAny);
                // return $query->orWhere('Owner', "like", "%".$request->searchAny."%");
            })
            ->when($request->switch == false, function($query) use ($request){
                return $query->whereDate('AppraisedDt',  $request->searchAny);
            })                
            ->groupBy('UpdateDesc')
            ->get();
        
        return $query;
        
    }

    private function postCountDailyCancelledTransaction($request){
        
        $query = RptasTaxdecMastExtn::when($request->switch == true, function($query) use ($request){
                    return $query->where('PIN', $request->searchAny);
                    // return $query->orWhere('Owner', "like", "%".$request->searchAny."%");
                })
                    ->when($request->switch == false, function($query) use ($request){
                    return $query->whereDate('InsertedDt',  $request->searchAny);
                }) 
            ->count('Prev_Arp');

        return $query;
        
    }

    private function postDailyTaxDecTransaction ($request){
        
        $query = ActiveAssessmentRoll::select('active_assessment_roll.ARP', 
        'active_assessment_roll.PIN', 'active_assessment_roll.Owner', 'active_assessment_roll.UpdateDesc', 'status')
        ->leftJoin('records_daily_transactions', 'active_assessment_roll.ARP', '=', 'records_daily_transactions.ARP')            
            ->when($request->switch == true, function($query) use ($request){
                return $query->where('active_assessment_roll.PIN', $request->searchAny);
                // return $query->orWhere('active_assessment_roll.Owner', "like", "%".$request->searchAny."%");
            })
                ->when($request->switch == false, function($query) use ($request){
                return $query->whereDate('AppraisedDt',  $request->searchAny);
            }) 
        ->orderBy('ARP')
        ->get();

        foreach($query as $key => $value){
            $query[$key]['ARP']            =       trim($value['ARP']);
            $query[$key]['PIN']            =       trim($value['PIN']);
            $query[$key]['Owner']          =       trim($value['Owner']);
            $query[$key]['UpdateDesc']     =       trim($value['UpdateDesc']);
            $query[$key]['status']         =       $value['status'];
        }

        return $query;
    }

    private function postDailyCancelledTaxDecTransaction ($request){
        
        $query = RptasTaxdecMastExtn::select('rptas_taxdec_mastextn.Prev_Arp', 
        'cancelled_assessment_roll.PIN', 'cancelled_assessment_roll.Owner', 'cancelled_assessment_roll.UpdateDesc', 'status')
        ->join('cancelled_assessment_roll', 'rptas_taxdec_mastextn.Prev_Arp', '=', 'cancelled_assessment_roll.ARP')
        ->leftJoin('records_daily_transactions', 'rptas_taxdec_mastextn.Prev_Arp', '=', 'records_daily_transactions.ARP')            
            ->when($request->switch ==true, function($query) use ($request){
                return $query->where('cancelled_assessment_roll.PIN', $request->searchAny);
                // return $query->orWhere('cancelled_assessment_roll.Owner', "like", "%".$request->searchAny."%");
            })
                ->when($request->switch ==false, function($query) use ($request){
                return $query->whereDate('rptas_taxdec_mastextn.InsertedDt',  $request->searchAny);
            }) 
        ->orderBy('rptas_taxdec_mastextn.Prev_Arp')
        ->get();

        foreach($query as $key => $value){
            $query[$key]['ARP']            =       trim($value['Prev_Arp']);
            $query[$key]['PIN']            =       trim($value['PIN']);
            $query[$key]['Owner']          =       trim($value['Owner']);
            $query[$key]['UpdateDesc']     =       trim($value['UpdateDesc']);
            $query[$key]['status']         =       $value['status'];
        }

        return $query;
    }

    public function postTaxDeclarationDetails(Request $request){
        
        $data = [];
        
        if ($request->aOC == 0) {

            $query = ActiveAssessmentRoll::select('ARP', 'PIN', 'Owner','Location', 'LotNo', 'BlkNo', 'memoranda')            
                            ->where('ARP',  $request->arp)
                            ->first();

            $queryTaxdecAssmnt = ActiveTaxdecAssmnt::select('kind','AUDesc', 'area', 'MV',
                            'AV')
                            ->where('ARP',  $request->arp)
                            ->get()
                            ->toArray();
        }

        else {

            $query = CancelledAssessmentRoll::select('ARP', 'PIN', 'Owner','Location', 'LotNo', 'BlkNo', 'memoranda')            
                            ->where('ARP',  $request->arp)
                            ->first();

            $queryTaxdecAssmnt = CancelledTaxdecAssmnt::select('kind','AUDesc', 'area', 'MV',
                            'AV')
                            ->where('ARP',  $request->arp)
                            ->get()
                            ->toArray();


        }

        $queryTdPins = null;
        $queryTdPins = TdPin::select("td")
                    ->where('pin', $query['PIN'])
                    ->get();

        $data['tdPins']             =       $queryTdPins;
        
       
            $query['ARP']            =       trim($query['ARP']);
            $query['PIN']            =       trim($query['PIN']);
            $query['Owner']          =       trim($query['Owner']);
            $query['Location']       =       trim($query['Location']);
            $query['LotNo']          =       trim($query['LotNo']);
            $query['BlkNo']          =       trim($query['BlkNo']);
            $query['memoranda']      =       trim($query['memoranda']);
            
    

        foreach($queryTaxdecAssmnt as $key => $value){

            $taxdecAssmnt[] = array(
                'kind'          =>      $value['kind'],
                'AUDesc'        =>      $value['AUDesc'],
                'area'          =>      number_format($value['area'],2,'.',',') . " sqm",
                'MV'            =>      number_format($value['MV'],2,'.',','),
                'AV'            =>      number_format($value['AV'],2,'.',','),
            );
        };

        $queryPrev_Arp = RptasTaxdecMastExtn::select('Prev_Arp')
                        ->where('ARP', $request->arp)   
                        ->first();
        
            if($queryPrev_Arp == null){
                $queryPrev_Arp['Prev_Arp'] = 'N/A';
            }
        
        $queryRecordsTransaction = RecordsDailyTransaction::select('city_assessor','officer_of_the_day','others',  
                                    'cancellation_number','distribute_to','is_owners_copy','received_date', 'td_no',
                                    'release_to','status')
                                    ->where('ARP', $request->arp)   
                                    ->first();

        
        $data['AssessmentRoll']     =       $query;
        $data['TaxdecAssmnt']       =       $taxdecAssmnt;
        $data['Prev_Arp']           =       $queryPrev_Arp['Prev_Arp'];
        $data['RecordsTransaction'] =       $queryRecordsTransaction;
        $data['recordsEmployee']    =       self::getRecordsEmployee();
        

        return $data;
    }

    public function saveData(Request $request){

        $RDT = new RecordsDailyTransaction;

        $RDT->ARP                     =       $request->ARP;
        $RDT->PIN                     =       $request->PIN;
        $RDT->Prev_Arp                =       $request->Prev_Arp;
        $RDT->Owner                   =       $request->Owner;
        $RDT->city_assessor           =       $request->city_assessor;
        $RDT->officer_of_the_day      =       $request->officer_of_the_day;
        $RDT->others                  =       $request->others;
        $RDT->cancellation            =       $request->cancellation;
        $RDT->cancellation_number     =       $request->cancellation_number;
        $RDT->distribute_to           =       $request->distribute_to;
        $RDT->is_owners_copy          =       $request->is_owners_copy;
        $RDT->td_no                   =       $request->td_no;
        $RDT->received_date           =       $request->received_date;
        $RDT->release_to              =       $request->releaseTo;
        $RDT->status                  =       $request->status;
        $RDT->user_id                 =       Auth::user()->id;

        // Save the RecordsDailyTransaction instance
        $statusSaveArray = $RDT->save();
        
        if ($statusSaveArray) {
            // Call the AddOrUpdateMiscRecordTransaction method
            $updateResult = $this->AddOrUpdateMiscRecordTransaction($request->ARP);
    
            // Check if the update result is successful and return appropriate response
            if ($updateResult && $updateResult->getStatusCode() == 200) {
                // Log the successful transaction
                Controller::addLog('Add', $request->ARP, "", "Saved Transaction Successfully");
    
                // Return a successful response
                return response()->json([
                    'data' => [],
                    'message' => 'Saved Transaction Successfully',
                    'status' => 1
                ], 200);
            } else {
                // Handle errors from AddOrUpdateMiscRecordTransaction
                return response()->json([
                    'message' => 'Failed to add or update record in MiscRecordTransaction.',
                    'status' => 0
                ], 500);
            }
        } else {
            // Handle errors if saving to RecordsDailyTransaction fails
            return response()->json([
                'message' => 'Failed to save transaction.',
                'status' => 0
            ], 500);
        }
    }

    public function updateData(Request $request){

        $RDT = RecordsDailyTransaction::where('ARP',$request->ARP)->first();        

        $RDT->PIN                     =       $request->PIN;
        $RDT->Prev_Arp                =       $request->Prev_Arp;
        $RDT->Owner                   =       $request->Owner;
        $RDT->city_assessor           =       $request->city_assessor;
        $RDT->officer_of_the_day      =       $request->officer_of_the_day;
        $RDT->others                  =       $request->others;
        $RDT->cancellation            =       $request->cancellation;
        $RDT->cancellation_number     =       $request->cancellation_number;
        $RDT->distribute_to           =       $request->distribute_to;
        $RDT->is_owners_copy          =       $request->is_owners_copy;
        $RDT->td_no                   =       $request->td_no;
        $RDT->received_date           =       $request->received_date;
        $RDT->release_to              =       $request->releaseTo;
        $RDT->status                  =       $request->status;
        $RDT->user_id                 =       Auth::user()->id;

        $statusUpdateArray = $RDT->save();

        if ($statusUpdateArray) {
            // Call the AddOrUpdateMiscRecordTransaction method
            $updateResult = $this->AddOrUpdateMiscRecordTransaction($request->ARP);
    
            // Check if the update result is successful and return appropriate response
            if ($updateResult && $updateResult->getStatusCode() == 200) {
                // Log the successful transaction
                Controller::addLog('Update', $new_data = "" , $prev_data = $request->ARP,  $remarks = "Update Transaction Succcessfully");
    
                // Return a successful response
                return response()->json([
                    'data' => [],
                    'message' => 'Update Transaction Successfully',
                    'status' => 1
                ], 200);
            } else {
                // Handle errors from AddOrUpdateMiscRecordTransaction
                return response()->json([
                    'message' => 'Failed to add or update record in MiscRecordTransaction.',
                    'status' => 0
                ], 500);
            }
        } else {
            // Handle errors if saving to RecordsDailyTransaction fails
            return response()->json([
                'message' => 'Failed to update transaction.',
                'status' => 0
            ], 500);
        }
    }

    private function getRecordsEmployee(){
        
        $data = [];

        $query = Profile::select('user_id', 'first_name', 'last_name')
                ->where('user_type', 'like', '%Records%')
                // ->where('user_type',  'Verifier')
                ->orderBy('last_name')
                ->get();

        foreach($query as $key => $value){
            $data[] = array(
            'user_id'       =>      $value['user_id'],
            'full_name'     =>      strtoupper($value['last_name']. ", " .$value['first_name']),
            );
        };

        return $data;
    }

    //---------------------------Report-------------------

    private function chartProfileWeekly($date){
        
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
        
        $data = [];

        $queryActiveTaxdec = ActiveAssessmentRoll::selectRaw('AppraisedDt, count(ARP) as countActiveARP')
                ->whereBetween('AppraisedDt',  [Carbon::parse($date)->startOfWeek()->format('Y-m-d'), 
                                        Carbon::parse($date)->endOfWeek()->format('Y-m-d')])
                ->groupBy('AppraisedDt')
                ->orderBy('AppraisedDt')
                ->get();
             

        $queryCancelledTaxdec = RptasTaxdecMastExtn::selectRaw('InsertedDt, count(Prev_Arp) as countCancelledARP')
                ->whereBetween('InsertedDt',  [Carbon::parse($date)->startOfWeek()->format('Y-m-d'), 
                                        Carbon::parse($date)->endOfWeek()->format('Y-m-d')])
                ->groupBy('InsertedDt')
                ->orderBy('InsertedDt')
                ->get();

            foreach($queryActiveTaxdec as $key => $value){
                foreach($queryCancelledTaxdec as $key1 => $value1){
                    if($value['AppraisedDt'] == $value1['InsertedDt']){

                        $data[] = array(
                            'AppraisedDt'           =>  $value['AppraisedDt'],
                            'countActiveARP'        =>  $value['countActiveARP'],
                            'countCancelledARP'     =>  $value1['countCancelledARP']
                        );
                    }
                    
                }
            }
            

        return $data;

    } 
    
    
    public function AddOrUpdateMiscRecordTransaction($arp)
    {

        // Initialize a variable to track success
        $success = false;
        
        $result = RecordsDailyTransaction::where('arp', $arp)
            ->select('pin', 'arp', 'td_no', 'prev_arp')
            ->first();

        if (!$result) {
            return response()->json(['message' => 'Record not found.'], 404);
        }

        // Step 2: Find if the current row's ARP becomes a prev_arp in another row
        $matchingRecord = RecordsDailyTransaction::where('prev_arp', $result->arp)
        ->first(['arp', 'td_no']);

        
        // Step 3: Update the result with cancellation data if a match is found
        $result->cancellation_number = $matchingRecord ? $matchingRecord->td_no : null;
        $result->cancelled_arp = $matchingRecord ? $matchingRecord->arp : null;
        
        // Step 4: Check if the record already exists in MiscRecordTransaction
        $existingRecord = MiscRecordTransaction::where('arp', $result->arp)->first();

        if ($existingRecord) {
            $updateData = [];

            // Check each field and log if it has changed, then add to the update array
            if ($existingRecord->cancellation_number !== $result->cancellation_number) {
                $updateData['cancellation_number'] = $result->cancellation_number;
                $updateData['cancellation_number_date_added'] = Carbon::now();
                $updateData['updated_at'] = Carbon::now();
            }

            if ($existingRecord->cancelled_arp !== $result->cancelled_arp) {
                $updateData['cancelled_arp'] = $result->cancelled_arp;
                $updateData['updated_at'] = Carbon::now();
            }

            if ($existingRecord->pin !== $result->pin) {
                $updateData['pin'] = $result->pin;
                $updateData['updated_at'] = Carbon::now();
            }

            if ($existingRecord->td_no !== $result->td_no) {
                $updateData['td_no'] = $result->td_no;
                $updateData['td_no_date_added'] = Carbon::now();
                $updateData['updated_at'] = Carbon::now();
            }

            // Update only if there are changes
            if (!empty($updateData)) {
                $updated = $existingRecord->update($updateData);
                Log::info('Updated record for ARP: ' . $result->arp, $updateData);

                // Set success to true if the update was successful
                $success = $updated;
            }
            else{
                Log::info('No changes detected for ARP: ' . $result->arp);
            }

        } else {
            // Create a new record
            $newRecord = MiscRecordTransaction::create([
                'arp' => $result->arp,
                'pin' => $result->pin,
                'cancelled_arp' => $result->cancelled_arp,
                'cancellation_number' => $result->cancellation_number,
                'cancellation_number_date_added' => Carbon::now(),
                'td_no' => $result->td_no,
                'td_no_date_added' => Carbon::now(),
            ]);

            Log::info("Created new record for arp {$result->arp}");

            // Set success to true if the creation was successful
            $success = $newRecord ? true : false;
        }

        if ($success) {
            return response()->json(['message' => 'Successfully added or updated to misc record transactions.'], 200);
        } else {
            return response()->json(['message' => 'Failed to add record.'], 500);
        }

    }
    


}
