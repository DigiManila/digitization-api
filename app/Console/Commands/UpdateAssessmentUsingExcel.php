<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Api\ExcelController;

class UpdateAssessmentUsingExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:assessment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update assessment records using excel file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = ExcelController::extractExcel();
        print $result;
    }
}
