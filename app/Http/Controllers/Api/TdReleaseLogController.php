<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ReleaseTaxDec;

use App\Http\Controllers\Api\EncryptDecryptController;

use PDF;
use Storage;

class TdReleaseLogController extends Controller
{
    public function getPdf(Request $request){

        $date       = date("Y-m-d");

        $epoch          = $request['epoch'];
        $td             = $request['td'];
        $or_text        = $request['or_num'];
        $or_date        = $request['orDate'];
        $amount         = $request['amount'];

        $requestor      = $request['requestor'];
        $verifier       = $request['verifier'];

        $or_file    = $request->file('file');
        $td_file    = $request->file('file2');

        $copies    = $request['copies'];

        $user           = json_decode($request['processed_by'], 1);
        $processed_by   = $user['first_name'] . " " . $user['last_name'];

        $certified_by = "MR. JOSE BONAPARTE HARON R. MAMUTUK";

        $return = ReleaseTaxDec::create(array(
            'td_no'         => $td,
            'requestor'     => $requestor,
            'copies'        => $copies,
            'printed_by'    => $processed_by,
            'certifier'     => $certified_by,
            'verifier'      => $verifier,
            'processed_by'  => $processed_by,
        ));

        $insert_id = $return->id;

        $encryptIt = new EncryptDecryptController;
        $barcode_data = $encryptIt->createEncryptedBarcode($insert_id);


        $released_td_dir = 'released_taxdecs_logs';

        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //
        // $released_td_dir = 'released_taxdecs_logs_test';
        // ------------ TEST ONLY: COMMENT AFTER TESTING BEFORE PUSHING TO GIT ------ //

        if(strpos($request['or_num'], "SR") !== false){
            
            $srcount = ReleaseTaxDec::where('or_no', 'like', $request['or_num'] . date('Y').'%')->count();
            $end_no = 0;

            if($srcount){
                $end_no = $srcount + 1;
            } else{
                $end_no = 1;
            }

            $or_text = $request['or_num'] . date('Y') . sprintf('%04d', $end_no);

        }

        $data = compact(
            'date',
            'epoch',
            'td',
            'or_text',
            'or_file',
            'td_file',
            'processed_by',
            'insert_id',
            'barcode_data',
            'released_td_dir',
            'requestor',
            'verifier',
            'or_date',
            'amount' ,
            // 'a',
        );

        return self::createPdf($data);
    }

    private function createPdf($data){

        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => false,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 6,
            'stretchtext' => 0
        );

        PDF::setPrintHeader(false);
        // PDF::AddPage('P', 'LEGAL');
        PDF::AddPage('P', 'A4');
        PDF::SetAutoPageBreak(false, 0);

        // 1st

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79); 
        PDF::MultiCell(218, 10, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, 9, true);
        PDF::SetFont('times', '', 11, '', false);
        PDF::MultiCell(218, 10, 'City of Manila', 0, 'C', 0, 0, 0, 14, true);
        PDF::SetFont('times', 'B', 15, '', false);
        PDF::SetTextColor(255, 102, 102);
        PDF::MultiCell(218, 10, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, 18, true);

        $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
        PDF::Image($logo1, 18, 6, 20, 20, '', '', '', false, 300, '', false, false, 0);

        $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
        PDF::Image($logo2, 173, 6, 21, 21, '', '', '', false, 300, '', false, false, 0);

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79);

        $col_1_start_pos = 20;
        $colon_1_add_pos = $col_1_start_pos + 50;
        $col_1_after_col_pos = $colon_1_add_pos + 6;

        $row_init_pos = 40;
        $row_add_pos = $row_init_pos;

        PDF::MultiCell('', '', 'OR No.', 0, '', 0, 0, $col_1_start_pos, $row_init_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_init_pos, true);
        PDF::MultiCell('', '', $data['or_text'], 0, '', 0, 0, $col_1_after_col_pos, $row_init_pos, true);

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Requestor', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['requestor'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);        

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Verified By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['verifier'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['processed_by'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['barcode_data'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::write1DBarcode($data['barcode_data'], 'C128', 20, $row_add_pos, 150, 17, 0.5, $style, 'N');

        // 2nd

        $row_add_pos = $row_add_pos + 50;

        $logo1 = public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png';
        PDF::Image($logo1, 18, $row_add_pos, 20, 20, '', '', '', false, 300, '', false, false, 0);

        $logo2 = public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png';
        PDF::Image($logo2, 173, $row_add_pos, 21, 21, '', '', '', false, 300, '', false, false, 0);

        PDF::SetFont('times', '', 13, '', false);
        PDF::SetTextColor(54,69,79); 
        PDF::MultiCell(218, 10, 'REPUBLIC OF THE PHILIPPINES', 0, 'C', 0, 0, 0, $row_add_pos, true);
        PDF::SetFont('times', '', 11, '', false);
        $row_add_pos = $row_add_pos + 5;
        PDF::MultiCell(218, 10, 'City of Manila', 0, 'C', 0, 0, 0, $row_add_pos, true);
        PDF::SetFont('times', 'B', 15, '', false);
        PDF::SetTextColor(255, 102, 102);
        $row_add_pos = $row_add_pos + 4;
        PDF::MultiCell(218, 10, 'DEPARTMENT OF ASSESSMENT', 0, 'C', 0, 0, 0, $row_add_pos, true);

        $row_add_pos = $row_add_pos + 20;

        PDF::SetFont('times', '', 14, '', false);
        PDF::SetTextColor(54,69,79);

        // $col_1_start_pos = 20;
        // $colon_1_add_pos = $col_1_start_pos + 50;
        // $col_1_after_col_pos = $colon_1_add_pos + 6;

        // $row_init_pos = 55;
        // $row_add_pos = $row_init_pos;

        PDF::MultiCell('', '', 'OR No.', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['or_text'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Requestor', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['requestor'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);        

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Verified By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['verifier'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['processed_by'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::MultiCell('', '', 'Transaction Log', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $data['barcode_data'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true); 

        $row_add_pos = $row_add_pos + 11;

        PDF::write1DBarcode($data['barcode_data'], 'C128', 20, $row_add_pos, 150, 17, 0.5, $style, 'N');

        $fn = $data['td'] . "_" . $data['epoch'] . ".pdf";
        $path = storage_path("app/temp/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        $file = Storage::get("temp/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        $created_pdf = Storage::get("temp/" . $fn);

        Storage::disk('svnas')->putFileAs("/" . $data['released_td_dir'] . "/" . $data['date'] , $temp_file_path, $fn);
        Storage::disk('svnas')->putFileAs("/" . $data['released_td_dir'] . "/" . $data['date'] , $data['or_file'], "or_" . $data['td'] . "_" . $data['epoch'] .".png");
        Storage::disk('svnas')->putFileAs("/" . $data['released_td_dir'] . "/" . $data['date'] , $data['td_file'], "td_" . $data['td'] . "_" . $data['epoch'] .".png");

        Storage::delete("temp/$fn");

        $release_td = ReleaseTaxDec::find($data['insert_id']);
            
        $release_td->or_no      = $data['or_text'];
        $release_td->or_path    = "/" . $data['released_td_dir'] . "/" . $data['date'] . "/" . "or_" . $data['td'] . "_" . $data['epoch'] .".png";
        $release_td->file_path  = "/" . $data['released_td_dir'] . "/" . $data['date'] . "/$fn";
        $release_td->td_path    = "/" . $data['released_td_dir'] . "/" . $data['date'] . "/" . "td_" . $data['td'] . "_" . $data['epoch'] .".png";
        $release_td->barcode    = $data['barcode_data'];
        $release_td->created_at = date('Y-m-d H:i:s');

        $release_td->or_date    = $data['or_date'];
        $release_td->amount     = $data['amount'];

        $release_td->save();

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);






















        // Tax Declaration Image
        $img_file = $value;

        if($value == "Not Exist"){
            $img_file = storage_path("app/not_exist.jpg");
        } else{
            $img_file = storage_path("app/$value");
        }

        // \Log::info();

        // $h = 228;
        $h = 250;

        $page_id = "Page $key";

        if($page_id === "Page 0" && $request['flip1'] == "true"){
            // Load the image
            $source = imagecreatefromjpeg($img_file);
            // Rotate
            $rotate = imagerotate($source, 180, 0);
            //and save it on your server...
            imagejpeg($rotate, storage_path("app/$value"));

            PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
        } elseif($page_id === "Page 1" && $request['flip2'] == "true"){
            // Load the image
            $source = imagecreatefromjpeg($img_file);
            // Rotate
            $rotate = imagerotate($source, 180, 0);
            //and save it on your server...
            imagejpeg($rotate, storage_path("app/$value"));

            PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
        } else{
            PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
        }

        // PDF::Image($img_file, 17.5, 30, 177.5, $h, '', '', '', false, 300, '', false, false, 0);
        
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => false,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 6,
            'stretchtext' => 0
        );

        $col_2_start_pos = 110;
        $colon_2_add_pos = $col_2_start_pos + 18;
        $col_2_after_col_pos = $colon_2_add_pos + 4;

        $col_1_start_pos = 18;
        $colon_1_add_pos = $col_1_start_pos + 18;
        $col_1_after_col_pos = $colon_1_add_pos + 4;

        // $row_init_pos = 261;
        $row_init_pos = 282;
        $row_add_pos = $row_init_pos;

        PDF::SetFont('times', 'I', 8, '', false);
        PDF::SetTextColor(120, 120, 120);

        PDF::MultiCell('', '', 'OR No.', 0, '', 0, 0, $col_1_start_pos, $row_init_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_init_pos, true);
        PDF::MultiCell('', '', $or_text, 0, '', 0, 0, $col_1_after_col_pos, $row_init_pos, true);



        $row_add_pos = $row_add_pos + 4;

        PDF::MultiCell('', '', 'Requestor', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $request['requestor'], 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);

        PDF::MultiCell('', '', 'Certified By', 0, '', 0, 0, $col_2_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_2_add_pos, $row_add_pos, true);
        // PDF::MultiCell('', '', 'By authority of the Officer in Charge' , 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);
        PDF::MultiCell('', '', 'By authority of the City Assessor' , 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);


        ////// FIX 


        $row_add_pos = $row_add_pos + 11;
        
        // PDF::MultiCell('', '', strtoupper($certifier['first_name'] . " " . $certifier['middle_name'] . " " . $certifier['last_name']), 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $certified_by, 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);

        PDF::MultiCell('', '', 'Printed By', 0, '', 0, 0, $col_1_start_pos, $row_add_pos, true);
        PDF::MultiCell('', '', ':', 0, '', 0, 0, $colon_1_add_pos, $row_add_pos, true);
        // PDF::MultiCell('', '', strtoupper($printed_by), 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos  , true);
        PDF::MultiCell('', '', strtoupper($processed_by), 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos  , true);

        $row_add_pos = $row_add_pos + 4;

        PDF::SetFont('times', 'I', 6, '', false);
        PDF::SetTextColor(120, 120, 120);
        // PDF::MultiCell('', '', $certifier['position'], 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);
        PDF::MultiCell('', '', $certifier_position, 0, '', 0, 0, $col_2_after_col_pos, $row_add_pos, true);

        // PDF::MultiCell('', '', $certifier_position, 0, '', 0, 0, $col_1_after_col_pos, $row_add_pos, true);

        // $row_add_pos = $row_add_pos + 5;

        // PDF::SetFont('times', 'I', 8, '', false);
        // PDF::SetTextColor(120, 120, 120);
        // PDF::MultiCell(170, '', 'This is a system-generated certified true copy of the Tax Declaration and does not require a manually affixed signature pursuant to R.A. No. 8792.', 0, '', 0, 0, 20, $row_add_pos, true);

        $row_add_pos = $row_add_pos + 3;

        // Barcode
        PDF::write1DBarcode($barcode_data, 'C128', 16, $row_add_pos, 135, 15, 0.4, $style, 'N');

        // Go Manila logo

        // $logo3 = public_path('img') . DIRECTORY_SEPARATOR . 'GoManila.png';
        // PDF::Image($logo3, 165, 303, 25, 15, '', '', '', false, 150, '', false, false, 0);

        $row_add_pos = $row_add_pos + 8;

        // Page
        PDF::SetFont('times', 'I', 7, '', false);
        PDF::SetTextColor(120, 120, 120);
        PDF::MultiCell('', '', 'Page ' . (intval($key) + 1) . ' of ' . count($filenames), 0, 'C', 0, 0, 152, $row_add_pos, true);

        // set alpha to semi-transparency
        PDF::SetAlpha(0.05);
    

        // Copy Files

        $fn = $td . "_" . $epoch. ".pdf";
        $path = storage_path("app/temp/" . $fn);

        $out = PDF::Output($path, 'F');
        PDF::reset();

        Storage::delete("temp/forpdf/1" . $td . ".jpg");
        Storage::delete("temp/forpdf/2" . $td . ".jpg");

        $file = Storage::get("temp/$fn");

        $temp_file_path = tempnam(sys_get_temp_dir(), $fn);
        file_put_contents($temp_file_path, $file);

        $created_pdf = Storage::get("temp/" . $fn);

        Storage::disk('svnas')->putFileAs("/$released_td_dir/$date" , $temp_file_path, $fn);
        Storage::disk('svnas')->putFileAs("/$released_td_dir/$date" , $request->file('file'), "or_$td"."_"."$epoch.png");

        Storage::delete("temp/$fn");

        // Save to database

       // Prevent double saving from rotation image
       // Check if file exist 

       $ifExist = ReleaseTaxDec::where('td_path', 'like', "%_" . $epoch . ".%")->get();


       if($is_save){
        if(!count($ifExist)){
            $release_td = ReleaseTaxDec::find($insert_id);
            
            $release_td->or_no      = $or_text;
            $release_td->or_path    = "/$released_td_dir/$date/or_$td"."_"."$epoch.png";
            $release_td->file_path  = "/$released_td_dir/$date/$fn";
            $release_td->barcode    = $barcode_data;
            $release_td->created_at = date('Y-m-d H:i:s');
    
            $release_td->save();
    
           }
       }


        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $fn,
        );

        return response()->download($temp_file_path, $fn, $headers);

        // return "data:application/pdf;base64," . $output_pdf;



    }
}
