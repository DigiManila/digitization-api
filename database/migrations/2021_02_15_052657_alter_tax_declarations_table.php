<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTaxDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_declarations', function (Blueprint $table) {
            $table->dropColumn('assessment_level');

            $table->date('tct_date')->change();
            $table->date('cct_date')->change();

            $table->boolean('is_databuildup')->after('currentAssessValue');
            $table->string('ref_tax_dec')->nullable()->after('previous_arp');
            $table->date('effectivity_date')->nullable()->after('ref_tax_dec');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_declarations', function (Blueprint $table) {
            //
        });
    }
}
