<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesDataLatest extends Model
{
    use HasFactory;

    // protected $table = 'view_sales_data_latest';
    protected $table = 'sales_data_all';

    public function subclass(){
        return $this->hasMany('App\TdSubclass', 'PIN', 'pin_display');
    }
    public function polygon(){
        return $this->hasMany('App\MapPolygon', 'pin', 'pin_display');
    }
}
