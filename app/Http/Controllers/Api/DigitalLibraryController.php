<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use File;

class DigitalLibraryController extends Controller
{
    public function getStructure(Request $request){

        $year = $request['year'];
        $path = storage_path("app/public/$year");
        $directories = array_map('basename', File::directories($path));

        sort($directories, SORT_NATURAL);

        $arr_dir = [];

        foreach($directories as $val){
            $arr_dir[]['zone'] = $val;
        }

        return $arr_dir;

    }

    public function getLinks(Request $request){

        $year = $request['year'];
        $folder = $request['folder'];

        $list = self::getFileList($year, $folder);

        $links = [];
		
		$url = "http://10.10.100.2";

		$links[] = 0;
		
        foreach ($list as $value) {
            if(strpos($value, ".jpg") !== false && strpos($value, "1A") !== false)
                $links[] = $url . "/storage/$year/$folder/" . $value;
                $links[] = $url . "/storage/$year/$folder/" . str_replace("1A", "2A", $value);
        }

        return $links;

    }

    private function getFileList($year, $folder){

        $path_str = storage_path("app/public/$year/$folder");
        $path = scandir($path_str);
        return array_diff($path, array('..', '.'));

    }
}
