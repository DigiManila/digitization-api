<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrTaxdec extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function or(){
        return $this->hasOne('App\RecordsTaxDecData', 'arp', 'tax_declaration');
    }

}
