<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckEncoderWork extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_encoder_work', function (Blueprint $table) {
            $table->id();
            $table->string("series")->nullable();
            $table->string("zone")->nullable();
            $table->string("barangay")->nullable();
            $table->string("created_by")->nullable();
            $table->string("fname")->nullable();
            $table->integer("work_pdf")->nullable();
            $table->integer("total_pdf")->nullable();
            $table->string("remarks")->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_encoder_work');
    }
}
