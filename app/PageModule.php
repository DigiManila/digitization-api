<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageModule extends Model
{
    use HasFactory;

    protected $table = 'page_module';

    protected $primaryKey = 'title';
    public $incrementing = false;

}
