<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AggKindBarangaySummary;
use App\AssessmentRoll;


class AggKindBarangaySummaryController extends Controller
{
    public function saveAggKindBarangay(request $request){
        $i = $request['loop'];
        $barangay = [];
        $kindarr = ['L-RESI', 'L-COMM', 'L-CHAR','L-EDUC', 'L-GOVT', 'L-INDU', 'L-RELI', 
                        'B-RESI', 'B-COMM', 'B-CHAR','B-EDUC', 'B-GOVT', 'B-INDU', 'B-RELI',
                        'M-RESI', 'M-COMM', 'M-CHAR','M-EDUC', 'M-GOVT', 'M-INDU', 'M-RELI',
                        'L-SPC1', 'L-SPC2','L-SPC3','L-SPC4','L-SPC5','L-SPC6',
                        'B-SPC1', 'B-SPC2','B-SPC3','B-SPC4','B-SPC5','B-SPC6',
                        'M-SPC1', 'M-SPC2','M-SPC3','M-SPC4','M-SPC5','M-SPC6',
                        'L-SPEC', 'B-SPEC','M-SPEC',
                        'L-OTHE', 'B-OTHE','M-OTHE',
                        
                    ];
        $countKind = count($kindarr);
        
        // for ($i = 2; $i<=3; $i++) {
            $brgyNo = str_pad($i,3,'0',STR_PAD_LEFT);
            
                
                for ($k = 0; $k < $countKind; $k++){
            
                    ;
                    $bQuery = AssessmentRoll::select('area','market_value','currentAssessValue',
                    'previousAssessValue','kind')
                                            ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                                            ->where('kind', $kindarr[$k])
                                            ->whereRaw('SUBSTRING(current_arp,1,2) = ?',  'AD' )
                                            ->get();
                    
            
                    $sumArea = $bQuery->sum('area');
                    $sumMarketValue = $bQuery->sum('market_value');
                    $sumAssessValue = $bQuery->sum('currentAssessValue');
                    $sumPreviousValue =$bQuery->sum('previousAssessValue');
                
                

                    $barangay[] = array(
                        'brgyNo'                =>  $brgyNo,
                        'kind'                  =>  $kindarr[$k],
                        'area'                  =>  $sumArea,
                        'market_value'          =>  $sumMarketValue,
                        'currentAssessValue'    =>  $sumAssessValue,
                        'PreviousAssessValue'   =>  $sumPreviousValue,

                            
                    );
                }    
                // AggBarangayD::where('brgyNo',$brgyNo)->delete();
            // }
            
        
        
        $status = AggKindBarangaySummary::insert($barangay);
        
        
        if($status){
            return response()->json([
                "data" => [],
                'message' => "Saved Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }
}
