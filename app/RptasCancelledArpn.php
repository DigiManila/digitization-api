<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasCancelledArpn extends Model
{
    use HasFactory;

    protected $table = 'rptas_cancelled_arpn';
}
