<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxDeclarationsAssmnt extends Model
{
    use HasFactory;
    
    protected $table = 'tax_declarations_assmnt';
    protected $fillable = ['pdf_filename','current_arp','pin','kind','area','actual_use','market_value','assessed_value'];
    
}
