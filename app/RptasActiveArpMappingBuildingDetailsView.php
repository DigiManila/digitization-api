<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasActiveArpMappingBuildingDetailsView extends Model
{
    use HasFactory;
    protected $table = 'rptas_active_arp_mapping_building_details';
}
