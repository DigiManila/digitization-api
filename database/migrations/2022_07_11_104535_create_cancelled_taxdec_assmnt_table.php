<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCancelledTaxdecAssmntTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancelled_taxdec_assmnt', function (Blueprint $table) {
            $table->id();
            $table->string('RecId')->nullable();
            $table->char('ARP',20);
            $table->char('AU',4);
            $table->char('AUDesc',20);
            $table->decimal('area', 20,2)->nullable();
            $table->decimal('MV', 20,2)->nullable();
            $table->decimal('AL', 20,2)->nullable();
            $table->decimal('AV', 20,2)->nullable();
            $table->decimal('DefArea', 20,2)->nullable();
            $table->decimal('DefMV', 20,2)->nullable();
            $table->decimal('DefAL', 20,2)->nullable();
            $table->decimal('DefAV', 20,2)->nullable();
            $table->string('InsertedBy',50)->nullable();
            $table->date('InsertedDt')->nullable();
            $table->string('UpdatedBy',50)->nullable();
            $table->date('UpdatedDt')->nullable();
            $table->char('kind',1)->nullable();
            $table->char('Taxability',30)->nullable();
            $table->char('LandClass',20)->nullable();
            $table->string('BldgClass',20)->nullable();
            $table->char('MachClass',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancelled_taxdec_assmnt');
    }
}
