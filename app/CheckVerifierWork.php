<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckVerifierWork extends Model
{
    use HasFactory;

    protected $table = 'check_verifier_work';
}
