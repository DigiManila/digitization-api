<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\TaxDeclaration;
use App\TempTaxDeclarations;
use App\TaxDeclarationsAssmnt;

use App\User;
use Auth;

class TaxDecController extends Controller
{

    public function saveTaxDec(Request $request){
        
        
        $saveArrayTaxDec = [];
        

        $saveArrayTaxDec[] = array(
            'pdf_filename_96' => $request['pdf_filename'],
            'current_arp' => $request['current_arp'],
            'current_arp_96' => $request['current_arp_96'],
            'pin' => $request['pin'],
            'owner_name' => $request['owner_name'],
            'location' => $request['location'],
            'lot' => $request['lot'],
            'block' => $request['block'],
            'tct' => $request['tct'],
            'tct_date' => $request['tct_date'],
            'cct' => $request['cct'],
            'cct_date' => $request['cct_date'],
            'cancelled_by_td' => $request['cancelled_by_td'],
            'previous_pin' => $request['previous_pin'],
            'remarks' => $request['remarks'],
            'previous_arp' => $request['previous_arp'],
            'memoranda' => $request['memoranda'],
            'status' => 2,
            'created_by' => $request['created_by'],
            'proofread_by' => Auth::user()->id,
            'proofread_date' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

            'is_databuildup' => 1,
        );

        $statusTaxDec = TaxDeclaration::insert($saveArrayTaxDec);

        $saveArrayAssmnt = [];
        foreach($request['property_assessment'] as $key => $value){
            if($value['kind']<> null){
                $saveArrayAssmnt[] = array(
                    'current_arp' => $request['current_arp'],
                    'pdf_filename' => $request['pdf_filename'],
                    'pin' => $request['pin'],
                    'kind' => $value['kind'],
                    'actual_use' => $value['actual_use'],
                    'area' => $value['area'],
                    'market_value' => $value['market_value'],
                    // 'assessment_level' => $value['assessment_level'],
                    'assessed_value' => $value['assessment_value'],
                    // 'taxability' => $value['taxability'],

                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
            }
        }

        $statusAssmnt = TaxDeclarationsAssmnt::insert($saveArrayAssmnt);

        TempTaxDeclarations::where('id', $request['id'] )
            ->update(['status' => 2 ]);

        TempTaxDeclarations::where('status', 1)
                        ->where('pdf_filename_96',  $request['pdf_filename'] )
                        ->delete();


        if($statusTaxDec){
            return response()->json([
                "data" => [],
                'message' => "Saved Tax Dec Succcessfully",
                'status' => 1
            ], 200);            
        }

        if($statusAssmnt){
            return response()->json([
                "data" => [],
                'message' => "Saved Assmnt Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }

    // public function saveTaxDec(Request $request){

    //     $id = $request['id'];
        
    //     $saveArray = [];

    //     $block = $request['block'];
    //     $cancelled_by_td = $request['cancelled_by_td'];
    //     $cct = $request['cct'];
    //     $cct_date = $request['cct_date'];
    //     $current_arp = $request['current_arp'];
    //     $current_arp_96 = $request['current_arp_96'];
    //     $pdf_filename_96 = $request['pdf_filename_96'];
    //     $location = $request['location'];
    //     $lot = $request['lot'];
    //     $owner_name = $request['owner_name'];
    //     $pin = $request['pin'];
    //     $previous_arp = $request['previous_arp'];
    //     $tct = $request['tct'];
    //     $tct_date = $request['tct_date'];

        
   
    //     $saveArray[] = array(
    //         'pin' => $pin,
    //         'current_arp' => $current_arp,
    //         'current_arp_96' => $current_arp_96,
    //         'pdf_filename_96' => $current_arp_96,
    //         'owner_name' => $owner_name,
    //         'cancelled_by_td' => $cancelled_by_td,
    //         'previous_arp' => $previous_arp,
    //         'location' => $location,
    //         'lot' => $lot,
    //         'block' => $block,
    //         'tct' => $tct,
    //         'tct_date' => $tct_date,
    //         'cct' => $cct,
    //         'cct_date' => $cct_date,
    //         'kind' => $request['kind'].'-'.$request['actual_use'],
    //         'area' => $request['area'],
    //         'market_value' => $request['market_value'],
    //         'currentAssessValue' => $request['currentAssessValue'],
    //         'is_databuildup' => $request['is_databuildup'],
    //         'status' => 2,
    //         'created_by' => $request['created_by'],

    //         'proofread_by' => Auth::user()->id,
    //         'proofread_date' => date('Y-m-d H:i:s')
    //     );  

    
        
    //     TempTaxDeclarations::where('id', $id )
    //     ->update(['status' => 2 ]);

    //     $status = TaxDeclaration::insert($saveArray);
    //     Controller::addLog('ProofRead', $new_data = "" , $prev_data = "",  $remarks = "save to TaxDec");
    //     // \Log::info($saveArray);

    //     if($status){
    //         return response()->json([
    //             "data" => [],
    //             'message' => "Saved Succcessfully",
    //             'status' => 1
    //         ], 200);            
    //     }

    //     return response()->json([
    //         "data" => [],
    //         'message' => "No data found",
    //         'status' => 2
    //     ], 200);
        
    // }

    public function getTaxDec() {
        $dataBuildUpReturn = TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96',
        'pin', 'owner_name', 'is_databuildup', 'status', 'created_by', 
        'proofread_by','verified_by','created_at')
        ->where('status', 2)
        ->orderBy('created_at')
        ->get();

        foreach($dataBuildUpReturn as $key => $value ){
            
            $dataBuildUpReturn[$key]['is_databuildup']=self::getDataBuildUp($value['is_databuildup']);
            $dataBuildUpReturn[$key]['status']=self::getStatus($value['status']);
        };

        return $dataBuildUpReturn;
    }

    public function verifyTaxDec(Request $request){

        $status = TaxDeclaration::where('pdf_filename_96', $request['pdf_filename'] )
        ->update(
            [
                'current_arp' => $request['current_arp'],
                'current_arp_96' => $request['current_arp_96'],
                'pin' => $request['pin'],
                'owner_name' => $request['owner_name'],
                'location' => $request['location'],
                'lot' => $request['lot'],
                'block' => $request['block'],
                'tct' => $request['tct'],
                'tct_date' => $request['tct_date'],
                'cct' => $request['cct'],
                'cct_date' => $request['cct_date'],
                'cancelled_by_td' => $request['cancelled_by_td'],
                'previous_pin' => $request['previous_pin'],
                'remarks' => $request['remarks'],
                'previous_arp' => $request['previous_arp'],
                'memoranda' => $request['memoranda'],
                'status' => 3,
                'verified_by' => Auth::user()->id,

                'verified_date' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

                'is_databuildup' => 1,
            ]
        );

        $saveArrayAssmnt = [];
        foreach($request['property_assessment'] as $key => $value){
            if($value['kind']<> null){
                $saveArrayAssmnt[] = array(
                    'id'                => $value['id'],
                    'pdf_filename'      => $request['pdf_filename'],
                    'current_arp'       => $request['current_arp'],
                    'pin'               => $request['pin'],
                    'kind'              => $value['kind'],
                    'actual_use'        => $value['actual_use'],
                    'area'              => $value['area'],
                    'market_value'      => $value['market_value'],
                    // 'assessment_level' => $value['assessment_level'],
                    'assessed_value'    => $value['assessment_value'],
                    // 'taxability' => $value['taxability'],

                   
                    'updated_at' => date('Y-m-d H:i:s'),
                );
            }
        }
        
        foreach($saveArrayAssmnt as $key => $value){
            
            
            TaxDeclarationsAssmnt::updateOrCreate([['id', $value['id']]],$value);

            
                
                // TaxDeclarationsAssmnt::updateOrCreate([['pdf_filename', $value['pdf_filename']],
                // ['current_arp', $value['current_arp']],
                // ['kind', $value['kind']],['actual_use', $value['actual_use']]], $value);
                    
        }


        Controller::addLog('Verified', $new_data = "" , $prev_data = "",  $remarks = "verify to TaxDec");

        if($status){
            return response()->json([
                "data" => [],
                'message' => "Update Succcessfully",
                'status' => 1
            ], 200);            
        }

        // if($statusAssmnt){
        //     return response()->json([
        //         "data" => [],
        //         'message' => "Saved Assmnt Succcessfully",
        //         'status' => 1
        //     ], 200);            
        // }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }

    public function deletePropertyAssessment(Request $request){
        
        $statusDelete = TaxDeclarationsAssmnt::where('id',$request['id'])->delete();

        if($statusDelete){
            return response()->json([
                "data" => [],
                'message' => "Update Succcessfully",
                'status' => 1
            ], 200);            
        }

        return response()->json([
            "data" => [],
            'message' => "No data found",
            'status' => 2
        ], 200);
    }

    public function getSingleTaxDec(Request $request){
        
        
        $id = $request['id'];
        

        $data = TaxDeclaration::select(
            'current_arp', 'current_arp_96', 'pdf_filename_96', 'owner_name', 'cancelled_by_td', 'previous_arp', 
            'location', 'lot', 'block', 'cct', 'tct', 'tct_date', 'cct_date', 
            'kind', 'area', 'market_value', 'currentAssessValue', 'is_databuildup',
            'created_by'
            )
        ->where('id',$id)
        ->first();
            
  

        $actual_use = $data['kind'];

            $data['kind'] = self::getKindType($data['kind']);
            $data['actual_use'] = self::getActualUse($actual_use);
        
            

        return $data;
    }

    private function getKindType($type){

        $exploded_type = explode("-", $type);
        
        return $exploded_type[0];

    }

    private function getActualUse($use){

        $exploded_type = explode("-", $use);
        
       return $exploded_type[1];
    }

    private function getDataBuildUp($buildUp) {
        if ($buildUp == 1) {
            return "Data Build-up";
        }
        else  {
            return  "Reconstruction";
        }
    }

    private function getStatus($status) {
        if ($status == 1) {
            return "Created";
        }
        else if($status == 2)  {
            return  "Proofed";
        }
        else {
            return "Verified";
        }
    }

    private function getKind($kind){
        if ($kind == 1) {
            return 'B-';
        }
        else if($kind == 2) {
            return 'M-';
        }
        else {
            return 'L-';
        }
    }
}
