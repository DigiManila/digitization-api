<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRecordsCertificationsTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('records_certifications', function (Blueprint $table) {
            $table->string('amount')->nullable()->after('or');
            $table->date('or_date')->nullable()->after('or');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('records_certifications', function (Blueprint $table) {
            //
        });
    }
}
