<?php

return [

    'cf'  => env('DISK_CONFIG', 'sftp'),
    'sv'  => env('SV_CONFIG', 'sv'),
];