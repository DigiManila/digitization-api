<?php

namespace App\Helper;

class Helper
{

    public function excelHeader($title, $asOfDate, $last_col, $logo2, $logo3, $logo_offset){

        // $title = "Test Title";
        // $asOfDate = "December 99, 9999";
        // $last_col = 'N';
        // $logo_offset = 'O';
        

        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder() );

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        // $worksheet->setTitle($title);
        $spreadsheet->getProperties()
                ->setTitle($title);
        $spreadsheet->getSheetByName($title);
        $worksheet = $spreadsheet->getActiveSheet();

        //headings
        $manila_city_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'manila_logo.png');
        $doa_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DOA.png');
        $digi_logo = str_replace("\\", "/",public_path('img') . DIRECTORY_SEPARATOR . 'DIGI.png');
        
        $drawingLogoManila = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoManila->setName('Manila City Hall')
            ->setDescription('Logo')
            ->setPath($manila_city_logo) // put your path and image here
            ->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoManila->setWorksheet($worksheet);

        $drawingLogoDOA = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDOA->setName('Department of Assessment')
                ->setDescription('Logo')
                ->setPath($doa_logo) // put your path and image here
                ->setCoordinates($logo2 . "1")
                ->setOffsetX(50)
                ->setOffsetY(10)
                ->setHeight(75)
                ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDOA->setWorksheet($worksheet);

        $drawingLogoDIGI = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawingLogoDIGI->setName('Digitization')
            ->setDescription('Logo')
            ->setPath($digi_logo) // put your path and image here
            ->setCoordinates($logo3 . "1")
            ->setOffsetX(130)
            ->setOffsetY(10)
            ->setHeight(75)
            ->getShadow()->setVisible(true)->setDirection(45);
        $drawingLogoDIGI->setWorksheet($worksheet);
        
        //REPUBLIC OF THE PHILIPPINES
        $worksheet->mergeCells("A1:" . $last_col . "1");
        $worksheet->setCellValue("A1", "REPUBLIC OF THE PHILIPPINES");
        // $worksheet->getStyle("B1:D1")->getFont()->setBold(true);
        $worksheet->getStyle("A1")->getFont()->setSize(12);
        $worksheet->getStyle("A1:" . $last_col . "1")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle("A1:" . $last_col . "1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //CITY OF MANILA
        $worksheet->mergeCells("A2:" . $last_col . "2");
        $worksheet->setCellValue("A2", "CITY OF MANILA");
        $worksheet->getStyle("A2:" . $last_col . "2")->getFont()->setBold(true);
        $worksheet->getStyle("A2")->getFont()->setSize(14);
        $worksheet->getStyle("A2:" . $last_col . "2")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle("A2:" . $last_col . "2")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //DEPARTMENT OF ASSESSMENT
        $worksheet->mergeCells("A3:" . $last_col . "3");
        $worksheet->setCellValue("A3", "DEPARTMENT OF ASSESSMENT");
        $worksheet->getStyle("A3:" . $last_col . "3")->getFont()->setBold(true);
        $worksheet->getStyle("A3")->getFont()->setSize(12)->getColor()->setRGB('d01c1f');
        $worksheet->getStyle("A3:" . $last_col . "3")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle("A3:" . $last_col . "3")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //*Title of Report
        $worksheet->mergeCells("A4:" . $last_col . "5");
        $worksheet->setCellValue("A4", $title);
        $worksheet->getStyle("A4:" . $last_col . "5")->getFont()->setBold(true);
        $worksheet->getStyle("A4")->getFont()->setSize(18);
        $worksheet->getStyle("A4:" . $last_col . "5")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet->getStyle("A4:" . $last_col . "5")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        //*Date of Report
        $worksheet->mergeCells("A7:" . $last_col . "7");
        $worksheet->getStyle("A7:" . $last_col . "7")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $worksheet->setCellValue("A7", "As of $asOfDate");
        $worksheet->getStyle("A7")->getFont()->setSize(12)->setBold(true);
        // $worksheet->setCellValue("B4", $excelPrintText);
        // $worksheet->getStyle("B4")->getFont()->setSize(8)->setItalic(true);
        // $worksheet->setCellValue("K4", "printed date:  ". date("F j, Y"));
        $worksheet->getStyle("K4")->getFont()->setSize(8);

        return compact("spreadsheet", "worksheet");

    }

}