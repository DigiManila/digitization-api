<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_rolls', function (Blueprint $table) {
            $table->id();
            $table->text("owner_name");
            $table->text("address");
            $table->string("lot")->nullable();
            $table->string("block")->nullable();
            $table->string("tct")->nullable();
            $table->string("pin")->nullable();
            $table->date("tax_effectivity")->nullable();
            $table->string("status")->nullable();
            $table->string("current_arp")->nullable();
            $table->string("previous_arp")->nullable();
            $table->string("kind");
            $table->decimal("area",20,2);
            $table->decimal("market_value",20,2);
            $table->integer("assessment_level");
            $table->decimal("currentAssessValue",20,2);
            $table->decimal("previousAssessValue",20,2)->nullable();
            $table->boolean("taxable")->default(1);
            $table->string("district_id")->nullable();
            $table->string("zone_id")->nullable();
            
            $table->string("created_by")->nullable();
            $table->string("updated_by")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_rolls');
    }
}
