<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingPoliticalBoundary extends Model
{
    use HasFactory;
    protected $table = 'mapping_political_boundaries';
}
