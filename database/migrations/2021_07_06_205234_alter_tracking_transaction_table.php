<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTrackingTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracking_transactions', function (Blueprint $table) {
            $table->integer("receiving_id")->after('status');
        });
        Schema::table('tracking_normal_flows', function (Blueprint $table) {
            $table->dropColumn('receiving_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracking_transactions', function (Blueprint $table) {
            //
        });
    }
}
