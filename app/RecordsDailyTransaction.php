<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordsDailyTransaction extends Model
{
    use HasFactory;

    protected $guarded = [];
}
