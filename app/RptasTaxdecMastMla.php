<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasTaxdecMastMla extends Model
{
    use HasFactory;

    protected $table = 'rptas_taxdec_mast_mla';
}
