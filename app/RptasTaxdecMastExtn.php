<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasTaxdecMastExtn extends Model
{
    use HasFactory;

    protected $table = 'rptas_taxdec_mastextn';
}
