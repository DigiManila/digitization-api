<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAllTrackingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Change tracking_processes to tracking_normal_flow table
        Schema::rename('tracking_transactions', 'tracking_normal_flow');
        Schema::table('tracking_normal_flow', function (Blueprint $table) {
            $table->dropColumn('file_no');
            $table->dropColumn('or');
            $table->dropColumn('transaction_datetime');
            $table->dropColumn('status');
            $table->bigInteger('tracking_id')->after('id');
        });

        // Change tracking_numbers to tracking_processes table
        Schema::rename('tracking_numbers', 'tracking_transactions');
        Schema::table('tracking_transactions', function (Blueprint $table) {
            $table->dropColumn('number');
            $table->mediumInteger("file_no")->after('id');
            $table->datetime("transaction_datetime")->after('file_no');
            $table->string("or")->after('transaction_datetime')->nullable();
            $table->tinyInteger("status")->after('or');
        });

        // Add column to tracking_processes
        Schema::table('tracking_processes', function (Blueprint $table) {
            $table->string("description")->nullable();
        });

        // Change tracking_numbers to tracking_processes table
        Schema::table('tracking_statuses', function (Blueprint $table) {
            $table->renameColumn("trans_id", "tracking_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
