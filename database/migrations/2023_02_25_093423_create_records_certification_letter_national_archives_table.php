<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsCertificationLetterNationalArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_certification_letter_national_archives', function (Blueprint $table) {
            $table->id();
            $table->string('ctr_no');
            $table->string('type');
            $table->string('requestor');
            $table->text('file_path')->nullable();
            $table->string('or')->nullable();
            $table->string('amount')->nullable();
            $table->date('or_date')->nullable();
            $table->integer('copies')->default(1);
            $table->boolean('printed')->default(false);
            $table->string('printed_by');
            $table->integer('user_id');
            $table->string('prepared_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_certification_letter_national_archives');
    }
}
