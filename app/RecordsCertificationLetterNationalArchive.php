<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordsCertificationLetterNationalArchive extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function letter_data(){
        return $this->hasMany('App\RecordsCertificationLetterNationalArchivesData', 'letter_id', 'id');
    } 
}
