<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_declarations', function (Blueprint $table) {
            $table->id();
            $table->string("pin")->nullable();
            $table->string("current_arp")->nullable();
            $table->text("owner_name");
            $table->string("cancelled_by_td")->nullable();
            $table->string("previous_arp")->nullable();
            $table->text("location")->nullable();
            $table->string("lot")->nullable();
            $table->string("block")->nullable();
            $table->string("tct")->nullable();
            $table->string("tct_date")->nullable();
            $table->string("cct")->nullable();
            $table->string("cct_date")->nullable();
            
            
            $table->string("kind");
            $table->decimal("area",20,2);
            $table->decimal("market_value",20,2);
            $table->integer("assessment_level");
            $table->decimal("currentAssessValue",20,2);
            
            
                        
            
            $table->string("verified_by")->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_declarations');
    }
}
