<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingNotActivePinPoliticalBoundary extends Model
{
    use HasFactory;
    protected $table = 'mapping_not_active_pin_political_boundaries';
}
