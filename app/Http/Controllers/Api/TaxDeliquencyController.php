<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\TaxDelinquency;
use App\DistrictPolygon;
use App\BgyPolygon;
use App\MapPolygon;
use App\RptasActiveArpAllDetails;
use App\MapPolygonWithSection;

use App\ActiveAssessmentRoll;

use Illuminate\Support\Facades\Redis;

class TaxDeliquencyController extends Controller
{
    public function getMounted(){

        $dist_out = [];
        
        $district = DistrictPolygon::select('district', 'polygon')
                                    ->get()
                                    ->toArray();

        foreach ($district as $key => $value) {
            $dist_out[] = array(
                'district'  => $value['district'],
                'path'      => str_replace("|", ",", $value['polygon']),
            );
        }

        $barangay = TaxDelinquency::select('barangay')
                            ->distinct()
                            ->orderBy('barangay')
                            ->pluck('barangay')
                            ->toArray();


        return response()->json([
            "data" => compact('dist_out', 'barangay'),
            'message' => "",
            'status' => 1
        ], 200);  

    }

    public function postDistrict(Request $request){

        $district = $request['district'];

        $bgy = BgyPolygon::select('bgy')
                ->where('district', $district)
                ->orderBy('id', 'asc')
                ->first();
        
        $brgyNo = str_pad($bgy['bgy'],3,'0',STR_PAD_LEFT);

        $pin = TaxDelinquency::selectRaw('LEFT(pin , 18) as basePin')
                ->where('Barangay',  $brgyNo )
                ->orderBy('basePin')
                ->first();
        
        return self::getAllDetails($pin['basePin'], $brgyNo, 1);
        
    }

    public function postBarangay(Request $request){

        $bgy = sprintf('%03d', $request['bgy']);

        $pin = TaxDelinquency::selectRaw('LEFT(pin , 18) as basePin')
                ->where('Barangay',  $bgy )
                ->orderBy('basePin')
                ->first();
        
        return self::getAllDetails($pin['basePin'], $bgy, 1);
    }

    public function postParcel(Request $request){

        $bgy = explode('-', $request['pin'])[2];

        return self::getAllDetails($request['pin'], $bgy, $request["delinquent_param"]);

    }

    public function postParcelSearch(Request $request){

        $bgy = explode('-', $request["pin"])[2];

        $if_delinquent = TaxDelinquency::select("pin")
                                        ->where("pin", "like", $request["pin"] . '%')
                                        ->get();


        return self::getAllDetails($request['pin'], $bgy, count($if_delinquent) ? 1 : 0);

    }
    
    private function getAllDetails($pin, $bgy, $param){

        $active = MapPolygon::select('pin', 'gmap_polygon', 'barangay', 'center_coords')
                                ->where('pin', $pin)
                                ->first();
        if($active){

            $active['gmap_polygon'] = str_replace("|", ",", $active['gmap_polygon']);
            $active['gmap_polygon'] = str_replace("\\", "", $active['gmap_polygon']);

        } else{
            $active['pin'] = $pin;
            $active['barangay'] = $bgy;
            $active['center_coords'] = "{}";
            $active['gmap_polygon'] = "[]";
        }

        // Get all pin that have delinquencies
        $all_bgy_delinquencies_pin = self::getPinTaxDelinquenciesList($bgy, $pin);

        // Classify which parcels are delinquent, separate parcels

        $delinquent_parcels = MapPolygon::select('pin', 'gmap_polygon', 'center_coords')
                                    ->where('barangay', $bgy)
                                    ->where('pin', '!=', $pin)
                                    ->whereIn('pin', $all_bgy_delinquencies_pin)
                                    ->get();
        
        $non_delinquent_parcels = MapPolygon::select('pin', 'gmap_polygon')
                                    ->where('barangay', $bgy)
                                    ->where('pin', '!=', $pin)
                                    ->whereNotIn('pin', $all_bgy_delinquencies_pin)
                                    ->get();
        
        $delinquents        = self::parseBarangay($delinquent_parcels);
        $nondelinquents     = self::parseBarangay($non_delinquent_parcels);
        $district           = "";

        // Get all unused barangay
        if(isset($bgy)){

            $district = BgyPolygon::select('district')
                            ->where('bgy', (int)$bgy)
                            ->first();


            $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                            ->where('bgy', '!=', (int)$bgy)
                            ->where('district', $district['district'])
                            ->get();
            

            $unused_district = DistrictPolygon::select('district', 'polygon')
                                ->where('district', '!=', $district['district'])
                                ->get();
           
            $bgy_polygon            = self::parseUnusedBarangay($unused_bgy);
            $dist_polygon           = self::parseUnusedDistrict($unused_district);
        }


        // Get Tax Delinquency 
        // find the current arp of the pin
        $queryUpdatedARP = ActiveAssessmentRoll::select('ARP','PIN')
                                ->where('PIN', $pin)
                                ->orderBy('ARP', 'desc')
                                ->first();

        if ($queryUpdatedARP !== null){
            //select the master data of the current arp
            $queryMasterTaxdec = ActiveAssessmentRoll::select('ARP','PIN','Owner','OwnerAddress',
                                'Location','BldgLocation','MuniDistName',
                                'LotNo', 'BlkNo')
                                ->where('ARP', $queryUpdatedARP['ARP'])
                                ->first();
        } else {
            $queryMasterTaxdec = [];
        }

        $building_list = [];
        $machinery_list = [];
        $land_delinquent_details = [];

        if($param){

            $land_delinquent_details = TaxDelinquency::where('pin', '=',  $pin)
                                            ->get()
                                            ->toArray();            

            $bldg_mach = TaxDelinquency::where('pin', 'like',  $pin.'%')
                                    ->whereRaw('LENGTH(pin) > 18')
                                    ->orderBy('pin', 'ASC')
                                    ->get()
                                    ->toArray();

            foreach ($bldg_mach as $bm_data){
                if (strpos($bm_data['pin'],'-B') !== false){
                    $building_list[] = $bm_data;
                }
                else if (strpos($bm_data['pin'],'-M') !== false){
                    $machinery_list[] = $bm_data;
                }
            }

        }

        $pictures = self::getPicturesUrl($pin);

        $markers = self::getMarkers($delinquents, $active, $param);

        $sections = self::getSections($bgy);

        $pin_list = self::getPinList($bgy);

        if($active['center_coords'] == "{}"){
            $active['center_coords'] = self::getCentroid($bgy);
        }

        return compact("active", "delinquents", "nondelinquents", "bgy_polygon", "dist_polygon", "queryMasterTaxdec", "building_list", "machinery_list", "land_delinquent_details", "pictures", "markers", "sections", "pin_list", "district");
        
    }

    public function getSectionMapPolygons(Request $request){

        $bgy = $request['bgy'];
        $sec = $request['sec'];

        $map_active = MapPolygonWithSection::select('gmap_polygon')
                                        ->where('barangay', $bgy)
                                        ->where('section', $sec)
                                        ->get()
                                        ->toArray();

        $map_inactive = MapPolygonWithSection::select('gmap_polygon')
                                        ->where('barangay', $bgy)
                                        ->where('section', '!=', $sec)
                                        ->get()
                                        ->toArray();

        $active     = self::parseSections($map_active);
        $inactive   = self::parseSections($map_inactive);

        return response()->json([
            "data" => compact("active", "inactive"),
            'message' => "Saved Succcessfully",
            'status' => 1
        ], 200);            
        
    }

    // public function getNoMapReport(){

    //     $pin_filtered = [];

    //     $pins = TaxDelinquency::select('pin')
    //                             ->orderBy('pin')
    //                             ->get();

    //     foreach ($pins as $key => $value) {

    //         $pin_base = substr($value['pin'], 0, 18);

    //         if(!array_key_exists($pin_base, $pin_filtered)){
    //             $pin_filtered[$pin_base] = 0;
    //         }

    //     }

    //     $filtered = array_keys($pin_filtered);

    //     $polygons = MapPolygon::select('pin')
    //                     ->distinct()
    //                     ->orderBy('pin')
    //                     ->pluck('pin')
    //                     ->toArray();

    //     $flippped_polygon = array_flip($polygons);

    //     $no_maps_list = [];

    //     foreach ($filtered as $key => $value) {
    //         if(!array_key_exists($value, $flippped_polygon)){
    //             $no_maps_list[] = $value;
    //         }
    //     }

    //     $groupByBgy = self::groupByBgy($no_maps_list);

    //     return self::createExcel($groupByBgy);

    // }

    public function getNoMapReport(Request $request){

        $pin_filtered = [];

        $pins = TaxDelinquency::select('pin')
                                ->where('barangay', $request['bgy'])
                                ->orderBy('pin')
                                ->get();

        foreach ($pins as $key => $value) {

            $pin_base = substr($value['pin'], 0, 18);

            if(!array_key_exists($pin_base, $pin_filtered)){
                $pin_filtered[$pin_base] = 0;
            }

        }

        $filtered = array_keys($pin_filtered);

        $polygons = MapPolygon::select('pin')
                        ->where('barangay', $request['bgy'])
                        ->distinct()
                        ->orderBy('pin')
                        ->pluck('pin')
                        ->toArray();

        $flippped_polygon = array_flip($polygons);

        $no_maps_list = [];

        foreach ($filtered as $key => $value) {
            if(!array_key_exists($value, $flippped_polygon)){
                $no_maps_list[] = $value;
            }
        }

        $groupByBgy = self::groupByBgy($no_maps_list);

        return self::createExcel($groupByBgy);

    }

    private function parseBarangay($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'pin'   => $value['pin'],
                'path'  => str_replace("|", ",", $value['gmap_polygon']),
                'center'  => str_replace("|", ",", $value['center_coords'])
            );

           
        }
        return $data_arr;

    }

    private function getPinTaxDelinquenciesList($bgy, $pin){

        $pin_filtered = [];

        $pins = TaxDelinquency::select('pin')
                                ->where('barangay', $bgy)
                                ->orderBy('pin')
                                ->get();

        foreach ($pins as $key => $value) {

            $pin_base = substr($value['pin'], 0, 18);

            if($pin_base != $pin){
                if(!in_array($pin_base, $pin_filtered)){
                    $pin_filtered[] = $pin_base;
                }
            }

        }

        return $pin_filtered;

    }

    private function parseUnusedBarangay($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'bgy'       => $value['bgy'],
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function parseUnusedDistrict($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function parseSections($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['gmap_polygon']),
            );
        }
        return $data_arr;
    }

    private function getMarkers($d, $a, $param){

        $out = [];

        if($param){
            if(isset($a['pin'])){
                if(json_decode($a['center_coords'], 1)){
                    $out[] = array(
                        'pin'       => $a['pin'],
                        'coords'    => $a['center_coords']
                    );                    
                }
            }
        }


        foreach ($d as $key => $value) {
            $out[] = array(
                'pin'       => $value['pin'],
                'coords'    => $value['center']
            );
        }

        return $out;

    }

    public function getPicturesUrl($pin){
        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data["BLDG IMAGE"];

        $list = [];

        foreach($path as $paths){
            if(gettype($paths) == 'string'){
                if(strpos($paths, $pin) !== false){
                    $list[] = $paths;
                }
            }
        }

        if(count($list)){

            sort($list, 4);
            
            return array(
                "listCount" => count($list),
                "list"      => $list,
            );
        }

        $list[] = "imagenf.jpg";

        return array(
            "listCount" => count($list),
            "list"      => $list,
        );
    }

    private function computeCenter($data){

        $arrX = [];
        $arrY = [];

        foreach ($data as $key => $value) {

            $latLng = explode(",", $value);

            if($latLng[0]){
                // $arrX[] = $latLng[1] / 0.99999796;
                // $arrY[] = $latLng[0] / 1.000000466230960;

                $arrX[] = $latLng[1];
                $arrY[] = $latLng[0];
            }
        }

        sort($arrX);
        sort($arrY);

        $minX = reset($arrX);
        $maxX = end($arrX);

        $minY = reset($arrY);
        $maxY = end($arrY);

        $getCenterX = ($maxX - $minX) / 2;
        $getCenterY = ($maxY - $minY) / 2;

        return "{ \"lat\": " . (string)($minX + $getCenterX) . ", \"lng\": " . (string)($minY + $getCenterY) . " }";

    }

    private function getSections($bgy){

        $list = [];
        $centroid = "";

        $latlng = [];

        $data = MapPolygonWithSection::select('section', 'center_coords')
                                        ->where('barangay', $bgy)                     
                                        ->get()
                                        ->toArray();

        foreach($data as $key => $val){

            $list[] = $val['section'];

            $tmp = json_decode($val['center_coords'], 1);

            $latlng[] = $tmp["lng"] . "," . $tmp["lat"];
        }

        $centroid = self::computeCenter($latlng);

        return compact('centroid', 'list');

    }

    private function getCentroid($bgy){

        $latlng = [];

        $data = MapPolygon::select('center_coords')
                                        ->where('barangay', $bgy)                     
                                        ->get()
                                        ->toArray();

        foreach($data as $key => $val){
            $tmp = json_decode($val['center_coords'], 1);
            $latlng[] = $tmp["lng"] . "," . $tmp["lat"];
        }

        return self::computeCenter($latlng);
    }

    private function getPinList($bgy){

        $output         = [];
        $list           = [];
        $pin_filtered   = [];

        $pins = TaxDelinquency::select('pin')
                                    ->where('barangay', $bgy)
                                    ->orderBy('pin', 'ASC')
                                    ->get()
                                    ->toArray();

        foreach ($pins as $key => $value) {

            $pin_base = substr($value['pin'], 0, 18);

            if(!array_key_exists($pin_base, $pin_filtered)){
                $pin_filtered[$pin_base] = 0;
            }

        }

        $filtered = array_keys($pin_filtered);

        $polygons = MapPolygon::select('pin')
                        ->distinct()
                        ->orderBy('pin')
                        ->pluck('pin')
                        ->toArray();

        $flippped_polygon = array_flip($polygons);

        foreach ($filtered as $key => $value) {
            if(array_key_exists($value, $flippped_polygon)){
                $list[] = $value;
            }
        }

        foreach ($list as $key => $value) {

            if(strlen($value) > 18){
                $pos        = strrpos($value, '-');
                $base_pin   = substr($value, 0, $pos);
            } else{
                $base_pin   = $value;
            }
            

            if(!in_array($base_pin, $output)){
                $output[] = $base_pin;
            }

        }

        return $output;

    }

    private function groupByBgy($data){

        $output = [];

        foreach ($data as $key => $value) {
            $exp = explode("-", $value);
            $output[$exp[2]][] = $value;
        }

        return $output;

    }

    private function createExcel($data){

        // return "OK";

        $spreadsheet    = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
       
        foreach ($data as $key => $value) {

            $sheet_name = "Barangay " . $key;

            if($key == 1){
                $worksheet = $spreadsheet->getActiveSheet();
                $worksheet->setTitle($sheet_name);
                $spreadsheet->getSheetByName($sheet_name);
                $worksheet = $spreadsheet->getActiveSheet();
            } else{
                $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $sheet_name);
                $spreadsheet->addSheet($myWorkSheet);
                // $spreadsheet->getSheet($spreadsheet->getSheetCount() - 1);
                $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);
            }

            $worksheet->getColumnDimension('A')->setWidth(25);

            $worksheet
                    ->setCellValue("A3", "PIN");

            $worksheet->getStyle("A3")->getFont()->setBold(true);
            $worksheet->getStyle('A3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $worksheet->getStyle('A3')->getAlignment()->setWrapText(true);

            $worksheet->setCellValue("A1", "REPORT AS OF:  " . date("F j, Y"));
            $worksheet->getStyle("A1")->getFont()->setBold(true);

            $startRow   = 4;
            
            foreach ($value as $key => $val) {

                $worksheet->setCellValue("A{$startRow}", $val);
                $worksheet->getStyle("A{$startRow}")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("A{$startRow}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle("A{$startRow}")->getAlignment()->setWrapText(true);

                $startRow = $startRow + 1;
            }

            $worksheet->setSelectedCells('A1');

        }

        $worksheet = $spreadsheet->setActiveSheetIndex(0);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;
    }

}
