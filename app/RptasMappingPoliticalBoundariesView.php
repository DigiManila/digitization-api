<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasMappingPoliticalBoundariesView extends Model
{
    use HasFactory;
    protected $table = 'rptas_mapping_political_boundaries';
}
