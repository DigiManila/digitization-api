<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AggKindBarangaySummary extends Model
{
    use HasFactory;
    protected $guarded = [];
}
