<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRptasTaxdecDistinctMachAssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rptas_taxdec_distinct_mach_ass', function (Blueprint $table) {
            $table->char('ARP',20);
            $table->char('Kind',1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxdec_distinct_mach_ass');
    }
}
