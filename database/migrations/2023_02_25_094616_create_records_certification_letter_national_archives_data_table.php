<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsCertificationLetterNationalArchivesDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_certification_letter_national_archives_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('letter_id');
            $table->string('owner')->nullable();
            $table->string('lot_blk')->nullable();
            $table->string('classification')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_certification_letter_national_archives_data');
    }
}
