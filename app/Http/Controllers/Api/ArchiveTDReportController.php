<?php 


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ArchiveTDReport;
use App\TempTaxDeclarations;
use App\TaxDeclaration;

use App\User;

use Auth;
use Carbon\Carbon;
class ArchiveTDReportController extends Controller
{


    public function getArchiveTaxDec(Request $request){

        $user_type = User::with('profile')->where('id', Auth::user()->id)->first();

        $user_type = $user_type["profile"]["user_type"];

        if ($request->typeOfTD == "Encoded"){
            if  ($request->dateMonth == null && $request->advanceSearch == null){


                if($user_type != "Encoder"  ) {
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname as encoder', 
                    'proofread_by','verified_by','created_at')
                    ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->whereDate('created_at', Carbon::today())
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                } 

                else {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where('created_by', Auth::user()->id )
                ->whereDate('created_at', Carbon::today())
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
                }
            }
            else if ($request->advanceSearch == null){
                
            $explodeDateMonth = explode('-',$request->dateMonth);

                $year = $explodeDateMonth[0];
                $month = $explodeDateMonth[1];

                if($user_type != "Encoder") {

         
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname as encoder', 
                    'proofread_by','verified_by','created_at')
                    ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }

                else{
                   
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date', 'status', 'created_by', 
                    'proofread_by','verified_by','created_at')
                    ->where('created_by', Auth::user()->id )
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }

            }
            else if($request->dateMonth == null){

                if($user_type != "Encoder") {
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname as encoder', 
                    'proofread_by','verified_by','created_at')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }

                else
                {
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname as encoder', 
                    'proofread_by','verified_by','created_at')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    ->where('created_by', Auth::user()->id )
                    ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                
                }
            }

            else {

                $explodeDateMonth = explode('-',$request->dateMonth);

                $year = $explodeDateMonth[0];
                $month = $explodeDateMonth[1];
                    
                if ($user_type != "Encoder"){
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname as encoder', 
                    'proofread_by','verified_by','created_at')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();

                }
                else {
            
                    $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname as encoder', 
                    'proofread_by','verified_by','created_at')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    ->where('created_by', Auth::user()->id )
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }
            }

        }

        else {

            if  ($request->dateMonth == null && $request->advanceSearch == null){
                
                if($user_type == 'ProofReader') {
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date', 'status', 'created_by', 
                    'proofread_by','verified_by','created_at')
                    ->where('proofread_by', Auth::user()->id )
                    ->whereDate('created_at', Carbon::today())
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();

                }

                else if($user_type != 'Encoder' || $user_type != 'ProofReader' ) {
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'tax_declarations.proofread_by', 'average_proofread_ranking.fname as proofreader',
                    'average_encoded_ranking.fname as encoder',
                    'verified_by','created_at')
                    ->leftJoin('average_proofread_ranking', 'tax_declarations.proofread_by', '=', 'average_proofread_ranking.proofread_by')
                    ->leftJoin('average_encoded_ranking', 'tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->whereDate('created_at', Carbon::today())
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                } 

                
            }
            else if ($request->advanceSearch == null){

            $explodeDateMonth = explode('-',$request->dateMonth);

                $year = $explodeDateMonth[0];
                $month = $explodeDateMonth[1];
                
                if($user_type == 'ProofReader' ) {
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date', 'status', 'created_by', 
                    'proofread_by','verified_by','created_at')
                    ->where('proofread_by', Auth::user()->id )
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }

                else if($user_type != 'Encoder' || $user_type != 'ProofReader' ) {
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'tax_declarations.proofread_by', 'average_proofread_ranking.fname as proofreader',
                    'average_encoded_ranking.fname as encoder', 
                    'verified_by','created_at')
                    ->leftJoin('average_proofread_ranking', 'tax_declarations.proofread_by', '=', 'average_proofread_ranking.proofread_by')
                    ->leftJoin('average_encoded_ranking', 'tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }

            }
            else if($request->dateMonth == null){

                if($user_type == 'ProofReader' ) {

                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                    'status', 'created_by', 
                    'proofread_by','verified_by','created_at')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    ->where('proofread_by', Auth::user()->id )
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }
                else if($user_type != 'Encoder' || $user_type != 'ProofReader' ) {
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'tax_declarations.proofread_by', 'average_proofread_ranking.fname  as proofreader', 
                    'average_encoded_ranking.fname  as encoder', 
                    'verified_by','created_at')
                    ->leftJoin('average_proofread_ranking', 'tax_declarations.proofread_by', '=', 'average_proofread_ranking.proofread_by')
                    ->leftJoin('average_encoded_ranking', 'tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
             
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }
            }
            else {

                $explodeDateMonth = explode('-',$request->dateMonth);

                $year = $explodeDateMonth[0];
                $month = $explodeDateMonth[1];

                if($user_type == 'ProofReader' ) {
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                    'status', 'created_by', 
                    'proofread_by','verified_by','created_at')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'EN': 
                                $query->where('created_by', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    ->where('proofread_by', Auth::user()->id )
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }

                else if($user_type != 'Encoder' || $user_type != 'ProofReader' ) {

                    
                    $dataBuildUpReturn =  TaxDeclaration::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                    'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                    'tct', 'tct_date', 'cct', 'cct_date',
                    'status', 'tax_declarations.proofread_by', 'average_proofread_ranking.fname  as proofreader',
                    'average_encoded_ranking.fname  as encoder', 
                    'verified_by','created_at')
                    ->leftJoin('average_proofread_ranking', 'tax_declarations.proofread_by', '=', 'average_proofread_ranking.proofread_by')
                    ->leftJoin('average_encoded_ranking', 'tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                    ->where(function($query) use ($request) {
                        switch($request->selectItem){
                            case 'PIN': 
                                $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TD': 
                                $query->where('current_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'ARP': 
                                $query->where('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PDF': 
                                $query->where('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'PR': 
                                $query->where('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CB': 
                                $query->where('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'OW': 
                                $query->where('owner_name', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'EN': 
                                $query->where('created_by', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOC': 
                                $query->where('location', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'BLK': 
                                $query->where('block', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'LOT': 
                                $query->where('lot', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'TCT': 
                                $query->where('tct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            case 'CCT': 
                                $query->where('cct', "like", "%" . $request->advanceSearch . "%");
                            break;
                            default:
                            $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                            $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        }
                    })
                    
                    ->whereMonth('created_at', '=', $month)
                    ->whereYear('created_at', '=', $year)
                    ->orderBy('created_at', 'desc')
                    ->limit(10000)
                    ->get();
                }
        
            }
        
        }

        foreach($dataBuildUpReturn as $key => $value ){
            
           
            $dataBuildUpReturn[$key]['status']                    = self::getStatus($value['status']);
            $dataBuildUpReturn[$key]['created_at_formatted']      = date('F j, Y  g:i A', strtotime($value['created_at']));
            $dataBuildUpReturn[$key]['area']                      = number_format($value['area'],2,'.',',') . ".sqm";
            $dataBuildUpReturn[$key]['market_value']              = number_format($value['market_value'],2,'.',',');
            $dataBuildUpReturn[$key]['currentAssessValue']        = number_format($value['currentAssessValue'],2,'.',',');

            $dataBuildUpReturn[$key]['fname']                     =  ucwords(strtolower($value['fname']));
        };

        return $dataBuildUpReturn;
    }


    //time for changing this api.
    public function getTempTaxDec(Request $request){
         
        if ($request->dateMonth == null && $request->advanceSearch == null){

            $user_type = User::with('profile')->where('id', Auth::user()->id)->first();

            $user_type = $user_type["profile"]["user_type"];
            if($user_type == 'Encoder') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where('created_by', Auth::user()->id )
                ->whereDate('created_at', Carbon::today())
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
            else if($user_type == 'ProofReader') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->whereDate('created_at', Carbon::today())
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else if($user_type == 'SA' || $user_type == 'Administrator') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->whereDate('created_at', Carbon::today())
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->whereDate('created_at', Carbon::today())
                ->where('status', 1)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }

        }
    
        else if ($request->advanceSearch == null)
        {
            $user_type = User::with('profile')->where('id', Auth::user()->id)->first();

            $user_type = $user_type["profile"]["user_type"];

            $explodeDateMonth = explode('-',$request->dateMonth);

            $year = $explodeDateMonth[0];
            $month = $explodeDateMonth[1];
            if($user_type == 'Encoder') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where('created_by', Auth::user()->id )
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
            else if($user_type == 'ProofReader') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else if($user_type == 'SA' || $user_type == 'Administrator') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->where('status', 1)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
        }

        else if($request->dateMonth == null){
            $user_type = User::with('profile')->where('id', Auth::user()->id)->first();

            $user_type = $user_type["profile"]["user_type"];
            
            if($user_type == 'Encoder') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                        })
                ->where('created_by', Auth::user()->id )
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
            else if($user_type == 'ProofReader' ) {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('fname', "like", "%" . $request->advanceSearch . "%");
                    })
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            
            else if($user_type == 'SA' || $user_type == 'Administrator') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('fname', "like", "%" . $request->advanceSearch . "%");
                    })
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    })
                ->where('status', 1)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
        }
        else {
            $user_type = User::with('profile')->where('id', Auth::user()->id)->first();

            $user_type = $user_type["profile"]["user_type"];

            $explodeDateMonth = explode('-',$request->dateMonth);

            $year = $explodeDateMonth[0];
            $month = $explodeDateMonth[1];
            if($user_type == 'Encoder') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    })
                ->where('created_by', Auth::user()->id )
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
            else if($user_type == 'ProofReader') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('fname', "like", "%" . $request->advanceSearch . "%");
                        })
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else if($user_type == 'SA' || $user_type == 'Administrator') {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'temp_tax_declarations.created_by', 'average_encoded_ranking.fname', 
                'proofread_by','verified_by','created_at')
                ->leftJoin('average_encoded_ranking', 'temp_tax_declarations.created_by', '=', 'average_encoded_ranking.created_by')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('fname', "like", "%" . $request->advanceSearch . "%");
                        })
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            } 
            else {
                $dataBuildUpReturn =  TempTaxDeclarations::select('id','current_arp', 'current_arp_96', 'pdf_filename_96', 'kind',
                'pin', 'owner_name', 'cancelled_by_td', 'previous_arp', 'location', 'block','lot', 
                'tct', 'tct_date', 'cct', 'cct_date', 'area', 'market_value', 'currentAssessValue',
                'status', 'created_by', 
                'proofread_by','verified_by','created_at')
                ->where(function($query) use ($request) {
                    $query->where('pin', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('current_arp_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('pdf_filename_96', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('previous_arp', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('cancelled_by_td', "like", "%" . $request->advanceSearch . "%");
                    $query->orWhere('owner_name', "like", "%" . $request->advanceSearch . "%");
                    })
                ->whereMonth('created_at', '=', $month)
                ->whereYear('created_at', '=', $year)
                ->where('status', 1)
                ->orderBy('created_at', 'desc')
                ->limit(10000)
                ->get();
            }
        }

        foreach($dataBuildUpReturn as $key => $value ){
            
           
            $dataBuildUpReturn[$key]['status']                    = self::getStatus($value['status']);
            $dataBuildUpReturn[$key]['created_at_formatted']      = date('F j, Y  g:i A', strtotime($value['created_at']));
            $dataBuildUpReturn[$key]['area']                      = number_format($value['area'],2,'.',',') . ".sqm";
            $dataBuildUpReturn[$key]['market_value']              = number_format($value['market_value'],2,'.',',');
            $dataBuildUpReturn[$key]['currentAssessValue']        = number_format($value['currentAssessValue'],2,'.',',');

            $dataBuildUpReturn[$key]['fname']                     =  ucwords(strtolower($value['fname']));
        };

        return $dataBuildUpReturn;
        
    }


    private function getStatus($status) {
        if ($status == 1) {
            return "Created";
        }
        else if($status == 2)  {
            return  "Proofed";
        }
        else {
            return "Verified";
        }
    }

    public function missingTD(Request $request) {

        $td = [];

        $missing = [];

        $series = $request['series'];

        $brgyNo = str_pad($request['brgyNo'],3,'0',STR_PAD_LEFT);
        // get all td per 
        // bgy = 6

        $query = ArchiveTDReport::select('current_arp')
                                ->where('brgyNo', $brgyNo)
                                ->orderBy('current_arp')
                                ->get()
                                ->toArray();

        //get all values of query                        
        foreach($query as $val){
            $td[] = $val['current_arp'];
        }
        
        if ($series == '1979'){

            $matchingFiles = preg_grep("/A-\d{3}-\d{4}$/", $td);
        }
        elseif ($series == '1985'){
            $matchingFiles = preg_grep("/B-\d{3}-\d{5}$/", $td);
        }
        elseif ($series == '1996'){
            $matchingFiles = preg_grep("/C-\d{3}-\d{5}$/", $td);
        }
        elseif ($series == '2005'){
            $matchingFiles = preg_grep("/AA-\d{5}-\d{5}$/", $td);
        }
        else{
            $matchingFiles = preg_grep("/AD-\d{5}-\d{5}$/", $td);
        }
        //lahat ng unggoy
        // $matchingFiles = preg_grep("/^[.A-Z]-\d{3}-\d{4}$/", $td);

        // //lahat ng di unggoy
        // $unmatchFiles = preg_grep("/^[.A-Z]-\d{3}-\d{4}$/", $td, PREG_GREP_INVERT);
        
      

        //last value array
        $end = end($matchingFiles);

        //pagasabog ng array
        $last = explode("-", $end)[2];
        $last = (int) $last;

        $complete_list = [];
        

        //loop para hanapin ang mga nawawala

        for($i = 1; $i <= $last; $i++){

            if ($series == '1979'){

            $compare_text = "A-" . $brgyNo . '-' . str_pad($i,4,'0',STR_PAD_LEFT);
            }
            elseif ($series == '1985'){
                $compare_text = "B-" . $brgyNo . '-' . str_pad($i,5,'0',STR_PAD_LEFT);
            }
            elseif ($series == '1996') {
                $compare_text = "C-" . $brgyNo . '-' . str_pad($i,5,'0',STR_PAD_LEFT);
            }
            elseif ($series == '2005') {
                $compare_text = "AA-" . $brgyNo . '-' . str_pad($i,5,'0',STR_PAD_LEFT);
            }
            else {
                $compare_text = "AD-" . $brgyNo . '-' . str_pad($i,5,'0',STR_PAD_LEFT);
            }
            

            if(!in_array($compare_text, $matchingFiles)){
                $missing[]['missing'] = $compare_text;
                $complete_list[] = array(
                    'td' => $compare_text,
                    'status' => '0'
                );
            } else{
                $complete_list[] = array(
                    'td' => $compare_text,
                    'status' => '1'
                );                
            }

        }
            //pagagawa ng 10 column fix
        $rowcol = [];
        $idx = 0;
        $ctr = 1;

        foreach($complete_list as $val){

            if($ctr == 10){
                $rowcol[$idx][] = $val;
                $idx++;
                $ctr = 1;
            } else{
                $rowcol[$idx][] = $val;
                $ctr++;
            }
        }

        

        return compact('missing', 'rowcol');

    }

    public function getBrgyno(Request $request){
        
        $series = $request['series'];
       

        $query = TempTaxDeclarations::select('current_arp')
                            ->distinct()
                            ->orderBy('current_arp')
                            ->get()
                            ->toArray();

        foreach($query as $key => $value){
            
            
            $current_arp [] = $value['current_arp'];
        }

        if ($series == '1979'){

            $matchingBrgy = preg_grep("/A-\d{3}-\d{4}$/", $current_arp);
        }
        elseif ($series =='1985'){
            $matchingBrgy = preg_grep("/B-\d{3}-\d{5}$/", $current_arp);
        }
        elseif ($series =='1996'){
            $matchingBrgy = preg_grep("/C-\d{3}-\d{5}$/", $current_arp);
        }
        elseif ($series == '2005'){
            $matchingBrgy = preg_grep("/AA-\d{3}-\d{4}$/", $current_arp);
        }
        else{
            $matchingBrgy = preg_grep("/AD-\d{3}-\d{4}$/", $current_arp);
        }
        
       
        // return $matchingBrgy;

        $barangays = [];

        // foreach matchingBrgy as val
            // explode val get [1]
            // if !in_array $barangays save[1] $barangays[] = val

        foreach ($matchingBrgy as $val){
            
            $brgy =  explode("-", $val)[1];

            if(!in_array($brgy, $barangays)){
                $barangays[] = $brgy;
            }

        }



        
        sort($barangays);
        return array_values($barangays);
    }

    //for 1975 to 1985 series uproper naming convention
    
    public function getUnproperTD(){

        $td = [];
        $unmatch = [];
        $query = TempTaxDeclarations::select('current_arp')
                                ->orderBy('current_arp')
                                ->get()
                                ->toArray();

        foreach($query as $val){
            $td[] = $val['current_arp'];
        }

        $unmatchFiles = preg_grep("/^[.A-Z]-\d{3}-\d{4}$/", $td, PREG_GREP_INVERT);

        foreach($unmatchFiles as $val){
            $unmatch[]['unmatch'] = $val;
        }
        
        return $unmatch;
    }


}
