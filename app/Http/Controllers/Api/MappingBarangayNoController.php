<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\RptasMappingBrgynoView;
use App\RptasActiveMapView;
use App\RptasNotActivePinMapView;
use App\RptasNotActivePinBgyPolygonsView;
use App\RptasMappingPoliticalBoundariesView;
use App\RptasNotActivePinPoliticalBoundariesView;
use App\RptasMappingNoMapPinView;
use App\MapPolygon;
use App\BgyPolygon;
use App\MappingBgyPolygon;
use App\DistrictPolygon;

use App\RptasTaxdecMastMla;
use App\RptasTaxdecAssmnt;
use App\RptasActiveArpAllDetails;
use App\RptasMappingCancelledPinHaveMap;

use Illuminate\Support\Facades\Redis;

class MappingBarangayNoController extends Controller
{
    public function getMappingBrgyno(){
        
        $brgyNo = [];

        $query = RptasMappingBrgynoView::get()->toArray();

        foreach($query as $key => $value){
            
            $brgyNo [] = $value['brgyNo'];
        }


        return $brgyNo;
    }

    public function postMappingMunicipalDistrict(Request $request){
        
        // $muniDist = [];

        if ($request->radio == 'Active') {
            $query = RptasActiveMapView::select('MuniDistName')
                            ->where('MuniDistName', '!=', null)
                            ->distinct()
                            ->orderBy('MuniDistName')
                            ->get();
                            
        }
        else {
            $query = RptasMappingNoMapPinView::select('MuniDistName')
                            ->where('MuniDistName', '!=', null)
                            ->distinct()
                            ->orderBy('MuniDistName')
                            ->get();
                            
        }
        // foreach($query as $key => $value){
            
        //     $muniDist[] = $value['MuniDistName'];
        // }


        return $query;
    }

    public function postMappingMuniToBarangay(Request $request){
        
        if ($request->radio == 'Active') {
            $query = RptasActiveMapView::select('Barangay')
                    ->where('MuniDistName', $request->muni)
                    ->where('Barangay', '!=', 000)
                    ->distinct()
                    ->orderBy('Barangay')
                    ->get();
        }
        else{
            $query = RptasMappingNoMapPinView::select('Barangay')
                    ->where('MuniDistName', $request->muni)
                    ->where('Barangay', '!=', 000)
                    ->distinct()
                    ->orderBy('Barangay')
                    ->get();
        }
        return $query;
        
    }

    public function postMappingMuniToBlock(Request $request){
        
        if ($request->barangay === null){
            if ($request->radio == 'Active'){
                $query = RptasActiveMapView::select('BlkNo')
                        ->where('MuniDistName', $request->muni)
                        ->where('Barangay', '!=', 000)
                        ->where('BlkNo', '!=', '')
                        
                        ->distinct()
                        ->orderBy('BlkNo')
                        ->get();
            }
            else
            {
                $query = RptasMappingNoMapPinView::select('BlkNo')
                ->where('MuniDistName', $request->muni)
                ->where('Barangay', '!=', 000)
                ->where('BlkNo', '!=', '')
                
                ->distinct()
                ->orderBy('BlkNo')
                ->get();
            }
        }
        else {
            if ($request->radio == 'Active'){
                $query = RptasActiveMapView::select('BlkNo')
                        ->where('Barangay', $request->barangay)
                        ->where('BlkNo', '!=', '')
                        
                        ->distinct()
                        ->orderBy('BlkNo')
                        ->get();
            }
            else {
                $query = RptasMappingNoMapPinView::select('BlkNo')
                        ->where('Barangay', $request->barangay)
                        ->where('BlkNo', '!=', '')
                        
                        ->distinct()
                        ->orderBy('BlkNo')
                        ->get();
            }
        }
    
        return $query;
    }

   

    public function postMappingLocationWithDetails(Request $request){

        $data = [];
        if ($request->radio == 'Active'){
            
            if ($request->barangay == '' && $request->blk == '' && $request->lot == '' ){
                
                $query =RptasActiveMapView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('MuniDistName', $request->muni)
                    ->get()
                    ->toArray();
               
            }
            else if ($request->blk == '' && $request->lot == ''){
                
                $query =RptasActiveMapView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('Barangay', $request->barangay)
                    ->get()
                    ->toArray();    
            }
            else if ($request->barangay == '' && $request->lot == ''){
                
                $query =RptasActiveMapView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('MuniDistName', $request->muni)
                    ->where('BlkNo', $request->blk)
                    ->get()
                    ->toArray();    
            }
            else if ($request->lot == ''){
                
                $query =RptasActiveMapView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('Barangay', $request->barangay)
                    ->where('BlkNo', $request->blk)
                    ->get()
                    ->toArray();    
            }
        }
        else   {
            if ($request->barangay == '' && $request->blk == '' && $request->lot == '' ){
                $query =RptasMappingNoMapPinView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('MuniDistName', $request->muni)
                    ->get()
                    ->toArray();
            }
            else if ($request->blk == '' && $request->lot == ''){
                $query =RptasMappingNoMapPinView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('Barangay', $request->barangay)
                    ->get()
                    ->toArray();    
            }
            else if ($request->barangay == '' && $request->lot == ''){
                $query =RptasMappingNoMapPinView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('MuniDistName', $request->muni)
                    ->where('BlkNo', $request->blk)
                    ->get()
                    ->toArray();    
            }
            else if ($request->lot == ''){
                $query =RptasMappingNoMapPinView::select('Location', 'LotNo','BlkNo','pin', 'arp', 'owner')
                    ->where('Barangay', $request->barangay)
                    ->where('BlkNo', $request->blk)
                    ->get()
                    ->toArray();    
            }
        }
            foreach($query as $key => $value){

                $data[] = array(
                'Location'          => $value['Location'],
                'LotNo'             => trim($value['LotNo']),
                'BlkNo'             => trim($value['BlkNo']),
                'arp'               => $value['arp'],
                'pin'               => $value['PIN'],
                'owner'             => $value['OWNER'],
                );

            }
    
        return $data;
    }

    // public function getMappingNotActiveBrgyno(){
        
    //     $brgyNo = [];

    //     $query = RptasNotActivePinMapView::select('barangay')
    //                             ->distinct()
    //                             ->where('barangay', '!=', 000)
    //                             ->orderBy('barangay')
    //                             ->get()
    //                             ->toArray();

    //     foreach($query as $key => $value){
            
    //         $brgyNo [] = $value['barangay'];
    //     }


    //     return $brgyNo;
    // }

    public function getMappingNoPinBrgyno(){
         
        $query = RptasNotActivePinMapView::select('barangay')
                    ->distinct()
                    ->get();
            
        
        return $query;
     }

    public function getMappingNoMapPinBrgyno(){    

        $query = RptasMappingNoMapPinView::select('barangay')
                                ->distinct()
                                ->get();

        return $query;
    }

    // public function getMappingCancelledPinHaveMapBrgyno(){    

    //     $query = RptasMappingCancelledPinHaveMap::select('barangay')
    //                             ->distinct()
    //                             ->get();

    //     return $query;
    // }

    
    public function getCancelledPinHaveMap(){
         
        $query = RptasMappingCancelledPinHaveMap::select('pin')
                            ->distinct()
                            ->get();
        
        
        return $query;
     }

    public function postNoPinMap(Request $request){

        return self::noMapList($request->barangay);

     }


// ----- Not-Active Mapping Module ----- //

    public function postNotActiveSinglePolygon(Request $request){

        // $pin = substr($request['pin'],0,18);
        $pin = $request['pin'];
        $data = RptasNotActivePinMapView::select('pin','gmap_polygon', 'barangay', 'center_coords')->where('pin', $pin)->first();


        if($data){

            $data['pin'] = $pin;
            $data['gmap_polygon'] = str_replace("|", ",", $data['gmap_polygon']);
            $data['gmap_polygon'] = str_replace("\\", "", $data['gmap_polygon']);    
            
            $data['pin'] = $pin;
            $data['gmap_polygon'] = str_replace("|", ",", $data['gmap_polygon']);
            $data['gmap_polygon'] = str_replace("\\", "", $data['gmap_polygon']);

            $all_barangay = RptasNotActivePinMapView::select('pin', 'gmap_polygon')
                            ->where('barangay', $data['barangay'])
                            ->where('pin', '!=', $pin)
                            ->get();

            $data['all_barangay'] = self::parseBarangay($all_barangay);
            
            $data['bgy_not_in_list'] =  self::getNoMapNotInList($data['barangay']);

            $queryUpdatedARP = RptasActiveArpAllDetails::select('ARP','PIN' )
                                ->where('PIN', $request->pin)
                                ->orderBy('ARP', 'desc')
                                ->first();

            if ($queryUpdatedARP !== null){
                     //select the master data of the current arp
                $queryMasterTaxdec = RptasActiveArpAllDetails::select('ARP','PIN','Owner','OwnerAddress',
                                    'Location','BldgLocation','Barangay', 'MuniDistName',
                                    'LotNo', 'BlkNo', 'TCTNo', 'CCTNO',
                                    'kind', 'BldgStorey', 'BldgTypeDesc',
                                    'StrucType','SubClass')
                                    ->where('ARP', $queryUpdatedARP['ARP'])
                                    ->first();

                
                //select the taxdecasssmnt of the current arp
                $taxdecAssmnt = [];

                $queryTaxdecAssmnt = RptasTaxdecAssmnt::select('kind','AUDesc', 'area',
                                            'MV','AL', 'AV','Taxability')
                                    ->where('ARP', $queryUpdatedARP['ARP'])
                                    ->get()
                                    ->toArray();

                    foreach($queryTaxdecAssmnt as $key => $value){

                        $taxdecAssmnt[] = array(
                                
                            'kind'                  => self::getKindType($value['kind']),
                            'AUDesc'                => $value['AUDesc'],
                            'area'                  => number_format($value['area'],2,".",","). ".sqm",
                            'marketValue'           => "₱ " . number_format($value['MV'],2,".",","),
                            'assessedLevel'         => number_format($value['AL']).' %',
                            'assessedValue'         => "₱ " .number_format($value['AV'],2,".",","),
                            'taxability'            => self::getTaxable($value['Taxability']),
            
                        );
                    }
            }
            else {
                $queryMasterTaxdec = [];
                $taxdecAssmnt = [];
            }
        
        
        
        }

        $mapping = $request['mapping'];

        $data['bgy_polygon'] = [];
        // Get all unused barangay
        if(isset($data['barangay'])){
            if($mapping == 1){
                $unused_bgy = RptasNotActivePinBgyPolygonsView::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$data['barangay'])->get();
            }
            else{
                $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$data['barangay'])->get();
            }
           
            $data['bgy_polygon'] = self::parseUnusedBarangay($unused_bgy);
        }

        $data['masterTaxdec'] =  $queryMasterTaxdec;
        $data['taxdecAssmnt'] =  $taxdecAssmnt;
       

        return $data;
    }

    

    public function postNotActiveSinglePolygonNoSelection(Request $request){


        $bgy = $request['bgy'];
        
       

        $brgyNo = str_pad($bgy,3,'0',STR_PAD_LEFT);

        
            $pin = RptasNotActivePinMapView::select('pin')
                    ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                    ->first();

     
        
        // return $pin;


        $request = new \Illuminate\Http\Request();
        $request->replace(['pin' => $pin['pin']]);
        return self::postNotActiveSinglePolygon($request);
        
  
    }

    // cancelled polygon and rptas
    public function postCancelledSinglePolygon(Request $request){
        
        //di pa ako tapos sau

        //find the parcel polygon and barangays
        $data = MapPolygon::select('gmap_polygon', 'barangay', 'center_coords')
                    ->where('pin', $request->pin)->first();
        
        
            if($data){

                $data['gmap_polygon'] = str_replace("|", ",", $data['gmap_polygon']);
                $data['gmap_polygon'] = str_replace("\\", "", $data['gmap_polygon']);

                $all_barangay = MapPolygon::select('pin', 'gmap_polygon')
                                ->where('barangay', $data['barangay'])
                                ->where('pin', '!=', $request->pin)
                                ->get();

                $data['all_barangay'] = self::parseBarangay($all_barangay);

            }
        //find the current arp of the pin
        $queryUpdatedARP = RptasTaxdecMastMla::select('ARP','PIN' )
                                ->where('PIN', $request->pin)
                                ->orderBy('ARP', 'desc')
                                ->first();

            if ($queryUpdatedARP !== null){
                     //select the master data of the current arp
                $queryMasterTaxdec = RptasTaxdecMastMla::select('ARP','PIN','Owner','OwnerAddress',
                                    'Location','BldgLocation','Barangay', 'MuniDistName',
                                    'LotNo', 'BlkNo', 'TCTNo', 'CCTNO',
                                    'kind', 'BldgStorey', 'BldgTypeDesc',
                                    'StrucType','SubClass')
                                    ->where('ARP', $queryUpdatedARP['ARP'])
                                    ->first();

                
                //select the taxdecasssmnt of the current arp
                $taxdecAssmnt = [];

                $queryTaxdecAssmnt = RptasTaxdecAssmnt::select('kind','AUDesc', 'area',
                                            'MV','AL', 'AV','Taxability')
                                    ->where('ARP', $queryUpdatedARP['ARP'])
                                    ->get()
                                    ->toArray();

                    foreach($queryTaxdecAssmnt as $key => $value){

                        $taxdecAssmnt[] = array(
                                
                            'kind'                  => self::getKindType($value['kind']),
                            'AUDesc'                => $value['AUDesc'],
                            'area'                  => number_format($value['area'],2,".",","). ".sqm",
                            'marketValue'           => "₱ " . number_format($value['MV'],2,".",","),
                            'assessedLevel'         => number_format($value['AL']).' %',
                            'assessedValue'         => "₱ " .number_format($value['AV'],2,".",","),
                            'taxability'            => self::getTaxable($value['Taxability']),
            
                        );
                    }
            }
            else {
                $queryMasterTaxdec = [];
                $taxdecAssmnt = [];
            }
        
        
        $queryBuildingMachinery = RptasTaxdecMastMla::select('PIN')
                                    ->where('PIN', 'like',  $request->pin.'%')
                                    ->whereRaw('LENGTH(PIN) > 18')
                                    ->distinct('PIN')
                                    ->orderBy('PIN', 'ASC')
                                    ->get()
                                    ->toArray();
        
                                    
        
        $building = [];
        $machinery = [];

            foreach ($queryBuildingMachinery as $bm_data){
                if (strpos($bm_data['PIN'],'-B') !== false){
                    $building[] = $bm_data['PIN'];
                }
                else if (strpos($bm_data['PIN'],'-M') !== false){
                    $machinery[] = $bm_data['PIN'];
                }
            }

        $data['building_list'] = $building;
        $data['machinery_list'] = $machinery;
        
        
        $pictures = self::getPicturesUrl($request->pin);

        $mapping = $request['mapping'];
        
        $data['bgy_polygon'] = [];

        // Get all unused barangay
        if(isset($data['barangay'])){

            $district = BgyPolygon::select('district')
                            ->where('bgy', (int)$data['barangay'])
                            ->first();


            if($mapping == 1){
                $unused_bgy = MappingBgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$data['barangay'])
                                ->where('district', $district['district'])
                                ->get();
            } else{
                $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')
                                ->where('bgy', '!=', (int)$data['barangay'])
                                ->where('district', $district['district'])
                                ->get();
            }

            $unused_district = DistrictPolygon::select('district', 'polygon')
                                ->where('district', '!=', $district['district'])
                                ->get();
           
            $data['bgy_polygon'] = self::parseUnusedBarangay($unused_bgy);
            $data['dist_polygon'] = self::parseUnusedDistrict($unused_district);
            $data['district'] = $district;
        }

       $data['masterTaxdec'] =  $queryMasterTaxdec;
       $data['taxdecAssmnt'] =  $taxdecAssmnt;
       $data['pictures']    =   $pictures;

       
       return $data;
        
    }

    public function getPicturesUrl($pin){
        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data["BLDG IMAGE"];

        $list = [];

        foreach($path as $paths){
            if(gettype($paths) == 'string'){
                if(strpos($paths, $pin) !== false){
                    $list[] = $paths;
                }
            }
        }

        if(count($list)){

            sort($list, 4);
            
            return array(
                "listCount" => count($list),
                "list"      => $list,
            );
        }

        $list[] = "imagenf.jpg";

        return array(
            "listCount" => count($list),
            "list"      => $list,
        );
    }



    private function parseBarangay($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'pin'   => $value['pin'],
                'path'  => str_replace("|", ",", $value['gmap_polygon'])
            );

            // if($key == 0){
            //     $str = str_replace("|", ",", $value['gmap_polygon']);
            // } else{
            //     $str = $str . "," . str_replace("|", ",", $value['gmap_polygon']);
            // }
        }
        return $data_arr;
        return $str;

    }

    private function parseUnusedBarangay($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'bgy'       => $value['bgy'],
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function parseUnusedDistrict($data){

        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'path'      => str_replace("|", ",", $value['polygon']),
                'district'  => $value['district']
            );
        }
        return $data_arr;
    }

    private function getKindType($type){

        switch ($type) {
            case "L":
                return "Land";
                break;
            case "B":
                return "Building";
                break;
            case "M":
                return "Machinery";
                break;
            default:
                return null;
        }

    }

    private function getTaxable($tax) {
        if ($tax === 'T') {
            return 'Taxable';
        }
        else {
            return  'Exempted';
        }
    }

    private function parseRoad($data){

        $str = "";
        $data_arr = [];

        foreach ($data as $key => $value) {

            $data_arr[] = array(
                'name'   => $value['road'],
                'path'   => json_decode(str_replace("|", ",", $value['polygon']))
            );

        }

        return $data_arr;
    }

    public function selectBgyBoundaries(Request $request){
        // return $request;


        if($request->sel == "1" && $request->mapping == "0"){
            $unused_bgy = BgyPolygon::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$request->bgy)->get();
            return self::parseUnusedBarangay($unused_bgy);
        } else if ($request->sel == "1" && $request->mapping == "1") {
            $unused_bgy = RptasNotActivePinBgyPolygonsView::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$request->bgy)->get();
            return self::parseUnusedBarangay($unused_bgy);
        }
        else if ($request->sel == "2" && $request->mapping == "0") {
            $unused_bgy = RptasMappingPoliticalBoundariesView::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$request->bgy)->get();
            return self::parseUnusedBarangay($unused_bgy);
        }
        else {
            $unused_bgy = RptasNotActivePinPoliticalBoundariesView::select('bgy', 'polygon','district')->where('bgy', '!=', (int)$request->bgy)->get();
            return self::parseUnusedBarangay($unused_bgy);
        }

       
    }

    private function getNoMapNotInList($bgy){

        $list = self::noMapList($bgy)->toArray();

        $data = MapPolygon::select('pin', 'gmap_polygon')
                            ->where('barangay', $bgy)
                            ->whereNotIn('pin', array_values($list))
                            ->orderBy('pin')
                            ->get();

        return self::parseBarangay($data);
    }

    private function noMapList($bgy){

        return RptasNotActivePinMapView::select('pin')
                                    ->where('barangay', $bgy)
                                    ->distinct()
                                    ->get();
    }

    
}
