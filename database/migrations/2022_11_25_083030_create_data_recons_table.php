<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataReconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_recons', function (Blueprint $table) {
            $table->id();
            $table->string('td_no')->nullable();
            $table->string("arp")->nullable();
            $table->string("owner")->nullable();
            $table->string("location")->nullable();
            $table->string("PIN");
            $table->string("lot")->nullable();
            $table->string("block")->nullable();

            $table->string("lr_au")->nullable();
            $table->string("lr_aud")->nullable();
            $table->decimal("lr_area",20,2)->nullable();
            $table->decimal("lr_mv",20,2)->nullable();
            $table->decimal("lr_av",20,2)->nullable();

            $table->string("lc_au")->nullable();
            $table->string("lc_aud")->nullable();
            $table->decimal("lc_area",20,2)->nullable();
            $table->decimal("lc_mv",20,2)->nullable();
            $table->decimal("lc_av",20,2)->nullable();

            $table->string("ir_au")->nullable();
            $table->string("ir_aud")->nullable();
            $table->decimal("ir_area",20,2)->nullable();
            $table->decimal("ir_mv",20,2)->nullable();
            $table->decimal("ir_av",20,2)->nullable();

            $table->string("ic_au")->nullable();
            $table->string("ic_aud")->nullable();
            $table->decimal("ic_area",20,2)->nullable();
            $table->decimal("ic_mv",20,2)->nullable();
            $table->decimal("ic_av",20,2)->nullable();

            $table->string("m_au")->nullable();
            $table->string("m_aud")->nullable();
            $table->decimal("m_area",20,2)->nullable();
            $table->decimal("m_mv",20,2)->nullable();
            $table->decimal("m_av",20,2)->nullable();

            $table->string("s_au")->nullable();
            $table->string("s_aud")->nullable();
            $table->decimal("s_area",20,2)->nullable();
            $table->decimal("s_mv",20,2)->nullable();
            $table->decimal("s_av",20,2)->nullable();

            $table->string("pvr_au")->nullable();
            $table->string("pvr_aud")->nullable();
            $table->decimal("pvr_av",20,2)->nullable();

            $table->string("pvc_au")->nullable();
            $table->string("pvc_aud")->nullable();
            $table->decimal("pvc_av",20,2)->nullable();
            
            $table->string("pvm_au")->nullable();
            $table->string("pvm_aud")->nullable();
            $table->decimal("pvm_av",20,2)->nullable();

            $table->string("pvs_au")->nullable();
            $table->string("pvs_aud")->nullable();
            $table->decimal("pvs_av",20,2)->nullable();

            $table->string("prev_arp")->nullable();
            $table->string("memoranda")->nullable();

            $table->string("status");
            $table->string("status_desc");
            $table->string("taxability");


            $table->timestamps();


            // $table->string("current_arp")->nullable();
            // $table->integer("assessment_level");
            // $table->decimal("currentAssessValue",20,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_recons');
    }
}
