<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterReleaseTaxDecsTable5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_tax_decs', function (Blueprint $table) {
            $table->string('amount')->nullable()->after('or_no');
            $table->date('or_date')->nullable()->after('or_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_tax_decs', function (Blueprint $table) {
            //
        });
    }
}
