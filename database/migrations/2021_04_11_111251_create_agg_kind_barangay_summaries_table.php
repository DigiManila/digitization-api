<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAggKindBarangaySummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agg_kind_barangay_summaries', function (Blueprint $table) {
            $table->id();
            $table->string('brgyNo');
            $table->string('kind');
            $table->decimal('area',20,2)->default(0.00);
            $table->decimal('market_value',20,2)->default(0.00);
            $table->decimal('CurrentAssessValue',20,2)->default(0.00);
            $table->decimal('PreviousAssessValue',20,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agg_kind_barangay_summaries');
    }
}
