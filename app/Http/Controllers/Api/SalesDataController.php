<?php

namespace App\Http\Controllers\Api;

use App\ZoneDistrict;
use App\AssessmentRoll;
use App\SalesData;
use App\MapPolygon;
use App\AllActivePin;
use App\SalesDataLatest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SalesDataController extends Controller
{

    // public function getZone(){
    //     $data = ZoneDistrict::select('zone')
    //                         ->distinct('zone')
    //                         ->pluck('zone');

    //     return $data;
    // }

    public function getBarangay(){
        $data = ZoneDistrict::select('barangay')
                            ->orderBy('barangay')
                            ->pluck('barangay');

        return $data;
    }

    public function getAssessmentRoll(Request $request){
        
        $bgy = $request['barangay'];

        // $get_barangay = ZoneDistrict::select('barangay')
        //                             ->where('zone', $zone)
        //                             ->pluck('barangay');

        $query = AllActivePin::select('pin','Owner', 'kind')
                        // ->whereIn('brgy', $get_barangay)
                        ->where('brgy', $bgy)
                        ->whereRaw('LENGTH(pin) = ?', [18])
                        ->orderBy('pin')
                        ->get();

        foreach ($query as $key => $value) {
            if(isset($sale_assessment[$value['pin']])){
                $sale_assessment[$value['pin']]['kind'] = $sale_assessment[$value['pin']]['kind'] . ", " . $value['kind'];
            } else{
                $sale_assessment[$value['pin']] = $value;
                $sale_assessment[$value['pin']]['checkedItems'] = false;
            }
        }
                        
        // foreach($query as $key => $value){
        //     $sale_assessment[] = array(
        //         'pin'   => $value['pin'],
        //         'owner_name' => $value['Owner'],
        //         'market_value' => $value['MV'],
        //         'checkedItems' => false,
        //     );
        // }

        return array_values($sale_assessment);
    }

    public function getSelected(Request $request){

        $data = $request['data'][0];

        if($data){

            $curr_arp = AllActivePin::select('Owner', 'area', 'MV', 'LotNo', 'BlkNo', 'Prev_Owner', 'SubClass', 'Location')
                                        ->where('pin', $data['pin'])
                                        ->first();

            $bldg_mach = AllActivePin::select('pin', 'MV', 'area', 'kind')
                                        ->where('pin', 'like', $data['pin'] . '-%')
                                        ->get();
                                        
            

            foreach ($bldg_mach as $key => $value) {
                $bldg_mach[$key]['id'] = $key;
            }

            $curr_arp['SubClass']   = str_replace("-", "", $curr_arp['SubClass']);
            $curr_arp['bldg_mach']  = $bldg_mach;


            // $details['seller']  = $prev_owner;
            // $details['buyer']   = $curr_arp['owner_name'];
            // $details['area']    = $curr_arp['area'];
            // // $details['area']    = $data['area'];
            // $details['mv']      = number_format($data['market_value'],2,'.',',');


            return $curr_arp;
            return $details;

        }

        return [];

    }

    public function saveData(Request $request){
        
        $zone   = ZoneDistrict::select('zone')->where('barangay', $request['bgy'])->pluck('zone')->first();
        $class  = self::getKind($request['pinobj'][0]['kind']);
        $coords = MapPolygon::select('gmap_polygon', 'center_coords')->where('pin', $request['pinobj'][0]['pin'])->first();

        $data = array(
            'pin'               => $request['pinobj'][0]['pin'],
            'pin_display'       => $request['pinobj'][0]['pin'], 
            'brgy'              => $request['bgy'],
            'zone'              => $zone,
            'date_inst'         => $request['form']['date_of_instrument'],
            'seller'            => strtoupper($request['form']['seller_name']),
            'buyer'             => strtoupper($request['form']['buyer_name']),
            'location'          => $request['form']['location'],
            'lot'               => $request['form']['lot'],
            'block'             => $request['form']['block'],
            'area'              => $request['form']['area'],
            'selling_price'     => $request['selling'],
            'addt_improvements' => $request['addt'],
            'unit_value'        => $request['form']['subclass'],
            'sale_sq_m'         => $request['salesqm'],
            'mv_imp'            => $request['mvImp'],
            'class'             => $class,
            'folder'            => "",
            'center_coords'     => $coords['center_coords'],
            'lat_lng'           => $coords['gmap_polygon'],
            'status'            => $request['status'],
        );

        $status = SalesData::create($data);

        if($status){
            return response()->json([
                "data" => [],
                'message' => "Data Saved",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                "data" => [],
                'message' => "Saving Failed. Please contant admin",
                'status' => 0
            ], 200);
        }

    }

    public function getBarangaySalesData(){

        $brgyNo = [];

        $data = SalesDataLatest::selectRaw("distinct substring_index(substring_index(`sales_data`.`pin`,'-',3),'-',-(1)) AS `brgyNo`")
                            ->where('pin', '<>', null)
                            ->orderBy('brgyNo')
                            ->get();

        foreach($data as $key => $value){
            $brgyNo [] = $value['brgyNo'];
        }

        return $brgyNo;

        // select distinct substring_index(substring_index(`mapping_active_map`.`pin`,'-',3),'-',-(1)) AS `brgyNo` from `mapping_active_map` order by `brgyNo`
    }

    public function getActiveSalesMap(Request $request){

        $brgyNo = $request['brgyNo'];
        $active_map = [];

        $query = SalesData::select('pin','pin_display','seller','buyer')
                            ->whereRaw('SUBSTRING(pin,8,3) = ?',  $brgyNo )
                            ->get();
            
            foreach ($query as $key => $value) {
                $active_map [] = array(
                    'pin'                   =>      $value['pin'],
                    'pin_display'           =>      $value['pin_display'],
                    'seller'                =>      $value['seller'],
                    'buyer'                 =>      $value['buyer']
                );
            }
        
        
        return $active_map;
    }

    public function getDataSalesMap(Request $request){

        $pin = $request['pin'];

        $data = SalesData::where('pin', $pin)
                            ->first();
        $polygon = MapPolygon::where('pin', $pin)
                            ->first();

        if($polygon['polygon']){
            $polygon['gmap_polygon'] = str_replace("|", ",", $polygon['gmap_polygon']);
        }

        return compact('data','polygon');
        
    }

    public function importSalesDataExcel(){

        // $ctr = 3;
        $data = [];
        $cancelled_pins = [];

        $app_path = "app" . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "Sales Data.xls";
        $inputFileName = storage_path($app_path);

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);

        $worksheet  = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow() - 1;

        for ($ctr = 3; $ctr <= $highestRow; $ctr+=2) {
            
            $pin                = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(1, $ctr)->getValue();
            // \Log::info($pin);
            $pin_display        = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(2, $ctr)->getValue();
            $date_inst          = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(3, $ctr)->getValue();
            $seller             = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $ctr)->getValue();
            $buyer              = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $ctr + 1)->getValue();
            $location           = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $ctr)->getValue();
            $lot                = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(7, $ctr)->getValue();
            $block              = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(8, $ctr)->getValue();
            $area               = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(9, $ctr)->getValue();
            $selling_price      = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(10, $ctr)->getValue();
            $unit_value         = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(11, $ctr)->getValue();
            $sale_sq_m          = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(12, $ctr)->getValue();
            $mv_imp             = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(13, $ctr)->getValue();
            $class              = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(14, $ctr)->getValue();

            $new_date_inst      = null;

            if($date_inst){
                $new_date_inst = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date_inst);
            }

            $data[] = array(
                'pin'           => $pin,
                'pin_display'   => $pin_display, 
                'date_inst'     => $date_inst ? $new_date_inst->format("Y-m-d") : null,
                'seller'        => $seller,
                'buyer'         => $buyer,
                'location'      => $location,
                'lot'           => $lot,
                'block'         => $block,
                'area'          => $area,
                'selling_price' => $selling_price,
                'unit_value'    => $unit_value,
                'sale_sq_m'     => $sale_sq_m,
                'mv_imp'        => $mv_imp,
                'class'         => $class,
            );
        }

        $chunk_count = 500;

        foreach (array_chunk($data,$chunk_count) as $chunk){
            $count = SalesData::insert($chunk);
        }

        return "OK";
    }


    // -------------------------------------------------------------------------- 
    
    public function getSalesDataDashboard(){

        $data = SalesDataLatest::selectRaw('COUNT(*) AS total_records, SUM(selling_price) AS total_selling_price, SUM(sale_sq_m) AS total_sale_sqm')
                        ->first();

        // $data = SalesData::selectRaw('COUNT(*) AS total_records, SUM(selling_price) AS total_selling_price, SUM(sale_sq_m) AS total_sale_sqm')
        //         ->first();

        $data['total_selling_price']    = number_format($data['total_selling_price'],2,'.',',');
        $data['total_sale_sqm']         = number_format($data['total_sale_sqm'],2,'.',',');
        $data['total_records']          = (string) $data['total_records'];

        $table_data = SalesDataLatest::selectRaw('brgy AS `brgyNo`,count(0) AS `total_count`,sum(`sales_data_all`.`selling_price`) AS `selling_price`,sum(`sales_data_all`.`sale_sq_m`) AS `sale_sq_m`')
                        ->groupBy('brgyNo')
                        ->orderBy('brgyNo')
                        ->get();

        foreach($table_data as $key => $value){
            $table_data[$key]['selling_price']  = number_format($value['selling_price'],2,".",",");
            $table_data[$key]['sale_sq_m']      = number_format($value['sale_sq_m'],2,".",",");
        }

        $data['table_record'] = $table_data;

        return $data;

    }

    public function getBarangaySalesDataDashboard(Request $request){

        $data = SalesDataLatest::selectRaw('class, count(0) AS total_count, sum(selling_price) AS selling_price, sum(sale_sq_m) AS sale_sq_m')
                            ->where('brgy', $request['brgyNo'])
                            ->groupBy('class')
                            ->orderBy('class')
                            ->get();

        foreach ($data as $key => $value) {

            if($value['class'] == '0'){
                $data[$key]['class'] = "UNCLASSIFIED";
            }

            $data[$key]['icon'] = self::classifyIcon($value['class']);
            $data[$key]['selling_price']  = number_format($value['selling_price'],2,".",",");
            $data[$key]['sale_sq_m']      = number_format($value['sale_sq_m'],2,".",",");


        }

        return $data;
    }

    public function getBarangayPinSalesDataDashboard(Request $request){

        $data = SalesDataLatest::select('pin', 'buyer', 'seller', 'location', 'area', 'selling_price', 'sale_sq_m', 'class', 'date_inst')
                            ->where('brgy', $request['brgyNo'])
                            ->where('class', $request['kind'])
                            ->get();

        foreach($data as $key => $value){
            $data[$key]['class']            = ucwords($value['class']);
            $data[$key]['date_inst']        = date("F j, Y",strtotime($value['date_inst']));
            $data[$key]['selling_price']    = number_format($value['selling_price'],2,".",",");
            $data[$key]['sale_sq_m']        = number_format($value['sale_sq_m'],2,".",",");
        }

        return $data;

    }

    public function exportReport(Request $request){

        // $from   = $request['dates'][0] . " 00:00:00";
        // $to     = $request['dates'][1] . " 23:59:59";

        $data   = SalesDataLatest::select('pin', 'brgy', 'zone', 'date_inst', 'seller', 'buyer', 'location', 'lot', 'block', 'area', 'selling_price', 'unit_value', 'sale_sq_m', 'addt_improvements', 'mv_imp', 'class')
                    // ->whereBetween('created_at', [$from, $to])
                    ->get()
                    ->toArray();

        $getZone    = self::groupZoneBrgy();

        if(count($data)){

            sort($data);

            $by_zone        = [];
            $merge_output   = [];

            foreach ($data as $key => $value) {

                $by_zone[$value['zone']][] = $value;
                
                // if(isset($by_zone[$value['zone']])){
                //     $by_zone[$value['zone']][] = $value;
                // } else{
                //     $by_zone[$value['zone']][] = $value;
                // }

            }

            foreach ($by_zone as $key => $value) {

                $merge_output[$key] = array(
                    'list' => $value,
                    'name' => $getZone[$key]
                );

                // \Log::info($key);

            }

            return self::createReport($merge_output);

            return response()->json([
                "data" => [],
                'message' => "Report Downloading",
                'status' => 1
            ], 200);
        } else{

            return "No Data";

            return response()->json([
                "data" => [],
                'message' => "No report to extract",
                'status' => 0
            ], 200);
        }

    }

    // --------------------------------------------------------------------------


    public function getSalesDataTagging(){

        $data = SalesData::select('sales_data.id','pin_display', 'buyer','rptas_taxdec_mast_mla.Owner')
                            ->leftJoin('sales_active_pins', 'sales_data.pin_display', '=', 'sales_active_pins.pin')
                            ->leftJoin('rptas_taxdec_mast_mla', 'sales_active_pins.arp', '=', 'rptas_taxdec_mast_mla.ARP')
                            ->where('status', 0)
                            ->get();

        return $data;

    }

    public function updateSalesData(Request $request){

        $data = $request['data'];

        $sales = SalesData::find($data['id']);
        $sales->status = 1;
        $status = $sales->save();
        
        if($status){
            return response()->json([
                "data" => [],
                'message' => "Data Saved",
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                "data" => [],
                'message' => "Saving Failed. Please contant admin",
                'status' => 0
            ], 200);
        }


    }

    // --------------------------------------------------------------------------

    private function classifyIcon($class){
        
        switch ($class) {
            case 'COMMERCIAL':
                return "mdi-office-building";
                break;
            case 'RESIDENTIAL':
                return "mdi-home-group";
                break;
            case 'RESCOM':
                return "mdi-home-city-outline";
                break;
            case 'UNCLASSIFIED':
                return "mdi-chart-scatter-plot";
                break;
            
            default:
                return "Error";
                break;
        }

    }

    private function getKind($val){

        if(strpos($val, 'COMM') !== false && strpos($val, 'RESI') !== false){
            return 'R/C';
        } elseif(strpos($val, 'COMM') !== false){
            return 'COM';
        } elseif(strpos($val, 'RESI') !== false){
            return 'RES';
        } else{
            return '0';
        }

    }

    private function groupZoneBrgy(){

        $output = [];

        $data = ZoneDistrict::select(\DB::raw("zone, MIN(barangay) AS minimum, MAX(barangay) AS maximum"))
                            ->groupBy('zone')
                            ->get()
                            ->toArray();

        foreach ($data as $key => $value) {
            $output[$value['zone']] = $value;
        }

        return $output;

    }

    // ---------------------------------- REPORT -------------------------                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                //

    private function createReport($data){

        ksort($data);

        $spreadsheet    = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
       
        foreach ($data as $key => $value) {

            $name_array = $value['name'];
            $sheet_name = "Z-" . $name_array['zone'] . " B" . (int)$name_array['minimum'] . "-" . (int)$name_array['maximum'];

            if($key == 1){
                $worksheet = $spreadsheet->getActiveSheet();
                $worksheet->setTitle($sheet_name);
                $spreadsheet->getSheetByName($sheet_name);
                $worksheet = $spreadsheet->getActiveSheet();
            } else{
                $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $sheet_name);
                $spreadsheet->addSheet($myWorkSheet);
                // $spreadsheet->getSheet($spreadsheet->getSheetCount() - 1);
                $worksheet = $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);
            }

            $worksheet->getColumnDimension('A')->setWidth(25);
            $worksheet->getColumnDimension('B')->setWidth(14);
            $worksheet->getColumnDimension('C')->setWidth(2.14);
            $worksheet->getColumnDimension('D')->setWidth(35);
            $worksheet->getColumnDimension('E')->setWidth(25);
            $worksheet->getColumnDimension('F')->setWidth(10);
            $worksheet->getColumnDimension('G')->setWidth(10);
            $worksheet->getColumnDimension('H')->setWidth(12);
            $worksheet->getColumnDimension('I')->setWidth(18);
            $worksheet->getColumnDimension('J')->setWidth(15);
            $worksheet->getColumnDimension('K')->setWidth(18);
            $worksheet->getColumnDimension('L')->setWidth(17);
            $worksheet->getColumnDimension('M')->setWidth(10);
    
            $worksheet->mergeCells('A4:A5');
            $worksheet->mergeCells('B4:B5');
            $worksheet->mergeCells('C4:D5');
            $worksheet->mergeCells('E4:E5');
            $worksheet->mergeCells('F4:F5');
            $worksheet->mergeCells('G4:G5');
            $worksheet->mergeCells('H4:H5');
            $worksheet->mergeCells('I4:I5');
            $worksheet->mergeCells('J4:J5');
            $worksheet->mergeCells('K4:K5');
            $worksheet->mergeCells('L4:L5');
            $worksheet->mergeCells('M4:M5');
    
            $worksheet
                    ->setCellValue("A4", "PIN No.")
                    ->setCellValue("B4", "DATE OF INSTRUMENT")
                    ->setCellValue("C4", "PARTIES")
                    ->setCellValue("E4", "LOCATION")
                    ->setCellValue("F4", "LOT No")
                    ->setCellValue("G4", "BLK No")
                    ->setCellValue("H4", "AREA")
                    ->setCellValue("I4", "SELLING PRICE")
                    ->setCellValue("J4", "UNIT VALUE")
                    ->setCellValue("K4", "SALE/SQ.M")
                    ->setCellValue("L4", "MV of IMP")
                    ->setCellValue("M4", "CLASS");
            $worksheet->getStyle("A4:M4")->getFont()->setBold(true);
            $worksheet->getStyle('A4:M4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A4:M4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $worksheet->getStyle('A4:M4')->getAlignment()->setWrapText(true);
    
            $worksheet->setCellValue("A1", "ZONE NO.:");
            $worksheet->getStyle("A1:B1")->getFont()->setBold(true);

            $worksheet->getStyle('B1')
                      ->getBorders()
                      ->getBottom()
                      ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

            $worksheet->setCellValue("B1", $name_array['zone']);

            $worksheet->mergeCells('A2:D2');
            $worksheet->setCellValue("A2", "REPORT DATE:  " . date("F j, Y"));
            $worksheet->getStyle("A2")->getFont()->setBold(true);

            $startRow   = 6;
            $row        = 6;
            
            foreach ($value['list'] as $key => $val) {

                $merge_cell_text = "A{$row}:A" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("A{$row}", $val["pin"]);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setWrapText(true);

                $merge_cell_text = "B{$row}:B" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("B{$row}", date("m/d/Y", strtotime($val['date_inst'])));
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setWrapText(true);

                $worksheet->setCellValue("C{$row}", "S");
                $worksheet->getStyle("C{$row}")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("C{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $worksheet->setCellValue("D{$row}", $val['seller']);
                $worksheet->getStyle("D{$row}")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("D{$row}")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $worksheet->setCellValue("C" . ($row + 1), "B");
                $worksheet->getStyle("C" . ($row + 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("C" . ($row + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                
                $worksheet->setCellValue("D" . ($row + 1), $val['buyer']);
                $worksheet->getStyle("D" . ($row + 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle("D" . ($row + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);                

                $merge_cell_text = "E{$row}:E" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("E{$row}", $val['location']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setWrapText(true);

                $merge_cell_text = "F{$row}:F" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("F{$row}", $val['lot']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setWrapText(true);

                $merge_cell_text = "G{$row}:G" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("G{$row}", $val['block']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setWrapText(true);

                $merge_cell_text = "H{$row}:H" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("H{$row}", $val['area']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $merge_cell_text = "I{$row}:I" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("I{$row}", $val['selling_price']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $merge_cell_text = "J{$row}:J" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("J{$row}", is_numeric($val['unit_value']) ?  number_format($val['unit_value'],2,'.',',') : $val['unit_value']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $merge_cell_text = "K{$row}:K" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("K{$row}", is_numeric($val['sale_sq_m']) ?  number_format($val['sale_sq_m'],2,'.',',') : $val['sale_sq_m']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $merge_cell_text = "L{$row}:L" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("L{$row}", is_numeric($val['mv_imp']) ?  number_format($val['mv_imp'],2,'.',',') : $val['mv_imp']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $merge_cell_text = "M{$row}:M" . ($row + 1);

                $worksheet->mergeCells($merge_cell_text);
                $worksheet->setCellValue("M{$row}", $val['class']);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle($merge_cell_text)->getAlignment()->setWrapText(true);

                $row = $row + 2;
            }

            $endRow = $row - 1;

            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
            ];
            
            $worksheet->getStyle("A4:M{$endRow}")->applyFromArray($styleArray);

            $worksheet->setSelectedCells('A1');

        }

        $worksheet = $spreadsheet->setActiveSheetIndex(0);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $ret = base64_encode(ob_get_contents());
        ob_end_clean();

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $ret;

    }

    public function importExcel(Request $request){

        $output = [];

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($request['excel']);

        $activesheet = $spreadsheet->getSheetByName("final with additional");

        $highestRow     = $activesheet->getHighestRow(); 
        $highestColumn  = "O";

        for ($row = 2; $row <= $highestRow; $row++){

            $rowData = $activesheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE, TRUE);
            $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($rowData[$row]['C']);
            $bgy  = explode("-", $rowData[$row]['O']);

            $class = self::identifyClass($rowData[$row]['N']);

            if(isset($bgy[2])){
                $output[] = array(
                    "pin" => $rowData[$row]['B'],
                    "pin_display" => $rowData[$row]['O'],
                    "brgy" => $bgy[2],
                    "zone" => $rowData[$row]['A'],
                    "date_inst" => date('Y-m-d', $date),
                    "seller" => $rowData[$row]['D'],
                    "buyer" => $rowData[$row]['E'],
                    "location" => $rowData[$row]['F'],
                    "lot" => $rowData[$row]['G'],
                    "block" => $rowData[$row]['H'],
                    "area" => $rowData[$row]['I'],
                    "selling_price" => $rowData[$row]['J'],
                    "unit_value" => $rowData[$row]['K'],
                    "sale_sq_m" => $rowData[$row]['L'],
                    "mv_imp" => $rowData[$row]['M'],
                    "class" => $class,
                    "created_at" => $request['year'] . "-" . date('m')  . "-" . date('d') . " " . date('H:i:s'),
                );
            }

        }

        $chunk_count = 500;
        foreach (array_chunk($output,$chunk_count) as $chunk){
            $status = SalesData::insert($chunk);
        }

        return "OK";

    }

    private function identifyClass($data){

        if(stripos($data, "res") !== false){
            return "RESIDENTIAL";
        } elseif(stripos($data, "com") !== false){
            return "COMMERCIAL";
        } elseif(stripos($data, "/") !== false){
            return "RESCOM";
        } else{
            return $data;
        }
    }

}
