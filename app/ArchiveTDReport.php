<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArchiveTDReport extends Model
{
    use HasFactory;
    protected $table = 'archive_td_report';
}
