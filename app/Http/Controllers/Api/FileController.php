<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use Response;
use Illuminate\Support\Facades\Redis;

use App\TempTaxDeclarations;
use App\TaxDeclaration;
use App\ReleaseTaxDec;

class FileController extends Controller
{

    public function listFiles(Request $request){

        $list = self::getFileList($request['type'], $request['pin']);
        
        $matchingFiles = preg_grep("/" . $request['pin'] . ".*.pdf/", $list);

        rsort($matchingFiles);
        
        $return_list = [];

        foreach ($matchingFiles as $val){

            $exploded_filename = explode('-', $val);
            $timestamp = end($exploded_filename);
            $timestamp = explode('.', $timestamp);

            // \Log::info($timestamp);

            $return_list[] = array(
                'filename' => $val,
                'date'  => date('F j, Y', $timestamp[0])
            );

        }

        return $return_list;

    }

    public function getFile(Request $request){

        $storagePath  = Storage::disk('sftp')->getDriver()->getAdapter()->getPathPrefix();

        $filepath = "";
        
        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        if($request['type'] == 'supporting'){

            // $filepath = "SUPPORTING DOCUMENTS/" . $request['filename'];

            
            $path = $redis_data["SUPPORTING DOCUMENTS"];

            $exp = explode("-", $request['filename']);

            $bgy = (int)$exp[2];
            // $sec = (int)$exp[3];

            foreach ($path as $key => $value) {

                if(strpos($key, "B-$bgy") !== false){
                    $filepath = "SUPPORTING DOCUMENTS/$key/" . $request['filename'];
                    \Log::info($filepath);
                    break;
                }

            }


        } elseif($request['type'] == 'faas'){

        
            $path = $redis_data["FAAS"];

            $exp = explode("-", $request['filename']);

            $bgy = (int)$exp[2];
            $sec = (int)$exp[3];

            foreach ($path as $key => $value) {

                if(strpos($key, "BRGY-$bgy SEC-$sec") !== false){
                    $filepath = "FAAS/$key/" . $request['filename'];
                    break;
                }

            }

        } elseif($request['type'] == 'mapinfo'){

           
            $path = $redis_data["LOCATION MAP"];

            $exp = explode("-", $request['filename']);

            $bgy = $exp[2];
            $sec = $exp[3];

            foreach ($path as $key => $value) {

                if(strpos($value, "BRGY $bgy SEC $sec.jpg") !== false || strpos($value, "BRGY $bgy SEC $sec.JPG") !== false){
                    $filepath = "LOCATION MAP/$value";
                    break;
                }
                

            }
            

        } elseif($request['type'] == 'cancellation'){


            $exp = explode("-", $request['filename']);

            // \Log::info($exp[0]);

            if ($exp[0] == 'C' || $exp[0] == 'D' ){

                $bgy = (int)$exp[2];
            

                $year="2005";
                
                if ($exp[0] == 'D'){
                    $year = "2014";
                }

                $path = $redis_data[$year];
            
                foreach ($path as $key => $value) {
                    // \Log::info($key);
                    if(strpos($key, "B-$bgy") !== false){
                        $filepath = "$year/$key/" . $request['filename'].".pdf";
                        // \Log::info($filepath);
                        break;
                    }

                }

            }

            else {

                $bgy = $exp[0];
                $year = $exp[2];
                $cancellationFolder= "";


                

                if ($year >= 00 || $year <= 25 ){
                    
                    $cancellationFolder = "CANCELLATION "."20".$year;
                }
                else{
                    $cancellationFolder = "CANCELLATION "."19".$year;
                }

            
                $path = $redis_data[$cancellationFolder];

                foreach ($path as $key => $value) {
                
                    if(strpos($key, "BGY $bgy") !== false ){
                        $filepath = "$cancellationFolder/$key/" . $request['filename'].".pdf";
                        break;
                    }
                    // \Log::info($value);

                }

            }

            
            
        }

        elseif($request['type'] == 'tdNo'){

            $exp = explode("-", $request['filename']);

            $bgy = (int)$exp[2];
            

            $year="2005";
            
            if ($exp[0] == 'D'){
                $year = "2014";
            }
            
            $path = $redis_data[$year];
        
            foreach ($path as $key => $value) {
                \Log::info($key);
                if(strpos($key, "B-$bgy") !== false){
                    $filepath = "$year/$key/" . $request['filename'].".pdf";
                    \Log::info($filepath);
                    break;
                }

            }
            
        }


        $file = Storage::disk('sftp')->get($filepath);

        $temp_file_path = tempnam(sys_get_temp_dir(), $request['filename']);
        file_put_contents($temp_file_path, $file);

        $headers = array(
            'Content-Description: File Transfer',
            'Content-Type: application/octet-stream',
            'Content-Disposition: attachment; filename="' . $request['filename'] . '"',
        );

        return response()->download($temp_file_path, $request['filename'], $headers);

    }

    private function getFileList($folder, $pin){

        $param = "";

        if($folder == 'supporting'){
            $param = "SUPPORTING DOCUMENTS";
        } elseif($folder == 'faas'){
            $param = "FAAS";
        }

        // $path = scandir(storage_path('app' . DIRECTORY_SEPARATOR . $folder));

        $redis_data = json_decode(Redis::get('folderStruct'), 1);
        $path = $redis_data[$param];

        if($param == "SUPPORTING DOCUMENTS"){

            $flag = false;

            $exp = explode("-", $pin);

            $bgy = (int)$exp[2];

            foreach ($path as $key => $value) {

                if(strpos($key, "B-$bgy") !== false){
                    $path = $redis_data[$param][$key];
                    $flag = true; 
                    break;
                }

            }

            if(!$flag){
                $path = [];
            }

        }


        elseif($param == "FAAS"){

            $flag = false;

            $exp = explode("-", $pin);

            $bgy = (int)$exp[2];
            $sec = (int)$exp[3];

            foreach ($path as $key => $value) {

                if(strpos($key, "BRGY-$bgy SEC-$sec") !== false){
                    $path = $redis_data[$param][$key];
                    $flag = true; 
                    break;
                }

            }

            if(!$flag){
                $path = [];
            }

        }

        return $path;

        return array_diff($path, array('..', '.'));

    }


    public function getReceiptPreview(Request $request){

        $id = $request['id'];
        
        $path = ReleaseTaxDec::select('or_path')->where('id', $id)->first();

        $pic = Storage::disk('svnas')->get($path['or_path']);

        $exp = explode("/", $path['or_path']);
        $fn = explode(".", end($exp));

        $temp_path = "temp/" . end($exp);
        Storage::disk('local')->put($temp_path, $pic);

        $temp_png = storage_path("app/temp/" .end($exp));

        $image = imagecreatefrompng($temp_png);
        $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
        imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
        imagealphablending($bg, TRUE);
        imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
        imagedestroy($image);
        $quality = 100; // 0 = worst / smaller file, 100 = better / bigger file 
        imagejpeg($bg, storage_path("app/temp/") . $fn[0] . ".jpg", $quality);
        imagedestroy($bg);

        $output = Storage::get("temp/" . $fn[0] . ".jpg");

        Storage::delete("temp/" . end($exp));
        Storage::delete("temp/" . $fn[0] . ".jpg");

        return "data:image/jpg;base64," . base64_encode($output);

    }


    // FOR FILE VIEWER MODULE

    public function getFolderStructure(Request $request){

        $year = $request['year'];

        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        // \Log::info($redis_data);

        $dir = $redis_data[$year];

        $return_list = [];

        

        foreach ($dir as $key => $val){
            $return_list[] = $key;
        }

        
        sort($return_list, 6);

        
        
        return $return_list;

    }

    public function getNASFolderStructure(){

        $file = Storage::disk('sftp2')->lastModified("/1996/BELANDRES,ANTHONY/Z-42 B-409 1996_BELANDRES/409-99-00424.pdf");
        return "OK";
    }

    public function getFilenames(Request $request){

        

        $year = $request['year'];
        $folder = $request['folder'];
    

        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        // if($year == 'SUPPORTING DOCUMENTS'){
        //     // $dir = Storage::disk(config('fsconf.cf'))->files("/$year");
        //     $dir = $redis_data[$year];
        // } else{
        //     $dir = $redis_data[$year][$folder];
        // }


        $dir = $redis_data[$year][$folder];
        \Log::info('dir');
        \Log::info($dir);

        // $dir = Storage::disk(config('fsconf.cf'))->files("/$year/$folder");
      
        $compare_arr = [];
        $filtered_list = [];
        $output = [];

        if($year == 'CONTROL BOOK 2005' || $year == 'CONTROL BOOK 1996' || $year == 'CONTROL COLUMNAR 1996' || $year == 'CONTROL COLUMNAR 1985' ||$year == 'CONTROL BOOK 1985' ){

            foreach ($dir as $val){

                $pos = strpos($val, ".") -1;

                $letter = substr($val, $pos, 1);
                $no     = substr($val, 0, $pos);
                
                $fn = self::getFilenameControlBook($no, $letter);

                if($fn){
                    $output[] = $fn;
                }
    
            }
          

        } elseif($year == 'MANILA RASTER FOLDER' || $year == 'TAX MAP CONTROL ROLL'|| $year == 'TAX MAP CONTROL MAP' || $year == 'CONTROL BOOK 1970'){

            foreach ($dir as $val){

                $exp = explode(".", $val);

                if($exp[1] == 'jpg' || $exp[1] == 'TIF'){
                    $output[] = $val;
                }
    
            }

        } else{
            
            foreach ($dir as $val){

                if($year == 'SUPPORTING DOCUMENTS'){
                    if(!is_array($val)){
                        $file_explode = explode('.', $val);
                    }
                } else{ 
                    $file_explode = explode('.', basename($val));
                }
    
                if(isset($file_explode[1]) && $file_explode[0]){
    
                    if($file_explode[1] == 'jpg'){
                        
                        if($year == 'CONTROL BOOK' || $year == 'CONTROL BOOK 1996'){
                            $name = substr($file_explode[0], 0, -1);
                        } 
                        elseif($year == 'CONTROL BOOK EXEMPT'){
                            $name = substr($file_explode[0], 0);
                        }
                        
                        else{
                            $name = substr($file_explode[0], 1);
                        }
    
                        
                    } elseif($file_explode[1] == 'pdf'){
                        $name = $file_explode[0];
                    }
    
                    $name = $name . "." . $file_explode[1];
    
                    if(!in_array($name, $output)){
                        $output[] = $name;
                    }
                }
    
            }
        }

        if($year == 'CONTROL BOOK 2005' || $year == 'CONTROL BOOK 1996' || $year == 'CONTROL COLUMNAR 1996' || $year == 'CONTROL COLUMNAR 1985' ||$year == 'CONTROL BOOK 1970' || $year == 'CONTROL BOOK 1985' ){
            
            sort($output, 6);
            
        } else {
            \Log::info('output');
            sort($output);
            \Log::info($output);
        }

     

        $out = [];
        $tempEncoderPdf = [];
        $tempProofReadPdf = [];
        $tempVerifierPdf = [];

        if ($year == '1996'){
            // \Log::info("====================================================================\n");
            $exp1 = explode(' ',$folder );
            $exp2 = explode('-', $exp1[1]);
            
            $barangay = sprintf('%03d', $exp2[1]);
           
            /* Status encoder */
            $queryEncoded = TempTaxDeclarations::select('pdf_filename_96')
                        ->where('pdf_filename_96', 'like', $barangay.'-%')
                        ->where('status', 1)
                        ->orWhere('status',2)
                         ->get()->toArray();
            
            foreach($queryEncoded as $key => $val){
                $tempEncoderPdf[] = $val['pdf_filename_96'];
            }

            /* Status proofreader */
            $queryProofRead = TempTaxDeclarations::select('pdf_filename_96')
                         ->where('pdf_filename_96', 'like', $barangay.'-%')
                         ->where('status', 2)
                          ->get()->toArray(); 

            foreach($queryProofRead as $key => $val){
                $tempProofReadPdf[] = $val['pdf_filename_96'];
            }

             /* Status verifier */
             $queryVerifier = TaxDeclaration::select('pdf_filename_96')
             ->where('pdf_filename_96', 'like', $barangay.'-%')
             ->where('status', 3)
              ->get()->toArray(); 

            foreach($queryVerifier as $key => $val){
                $tempVerifierPdf[] = $val['pdf_filename_96'];
            }
            
            foreach($output as $key => $val){

                

                $encoded = "";
                $proofread = "";
                $verified = "";


                $exp = explode(".", $val);
    
                if(in_array($exp[0], $tempEncoderPdf)) {
                    $encoded = "Done";
                }
                else {
                    $encoded = "Open";
                }

                

                if(in_array($exp[0], $tempProofReadPdf)) {
                    $proofread = "Done";
                }
                else {
                    $proofread = "Open";
                }

                if(in_array($exp[0], $tempVerifierPdf)) {
                    $verified = "Done";
                }
                else {
                    $verified = "Open";
                }

                $out[] = array(
                    'td_no' => $exp[0],
                    'encoded' => $encoded,
                    'proofread' => $proofread,
                    'verified' => $verified,
                    'file_type' => $exp[1]
                );

                //\Log::info($out);
            }

            // \Log::info("====================================================================\n");
           
        }
                /*for 1979 and 1985*/
        else if($year == '1979' || $year == '1985'){

            $tempEncoded = [];
            $tempProofRead = [];
            $tempVerified = [];

            if ($year == '1979') {
            $exp = explode(' ',$folder );
            
            $zone = sprintf('%03d', $exp[1]);


            $queryEncoded = TempTaxDeclarations::select('current_arp')
                        ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'A')
                        ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $zone)
                        ->where('owner_name','!=', null)
                        ->where('location','!=', null)
                        ->where('lot','!=', null)
                        ->where('block','!=', null)
                        ->where('status', 1)
                        ->orWhere('status',2)
                        ->get()->toArray();

                foreach($queryEncoded as $key => $val){
                    $tempEncoded[] = $val['current_arp'];
                }

            $queryProofRead = TempTaxDeclarations::select('current_arp')
                        ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'A')
                        ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $zone)
                        ->where('status', 2)
                        ->get()->toArray();

                foreach($queryProofRead as $key => $val){
                    $tempProofRead[] = $val['current_arp'];
                }

            $queryVerified = TaxDeclaration::select('current_arp')
                        ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'A')
                        ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $zone)
                        ->where('status', 3)
                        ->get()->toArray();

                        foreach($queryVerified as $key => $val){
                            $tempVerifier[] = $val['current_arp'];
                        }
            
                foreach($output as $key => $val){

                    $encoded = "";
                    $proofread = "";
                    $verified = "";
    
                    $exp = explode(".", $val);
        
                    if(in_array($exp[0], $tempEncoded)) {
                        $encoded = "Done";
                    }
                    else {
                        $encoded = "Open";
                    }

                    if(in_array($exp[0], $tempProofRead)) {
                        $proofread = "Done";
                    }
                    else {
                        $proofread = "Open";
                    }

                    if(in_array($exp[0], $tempVerified)) {
                        $verified = "Done";
                    }
                    else {
                        $verified = "Open";
                    }
                    
                    // $exp = explode(".", $val);
        
                    $out[] = array(
                        'td_no' => $exp[0],
                        'encoded' => $encoded,
                        'proofread' => $proofread,
                        'verified' => $verified,
                        'file_type' => $exp[1]
                    );
                // \Log::info($temp1979);
                }            
            
            }

            else if ($year == '1985') {
                $exp = explode(' ',$folder );
                
                $zone = sprintf('%03d', $exp[1]);

                $queryEncoded = TempTaxDeclarations::select('current_arp')
                        ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'B')
                        ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $zone)
                        ->where('owner_name','!=', null)
                        ->where('location','!=', null)
                        ->where('lot','!=', null)
                        ->where('block','!=', null)
                        ->where('status', 1)
                        ->orWhere('status',2)
                        ->get()->toArray();

                foreach($queryEncoded as $key => $val){
                    $tempEncoded[] = $val['current_arp'];
                }

            $queryProofRead = TempTaxDeclarations::select('current_arp')
                        ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'B')
                        ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $zone)
                        ->where('status', 2)
                        ->get()->toArray();

                foreach($queryProofRead as $key => $val){
                    $tempProofRead[] = $val['current_arp'];
                }

            $queryVerified = TaxDeclaration::select('current_arp')
                        ->whereRaw('SUBSTRING(current_arp,1,1) = ?',  'B')
                        ->whereRaw('SUBSTRING(current_arp,3,3) = ?',  $zone)
                        ->where('status', 3)
                        ->get()->toArray();

                foreach($queryVerified as $key => $val){
                    $tempVerified[] = $val['current_arp'];
                }
            
                foreach($output as $key => $val){

                    $encoded = "";
                    $proofread = "";
                    $verified = "";
    
                    $exp = explode(".", $val);
        
                    if(in_array($exp[0], $tempEncoded)) {
                        $encoded = "Done";
                    }
                    else {
                        $encoded = "Open";
                    }

                    if(in_array($exp[0], $tempProofRead)) {
                        $proofread = "Done";
                    }
                    else {
                        $proofread = "Open";
                    }

                    if(in_array($exp[0], $tempVerified)) {
                        $verified = "Done";
                    }
                    else {
                        $verified = "Open";
                    }
                    
                    // $exp = explode(".", $val);
        
                    $out[] = array(
                        'td_no' => $exp[0],
                        'encoded' => $encoded,
                        'proofread' => $proofread,
                        'verified' => $verified,
                        'file_type' => $exp[1]
                    );
                }            
            }
            
        } 

        else{
            
            foreach($output as $key => $val){
                
                $exp = explode(".", $val);
    
                $out[] = array(
                    'td_no' => $exp[0],
                    'file_type' => $exp[1]
                );
            }
        } 
        return $out;
    }

    public function getPictureForArchiveRecords(Request $request){

        $fn = $request['filename'];
        $redis_data = json_decode(Redis::get('folderStruct'), 1);

        $yr = "";
        $folder = "";

        $exp = explode("-", $fn);

        switch ($exp[0]) {
            case 'A':
                $yr = "1979";
                break;

            case 'B':
                $yr = "1985";
                break;

            default:
                $yr = "1996";
                break;
        }

        $folder_struct = $redis_data[$yr];

        if($yr == "1979" || $yr == "1985"){
            $folder = "ZONE " . intval($exp[1]);
        } else{
            foreach($folder_struct as $key => $val){
                if(strpos($key, "B-" . intval($exp[0])) !== false){
                    $folder = $key;
                    break;
                }
            }
        }

        $fn_list = $redis_data[$yr][$folder];

        $ret = []; 

        if(in_array($fn . ".pdf", $fn_list)){
            $ret = array([
                'year' => $yr, 
                'folder' => $folder,
                'filename' => array(
                    'td_no' => $fn,
                    'file_type' => 'pdf'
                )
            ]);        
        } else{
            $ret = array([
                'year' => $yr, 
                'folder' => $folder,
                'filename' => array(
                    'td_no' => $fn,
                    'file_type' => 'jpg'
                )
            ]);
        }

        return $ret;

    }

    public function getPictures(Request $request){

        $year   = $request['year'];
        $folder = $request['folder'];
        $fn     = $request['filename'];

        $pic1   = null;
        $pic2   = null;        

        if($year == '1979' || $year == '1985' || $year == '1996' || $year == '2005' || $year == '2014'){
            //Check page 1 if exists
            $page1_filename = '1' . $fn['td_no'] . '.jpg';

            if(Storage::disk(config('fsconf.cf'))->exists("/$year/$folder/$page1_filename")){
                $pic1 = Storage::disk(config('fsconf.cf'))->get("/$year/$folder/$page1_filename");
            } else{
                $pic1 = Storage::disk('local')->get("not_exist.jpg");
            }
    
            //Check page 2 if exists
            $page2_filename = '2' . $fn['td_no'] . '.jpg';
    
            if(Storage::disk(config('fsconf.cf'))->exists("/$year/$folder/$page2_filename")){
                $pic2 = Storage::disk(config('fsconf.cf'))->get("/$year/$folder/$page2_filename");
            } else{
                $pic2 = Storage::disk('local')->get("not_exist.jpg");
            }

        } elseif ($year == 'CONTROL BOOK 2005' || $year == 'CONTROL BOOK 1996'|| $year == 'CONTROL COLUMNAR 1996' || $year == 'CONTROL COLUMNAR 1985' || $year == 'CONTROL BOOK 1985') {

            $create_fn = self::getDisplayFnCb($fn['td_no']);

            //Check page 1 if exists
            $page1_filename = $create_fn[0];

            if(Storage::disk(config('fsconf.cf'))->exists("/$year/$folder/$page1_filename")){
                $pic1 = Storage::disk(config('fsconf.cf'))->get("/$year/$folder/$page1_filename");
            } else{
                $pic1 = Storage::disk('local')->get("not_exist.jpg");
            }
    
            //Check page 2 if exists
            $page2_filename = $create_fn[1];
    
            if(Storage::disk(config('fsconf.cf'))->exists("/$year/$folder/$page2_filename")){
                $pic2 = Storage::disk(config('fsconf.cf'))->get("/$year/$folder/$page2_filename");
            } else{
                $pic2 = Storage::disk('local')->get("not_exist.jpg");
            }             
        } 
        // elseif ($year == 'MANILA RASTER FOLDER') {

        //     $fs = Storage::disk(config('fsconf.cf'))->getDriver();
        //     $stream = $fs->readStream("/$year/$folder/" . $fn['td_no'] . "." . $fn['file_type']);
    
        //     $content_type = "";

        //     if($fn['file_type'] == 'jpg'){
        //         $content_type = "image/jpeg";
        //     } else{
        //         $content_type = "image/tiff";
        //     }
    
        //     return Response::stream(function() use($stream) {
        //         fpassthru($stream);
        //     }, 200, [
        //         "Content-Type" => $content_type,
        //         "Content-disposition" => "attachment; filename=\"" . basename($fn['td_no'] . "." . $fn['file_type']) . "\"",
        //     ]);
          
        // }  
        elseif ($year == 'TAX MAP CONTROL ROLL' || $year == 'MANILA RASTER FOLDER' || $year == 'TAX MAP CONTROL MAP' || 'CONTROL BOOK 1970' ) {

            // $fs = Storage::disk(config('fsconf.cf'))->getDriver();
            // $stream = $fs->readStream("/$year/$folder/" . $fn['td_no'] . "." . $fn['file_type']);

            $pic = Storage::disk(config('fsconf.cf'))->get("/$year/$folder/" . $fn['td_no'] . "." . $fn['file_type']);
    
            return array(
                'filename'  => $fn['td_no'] . "." . $fn['file_type'],
                'pic'      => "data:image/jpeg;base64," . base64_encode($pic),
            );
        }

        return array(

            
            'filename1' => $page1_filename,
            'filename2' => $page2_filename,
            'pic1'      => "data:image/jpeg;base64," . base64_encode($pic1),
            'pic2'      => "data:image/jpeg;base64," . base64_encode($pic2),
        );

    }

    public function getPdf(Request $request){

        $year   = $request['year'];
        $folder = $request['folder'];
        $fn     = $request['filename'];

        $fs = Storage::disk(config('fsconf.cf'))->getDriver();
        $stream = $fs->readStream("/$year/$folder/" . $fn['td_no'] . "." . $fn['file_type']);

        // return $stream;

        return Response::stream(function() use($stream) {
            fpassthru($stream);
        }, 200, [
            "Content-Type" => "application/pdf",
            "Content-disposition" => "attachment; filename=\"" . basename($fn['td_no'] . "." . $fn['file_type']) . "\"",
        ]);

        // $pdf    = Storage::disk(config('fsconf.cf'))->get("/$year/$folder/" . $fn['td_no']  . "." . $fn['file_type']);

        // $headers = array(
        //     'Content-Description: File Transfer',
        //     'Content-Type: application/octet-stream',
        //     'Content-Disposition: attachment; filename="' . $fn['td_no'] . '.pdf"',
        // );

        // return response()->download($filepath, $fn['td_no']  . '.pdf', $headers);

        // return $out;


    }

    private function getFilenameControlBook($no, $letter){

        switch ($letter) {
            case 'A':
                return $no . ".jpg";
                break;

            case 'C':
                return $no . "_2.jpg";
                break;

            case 'E':
                return $no . "_3.jpg";
                break;

            case 'G':
                return $no . "_4.jpg";
                break;

            case 'I':
                return $no . "_5.jpg";
                break;

            case 'K':
                return $no . "_6.jpg";
                break;

            case 'M':
                return $no . "_7.jpg";
                break;

            case 'O':
                return $no . "_8.jpg";
                break;

            case 'Q':
                return $no . "_9.jpg";
                break;

            case 'S':
                return $no . "_10.jpg";
                break;

            case 'U':
                return $no . "_11.jpg";
                break;

            case 'W':
                return $no . "_12.jpg";
                break;

            case 'Y':
                return $no . "_13.jpg";
                break;

            default:
                return false;
                break;
        }

    }

    private function getDisplayFnCb($fn){

        if(strpos($fn, "_") !== false){

            $exp = explode("_", $fn);

            switch ($exp[1]) {
                case '2':
                    return array(
                        $exp[0] . "C.jpg",
                        $exp[0] . "D.jpg"
                    );
                    break;

                case '3':
                    return array(
                        $exp[0] . "E.jpg",
                        $exp[0] . "F.jpg"
                    );
                    break;

                case '4':
                    return array(
                        $exp[0] . "G.jpg",
                        $exp[0] . "H.jpg"
                    );
                    break;

                case '5':
                    return array(
                        $exp[0] . "I.jpg",
                        $exp[0] . "J.jpg"
                    );
                    break;

                case '6':
                    return array(
                        $exp[0] . "K.jpg",
                        $exp[0] . "L.jpg"
                    );
                    break;

                case '7':
                    return array(
                        $exp[0] . "M.jpg",
                        $exp[0] . "N.jpg"
                    );
                    break;

                case '8':
                    return array(
                        $exp[0] . "O.jpg",
                        $exp[0] . "P.jpg"
                    );
                    break;

                case '9':
                    return array(
                        $exp[0] . "Q.jpg",
                        $exp[0] . "R.jpg"
                    );
                    break;

                case '10':
                    return array(
                        $exp[0] . "S.jpg",
                        $exp[0] . "T.jpg"
                    );
                    break;

                case '11':
                    return array(
                        $exp[0] . "U.jpg",
                        $exp[0] . "V.jpg"
                    );
                    break;

                case '12':
                    return array(
                        $exp[0] . "W.jpg",
                        $exp[0] . "X.jpg"
                    );
                    break;

                case '13':
                    return array(
                        $exp[0] . "Y.jpg",
                        $exp[0] . "Z.jpg"
                    );
                    break;

                default:
                    return 0;
                    break;
            }



        } else{

            return array(
                $fn . "A.jpg",
                $fn . "B.jpg"
            );

        }


    }

    // For image display on Tax Mapping

    public function getImageData(Request $request){

        if($request['fn'] == 'imagenf.jpg'){
            return response()->file(storage_path('app/' . $request['fn']));
        } else{
            return Storage::disk('sftp')->get("BLDG IMAGE/" . $request['fn']);
        }

    }


}
