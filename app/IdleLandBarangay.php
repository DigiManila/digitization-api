<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdleLandBarangay extends Model
{
    use HasFactory;

    // protected $table = "view_idle_lands_with_bgy";
    protected $table = "view_idle_lands_bgy";
}
