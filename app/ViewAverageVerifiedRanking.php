<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAverageVerifiedRanking extends Model
{
    use HasFactory;

    protected $table = 'average_verified_ranking';
}
