<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasMappingNoMapPinView extends Model
{
    use HasFactory;
    protected $table = 'rptas_mapping_no_map_pin';
}
