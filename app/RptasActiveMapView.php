<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RptasActiveMapView extends Model
{
    use HasFactory;
    protected $table = 'rptas_active_map';
}
